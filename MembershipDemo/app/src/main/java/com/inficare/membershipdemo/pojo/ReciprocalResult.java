package com.inficare.membershipdemo.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by avinash.verma on 6/27/2017.
 */

public class ReciprocalResult implements Parcelable {

    @SerializedName("reciprocals")
    @Expose
    private List<Reciprocal> reciprocals = null;
    @SerializedName("Success")
    @Expose
    private boolean success;

    @SerializedName("OrgReciprocals")
    @Expose
    private List<Reciprocal> OrgReciprocals = null;

    public List<Reciprocal> getReciprocals() {
        return reciprocals;
    }

    public void setReciprocals(List<Reciprocal> reciprocals) {
        this.reciprocals = reciprocals;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.reciprocals);
        dest.writeByte(this.success ? (byte) 1 : (byte) 0);
    }

    public ReciprocalResult() {
    }

    protected ReciprocalResult(Parcel in) {
        this.reciprocals = new ArrayList<Reciprocal>();
        in.readList(this.reciprocals, Reciprocal.class.getClassLoader());
        this.success = in.readByte() != 0;
    }

    public static final Creator<ReciprocalResult> CREATOR = new Creator<ReciprocalResult>() {
        @Override
        public ReciprocalResult createFromParcel(Parcel source) {
            return new ReciprocalResult(source);
        }

        @Override
        public ReciprocalResult[] newArray(int size) {
            return new ReciprocalResult[size];
        }
    };

    public List<Reciprocal> getOrgReciprocals() {
        return OrgReciprocals;
    }
}
