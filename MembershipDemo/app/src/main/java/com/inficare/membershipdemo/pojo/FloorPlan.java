package com.inficare.membershipdemo.pojo;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akshay.kumar on 11/7/2016.
 */

public class FloorPlan {
    private String id;
    private String title;
    private String image_name;
    private String image_type;
    private String image_size;
    private String mid;
    private String created_at;
    private String updated_at;
    private String imagePath;
    private String imageBasePath;
    private String imagesdcardpath;
    private List<FloorPlan> floorPlanList;
    public FloorPlan(){}
    public FloorPlan(String json, String imageBasePath) {
        floorPlanList = new ArrayList<>();
        try {
            JSONObject object1 = new JSONObject(json);
          /* String imagePath=object.getString("imagepath");
            JSONArray array=object.getJSONArray("Result");
            for (int i=0;i<array.length();i++){
                JSONObject object1=array.getJSONObject(i);*/
            id = String.valueOf(object1.getInt("id"));
            title = object1.getString("title");
            image_name = object1.getString("image_name");
            image_type = object1.getString("image_type");
            image_size = object1.getString("image_size");
            mid = object1.getString("mid");
            created_at = object1.getString("created_at");
            updated_at = object1.getString("updated_at");
            this.imagePath = imageBasePath + "/" + image_name;
            //floorPlanList.add(this);
            //}

        } catch (Exception e) {

        }

    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImage_name() {
        return image_name;
    }

    public String getImage_type() {
        return image_type;
    }

    public String getImage_size() {
        return image_size;
    }

    public String getMid() {
        return mid;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getImagePath() {
        return imagePath;
    }

    public List<FloorPlan> getFloorPlanList() {
        return floorPlanList;
    }

    public void setFloorPlanList(List<FloorPlan> floorPlanList) {
        this.floorPlanList = floorPlanList;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public void setImage_type(String image_type) {
        this.image_type = image_type;
    }

    public void setImage_size(String image_size) {
        this.image_size = image_size;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageBasePath() {
        return imageBasePath;
    }

    public String getImagesdcardpath() {
        return imagesdcardpath;
    }

    public void setImagesdcardpath(String imagesdcardpath) {
        this.imagesdcardpath = imagesdcardpath;
    }

    public void setImageBasePath(String imageBasePath) {
        this.imageBasePath = imageBasePath;
    }
}
