package com.inficare.membershipdemo.pojo;

/**
 * Created by pratyush on 3/12/2018.
 */

public class RxBusResultModel {

    private String tag;

    private String amount;

    private String language;

    private String languageID;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageID() {
        return languageID;
    }

    public void setLanguageID(String languageID) {
        this.languageID = languageID;
    }
}
