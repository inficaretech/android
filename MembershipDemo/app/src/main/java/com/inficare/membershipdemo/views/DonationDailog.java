package com.inficare.membershipdemo.views;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.DonationRecyclerAdapter;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Unbinder;

/**
 * Created by akshay.kumar on 3/13/2018.
 */

public class DonationDailog extends DialogFragment{

    Context mContext;
    TextView tvSelectAnAmount;
    RecyclerView rvDonate;
    EditText etOtherAmount;
    CardView mediaCardView;
    TextView btDonate;
    Unbinder unbinder;

    private DonationRecyclerAdapter donationRecyclerAdapter;
    private List<String> amountList;
    private List<Configuration> configurationsList;
    private int clickedMember;
    private MuseumDemoPreference preferences;
    private int color;
    private String amount;
    private String donationAmounts;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getActivity();

        Bundle bundle = getArguments();
        configurationsList = MemberParser.parsConfig(mContext);

        preferences = MuseumDemoPreference.getInstance(mContext);

        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        amountList = new ArrayList<>();

        color = Color.parseColor(configurationsList.get(clickedMember).getColorCode
                ());


        donationAmounts = configurationsList.get(clickedMember).getDonationAmounts();
        populateList(donationAmounts);
        final Dialog dialog = new Dialog(mContext);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.donation_dailog);
//        dialog.getWindow().setBackgroundDrawable(
//                new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        findViews(dialog);


        return dialog;
    }


    private void populateList(String donationAmounts) {

        amountList = Arrays.asList(donationAmounts.split("\\s*,\\s*"));
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        setUi();
        donationRecyclerAdapter=new DonationRecyclerAdapter(mContext, amountList, color,this);
        rvDonate.setAdapter(donationRecyclerAdapter);
        rvDonate.setLayoutManager(new GridLayoutManager(mContext, 2));
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void findViews(Dialog dialog){
        tvSelectAnAmount=dialog.findViewById(R.id.tv_select_an_amount);
        rvDonate=dialog.findViewById(R.id.rv_donate);
        etOtherAmount=dialog.findViewById(R.id.et_other_amount);
        btDonate=dialog.findViewById(R.id.bt_donate);
        btDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(donationRecyclerAdapter!=null){
                    donationRecyclerAdapter.onDonate();
                }
            }
        });

        etOtherAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (view.getId() == R.id.et_other_amount && b) {
                    if (donationRecyclerAdapter != null) {
                        donationRecyclerAdapter.setRow_index(-1);
                    } else {

                    }
                }
            }
        });
    }
    private void setUi() {

        tvSelectAnAmount.setTextColor(Color.parseColor(configurationsList.get(clickedMember).getColorCode
                ()));

        Utility.changeDrawableColor(mContext, btDonate, configurationsList.get(clickedMember)
                .getColorCode());
    }


}