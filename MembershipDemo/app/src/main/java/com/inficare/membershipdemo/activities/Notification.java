package com.inficare.membershipdemo.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.NotifAdapter;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.NotifList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Notification extends Activity implements OnClickListener{

	private static ListView listNotif;

	private static NotifAdapter adapter;

	private static Context ctx;

	private static NotifList notifList = new NotifList();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.notification);
		ctx = this;
		getUpdatedNotification();
	}

	@Override
	protected void onResume() {
		super.onResume();
		getView();

	}

	private void getUpdatedNotification() {
		listNotif = (ListView) findViewById(R.id.notif_List);
		notifList.clear();
		//notifList = getNotif();
		if (Utility.isNetworkAvailable(ctx)) {
			//new NotifTask(ctx).execute();
		} else {
			adapter = new NotifAdapter(ctx, R.layout.notif_list, notifList);
			listNotif.setAdapter(adapter);
		}
	}

	private void getView() {

	}


	@Override
	public void onClick(View v) {


	}

	public class NotifTask extends AsyncTask<String, Void, NotifList> {

		private final static String CN = "NotifTask";

		private ProgressDialog dialog;

		private Context mContext;

		private String notifReturnValue;

		public NotifTask(Context context) {
			mContext = context;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.show();
		}

		@Override
		protected NotifList doInBackground(String... params) {
			NotifList info = new NotifList();
			/*String jsonString = HttpMethod.getInstance(mContext).doOperation(
					HttpMethod.TYPE.GET,
					Constants.URL_NOTIF
							+ SharedManager.getInstance(mContext).getInt(
									Constants.LANG_ID, 0));

			if (jsonString != null
					&& !jsonString.equals(Constants.timeoutException)) {
				info = JSONPaser.getInstance(mContext).parseNoitif(jsonString);
			} else {
				info = null;
				notifReturnValue = jsonString;

			}*/
			return info;
		}

		@Override
		protected void onPostExecute(NotifList result) {
			super.onPostExecute(result);
			dialog.dismiss();


		}
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}
}
