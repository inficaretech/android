package com.inficare.membershipdemo.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by avinash.verma on 4/25/2017.
 */

public class PaymentDetailJSON implements Serializable{

    public String Organisation_key;
    public String Transaction_id;
    public String Transaction_status;
    public String Transaction_type;
    public String Transaction_amount;
    public String Transaction_datetime;
    public String Membershiplevel;
    public String Payer_firstname;
    public String Payer_lastname;
    public String Payer_contact;
    public String Payer_email;
    public String Payer_city;
    public String Payer_state;
    public String Payer_country;
    public String Payer_zip;
    public String Payer_type;
    public String trans_raw_data;
    public String Udid;
    public String deviceType;
    public String Payer_address;
    public String Payer_dob;
    public String donation_amount;
    public String membership_amount ;
    public String haveDonation ;
    public List<ChildJson> childs ;


    //Renew
    public String MembershipID;
    public String ConstituentID;
    public String Expirationdate;
    public String Membersince;

    //Gift
    public String Recipient_firstname;
    public String Recipient_lastname;
    public String Recipient_contact;
    public String Recipient_email;
    public String Recipient_city;
    public String Recipient_state;
    public String Recipient_country;
    public String Recipient_zip;
    public String Recipient_address;
    public String Recipient_dob;
    public String Message;
    public String Ref_id;

    public void setOrganisation_key(String organisation_key) {
        Organisation_key = organisation_key;
    }

    public void setTransaction_id(String transaction_id) {
        Transaction_id = transaction_id;
    }

    public void setTransaction_status(String transaction_status) {
        Transaction_status = transaction_status;
    }

    public void setTransaction_type(String transaction_type) {
        Transaction_type = transaction_type;
    }

    public void setTransaction_amount(String transaction_amount) {
        Transaction_amount = transaction_amount;
    }

    public void setTransaction_datetime(String transaction_datetime) {
        Transaction_datetime = transaction_datetime;
    }

    public void setMembershiplevel(String membershiplevel) {
        Membershiplevel = membershiplevel;
    }

    public void setPayer_firstname(String payer_firstname) {
        Payer_firstname = payer_firstname;
    }

    public void setPayer_lastname(String payer_lastname) {
        Payer_lastname = payer_lastname;
    }

    public void setPayer_contact(String payer_contact) {
        Payer_contact = payer_contact;
    }

    public void setPayer_email(String payer_email) {
        Payer_email = payer_email;
    }

    public void setPayer_city(String payer_city) {
        Payer_city = payer_city;
    }

    public void setPayer_state(String payer_state) {
        Payer_state = payer_state;
    }

    public void setPayer_country(String payer_country) {
        Payer_country = payer_country;
    }

    public void setPayer_zip(String payer_zip) {
        Payer_zip = payer_zip;
    }

    public void setPayer_type(String payer_type) {
        Payer_type = payer_type;
    }

    public void setMembershipID(String membershipID) {
        MembershipID = membershipID;
    }

    public void setConstituentID(String constituentID) {
        ConstituentID = constituentID;
    }

    public void setExpirationdate(String expirationdate) {
        Expirationdate = expirationdate;
    }

    public void setMembersince(String membersince) {
        Membersince = membersince;
    }

    public void setRecipient_firstname(String recipient_firstname) {
        Recipient_firstname = recipient_firstname;
    }

    public void setRecipient_lastname(String recipient_lastname) {
        Recipient_lastname = recipient_lastname;
    }

    public void setRecipient_contact(String recipient_contact) {
        Recipient_contact = recipient_contact;
    }

    public void setRecipient_email(String recipient_email) {
        Recipient_email = recipient_email;
    }

    public void setRecipient_city(String recipient_city) {
        Recipient_city = recipient_city;
    }

    public void setRecipient_state(String recipient_state) {
        Recipient_state = recipient_state;
    }

    public void setRecipient_country(String recipient_country) {
        Recipient_country = recipient_country;
    }

    public void setRecipient_zip(String recipient_zip) {
        Recipient_zip = recipient_zip;
    }

    public void setTrans_raw_data(String trans_raw_data) {
        this.trans_raw_data = trans_raw_data;
    }

    public void setUdid(String udid) {
        Udid = udid;
    }
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void setPayer_address(String payer_address) {
        Payer_address = payer_address;
    }

    public void setRecipient_address(String recipient_address) {
        Recipient_address = recipient_address;
    }

    public void setPayer_dob(String payer_dob) {
        Payer_dob = payer_dob;
    }

    public void setRecipient_dob(String recipient_dob) {
        Recipient_dob = recipient_dob;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public void setDonation_amount(String donation_amount) {
        this.donation_amount = donation_amount;
    }

    public void setMembership_amount(String membership_amount) {
        this.membership_amount = membership_amount;
    }

    public void setHaveDonation(String haveDonation) {
        this.haveDonation = haveDonation;
    }

    public void setChilds(List<ChildJson> childs) {
        this.childs = childs;
    }


    public String getTransaction_amount() {
        return Transaction_amount;
    }

    public String getDonation_amount() {
        return donation_amount;
    }

    public String getMembership_amount() {
        return membership_amount;
    }

    public String getRef_id() {
        return Ref_id;
    }

    public void setRef_id(String ref_id) {
        this.Ref_id = ref_id;
    }
}
