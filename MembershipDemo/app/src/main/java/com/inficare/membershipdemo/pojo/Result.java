package com.inficare.membershipdemo.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Result implements Serializable {

    @SerializedName("Success")
    @Expose
    private String Success;

    @SerializedName("Membershipleveltype")
    @Expose
    private String Membershipleveltype;
    @SerializedName("MembershiplevelID")
    @Expose
    private String MembershiplevelID;
    @SerializedName("Price")
    @Expose
    private String Price;
    @SerializedName("Description")
    @Expose
    private String Description;
    @SerializedName("Upselltext")
    @Expose
    private String Upselltext;
    @SerializedName("Benefits")
    @Expose
    private String Benefits;
    @SerializedName("OtherBenefits")
    @Expose
    private String OtherBenefits;
    @SerializedName("Notifications")
    @Expose
    private List<Notification> notifications = null;
    @SerializedName("states")
    @Expose
    private List<State> states = null;

    @SerializedName("Reciprocals")
    @Expose
    private List<Reciprocal> reciprocals = null;

    @SerializedName("languages")
    @Expose
    private List<Language> languages = null;


    public List<State> getStates() {
        return states;
    }
    public void setStates(List<State> states) {
        this.states = states;
    }

    private String MyMembershipID;

    private String OrganisationKey;


    /**
     * @return The Membershipleveltype
     */
    public String getMembershipleveltype() {
        return Membershipleveltype;
    }

    /**
     * @param membershipleveltype The Membershipleveltype
     */
    public void setMembershipleveltype(String membershipleveltype) {
        this.Membershipleveltype = membershipleveltype;
    }

    /**
     * @return The Price
     */
    public String getPrice() {
        return Price;
    }

    /**
     * @param price The Price
     */
    public void setPrice(String price) {
        this.Price = price;
    }

    /**
     * @return The Description
     */
    public String getDescription() {
        return Description;
    }

    /**
     * @param description The Description
     */
    public void setDescription(String description) {
        this.Description = description;
    }

    /**
     * @return The Upselltext
     */
    public String getUpselltext() {
        return Upselltext;
    }

    /**
     * @param upselltext The Upselltext
     */
    public void setUpselltext(String upselltext) {
        this.Upselltext = upselltext;
    }

    /**
     * @return The Benefits
     */
    public String getBenefits() {
        return Benefits;
    }

    /**
     * @param benefits The Benefits
     */
    public void setBenefits(String benefits) {
        this.Benefits = benefits;
    }

    /**
     * @return The OtherBenefits
     */
    public String getOtherBenefits() {
        return OtherBenefits;
    }

    /**
     * @param otherBenefits The OtherBenefits
     */
    public void setOtherBenefits(String otherBenefits) {
        this.OtherBenefits = otherBenefits;
    }

    //Variable and method for Guest Passes
    @SerializedName("GuestPass")
    @Expose
    private List<GuestPas> GuestPass = new ArrayList<GuestPas>();

    /**
     * @return The GuestPass
     */
    public List<GuestPas> getGuestPass() {
        return GuestPass;
    }

    /**
     * @param guestPass The GuestPass
     */
    public void setGuestPass(List<GuestPas> guestPass) {
        this.GuestPass = guestPass;
    }

    @SerializedName("GiftableGuestPass")
    @Expose
    private List<GiftableGuestPas> giftableGuestPass = null;

    public List<GiftableGuestPas> getGiftableGuestPass() {
        return giftableGuestPass;
    }

    public void setGiftableGuestPass(List<GiftableGuestPas> giftableGuestPass) {
        this.giftableGuestPass = giftableGuestPass;
    }

    //Methods and variable for Member Report starts here

    @SerializedName("Report")
    @Expose
    private Report Report;

    /**
     * @return The Report
     */
    public Report getReport() {
        return Report;
    }

    /**
     * @param report The Report
     */
    public void setReport(Report report) {
        this.Report = report;
    }

    ////Methods and variable for Member Report ends here

    public String getMembershiplevelID() {
        return MembershiplevelID;
    }

    public void setMembershiplevelID(String membershiplevelID) {
        this.MembershiplevelID = membershiplevelID;
    }

    public String getMyMembershipID() {
        return MyMembershipID;
    }

    public void setMyMembershipID(String myMembershipID) {
        MyMembershipID = myMembershipID;
    }

    public String getOrganisationKey() {
        return OrganisationKey;
    }

    public void setOrganisationKey(String organisationKey) {
        OrganisationKey = organisationKey;
    }
    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public List<Reciprocal> getReciprocals() {
        return reciprocals;
    }

    public void setReciprocals(List<Reciprocal> reciprocals) {
        this.reciprocals = reciprocals;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }
}
