package com.inficare.membershipdemo.methods;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.inficare.membershipdemo.R;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MyApplication extends Application {

    public static MyApplication get(Activity activity){
        return (MyApplication) activity.getApplication();
    }


    @Override
    public void onCreate() {

        super.onCreate();
        MultiDex.install(this);
       // Fabric.with(this, new Crashlytics());
        initImageLoader();
        try {
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Raleway-Regular.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build());
        }catch (NoClassDefFoundError e){
            e.printStackTrace();
        }

        if(Build.VERSION.SDK_INT>=24){
            try{
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                builder.detectFileUriExposure();
            }catch(Exception e){
                e.printStackTrace();
            }
        }


    }

    private void initImageLoader() {


        File cacheDir = StorageUtils.getOwnCacheDirectory(
                getApplicationContext(), Constants.FILE_PATH);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
               /// .showImageOnFail(R.drawable.no_image2)
                // resource or drawable
                .resetViewBeforeLoading(true)
                // default
                // .delayBeforeLoading(10)
                // .cacheInMemory(true)
                // default
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .cacheOnDisc(true)
                // default
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext()).memoryCache(new WeakMemoryCache())
                .diskCacheExtraOptions(480, 320, null)
                // default
                .defaultDisplayImageOptions(options)
                // default
                ///.imageDownloader(new AuthImageDownloader(this,10000,10000))
                .writeDebugLogs().build();

        ImageLoader.getInstance().init(config);

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
