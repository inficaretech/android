package com.inficare.membershipdemo.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.NotifInfo;
import com.inficare.membershipdemo.pojo.NotifList;

public class NotifAdapter extends ArrayAdapter<NotifInfo> {

	private Context mContext;

	private int mResource;

	private NotifList mList;

	transient private Typeface facedescription;

	private ArrayList<String> list = new ArrayList<String>();

	public NotifAdapter(Context context, int resource, NotifList list) {
		super(context, resource, list);
		mContext = context;
		mResource = resource;
		mList = list;
		facedescription = Utility.setFaceTypeContent(mContext);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		NotifInfo notifInfo = (NotifInfo) getItem((mList.size() - 1) - position);
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext)
					.inflate(mResource, null);
			viewHolder.notifText = (TextView) convertView
					.findViewById(R.id.notif_text);

			viewHolder.date = (TextView) convertView
					.findViewById(R.id.text_date);

			viewHolder.time = (TextView) convertView
					.findViewById(R.id.text_time);

			convertView.setTag(viewHolder);
		} else
			viewHolder = (ViewHolder) convertView.getTag();

		viewHolder.notifText.setTypeface(facedescription);
		viewHolder.date.setTypeface(facedescription);
		viewHolder.time.setTypeface(facedescription);
		// if (notifInfo.getDatetime() != null
		// || !(notifInfo.getDatetime().equals("null"))) {
		// list = Utility.getDateFormTimestamp(mContext,
		// notifInfo.getDatetime());
		// }

		viewHolder.notifText.setText(notifInfo.getNotifications());
		// if (list.size() > 0) {
		if (notifInfo.getDatetime() != null) {

			/*String timestampToConvert = String.valueOf(Long.parseLong(notifInfo
					.getDatetime()) * 1000);
			viewHolder.date.setText(Utility.getDateNew(timestampToConvert));
			viewHolder.time.setText(Utility.getHHMMA(timestampToConvert));*/
		}

		// }

		return convertView;
	}

	private class ViewHolder {

		TextView notifText;

		TextView date;

		TextView time;

	}

}
