package com.inficare.membershipdemo.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inficare.membershipdemo.R;

import org.w3c.dom.Text;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InstActivityMain extends AppCompatActivity implements View.OnClickListener{
    LinearLayout llParent;
    TextView tvInstruction;
    public static final String INST_DATA="instData";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inst_find_card);
        llParent= (LinearLayout) findViewById(R.id.llParent);
        tvInstruction=findViewById(R.id.tvInstruction);
        llParent.setOnClickListener(this);
        String data=getIntent().getStringExtra(INST_DATA);
        tvInstruction.setText(data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llParent:
                finish();
                break;
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
