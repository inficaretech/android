package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 11/11/2016.
 */

public class Notifications {
    String id,deviceType,message,createdDate;

    public Notifications(String id, String deviceType, String message, String createdDate) {
        this.id = id;
        this.deviceType = deviceType;
        this.message = message;
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
