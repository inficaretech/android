package com.inficare.membershipdemo.pojo;

public class ModifiedInfo {

	private String TableName;

	private String Date;

	private int LanguageId;

	private int ExhibId;

	public String getTableName() {
		return TableName;
	}

	public void setTableName(String tableName) {
		TableName = tableName;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public int getLanguageId() {
		return LanguageId;
	}

	public void setLanguageId(int languageId) {
		LanguageId = languageId;
	}

	public int getExhibId() {
		return ExhibId;
	}

	public void setExhibId(int exhibId) {
		ExhibId = exhibId;
	}

}
