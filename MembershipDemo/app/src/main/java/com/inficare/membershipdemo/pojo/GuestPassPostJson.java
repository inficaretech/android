package com.inficare.membershipdemo.pojo;

import java.util.List;

/**
 * Created by akshay.kumar on 3/27/2018.
 */

public class GuestPassPostJson {
    List<GuestPasses> GuestPasses;

    public List<com.inficare.membershipdemo.pojo.GuestPasses> getGuestPasses() {
        return GuestPasses;
    }

    public void setGuestPasses(List<com.inficare.membershipdemo.pojo.GuestPasses> guestPasses) {
        GuestPasses = guestPasses;
    }
}
