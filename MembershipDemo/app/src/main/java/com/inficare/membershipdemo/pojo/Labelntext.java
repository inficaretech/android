package com.inficare.membershipdemo.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Labelntext {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("title_fr")
    @Expose
    private String titleFr;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;

    public Labelntext(JSONObject obj) {
        try {
            if (obj.has("id")) {
                id = obj.getInt("id");
            }

            if (obj.has("key")) {
                key = obj.getString("key");
            }
            if (obj.has("title")) {
                title = obj.getString("title");
            }

            if (obj.has("title_fr")) {
                titleFr = obj.getString("title_fr");
            }


            if (obj.has("is_active")) {
                isActive = obj.getBoolean("is_active");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleFr() {
        return titleFr;
    }

    public void setTitleFr(String titleFr) {
        this.titleFr = titleFr;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

}
