package com.inficare.membershipdemo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.CardAdapter;
import com.inficare.membershipdemo.adapter.UrlRecyAdapter;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.Model;
import com.inficare.membershipdemo.pojo.MyReciprocalResponse;
import com.inficare.membershipdemo.pojo.Reciprocal;
import com.inficare.membershipdemo.pojo.ReciprocalResult;
import com.inficare.membershipdemo.pojo.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


//Not in Use
public class ReciprocalActivity extends AppCompatActivity {
    public static final String RECIPROCAL_SAVED_DATA = "reciprocal";
    private static final String TAG = ReciprocalActivity.class.getSimpleName();
    private Context mContext;
    //UI
    private Toolbar toolbar;
    private TextView tb_title;
    private View tvTopStrip;
    private TextView tvNoReciprocal;
    private RecyclerView rcv_urlList;


    private MuseumDemoPreference preferences;
    int clickedMember;
    private List<Configuration> configList;
    private List<Reciprocal> list;
    private int colorInt;
    private List<Reciprocal> reciprocalList;
    private String mid;
    private boolean progressVisible = true;
    String callerCard;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private TextView toolbar_title;
    private String toast_access_external_storage, pleaseWait, something_went_wrong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.activity_reciprocal);
        mContext = this;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        colorInt = Color.parseColor(configList.get(clickedMember)
                .getColorCode());
        mid = configList.get(clickedMember).getMid();
        //reciprocalList = getIntent().getParcelableArrayListExtra("list");
        initLanguage();
        findViews();
        callerCard = getIntent().getStringExtra(CardAdapter.CALLER_CARD);
        list = (List<Reciprocal>) getIntent().getSerializableExtra("reciprocal");
        if (callerCard != null) {
           /// List<Result> results = getMembershipDetails();
            List<Reciprocal> results = getSavedReciprocalByLevelId();
            if (results!=null&&results.size() > 0) {
                Log.d("results", results.size() + "");
                rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, results));
                tvNoReciprocal.setVisibility(View.GONE);
                getReciprocalByLevelId(callerCard,mid,false);
            } else {
                ///getAllBenefitRetrofitAPI(true);
                getReciprocalByLevelId(callerCard,mid,true);
            }
           /* if (results != null && results.size() > 0) {
                Log.d("results", results.size() + "");
                for (Result result : results) {
                    if (result.getMembershiplevelID().trim().equalsIgnoreCase(callerCard)) {
                        rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, result.getReciprocals()));
                        tvNoReciprocal.setVisibility(View.GONE);
                        break;
                    }
                    getAllBenefitRetrofitAPI(false);
                }

            } else {
                getAllBenefitRetrofitAPI(true);
            }*/
        } else if (list != null && list.size() > 0) {
            rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, list));
            tvNoReciprocal.setVisibility(View.GONE);
        } else {
            getSavedData();
            if (Utility.isNetworkAvailable(mContext)) {
                getReciprocalCall(configList.get(clickedMember).getMid(), progressVisible, language);
            } else {
                //Utility.showToast(mContext, getString(R.string.msg_no_internet));
            }
        }

        setUi(language);

    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title =Utility.getTitle(language);


        try {
            tb_title.setText(languageMap.get(LanguageConstants.RECIPROCALS).getString(title));
            tvNoReciprocal.setText(languageMap.get(LanguageConstants.NO_RECIPROCALS_AS_YET).getString(title));


            toast_access_external_storage = languageMap.get(LanguageConstants.STORAGE_ACCESS_MESSAGE).getString(title) + ".";
            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            something_went_wrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void findViews() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        tb_title.setSelected(true);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        tvTopStrip = findViewById(R.id.tvTopStrip);
        tvNoReciprocal = findViewById(R.id.tvNoReciprocal);
        tvTopStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode
                ()));
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        rcv_urlList = (RecyclerView) findViewById(R.id.rcv_urlList);
        rcv_urlList.setLayoutManager(manager);
        //rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, reciprocalList));
       /* DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rcv_urlList
                .getContext(), manager.getOrientation());
        rcv_urlList.addItemDecoration(dividerItemDecoration);*/

        rcv_urlList.addItemDecoration(new DividerItemDecoration(rcv_urlList.getContext(), 0));
    }


   /* private void getReciprocalCall(String organizationKey) {
        Call<ReciprocalResult> bodyCall = Utility.callRetrofit3().getReciprocalData
                (organizationKey);
        bodyCall.enqueue(new Callback<ReciprocalResult>() {
            @Override
            public void onResponse(Call<ReciprocalResult> call, Response<ReciprocalResult>
                    response) {
                if (response.body().getSuccess()) {

                }
                Utility.showToast(mContext, "success" + response.code());
            }

            @Override
            public void onFailure(Call<ReciprocalResult> call, Throwable MatcherNode) {

            }
        });
    }*/

    private void getSavedData() {
        List<Reciprocal> reciprocalList = null;
        String jsonData = preferences.getString(RECIPROCAL_SAVED_DATA.concat(mid), null);
        if (jsonData != null) {
            Type listType = new TypeToken<ArrayList<Reciprocal>>() {
            }.getType();

            reciprocalList = new Gson().fromJson(jsonData, listType);

            if (reciprocalList != null && reciprocalList.size() > 0) {
                tvNoReciprocal.setVisibility(View.GONE);
                rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, reciprocalList));
                progressVisible = false;
            }
        }


    }

    private void getReciprocalCall(String organizationKey, boolean progressVisible, String language) {
        final ProgressDialog dialog = Utility.showProgressDialog(mContext, pleaseWait);
        if (progressVisible)
            dialog.show();
        Call<ReciprocalResult> bodyCall = null;


        if (language.equalsIgnoreCase(Constants.ENGLISH)) {
            bodyCall = Utility.callRetrofit3().getOrgReciprocal
                    (organizationKey, "");
        } else {
            bodyCall = Utility.callRetrofit3().getOrgReciprocal
                    (organizationKey, language);
        }

        bodyCall.enqueue(new Callback<ReciprocalResult>() {
            @Override
            public void onResponse(Call<ReciprocalResult> call, Response<ReciprocalResult>
                    response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                if (response.body().getSuccess()) {

//                    loadURLinWebBrowser(response.body().getReciprocals().get(0).getUrl());
//                    loadDialog(response.body().getReciprocals());
                    List<Reciprocal> list = response.body().getOrgReciprocals();
                    if (list != null && list.size() > 0) {
                        rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, list));
                        Gson gson = new Gson();
                        String jsonData = gson.toJson(response.body().getOrgReciprocals());
                        preferences.saveString(RECIPROCAL_SAVED_DATA.concat(mid), jsonData);

                        tvNoReciprocal.setVisibility(View.GONE);
                    }

                } else {
                    Utility.showToast(mContext, something_went_wrong);

                }
//                Utility.showToast(mContext, "success" + response.code());
            }

            @Override
            public void onFailure(Call<ReciprocalResult> call, Throwable t) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                Utility.showToast(mContext, something_went_wrong);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getAllBenefitRetrofitAPI(final boolean progress) {
        final ProgressDialog dialog = Utility.showProgressDialog(mContext, pleaseWait);
        if (progress)
            dialog.show();
        Call<Model> modelCall = Utility.callRetrofit().getAllBenefitsCall(Utility.getHeaderData()
                , configList.get(clickedMember).getMid(), Utility.getLanguage(language));
        modelCall.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                if (response.code() == 200) {

                    List<Result> list = response.body().getResult();

                    if (list != null && list.size() > 0) {
                        Gson gson = new Gson();
                        String jsonData = gson.toJson(list);
                        preferences.saveString(MuseumDemoPreference.ALL_BENEFITS, jsonData);

                        for (Result result : list) {
                            if (result.getMembershiplevelID().trim().equalsIgnoreCase(callerCard)) {
                                tvNoReciprocal.setVisibility(View.GONE);
                                rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, result.getReciprocals()));
                                break;
                            }
                        }

                    }


                }
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        });

    }

    private List<Result> getMembershipDetails() {

        Gson gson = new Gson();
        String jData = preferences.getString(MuseumDemoPreference.ALL_BENEFITS, null);
        if (jData != null) {
            Result[] results = gson.fromJson(jData, Result[].class);
            List<Result> mdata = Arrays.asList(results);
            return mdata;
        }
        return null;
    }

    private void getReciprocalByLevelId(String levelId,String organizationKey, boolean progressVisible) {
        final ProgressDialog dialog = Utility.showProgressDialog(mContext, "Please wait...");
        if (progressVisible)
            dialog.show();

        Call<MyReciprocalResponse> bodyCall = Utility.callRetrofit3().getReciprocalByLeve(levelId,organizationKey);
        bodyCall.enqueue(new Callback<MyReciprocalResponse>() {
            @Override
            public void onResponse(Call<MyReciprocalResponse> call, Response<MyReciprocalResponse>
                    response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                MyReciprocalResponse myReciprocalResponse=response.body();
                if(myReciprocalResponse==null)
                    return;
                if (myReciprocalResponse.getSuccess().equalsIgnoreCase("true")) {
                    rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, response.body().getOrgReciprocals()));
                    Gson gson = new Gson();
                    String jsonData = gson.toJson(response.body().getOrgReciprocals());
                    preferences.saveString(MuseumDemoPreference.RECIPROCAL_BY_LEVEL_ID.concat(mid).concat(callerCard), jsonData);
                    if (response.body().getOrgReciprocals().size() > 0) {
                        tvNoReciprocal.setVisibility(View.GONE);
                    }
                } else {
                    preferences.saveString(MuseumDemoPreference.RECIPROCAL_BY_LEVEL_ID.concat(mid), null);
                    Utility.showToast(mContext, "Unable to connect");
                }
//                Utility.showToast(mContext, "success" + response.code());
            }

            @Override
            public void onFailure(Call<MyReciprocalResponse> call, Throwable t) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                Utility.showToast(mContext, "Unable to connect");
            }
        });
    }

    private List<Reciprocal> getSavedReciprocalByLevelId() {

        Gson gson=new Gson();
        String jData=preferences.getString(MuseumDemoPreference.RECIPROCAL_BY_LEVEL_ID.concat(mid).concat(callerCard),null);
        if(jData!=null){
            Reciprocal[] results=gson.fromJson(jData,Reciprocal[].class);
            List<Reciprocal> mdata= Arrays.asList(results);
            return mdata;
        }
        return null;
    }
}
