package com.inficare.membershipdemo.methods;

import android.os.Environment;

import java.util.regex.Pattern;

public class Constants {

    //live url
              // public static final String BASE_URL = "http://52.6.141.191/ApiOceanMaSky/";
               public static final String BASE_URL = "https://museum.inficaresoft.com/ecardapi/";

    //staging url
   // public static final String BASE_URL = "http://49.249.240.100:9999/ApiEcard/";
   // public static final String BASE_URL = "http://125.63.96.186:9999/ApiEcard/";

    //http://14.141.136.170:9999/ApiEcard/api/guest/genrateGuestPass?mid=10021769
    // &organisation_key=TXVzZXVtQW55V2hlcmU=

    public static final String GET_CONFIGURATION_URL = BASE_URL +
            "api/configuration?device_token=[device_token]";

    //public static final String GET_FLOOR_PLAN_URL =
    // "http://54.164.146.171:8005/api/floorplan?mid=[mid]";
    public static final String GET_FLOOR_PLAN_URL = BASE_URL + "api/Config/getFloorPlan?mid=[mid]";

    public static final String GET_NOTIFICATIONS_URL = BASE_URL +
            "api/upsell/getNotifications";

    //faq url
    //public static final String GET_FAQ_URL = "http://54.164.146.171:8003/";
    public static final String GET_FAQ_URL = BASE_URL;


    public static final String MEMBER_URL = BASE_URL
            + "api/MembershipDetail/searchMember?short_code=";

    public static final String MEMBER_MEMBER_UPDATE_URL = BASE_URL
            + "api/MembershipDetail/getMemberUpdate";

    public static final String MEMBER_URL_UPLOAD_IMAGE = BASE_URL
            + "api/MembershipDetail/UploadFile?";

    public static final String DOWNLOAD_CARD_URL = BASE_URL
            + "api/MembershipDetail/downloadCard";


    // Museum Demo
    public static String MID_ADMIN = "ZGFkYW1zQGFuY2hvcmFnZW11c2V1bS5vcmc=";

    public static final String FILE_PATH = Environment
            .getExternalStorageDirectory() + "/MembershipApp/";


    public static final String FONT_BOLD = "fonts/Raleway-SemiBold.ttf";

    public static final String FONT_REGULAR = "fonts/Raleway-Regular.ttf";

    public static final String FONT_MEDIUM = "fonts/Raleway-Medium.ttf";


    public static final String FBERROR = "Unable to perform selected action because permissions " +
            "were not granted.";

    // public static final String TWITTER_CONSUMER_KEY =
    // "2bwR7ay8K8jVEveLt68LZQ";
    public static final String TWITTER_CONSUMER_KEY = "DfaD9oBG5cJOyHAraGS3G9rxT";

    // public static final String TWITTER_CONSUMER_SECRET =
    // "2y31zINQ431GovFsZSZYhDfuopUFkzxrTrIEMvzOGs";
    public static final String TWITTER_CONSUMER_SECRET =
            "wtrMzycqtOvd2d99pcxyup2fL3LHuJELYsJ75IKVwqMDuwSMiq";


    public static String timeoutException = "Connection Timeout";
    public static final String DEVICE_TOKEN_FCM = "device_token";


    public static boolean langChange = false;


    public static final String MODE_ADV = "AdvaneMode";

    public static final String MODE_NORM = "NOrmalMode";

    public static final String MEMBER_DATA = "MemberData";

    public static final String DEFAULT_TYPE = "Membership Type";//MemberShip Type

    public static final String FLAG_CLEAR = "flagClearSearch";

    public static final String FLAG_STACK_CLEAR = "flagClearStack";

    public static final String ORGANISATION_KEY = "organisation_key";
    public static final String ORGANISATION_KEY_CAP = "Organisation_key";
    public static final String COLOUR = "colour";
    public static final String AMOUNT = "amount";


    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(

            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\."
                    + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

    public static final String DOCUMENT_LOCATION_VISIBLE = "/Documents/";
    public static final String DATE_FORMAT = "MMM dd, yyyy";

    public static final String APPRATER = "apprater";
    public static final String LANGUAGE_SHARED_PREFERENCE = "language shared preference";
    public static final String DONT_SHOW_AGAIN = "dontshowagain";
    public static final String LATER_COUNT = "laterCount";
    public static final String LAUNCH_COUNT = "launch_count";
    public static final String DATE_FIRST_LAUNCH = "date_firstlaunch";
    public static final String ISCHECKED = "isChecked";
    public static final String ENGLISH = "";
    public static final String FRENCH = "fr";
    public static final String UDID = "udid";
    public static final String LANGUAGE_ID = "language_id";
    public static final String FIRST_TIME = "first_time";

}
