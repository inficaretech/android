package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 3/27/2018.
 */

public class GuestPasses {
    String MembershipID;
    String GuestPassID;
    String Organisation_key;

    public String getMembershipID() {
        return MembershipID;
    }

    public void setMembershipID(String membershipID) {
        MembershipID = membershipID;
    }

    public String getGuestPassID() {
        return GuestPassID;
    }

    public void setGuestPassID(String guestPassID) {
        GuestPassID = guestPassID;
    }

    public String getOrganisation_key() {
        return Organisation_key;
    }

    public void setOrganisation_key(String organisation_key) {
        Organisation_key = organisation_key;
    }
}
