package com.inficare.membershipdemo.dialogFragment;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.inficare.membershipdemo.methods.AppRater.APP_PNAME;

public class RateDialogFragment extends DialogFragment {
    @BindView(R.id.bt_rate)
    Button btRate;
    @BindView(R.id.bt_later)
    Button btLater;
    Unbinder unbinder;
    @BindView(R.id.tv_rate_us)
    TextView tvRateUs;
    @BindView(R.id.tv_rate_desc)
    TextView tvRateDesc;
    @BindView(R.id.tv_rate_desc1)
    TextView tvRateDesc1;
    private Context mContext;
    public static final String TAG = "RateDialogFragment";
    private SharedPreferences.Editor editor;
    private SharedPreferences prefs;

    private List<Configuration> configurationsList;

    private int clickedMember;

    private MuseumDemoPreference preferences;

    private int color;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor langEditor;
    private String language;
    private Map<String, JSONObject> languageMap;


    private Typeface faceHeader, faceRegular;


    public RateDialogFragment() {
        // Required empty public constructor
    }


    public static RateDialogFragment newInstance() {
        RateDialogFragment fragment = new RateDialogFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        this.mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_rate, container, false);
        unbinder = ButterKnife.bind(this, view);

        mContext = getActivity();

        initialize();

        setUi();

        initLanguage();

        setUi(language);

        return view;
    }

    private void initialize() {

        prefs = mContext.getSharedPreferences(Constants.APPRATER, Context.MODE_PRIVATE);

        editor = prefs.edit();

        configurationsList = MemberParser.parsConfig(mContext);

        preferences = MuseumDemoPreference.getInstance(mContext);

        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);

        color = Color.parseColor(configurationsList.get(clickedMember).getColorCode
                ());


        faceHeader = Utility.setFaceTypeHeader(mContext);
        faceRegular = Utility.setFaceTypeContent(mContext);
    }


    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configurationsList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configurationsList.get(clickedMember).getMid() + configurationsList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);



        try {
            btRate.setText(languageMap.get(LanguageConstants.RATE).getString(title));
            btLater.setText(languageMap.get(LanguageConstants.LATER).getString(title));

            tvRateUs.setText(languageMap.get(LanguageConstants.RATE_US).getString(title));
            tvRateDesc.setText(languageMap.get(LanguageConstants.RATE_US_QUES_MESSAGE).getString(title));
            tvRateDesc1.setText(languageMap.get(LanguageConstants.RATE_US_FEEDBACK_MESSAGE).getString(title));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void setUi() {

        tvRateUs.setTypeface(faceHeader);
        tvRateDesc.setTypeface(faceRegular);
        tvRateDesc1.setTypeface(faceRegular);
        btLater.setTypeface(faceRegular);
        btRate.setTypeface(faceRegular);

        Utility.changeDrawableColor(mContext, btRate, configurationsList.get(clickedMember)
                .getColorCode());

        Utility.changeDrawableColor(mContext, btLater, configurationsList.get(clickedMember)
                .getColorCode());

    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @OnClick({R.id.bt_rate, R.id.bt_later})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_rate:
                Log.d(TAG, "onViewClicked: bt_rate");

//                editor.putBoolean(Constants.DONT_SHOW_AGAIN, true);
//                editor.commit();
                editor.putLong(Constants.DATE_FIRST_LAUNCH, System.currentTimeMillis());
                editor.commit();

                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
                getDialog().dismiss();
                break;
            case R.id.bt_later:
                Log.d(TAG, "onViewClicked: bt_later");


                /*editor.putInt(Constants.LAUNCH_COUNT, 0);
                int laterCount = prefs.getInt(Constants.LATER_COUNT, 0) + 1;
                editor.putInt(Constants.LATER_COUNT, laterCount);
                if (laterCount >= 2) {
                    editor.putLong(Constants.DATE_FIRST_LAUNCH, System.currentTimeMillis());
                    Log.d(TAG, "date_firstlaunch: " + "updated");
                }*/
                Log.d(TAG, "date_firstlaunch: " + "updated");
                Log.d(TAG, "date_firstlaunch: " + System.currentTimeMillis());

                editor.putLong(Constants.DATE_FIRST_LAUNCH, System.currentTimeMillis());
                editor.commit();

                getDialog().dismiss();

                break;

        }
    }
}
