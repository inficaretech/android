package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.activities.MyWebViewActivity;
import com.inficare.membershipdemo.pojo.Reciprocal;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by avinash.verma on 6/28/2017.
 */

public class UrlRecyAdapter extends RecyclerView.Adapter<UrlRecyAdapter.URLViewHolder> {
    private Context mContext;
    private List<Reciprocal> reciprocalList;

    public UrlRecyAdapter(Context mContext, List<Reciprocal> reciprocalList) {
        this.mContext = mContext;
        this.reciprocalList = reciprocalList;
    }

    @Override
    public URLViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.url_row, parent, false);

        URLViewHolder holder = new URLViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(URLViewHolder holder, int position) {

        final String description = reciprocalList.get(position).getDescription();//reciprocalList.get(position).getUrl();
        final String url= reciprocalList.get(position).getExtrnalLink();
       // holder.tv_url_title.setText((position + 1) + ". " + url);
        holder.tv_url_title.setText(description);
        /*Picasso.with(mContext).load(reciprocalList.get(position).getImagePath())
                .into(holder.tvLogo);*/
        ImageLoader.getInstance().displayImage(reciprocalList.get(position).getImagePath(),holder.tvLogo);

       /* holder.tvLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (URLUtil.isValidUrl(url))
                    loadURLinWebBrowser(url);
                else
                    Toast.makeText(mContext, "Url not valid", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return reciprocalList.size();
    }

    protected  class URLViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        public TextView tv_url_title;
        public ImageView tvLogo;

        public URLViewHolder(View itemView) {
            super(itemView);
            tv_url_title =  itemView.findViewById(R.id.tv_url_title);
            tvLogo =  itemView.findViewById(R.id.tvLogo);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int pos=  getLayoutPosition();
            final String url= reciprocalList.get(pos).getExtrnalLink();
            if (URLUtil.isValidUrl(url))
                loadURLinWebBrowser(url);
            else
                Toast.makeText(mContext, "Url not valid", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadURLinWebBrowser(String url) {
       /* Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        mContext.startActivity(intent);*/
        Intent intent = new Intent(mContext,MyWebViewActivity.class);
        intent.putExtra(MyWebViewActivity.LINK_URL,url);
        mContext.startActivity(intent);

    }

}
