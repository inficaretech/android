package com.inficare.membershipdemo.backgroundtask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.networks.HttpMethod;
import com.inficare.membershipdemo.pojo.Configuration;

import java.util.List;

public abstract class MemberTaskInitial extends AsyncTask<String, Void, String> {

    private Context mContext;

    private static final String CN = "MemberTask";

    private ProgressDialog dialog;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    private String language, pleaseWait;

    public MemberTaskInitial(Context context, String language, String pleaseWait) {
        mContext = context;
        this.language = language;
        this.pleaseWait = pleaseWait;
//        createDialog(mContext);
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        dialog.show();

    }

    @Override
    protected String doInBackground(String... params) {

        String fName = null;
        String lName = null;
        String zip = null;
        String phone = null;
        String type = null;
        String mId = null;
        String responsedata = null;
        String mode = params[0];

        if (mode.equals(Constants.MODE_ADV)) {


        } else {
            lName = params[1].trim();
            mId = params[2];
            lName = lName.replace(" ", "%20");
        }

        String url = "";
        try {
            String appVersionName=Utility.getAppVersion(mContext,0);
            Integer appVersionCode=Utility.getAppVersion(mContext,1);
            url = Constants.MEMBER_MEMBER_UPDATE_URL + "?" + "mid=" + mId + "&lname=" + lName
                    + "&udid=" + Utility.getDeviceToken
                    (mContext) + "&devicename=ANDROID&" + Constants.ORGANISATION_KEY + "=" + configList.get(clickedMember).getMid()+"&appVersion="+appVersionName+"&appVersionNumber="+appVersionCode+"&uniqueDeviceId="+Utility.getDeviceId(mContext) + "&short_code=" + Utility.getLanguage(language);

        } catch (Exception e) {

        }
        //url = url.replaceAll(" ", "");
        //url = url.replaceAll("'", "%27");

        responsedata = HttpMethod.getInstance(mContext).doOperation(
                HttpMethod.TYPE.GET, url);


        return responsedata;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
//        dialog.dismiss();

        if (result != null) {
            if (result.equals(Constants.timeoutException)) {
                //Utility.showAlertDialog(mContext,"Connection Time Out");
            } else {
                try {

                    if (result.contains("MembershipID")) {

                        MemberList list = MemberParser.createInstance(mContext)
                                .getMemberDetails(result);
                        onTaskFinished(list);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        } else {
            MemberList list = null;

        }

    }

    public void createDialog(final Context context) {
//		TagInfo infoTag = (Utility.getTagForLangChange(SharedManager
//				.getInstance(context).getInt(Constants.PREV_LANG, 0), context));

//		if (!Constants.langChange) {
//			dialog = Utility.showProgressDialog(mContext, context
//					.getResources().getString(R.string.msg_wait));
//		} else {

//			if (infoTag != null) {
//				dialog = Utility.showProgressDialog(mContext,
//						infoTag.getAlertWait());
//			} else {
        dialog = Utility.showProgressDialog(mContext, pleaseWait);
        //}

        //}

    }

    abstract public void onTaskFinished(MemberList list);
}
