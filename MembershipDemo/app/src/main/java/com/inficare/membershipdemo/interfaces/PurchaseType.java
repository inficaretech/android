package com.inficare.membershipdemo.interfaces;

/**
 * Created by avinash.verma on 4/25/2017.
 */

public enum PurchaseType {
    BUY("Buy"), RENEW("Renew Membership Card"), GIFT("Gift"),DONATION("Donation");


    private final String toolbarTitle;

    PurchaseType(String toolbarTitle) {
        this.toolbarTitle = toolbarTitle;
    }

    public String getToolbarTitle() {
        return toolbarTitle;
    }
}
