package com.inficare.membershipdemo.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.GuestPassAdapter;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.GiftableGuestPas;
import com.inficare.membershipdemo.pojo.GuestPas;
import com.inficare.membershipdemo.pojo.GuestPassPostJson;
import com.inficare.membershipdemo.pojo.GuestPasses;
import com.inficare.membershipdemo.pojo.Response;
import com.inficare.membershipdemo.pojo.Result;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import me.xiaopan.barcodescanner.EncodeUtils;
import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Abhishek.jha on 10/17/2016.
 */

public class GuestPassesActivity extends AppCompatActivity {

    private Context mContext;
    private static final String GUEST_PASS_OFFLINE_DATA = "guestPassOfflineData";
    //Typeface Elements
    transient private Typeface faceheader;
    transient private Typeface faceRegular;
    View topStrip;
    //Toolbar elements
    private Toolbar toolbar;
    private TextView tb_title;
    private ImageView ivList;

    //define UI elements
    private RecyclerView mRecyclerView;
    /*private RecyclerView.LayoutManager mLayoutManager;*/
    private LinearLayoutManager mLayoutManager;
    private GuestPassAdapter guestPassAdapter;
    public static Map<String, File> bitmapMapFile;
    //memberId variable
    private String memberId;

    //UI Elements
    private ProgressDialog dialog;
    int clickedMember;
    List<Configuration> configList;
    boolean isPermission;
    private PDFView pdfView;
    Configuration configuration;
    List<GuestPas> guestPases;
    MuseumDemoPreference preferences;
    private static boolean isOffLineAvailable;
    private boolean isInstructionShown;
    private boolean isShareClicked;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    public static final String TAG = GuestPassesActivity.class.getSimpleName();
    private String pleaseWait, somethingWentWrong, msg_no_internet;
    private String membership_type, guest_pass, no_sharable_pass_available, please_select_atleast_one_pass,
            exporting_database_to_pdf, gift_pass_pdf_failed, select_guest_passes_you_need_to_share;
    private String allow_external_storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_passes);
        mContext = this;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        if (configList != null) {
            configuration = configList.get(clickedMember);
        }

        memberId = getMembershipId();

        /*if (Build.VERSION.SDK_INT >= 23) {
            if (checkCallingPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
            PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                .WRITE_EXTERNAL_STORAGE}, 1);
            }else {
                getAllGuestPassesRetrofitApi();
            }
        }else {
            getAllGuestPassesRetrofitApi();
            //isPermission=true;
        }*/
        setFonts();
        setToolbarLayout();
        findViews();
        initLanguage();
        setUi(language);
        bitmapMapFile = new HashMap<>();
        setOffLineData();
    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);



        try {
            tb_title.setText(languageMap.get(LanguageConstants.GUEST_PASSES).getString(title));
            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            somethingWentWrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);
            membership_type = languageMap.get(LanguageConstants.MEMBERSHIP_TYPE).getString(title);
            msg_no_internet = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);
            guest_pass = languageMap.get(LanguageConstants.GUEST_PASS).getString(title);
            no_sharable_pass_available = languageMap.get(LanguageConstants.NO_SHARABLE_PASS_AVAILABLE).getString(title);
            please_select_atleast_one_pass = languageMap.get(LanguageConstants.PLEASE_SELECT_ATLEAST_ONE_PASS).getString(title);
            exporting_database_to_pdf = languageMap.get(LanguageConstants.EXPORTING_DATABASE_TO_PDF).getString(title);
            select_guest_passes_you_need_to_share = languageMap.get(LanguageConstants.SELECT_GUEST_PASSES_YOU_NEED_TO_SHARE).getString(title);
            gift_pass_pdf_failed = languageMap.get(LanguageConstants.GIFT_PASS_PDF_FAILED).getString(title);
            allow_external_storage = languageMap.get(LanguageConstants.STORAGE_ACCESS_MESSAGE).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isInstructionShown &&clickedMember!=-1&&configList.get(clickedMember).getMid().equalsIgnoreCase
                ("TXVzZXVtQW55V2hlcmU=")) {
            Intent intent = new Intent(this, InstActivityMain.class);
            intent.putExtra(InstActivityMain.INST_DATA, getResources().getString(R.string.this_screen_shows_status));
            startActivity(intent);
            isInstructionShown = true;
        }
        if (Utility.isNetworkAvailable(mContext)) {
            if (memberId != null) {
                /*if(QRCodeActivity.isBacked) {
                    QRCodeActivity.isBacked = false;*/
                getAllGuestPassesRetrofitApi();
                //}

            } else {
                finish();
            }
        } else {
            // Utility.showToast(mContext, "Internet not connected!!");
            //This feature is available in online mode only.
        }
    }
/*
    private void setUIElements(){
        dialog = Utility.showProgressDialog(mContext, mContext.getResources().getString(R.string
        .msg_wait));
    }*/

    private void getAllGuestPassesRetrofitApi() {
        //GuestPass
        if (!isOffLineAvailable) {
            dialog = Utility.showProgressDialog(mContext, pleaseWait);
            dialog.show();
        }
        Call<Response> guestPasses = Utility.callRetrofit().getGuestPassesDataCall(Utility
                .getHeaderData(), memberId, configList.get(clickedMember).getMid(), Utility.getLanguage(language));
        guestPasses.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (!isOffLineAvailable)
                    dialog.dismiss();
                if (response.code() == 200) {
                    if (response.body().getSuccess().equals("true")) {

                       /* //List<GuestPas> guestPases = new ArrayList<GuestPas>();
                        List<GiftableGuestPas> giftableGuestPases;

                        if (response.body().getResult().getGuestPass() != null) {
                            guestPases = response.body().getResult().getGuestPass();
                        }

                        if (response.body().getResult().getGiftableGuestPass() != null) {
                            giftableGuestPases = response.body().getResult().getGiftableGuestPass();
                            for (int i = 0; i < giftableGuestPases.size(); i++) {
                                //giftableGuestPases.get(i).setHeader(giftableGuestPases.get(i)
                                // .getHeader()+(i+1));
                                GuestPas guestPas = new GuestPas();
                                guestPas.setID(giftableGuestPases.get(i).getID());
                                guestPas.setGuestPassID(giftableGuestPases.get(i).getGuestPassID());
                                guestPas.setMembershipID(giftableGuestPases.get(i)
                                        .getMembershipID());
                                guestPas.setIsUsed(giftableGuestPases.get(i).getIsUsed());
                                guestPas.setGifted(giftableGuestPases.get(i).getIsGifted());
                                guestPas.setGiftedPass(giftableGuestPases.get(i).getIsGiftedPass());
                                guestPas.setName(giftableGuestPases.get(i).getName());
                                guestPas.setAddress(giftableGuestPases.get(i).getAddress());
                                guestPas.setEmail(giftableGuestPases.get(i).getEmail());
                                guestPas.setPhone(giftableGuestPases.get(i).getPhone());
                                guestPas.setValidUpto(giftableGuestPases.get(i).getValidUpto());
                                guestPas.setCreatedOn(giftableGuestPases.get(i).getCreatedOn());
                                guestPas.setModifiedOn(giftableGuestPases.get(i).getModifiedOn());
                                guestPas.setStatus(giftableGuestPases.get(i).getStatus());
                                guestPas.setDiscription(giftableGuestPases.get(i).getDiscription());
                                guestPas.setHeader(giftableGuestPases.get(i).getHeader() + " " +
                                        (i + 1));
                                guestPas.setPrimaryName(giftableGuestPases.get(i).getPrimaryName());
                                //giftTable
                                guestPas.setReserveField1(giftableGuestPases.get(i).getReserveField1());
                                guestPas.setReserveField2(giftableGuestPases.get(i).getReserveField2());
                                guestPas.setGenralAdmissionDesc(giftableGuestPases.get(i).getGenralAdmissionDesc());
                                guestPas.setMemberPresenceDesc(giftableGuestPases.get(i).getMemberPresenceDesc());
                                guestPases.add(guestPas);


                            }
                        }

                        setDataInUI(guestPases);
                        try {
                            Gson gson=new Gson();
                           String strJson= gson.toJson(response.body().getResult());

                               preferences.saveString(GUEST_PASS_OFFLINE_DATA+configuration.getMid(),strJson);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
*/
                        setGuestPassData(response.body().getResult());
                        try {
                            Gson gson = new Gson();
                            String strJson = gson.toJson(response.body().getResult());

                            preferences.saveString(GUEST_PASS_OFFLINE_DATA + configuration.getMid(), strJson);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                if (!isOffLineAvailable)
                    dialog.dismiss();
                // Toast.makeText(mContext,MatcherNode.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setGuestPassData(Result result) {
        if (isShareClicked)
            return;

        List<GuestPas> guestPassTemp = new ArrayList<>();
        if (guestPases != null) {
            guestPassTemp.addAll(guestPases);
        }
        List<GiftableGuestPas> giftableGuestPases;

        if (result != null) {
            guestPases = result.getGuestPass();
        }

        if (result != null) {
            giftableGuestPases = result.getGiftableGuestPass();
            for (int i = 0; i < giftableGuestPases.size(); i++) {
                //giftableGuestPases.get(i).setHeader(giftableGuestPases.get(i)
                // .getHeader()+(i+1));
                GuestPas guestPas = new GuestPas();
                guestPas.setID(giftableGuestPases.get(i).getID());
                guestPas.setGuestPassID(giftableGuestPases.get(i).getGuestPassID());
                guestPas.setMembershipID(giftableGuestPases.get(i)
                        .getMembershipID());
                guestPas.setIsUsed(giftableGuestPases.get(i).getIsUsed());
                guestPas.setGifted(giftableGuestPases.get(i).getIsGifted());
                guestPas.setGiftedPass(giftableGuestPases.get(i).getIsGiftedPass());
                guestPas.setName(giftableGuestPases.get(i).getName());
                guestPas.setAddress(giftableGuestPases.get(i).getAddress());
                guestPas.setEmail(giftableGuestPases.get(i).getEmail());
                guestPas.setPhone(giftableGuestPases.get(i).getPhone());
                guestPas.setValidUpto(giftableGuestPases.get(i).getValidUpto());
                guestPas.setCreatedOn(giftableGuestPases.get(i).getCreatedOn());
                guestPas.setModifiedOn(giftableGuestPases.get(i).getModifiedOn());
                guestPas.setStatus(giftableGuestPases.get(i).getStatus());
                guestPas.setDiscription(giftableGuestPases.get(i).getDiscription());
                guestPas.setHeader(giftableGuestPases.get(i).getHeader() + " " +
                        (i + 1));
                guestPas.setPrimaryName(giftableGuestPases.get(i).getPrimaryName());
                //giftTable
                guestPas.setReserveField1(giftableGuestPases.get(i).getReserveField1());
                guestPas.setReserveField2(giftableGuestPases.get(i).getReserveField2());
                guestPas.setGenralAdmissionDesc(giftableGuestPases.get(i).getGenralAdmissionDesc());
                guestPas.setMemberPresenceDesc(giftableGuestPases.get(i).getMemberPresenceDesc());

                guestPas.setUsedPurpose(giftableGuestPases.get(i).getUsedPurpose());
                try {
                    /*for(int j=0;j<guestPassTemp.size();j++){
                        if(guestPas.getGuestPassID().equalsIgnoreCase(guestPassTemp.get(j).getGuestPassID()))
                           guestPas.setChecked(guestPassTemp.get(j).isChecked());
                     }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
                guestPases.add(guestPas);


            }
        }

        setDataInUI(guestPases);

    }

    private void setDataInUI(List<GuestPas> guestPasList) {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                for (int i = 0; i < guestPasList.size(); i++) {

            /*File file = saveImage(Utility.generateQRBitmap(guestPasList.get(i).getGuestPassID(),
                    mContext, 0));*/

                    Bitmap bitmap = getBarcodeBitmap(guestPasList.get(i).getGuestPassID());
                    File file = saveImage(bitmap);
                    bitmapMapFile.put(String.valueOf(i), file);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                guestPassAdapter.notifyDataSetChanged();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        guestPassAdapter = new GuestPassAdapter(mContext, guestPasList, guest_pass);
        mRecyclerView.setAdapter(guestPassAdapter);

        //shareGiftPass(1);

    }

    private String getMembershipId() {
        Intent i = getIntent();
        return i.getStringExtra("memberId");
    }

    private void setToolbarLayout() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.toolbar_title);
        ivList = findViewById(R.id.ivList);
        ivList.setVisibility(View.VISIBLE);
        //ivList.setBackgroundResource(R.drawable.share_icon);
        ivList.setImageResource(R.drawable.share_icon);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tb_title.setTypeface(faceheader);
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        ivList.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_button));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ivList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (guestPassAdapter != null) {

                    int fPos = -1;
                    for (int i = 0; i < guestPases.size(); i++) {
                        String status = guestPases.get(i).getStatus();
                        if (guestPases.get(i).getGiftedPass() && (status.equalsIgnoreCase("Ready to use") || status.equalsIgnoreCase("Shared"))) {
                            fPos = i;
                            break;
                        }
                    }
                    if (fPos == -1) {
                        Toast.makeText(GuestPassesActivity.this, no_sharable_pass_available, Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        mRecyclerView.smoothScrollToPosition(fPos);
                    }
                    if (isShareClicked) {
                        shareGiftPass();
                        getGiftGuestPassesRetrofitApi();
                        return;
                    } else {
                        Toast.makeText(mContext, select_guest_passes_you_need_to_share, Toast.LENGTH_LONG).show();
                    }
                    isShareClicked = true;
                    guestPassAdapter.onShareClicked(isShareClicked);
                }
            }
        });
    }

    private void setFonts() {
        faceheader = Utility.setFaceTypeHeader(mContext);
        faceRegular = Typeface.createFromAsset(getAssets(),
                "fonts/Raleway-Medium.ttf");
    }

    private void findViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_guest_passes);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        pdfView = (PDFView) findViewById(R.id.pdfView);

        topStrip = findViewById(R.id.topStrip);
        topStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));

    }

    /* private File saveImage(Bitmap finalBitmap) {

         String root = Environment.getExternalStorageDirectory().toString();
         File myDir = new File(root + "/.saved_images");
         myDir.mkdirs();
         Random generator = new Random();
         int n = 10000;
         n = generator.nextInt(n);
         String fname = "Image-"+ n +".jpg";
         File file = new File(myDir, fname);
         if (file.exists ()) file.delete ();
         try {
             FileOutputStream out = new FileOutputStream(file);
             finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
             out.flush();
             out.close();
             return file;
         } catch (Exception e) {
             e.printStackTrace();
         }
         return null;
     }*/
    private File saveImage(Bitmap finalBitmap) {

        File file = null;
        FileOutputStream outputStream;
        try {
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            file = new File(getCacheDir(), "MyCache" + n);

            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //For Gallery
        if (grantResults.length == 0) {
            return;
        }
        /*if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getAllGuestPassesRetrofitApi();
            } else {
                Toast.makeText(this, "Please Allow Membership App to Access External Storage .",
                        Toast.LENGTH_LONG).show();
            }
        }*/

        if (requestCode == EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                proceedShareCall();
            } else {
                Toast.makeText(this, allow_external_storage,
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private final int EXTERNAL_STORAGE_PERMISSION = 100;


    public void shareGiftPass() {

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission
                    .WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(GuestPassesActivity.this, new String[]{android
                        .Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION);
            } else {
                proceedShareCall();
            }
        } else {
            proceedShareCall();
        }
    }

    private void proceedShareCall() {


        try {
            List<byte[]> listBytes = new ArrayList<>();
            List<Integer> posList = new ArrayList<>();
            for (int i = 0; i < guestPases.size(); i++) {
                if (guestPases.get(i).isChecked()) {
                    Uri uri = Uri.fromFile(bitmapMapFile.get(String.valueOf(i)));
                    InputStream iStream = null;
                    iStream = getContentResolver().openInputStream(uri);
                    byte[] inputData = getBytes(iStream);
                    listBytes.add(inputData);
                    posList.add(i);
                }

            }

            if (posList.size() != 0) {
                new ExportPDFTask(true, listBytes, posList).execute();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class ExportPDFTask extends AsyncTask<String, String, String> {
        private final ProgressDialog dialog = new ProgressDialog(mContext);
        File pdfFile;

        boolean isPreview;
        List<byte[]> listBytes;
        List<Integer> posList;

        public ExportPDFTask(boolean isPreview, List<byte[]> listBytes, List<Integer> posList) {

            this.isPreview = isPreview;
            this.listBytes = listBytes;
            this.posList = posList;
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage(exporting_database_to_pdf);
            dialog.setCancelable(false);
            dialog.show();
        }

        protected String doInBackground(final String... args) {

            File exportDir = new File(Environment.getExternalStorageDirectory(), Constants
                    .DOCUMENT_LOCATION_VISIBLE);
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            String docFileName = "gift_pass" + "-" + System.currentTimeMillis();
            pdfFile = Utility.renameFileToNew(exportDir, docFileName + "", ".pdf");

            float left = 120;
            float right = 120;
            float top = 60;
            float bottom = 0;
            com.itextpdf.text.Document doc = new com.itextpdf.text.Document(PageSize.A4, left, right, top, bottom);//
            PdfWriter docWriter = null;
            DecimalFormat df = new DecimalFormat("0.00");

            int position;


            try {
                //special font sizes
                /// BaseFont urName = BaseFont.createFont("assets/fonts/Raleway-Regular.ttf", "UTF-8", BaseFont.EMBEDDED);
                Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 30f, Font.BOLD, new
                        BaseColor(hex2Rgb(configuration.getColorCode(), 1), hex2Rgb(configuration.getColorCode(), 2), hex2Rgb(configuration.getColorCode(), 3)));
                Font contentFont = new Font(Font.FontFamily.TIMES_ROMAN, 20f);
                Font contentFontBold = new Font(Font.FontFamily.TIMES_ROMAN, 20f, Font.BOLD);
                //file path
                docWriter = PdfWriter.getInstance(doc, new FileOutputStream(pdfFile));
                //document header attributes
                doc.addAuthor("Inficare ");
                doc.addCreationDate();
                doc.addProducer();
                doc.addCreator("inficaretech.com");
                doc.addTitle("Gift Pass");// with Column Headings
                doc.setPageSize(PageSize.LETTER);
                //open document
                doc.open();
              /*  Rectangle one = new Rectangle(70,100);
                doc.setPageSize(one);
                doc.setMargins(20, 20, 20, 20);*/
                for (int i = 0; i < listBytes.size(); i++) {
                    position = posList.get(i);
                    //para 1
                    String museumName = configuration.getName();
                    Paragraph hosParagraph = new Paragraph(museumName, headerFont);
                    hosParagraph.setAlignment(Element.ALIGN_CENTER);
                    if (i != 0) {
                        doc.newPage();
                        hosParagraph.setSpacingBefore(top);
                    }
                    /*hosParagraph.setFont(bfBold12);*/
                    doc.add(hosParagraph);
                    //para 2
                    String address = configuration.getAddress();
                    Paragraph addressParagraph = new Paragraph(address, contentFont);
                    addressParagraph.setAlignment(Element.ALIGN_CENTER);
                    addressParagraph.setSpacingBefore(30);
                    doc.add(addressParagraph);
                    //para 3
                    String reservedField1 = guestPases.get(position).getReserveField1();
                    // reservedField1="reservedField1";
                    if (reservedField1 != null && !reservedField1.isEmpty()) {
                        Paragraph reserved1Paragraph = new Paragraph(reservedField1, contentFont);
                        reserved1Paragraph.setAlignment(Element.ALIGN_CENTER);
                        doc.add(reserved1Paragraph);
                    }


                    Image image = Image.getInstance(listBytes.get(i));
                    //image.scaleAbsolute(250, 250);
                    image.scaleAbsolute(350, 150);
                    image.setAlignment(Element.ALIGN_CENTER);

                    Paragraph paragraph = new Paragraph();
                    paragraph.setSpacingBefore(20);
                    //specify column widths
                    float[] columnWidths = {1f};//, 1f
                    //create PDF table with the given widths
                    PdfPTable table = new PdfPTable(columnWidths);
                    // set table width a percentage of the page width
                    table.setWidthPercentage(100f);

                    //insert column headings
                    /*insertCellForDateAndSignature(table, "Date", Element.ALIGN_LEFT, 1, bfBold12);
                    insertCellForDateAndSignature(table, "Gift Card", Element.ALIGN_RIGHT, 1,
                            bfBold12);
                    table.setHeaderRows(1);
                    SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
                    insertCellForDateAndSignature(table, sdf.format(new Date()), Element
                            .ALIGN_LEFT, 1, bf12);*/

                    PdfPCell pdfPCell = new PdfPCell(image);
                    pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    //set the cell column span in case you want to merge two or more cells
                    //  pdfPCell.setColspan(1);
                    pdfPCell.setBorderWidth(0);
                    //in case there is no text and you wan to create an empty row

                    //add the call to the table
                    table.addCell(pdfPCell);
                    paragraph.add(table);
                    doc.add(paragraph);

                    //para qrId
                    String qrId = guestPases.get(position).getGuestPassID();
                    Paragraph qrParagraph = new Paragraph(qrId, contentFontBold);
                    qrParagraph.setAlignment(Element.ALIGN_CENTER);
                    qrParagraph.setSpacingBefore(10);
                    qrParagraph.setSpacingAfter(10);
                    doc.add(qrParagraph);
                    //para 5
                    String generalAdmissionDesc = guestPases.get(position).getGenralAdmissionDesc();
                    Paragraph admissionParagraph = new Paragraph(generalAdmissionDesc, contentFont);
                    admissionParagraph.setAlignment(Element.ALIGN_CENTER);
                    doc.add(admissionParagraph);
                    //para 6
                    String validDate = guestPases.get(position).getValidUpto();
                    String[] stringParts = validDate.split(" ");
                    String dateOnly = stringParts[0];
                    dateOnly = "Valid Until " + Utility.convertDateFormat(dateOnly);
                    Paragraph validUpToParagraph = new Paragraph(dateOnly, contentFontBold);
                    validUpToParagraph.setAlignment(Element.ALIGN_CENTER);
                    validUpToParagraph.setSpacingBefore(10);
                    validUpToParagraph.setSpacingAfter(10);
                    doc.add(validUpToParagraph);
                    //para 7
                    String memberPresence = guestPases.get(position).getMemberPresenceDesc();
                    Paragraph presenceParagraph = new Paragraph(memberPresence, contentFont);
                    presenceParagraph.setAlignment(Element.ALIGN_CENTER);
                    doc.add(presenceParagraph);
                    //para 8
                    String reserveField2 = guestPases.get(position).getReserveField2();
                    //reserveField2="reserveField2";
                    if (reserveField2 != null && !reserveField2.isEmpty()) {
                        Paragraph field2Paragraph = new Paragraph(reserveField2, contentFont);
                        field2Paragraph.setAlignment(Element.ALIGN_CENTER);
                        doc.add(field2Paragraph);
                    }
                }
            } catch (DocumentException dex) {
                dex.printStackTrace();
                return "";

            } catch (Exception ex) {
                ex.printStackTrace();
                return "";
            } finally {

                if (doc != null) {
                    //close the document
                    doc.close();
                }
                if (docWriter != null) {
                    //close the writer
                    docWriter.close();
                }


                /*return "" + pdfFile.getPath();*/

            }
            return "" + pdfFile.getPath();
        }

        @SuppressLint("NewApi")
        @Override
        protected void onPostExecute(final String filePath) {
            if (dialog != null) {

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

            if (!filePath.isEmpty()) {
                // Utility.showToast(context, getString(R.string.toast_report_pdf_success));

                filename = Utility.getFileNameFromUrl(filePath);

                /*fileContent = UtilityDoc.convertFileToByteArray
                        (filePath);*/

                /*if (isPreview)
                    showPreview();
                else
                    saveSignInsReport(filePath);*/
                //openOptionDialog(filePath);
                /*pdfView.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                loadDocumentUI();*/

                //Utility.sendFileToMailIntent(mContext, filePath, "Attach Gift Pass");
                guestPassUsesAlert(filePath);

            } else {
                Utility.showToast(mContext, gift_pass_pdf_failed);
            }
        }

    }

    private String filename;
    private String fileContent;

    private void loadDocumentUI() {

        try {
            File file = new File(Environment.getExternalStorageDirectory() + Constants
                    .DOCUMENT_LOCATION_VISIBLE + filename);
            if (file.exists()) {
                pdfView.fromFile(file)
                        .enableSwipe(true)
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(0)
                        .enableAnnotationRendering(false)
                        .password(null)
                        .scrollHandle(null)
                        .load();
                pdfView.setScaleX(1.0f);
            }
        } catch (Exception e) {

        }

    }

    private void insertCellForDateAndSignature(PdfPTable table, String text, int align, int
            colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        cell.setBorderWidth(0);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }

        //add the call to the table
        table.addCell(cell);

    }

    private void shareVia(Uri uri) {
        /*String fileName = "image-3116.jpg";//Name of an image
        String externalStorageDirectory = Environment.getExternalStorageDirectory().toString();
        String myDir = externalStorageDirectory + "/saved_images/"; // the file will be in
        saved_images
        Uri uri = Uri.parse("file:///" + myDir + fileName);*/
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/html");
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Test Mail");
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Launcher");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(shareIntent, "Share Deal"));
    }

    private void guestPassUsesAlert(final String filePath) {
        shareWithFile(filePath);
       /* final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.guest_pass_alert, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle("Gift GuestPass?");

        Button bt_assign_guest_pass = (Button) dialogView.findViewById(R.id.bt_assign_guest_pass);
        Button bt_cancel = (Button) dialogView.findViewById(R.id.bt_cancel);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        bt_assign_guest_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /// getGiftGuestPassesRetrofitApi(filePath, position);
                shareWithFile(filePath, position);
                alertDialog.dismiss();
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });*/
    }


    private void getGiftGuestPassesRetrofitApi() {
        //GuestPass
        List<GuestPasses> posList = new ArrayList<>();
        GuestPassPostJson guestPassPostJson = new GuestPassPostJson();
        for (int i = 0; i < guestPases.size(); i++) {
            if (guestPases.get(i).isChecked()) {
                GuestPasses guestPasses = new GuestPasses();
                guestPasses.setGuestPassID(guestPases.get(i).getGuestPassID());
                guestPasses.setOrganisation_key(configuration.getMid());
                guestPasses.setMembershipID(memberId);
                posList.add(guestPasses);
            }

        }
        guestPassPostJson.setGuestPasses(posList);
        Call<Response> guestPasses = Utility.callRetrofit().getGiftGuestPassCall(Utility
                .getHeaderData(), guestPassPostJson);
        guestPasses.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                if (response.code() == 200) {
                    if (response.body().getSuccess().equals("true")) {
                        Log.i("gift level", "true");
                    } else {

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });
    }

    private void shareWithFile(String filePath) {
        /*String fileName = "image-3116.jpg";//Name of an image
        String externalStorageDirectory = Environment.getExternalStorageDirectory().toString();
        String myDir = externalStorageDirectory + "/saved_images/"; // the file will be in
        saved_images
        Uri uri = Uri.parse("file:///" + myDir + fileName);*/
        /*Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/html");
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Test Mail");
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Launcher");
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(shareIntent, "Share Deal"));*/

        /*startActivity(new Intent(Intent.ACTION_SEND).setDataAndType(Uri.fromFile(bitmapMapFile
                .get(String.valueOf(position))), "application/pdf"));

        File outputFile = new File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_DOWNLOADS), "example.pdf");
        Uri uri2 = Uri.fromFile(outputFile);*/

        //Uri uri = Uri.fromFile(bitmapMapFile.get(String.valueOf(position)));
        Uri path = Uri.fromFile(new File(filePath));
        Intent share = new Intent();
        share.setAction(Intent.ACTION_SEND);
        share.setType("application/pdf");
        share.putExtra(Intent.EXTRA_STREAM, path);
//        share.setPackage("com.whatsapp");
        //startActivity(share);
        startActivityForResult(share, 10);


    }

    public static int hex2Rgb(String hex1, int color) {
        int r = Integer.valueOf(hex1.substring(1, 3), 16);
        int g = Integer.valueOf(hex1.substring(3, 5), 16);
        int b = Integer.parseInt(hex1.substring(5, 7), 16);
        if (color == 1) {
            return r;
        } else if (color == 2) {
            return g;
        } else {
            return b;
        }
    }

    public Bitmap getBarcodeBitmap(String content) {
        Bitmap bmp = null;
        try {
            bmp = EncodeUtils.encode(content, BarcodeFormat.CODE_39, null, 1000,
                    200, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;

    }

    private void setOffLineData() {
        try {
            String jData = preferences.getString(GUEST_PASS_OFFLINE_DATA + configuration.getMid(), null);
            if (jData != null) {
                isOffLineAvailable = true;
                Gson gson = new Gson();
                Result result = gson.fromJson(jData, Result.class);
                setGuestPassData(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (isShareClicked) {
            isShareClicked = false;
            for (GuestPas pas : guestPases) {
                pas.setChecked(false);
            }
            guestPassAdapter.onShareClicked(isShareClicked);
        } else {
            super.onBackPressed();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Activity.RESULT_OK && requestCode == 10) {
            Log.i("onActivityResult", "requestCode:" + requestCode);

        }
    }


}
