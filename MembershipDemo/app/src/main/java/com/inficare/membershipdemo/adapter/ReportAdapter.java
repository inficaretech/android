package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Report;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Abhishek.jha on 10/21/2016.
 */

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapterViewHolder> {

    private Context mContext;
    private Report mReport;
    private String language;
    private Map<String, JSONObject> languageMap;
    private String membership, member_name, used_pass, left_pass, valid_from, valid_upto;

    public ReportAdapter(Context mContext, Report mReport, String language, Map<String, JSONObject> languageMap) {
        this.mContext = mContext;
        this.mReport = mReport;
        this.language = language;
        this.languageMap = languageMap;

        String title = Utility.getTitle(language);

        try {
            this.membership = languageMap.get(LanguageConstants.MEMBERSHIP_NUMBER).getString(title);
            this.member_name = languageMap.get(LanguageConstants.MEMBER_NAME).getString(title);
            this.used_pass = languageMap.get(LanguageConstants.USED_PASS).getString(title);
            this.left_pass = languageMap.get(LanguageConstants.LEFT_PASS).getString(title);
            this.valid_from = languageMap.get(LanguageConstants.VALID_FROM).getString(title);
            this.valid_upto = languageMap.get(LanguageConstants.VALID_UPTO).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public ReportAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_item_view, parent, false);
        ReportAdapterViewHolder reportAdapterViewHolder = new ReportAdapterViewHolder(view);
        return reportAdapterViewHolder;
    }

    @Override
    public void onBindViewHolder(ReportAdapterViewHolder holder, int position) {
        holder.membership_number.setText(membership + " : " + mReport.getMembershipId());
        holder.member_name.setText(member_name + " : " + mReport.getPrimaryName());
        holder.used_pass.setText(used_pass + " : " + mReport.getUsedPass());
        holder.left_pass.setText(left_pass + " : " + mReport.getRemainsPass());

        String dateandtimeValidFrom = mReport.getValidFrom();
        String[] stringPartsValidFrom = dateandtimeValidFrom.split(" ");
        String dateOnlyValidFrom = stringPartsValidFrom[0];
        holder.valid_from.setText(valid_from + " : " + Utility.convertDateFormat(dateOnlyValidFrom));

        String dateandtimeValidUpto = mReport.getValidUpto();
        String[] stringPartsValidUpto = dateandtimeValidUpto.split(" ");
        String dateOnlyValidUpto = stringPartsValidUpto[0];

        holder.valid_till.setText(valid_upto + " : " + Utility.convertDateFormat(dateOnlyValidUpto));
    }

    @Override
    public int getItemCount() {
        return 1;
    }

}
