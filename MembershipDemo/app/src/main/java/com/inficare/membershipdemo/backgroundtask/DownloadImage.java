package com.inficare.membershipdemo.backgroundtask;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public abstract class DownloadImage extends AsyncTask<String, Void, List<File>> {
    private Context context;
    private ProgressDialog dialog;
    private static final String RESULT = "result";
    String folder=".ecard";
    List<String> urlList;
//String accessToken;

    public DownloadImage(Context context, List<String> urlList) {
        this.context = context;
        dialog = new ProgressDialog(context);
        this.urlList=urlList;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (dialog != null) {
            dialog.setMessage("Please wait...");
            dialog.setCancelable(false);
            //dialog.show();
        }
    }

    @Override
    protected List<File> doInBackground(String... params) {
        Bitmap bitmap = null;
        InputStream input = null;
        File directory=null;
        List<File> fileList=new ArrayList<>();
        try {
             directory = new File(Environment.getExternalStorageDirectory() + File.separator + folder);
            // String directory = Environment.getExternalStorageDirectory().getAbsolutePath();
            if (!directory.exists() && !directory.isDirectory()) {
                // create empty directory
                directory.mkdirs();
            }
            for(int i=0;i<urlList.size();i++)
            {
                String url=urlList.get(i);
                String[] lastName = url.split("/");

                String fileName = lastName[lastName.length - 1];
                File mypath = new File(directory, fileName);
                if (!mypath.exists()) {
                    File file = getPath(url, mypath.getPath());
                    fileList.add(file);
                } else {
                    fileList.add(mypath);
                   // return mypath;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        return fileList;
    }

    @Override
    protected void onPostExecute(List<File> result) {
        super.onPostExecute(result);
        if (dialog.isShowing()) {
            dialog.dismiss();
            // Toast.makeText(mContext, " " + result, Toast.LENGTH_LONG).show();
        }
        onTaskFinished(result);
    }

    public abstract void onTaskFinished(List<File> file);

    /**
     *
     * @param imageurl remote image url
     * @param localpath your sd card path
     * @return file object
     */
    private File getPath(String imageurl, String localpath) {
        String filepath = null;
        File file = null;
        InputStream inputStream = null;
        try {

            URL url = new URL(imageurl);
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.setConnectTimeout(20000);
            try {
                urlConnection.connect();
                inputStream = urlConnection.getInputStream();
            } catch (SocketTimeoutException e) {

                return file;
            } catch (Exception e) {

                return file;
            }
            if (localpath != null) {
                file = new File(localpath);

                FileOutputStream fileOutput = new FileOutputStream(
                        file);

                int totalSize = urlConnection.getContentLength();
                int downloadedSize = 0;
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                if (inputStream != null) {
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        fileOutput.write(buffer, 0, bufferLength);
                        downloadedSize += bufferLength;
                        // Log.i("Progress:", "downloadedSize:" +
                        // downloadedSize
                        // + "totalSize:" + totalSize);
                    }
                }

                fileOutput.close();
                ///  if (downloadedSize == totalSize)
                /// filepath = downloadImage.getPath();


            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("ImageDownload", "Exception in DownloadRunnable" + e);
        }
        Log.i("filepath:", " " + file.getPath());
        return file;

    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
