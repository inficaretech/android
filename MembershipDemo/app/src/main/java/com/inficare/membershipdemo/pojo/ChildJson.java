package com.inficare.membershipdemo.pojo;

import java.io.Serializable;

/**
 * Created by akshay.kumar on 3/20/2018.
 */

public class ChildJson implements Serializable{
    private String firstname;
    private String lastname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
