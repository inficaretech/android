package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.FloorPlan;

import java.util.List;

public class FloorAdapter extends ArrayAdapter<FloorPlan> {

	private Context mContext;

	private int mResource;

	private List<FloorPlan> floornames;

	private Typeface facedescription;

	public FloorAdapter(Context context, int resource, List<FloorPlan> objects) {
		super(context, resource, objects);
		mContext = context;
		mResource = resource;
		floornames = objects;
		facedescription = Utility.setFaceTypeContent(mContext);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = LayoutInflater.from(mContext).inflate(mResource, null);
		TextView text = (TextView) convertView.findViewById(R.id.txt_floorname);
		if (!floornames.get(position).getTitle().equals(null)) {
			text.setText(floornames.get(position).getTitle());
		}
		text.setTypeface(facedescription, Typeface.BOLD);
		return convertView;
	}

}
