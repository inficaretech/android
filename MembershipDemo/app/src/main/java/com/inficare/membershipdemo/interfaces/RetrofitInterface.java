package com.inficare.membershipdemo.interfaces;

import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.pojo.CardMember;
import com.inficare.membershipdemo.pojo.FAQResponse;
import com.inficare.membershipdemo.pojo.GuestPassPostJson;
import com.inficare.membershipdemo.pojo.LanguageResponse;
import com.inficare.membershipdemo.pojo.Model;
import com.inficare.membershipdemo.pojo.MyReciprocalResponse;
import com.inficare.membershipdemo.pojo.MyResponse;
import com.inficare.membershipdemo.pojo.NotificationsResult;
import com.inficare.membershipdemo.pojo.PaymentDetailJSON;
import com.inficare.membershipdemo.pojo.ReciprocalResult;
import com.inficare.membershipdemo.pojo.Response;
import com.inficare.membershipdemo.pojo.ResponseBarcode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by avinash.verma on 9/20/2016.
 */
public interface RetrofitInterface {

    @GET("artists")
    Call<Model> demoAPI(@Header("Authorization") String auth, @Query("page") int page_id, @Query
            ("user_id") String id, @Query("access_token") String quesry);


    @GET("api/upsell/getBenefitsByMembershipID")
    Call<Model> myMemberBenifitCall(@Header("Authorization") String header, @Query("memId")
            String memId, @Query(Constants.ORGANISATION_KEY) String orgKey, @Query("short_code") String short_code);

    @GET("api/upsell/getAllBenefits")
    Call<Model> getAllBenefitsCall(@Header("Authorization") String header, @Query(Constants
            .ORGANISATION_KEY) String memId, @Query("short_code") String short_code);

    @GET("api/guest/genrateGuestPass")
    Call<Response> getGuestPassesDataCall(@Header("Authorization") String header, @Query("mid")
            String memberId, @Query(Constants.ORGANISATION_KEY) String orgKey, @Query("short_code") String short_code);

    @POST("api/guest/giftGuestPass")
    Call<Response> getGiftGuestPassCall(@Header("Authorization") String header, @Body GuestPassPostJson guestPassPostJson);

    /*http://14.141.136.170:9999/ApiEcard/api/guest/giftGuestPass?guestPassId=MFDH5593&organisation_key=TXVzZXVtQW55V2hlcmU=*/

    @GET("api/guest/AdminAccess")
    Call<Response> getAdminAccessCode(@Header("Authorization") String header, @Query("key")
            String key, @Query(Constants.ORGANISATION_KEY) String orgKey);

    @GET("api/Guest/UpdatePassStatus")
    Call<ResponseBarcode> sendScannedQRCode(@Header("Authorization") String header, @Query
            ("guestPassId") String guestPassId, @Query("status") String status, @Query(Constants
            .ORGANISATION_KEY) String orgKey, @Query("purpouse") String purpouse);

    @GET("api/guest/getReport")
    Call<Response> getMemberReport(@Header("Authorization") String header, @Query("mid") String
            mid, @Query(Constants.ORGANISATION_KEY) String orgKey);

    @GET("api/Config/getFaq")
    Call<FAQResponse> getFAQs(@Query(Constants.ORGANISATION_KEY_CAP) String orgKey, @Query("short_code") String short_code);

    @POST("api/paypal")
    Call<ResponseBody> sendTransitionDetailToServer(@Body PaymentDetailJSON json);

    @POST("api/Transaction/MembershipTransaction")
    Call<ResponseBody> sendBuyer(@Header("Authorization") String token, @Body PaymentDetailJSON
            json);

    @POST("api/Transaction/MembershipTransaction ")
    Call<ResponseBody> sendRenew(@Header("Authorization") String token, @Body PaymentDetailJSON
            json);

    @POST("api/Transaction/MembershipTransaction")
    Call<ResponseBody> sendGift(@Header("Authorization") String token, @Body PaymentDetailJSON
            json);

    @POST("api/membershipdetail/searchMember")
    Call<List<CardMember>> searchMember(@Body Map<String, String> json);

    @GET("api/config/getConfiguration")
    Call<ResponseBody> getconfiguration(@Query("device_token") String device_token);

    @POST("api/config/ChooseLanguage")
    Call<LanguageResponse> chooseLanguage(@Body HashMap<String, String> map);

    @GET("api/config/getReciprocal")
    Call<ReciprocalResult> getReciprocalData(@Query("Organisation_key") String organisationkey);

    @GET("api/config/getOrgReciprocal")
    Call<ReciprocalResult> getOrgReciprocal(@Query("Org_key") String organisationkey, @Query("short_code") String short_code);

    @GET("api/upsell/getNotifications")
    Call<NotificationsResult> getNotifications(@Query("Organisation_key") String organisationkey, @Query("UniqueDeviceID") String UniqueDeviceID, @Query("memId") String memId);

    @POST("api/Transaction/validateTransaction ")
    Call<MyResponse> validateDataFromServer(@Header("Authorization") String token, @Body PaymentDetailJSON json);

    @GET("api/config/getStates")
    Call<Response> getStates();

    @GET("api/config/VersionConfig")
    Call<MyResponse> getVersionConfig(@Query("version") String version, @Query("devicetype") String devicetype);

    @GET("api/upsell/getReciprocalByLevel")
    Call<MyReciprocalResponse> getReciprocalByLeve(@Query("levelId") String levelId, @Query("Organisation_key") String Org_key);

}
