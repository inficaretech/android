package com.inficare.membershipdemo.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.CardAdapterExpired;
import com.inficare.membershipdemo.databasecreation.CBO;
import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.interfaces.PurchaseType;
import com.inficare.membershipdemo.interfaces.SetCardPosition;
import com.inficare.membershipdemo.membership.MemberInfo;
import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RenewMembershipActivity extends AppCompatActivity implements SetCardPosition, View
        .OnClickListener {
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    private Toolbar toolbar;
    private Context mContext;
    private TextView tb_title;
    private ArrayList<MemberInfo> list;
    CardAdapterExpired adapter;
    //    private Dialog dialog;
    ViewPager pager;
    private int pos;
    ViewAnimator animator;
    ImageView profimage;
    boolean flag_fb = false;
    private LinearLayout ll_renew;
    Button btnRenew;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renew_membership);
        mContext = this;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        findViews();
        list = (ArrayList<MemberInfo>) getIntent().getSerializableExtra(Constants.MEMBER_DATA);
        if(list==null){
            list=setData();
        }
        if (list != null && list.size() > 0) {
//            dialog = Utility.initializeDialogforDescription1(this, R.layout.popup_card);

//            View view = LayoutInflater.from(this).inflate(R.layout.activity_renew_membership,
// null);
//            findTicketViews(view);
            findTicketViews();
//            dialog.show();
//            Utility.setscreenCard(dialog);
        }

    }

    private void findViews() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title=findViewById(R.id.tb_title);
        btnRenew =findViewById(R.id.btnRenew);
        btnRenew.setText("Renew Membership");
        tb_title.setText("Renew Membership");
        //btnRenew.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        Utility.changeDrawableColor(this, btnRenew, configList.get
                (clickedMember).getColorCode());
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.hideKeyboard((Activity) mContext);
                onBackPressed();
            }
        });
    }

    @Override
    public void setPosition(int pos, ViewAnimator animator, ImageView image, boolean flag, int
            type) {
        this.pos = pos;
        this.animator = animator;
        profimage = image;
        flag_fb = flag;

        //IF CLICK ON GALLERY,Camera,Facebook TO TAKE PHOTO
       /* if (type == TYPE_GALLERY) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkCallingPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                    .WRITE_EXTERNAL_STORAGE}, TYPE_GALLERY);
                } else {
                    Utility.selectFromGallery(ctx);
                }

            } else {

                Utility.selectFromGallery(ctx);
            }
        } else if (type == TYPE_CAMERA) {
            if (Build.VERSION.SDK_INT >= 23) {
                if ((checkCallingPermission(Manifest.permission.CAMERA) != PackageManager
                .PERMISSION_GRANTED) || (checkCallingPermission(Manifest.permission
                .WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                    .CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, TYPE_CAMERA);
                }else{
                    Utility.selectFromCamera(ctx);
                }

            } else {
                Utility.selectFromCamera(ctx);
            }
        } else if (type == TYPE_FB) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkCallingPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                    .WRITE_EXTERNAL_STORAGE}, TYPE_FB);
                } else {
                    if (flag) {
                        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList
                        ("public_profile"));
                    }
                }
            } else {
                if (flag) {
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList
                    ("public_profile"));
                }
            }

        }*/
    }

    private void findTicketViews() {

//        CircleButton bt_renew = (CircleButton) findViewById(R.id.bt_renew);
//        Button bt_renew = (Button) findViewById(R.id.bt_renew);
        final TextView bt_renew = (TextView) findViewById(R.id.btnRenew);
        btnRenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(mContext, MembershipPlanDetailActivity.class);
                intent.putExtra("purchaseType", PurchaseType.RENEW);
                intent.putExtra("MemberInfo", list.get(0));
//                intent.putExtra("userdetails", userProfileDetails);
                startActivity(intent);*/
               String renewUrl=configList.get(clickedMember).getRenewLink();
               if(renewUrl!=null&&!renewUrl.isEmpty()){
                   openWebPage(renewUrl);
               }

            }
        });

        String renewUrl=configList.get(clickedMember).getRenewLink();
        if(renewUrl==null||renewUrl.isEmpty()){
            btnRenew.setVisibility(View.GONE);
        }

        List<String> listFrequency= configList.get(clickedMember).getNotificationFrequency();
        if (!Utility.isExpiredCard(list.get(0).getValidThroughTimeSpan())||(Utility.isNotificationFrequency(list.get(0).getValidThroughTimeSpan(),listFrequency))) {
            btnRenew.setVisibility(View.VISIBLE);
        } else {
            btnRenew.setVisibility(View.GONE);
            Toast.makeText(this,getResources().getString(R.string.your_card_is_not_expire),Toast.LENGTH_LONG).show();

        }

        Collections.reverse(list);
        pager = (ViewPager) findViewById(R.id.mid_layout);
        adapter = new CardAdapterExpired(this, list, this);
        pager.setAdapter(adapter);

        final ImageView arrow_left = (ImageView) findViewById(R.id.arrow_left);

        final ImageView arrow_right = (ImageView) findViewById(R.id.arrow_right);
        if (list != null) {
            if (list.size() == 1) {
                arrow_left.setVisibility(View.INVISIBLE);
                arrow_right.setVisibility(View.INVISIBLE);
            } else {
                arrow_left.setVisibility(View.INVISIBLE);
                arrow_right.setVisibility(View.VISIBLE);
            }

        }
        arrow_left.setOnClickListener(this);
        arrow_right.setOnClickListener(this);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                if (pos == 0) {
                    arrow_left.setVisibility(View.INVISIBLE);
                } else {
                    arrow_left.setVisibility(View.VISIBLE);
                }

                if (pos == adapter.getCount() - 1) {
                    arrow_right.setVisibility(View.INVISIBLE);
                } else {
                    arrow_right.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

    }
    public void openWebPage(String url) {
        try {
            Uri webpage = Uri.parse(url);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "No application can handle this request. Please install a web browser or check your URL.",  Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.arrow_left:
                int pos = pager.getCurrentItem();
                if (pos > 0) {
                    pager.setCurrentItem(pos - 1);
                }
                break;
            case R.id.arrow_right:
                int pos1 = pager.getCurrentItem();
                if (pos1 < adapter.getCount() - 1) {
                    pager.setCurrentItem(pos1 + 1);
                }
                break;


            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private MemberList setData() {

        @SuppressWarnings("unchecked")
        ArrayList<MemberInfo> info = CBO.FillCollection(
                DataProvider.getInstance(this).getMemberDetails(DBController.createInstance(this,
                        false), 0), MemberInfo.class);

        MemberList listMember = new MemberList();

        listMember.addAll(info);


        return listMember;

    }
}
