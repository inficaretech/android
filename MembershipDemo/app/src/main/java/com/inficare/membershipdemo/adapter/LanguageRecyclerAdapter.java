package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hwangjr.rxbus.RxBus;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.dialogFragment.LanguageDialogFragment;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Language;
import com.inficare.membershipdemo.pojo.RxBusResultModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageRecyclerAdapter extends RecyclerView.Adapter<LanguageRecyclerAdapter.ViewHolder> {


    private static final String TAG = "LanguageRecyclerAdapter";

    private Context mContext;
    private List<Language> languageList;
    private String mid, language_already_selected, no_internet, colorCode, selectedLanguage;
    private LanguageDialogFragment languageDialogFragment;
    private boolean showCheckedLanguage;

    public LanguageRecyclerAdapter(Context mContext, List<Language> languageList, String mid,
                                   LanguageDialogFragment languageDialogFragment, String language_already_selected,
                                   String no_internet, String colorCode, boolean showCheckedLanguage, String selectedLanguage) {

        this.mContext = mContext;
        this.languageList = languageList;
        this.mid = mid;
        this.languageDialogFragment = languageDialogFragment;
        this.language_already_selected = language_already_selected;
        this.no_internet = no_internet;
        this.colorCode = colorCode;
        this.showCheckedLanguage = showCheckedLanguage;
        this.selectedLanguage = selectedLanguage;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_language_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Language language = languageList.get(position);

        holder.tvLanguageName.setText(language.getTitle());

        if (showCheckedLanguage) {

            if (selectedLanguage.equals(language.getShortCode())) {
                holder.ivSelected.setVisibility(View.VISIBLE);
            } else {
                holder.ivSelected.setVisibility(View.GONE);

            }
        } else {
            holder.ivSelected.setVisibility(View.GONE);

        }


        holder.llContainer.setOnClickListener(v -> {
            if (Utility.isNetworkAvailable(mContext)) {


                if (!selectedLanguage.equalsIgnoreCase(language.getShortCode())) {

                    Log.d(TAG, "onclick: " + mid + " :" + language.getShortCode());

                    if (languageDialogFragment != null) {
                        languageDialogFragment.dismiss();
                    }

                    RxBusResultModel rxBusResultModel = new RxBusResultModel();
                    rxBusResultModel.setTag(LanguageRecyclerAdapter.class.getSimpleName());
                    rxBusResultModel.setLanguageID(String.valueOf(language.getId()));
                    rxBusResultModel.setLanguage(language.getShortCode());
                    RxBus.get().post(rxBusResultModel);
                } else {
                    if (languageDialogFragment != null) {
                        languageDialogFragment.dismiss();
                    }
                    if (showCheckedLanguage) {

                        Utility.showToast(mContext, language_already_selected);
                    } else {

                        RxBusResultModel rxBusResultModel = new RxBusResultModel();
                        rxBusResultModel.setTag(LanguageRecyclerAdapter.class.getSimpleName());
                        rxBusResultModel.setLanguage(language.getShortCode());
                        rxBusResultModel.setLanguageID(String.valueOf(language.getId()));

                        RxBus.get().post(rxBusResultModel);

                    }
                }

            } else {
                if (languageDialogFragment != null) {
                    languageDialogFragment.dismiss();
                }

                Utility.showToast(mContext, no_internet);


            }


        });

        holder.tvLanguageName.setOnClickListener(v -> {
            holder.llContainer.performClick();
        });
    }

    @Override
    public int getItemCount() {
        return languageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_language_name)
        TextView tvLanguageName;

        @BindView(R.id.ll_container)
        LinearLayout llContainer;

        @BindView(R.id.iv_selected)
        ImageView ivSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

