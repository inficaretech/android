package com.inficare.membershipdemo.backgroundtask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;


public class UploadMemberImageTask extends AsyncTask<String, Void, String> {

	Context mContext;

	private String path;

	private String mId;

	private String fileName;

	ProgressDialog pDialog;

	ViewAnimator animator;

	ImageView image;
	MuseumDemoPreference preferences;
	int clickedMember;
	Configuration config;

	public UploadMemberImageTask(Context context, String path, String mId,
			String fileName, ViewAnimator animator, ImageView image) {
		this.mContext = context;
		this.path = path;
		this.mId = mId;
		this.fileName = fileName;
		this.animator = animator;
		this.image = image;
		pDialog = new ProgressDialog(mContext);
		pDialog.setMessage("PleaseWait..");
		pDialog.setIndeterminate(true);
		pDialog.setCancelable(false);

		preferences=MuseumDemoPreference.getInstance(mContext);
		clickedMember=preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER,-1);
		List<Configuration> configurationList= MemberParser.parsConfig(mContext);
		config=configurationList.get(clickedMember);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog.show();
	}

	@Override
	protected String doInBackground(String... params) {

		String link = Constants.MEMBER_URL_UPLOAD_IMAGE + "ConstituentID="
				+ mId + "&FileName=" + fileName+"&"+Constants.ORGANISATION_KEY+"="+config.getMid();
		String imageUrl = null;
		URL url = null;
		HttpURLConnection urlConnection = null;
		DataOutputStream outputStream = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		FileInputStream fileInputStream = null;
		int statusCode = 0;
		// create connection
		try {
			fileInputStream = new FileInputStream(new File(path));
			url = new URL(link);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			urlConnection = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// handle POST parameters
		// if (urlParameters != null) {
		urlConnection.setDoOutput(true);
		try {
			urlConnection.setUseCaches(false);
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Connection", "Keep-Alive");
			urlConnection.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			outputStream = new DataOutputStream(urlConnection.getOutputStream());
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream
					.writeBytes("Content-Disposition: form-data; name=\"ProfilePicture\";filename=\""
							+ fileName + "\"" + lineEnd);
			outputStream.writeBytes(lineEnd);
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];
			// Read file
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while (bytesRead > 0) {
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}
			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens
					+ lineEnd);
			// Responses from the server (code and message)
			@SuppressWarnings("unused")
			int serverResponseCode = urlConnection.getResponseCode();
			statusCode = serverResponseCode;
			String serverResponseMessage = urlConnection.getResponseMessage();
			fileInputStream.close();
			outputStream.flush();
			outputStream.close();
			
			if(statusCode==200){
				 //Get Response	
			      InputStream is = urlConnection.getInputStream();
			      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			      String line;
			      StringBuffer response = new StringBuffer(); 
			      while((line = rd.readLine()) != null) {
			        response.append(line);
			        response.append('\r');
			      }
			      rd.close();
			     imageUrl= response.toString();
			}else{
				imageUrl=Constants.timeoutException;
			}

		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return imageUrl;

		//
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		pDialog.dismiss();

		if (result != null) {
			if (result.equals(Constants.timeoutException)) {
//				TagInfo info = GetTagData.getTag(mContext);
//				Utility.showAlertDialog(mContext, info.getConnectionTimout());
				Utility.showAlertDialog(mContext,Constants.timeoutException);
			} else {
				result = parseUploadResponse(result);

				if (result.equals("-1")) {
					Utility.showAlertDialog(mContext, "Error updating");
				} else {
					boolean flag = false;
					if (!result.equals("")) {
						flag = DataProvider.getInstance(mContext)
								.updateMemberProfImage(
										DBController.createInstance(mContext,
												true), mId, result);
					}

					if (!flag) {
						Utility.showAlertDialog(mContext, "Error updating");
					} else {
						if (result.equals("")) {
							Utility.showAlertDialog(mContext, "Error updating");
							// image.setImageResource(R.drawable.ic_launcher);
						} else {
							Toast.makeText(mContext,
									"Your profile picture was changed", Toast.LENGTH_LONG)
									.show();
							image.setImageURI(Uri.parse(path));
							// Utility.showImageFromUrl(path, image, animator);
						}

					}
				}
			}
		} else {
			Utility.showAlertDialog(mContext, "Error updating");
			// image.setImageResource(R.drawable.ic_launcher);
		}

	}

	private String parseUploadResponse(String json) {
		String url = null;
		try {
			JSONObject obj = new JSONObject(json);
			url = obj.getString("FilePath");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return url;

	}
}
