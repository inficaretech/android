package com.inficare.membershipdemo.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyReciprocalResponse {
    @SerializedName("Success")
    @Expose
    private String success;
    @SerializedName("Result")
    @Expose
    private List<Reciprocal> orgReciprocals = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Reciprocal> getOrgReciprocals() {
        return orgReciprocals;
    }

    public MyReciprocalResponse setOrgReciprocals(List<Reciprocal> orgReciprocals) {
        this.orgReciprocals = orgReciprocals;
        return this;
    }
}
