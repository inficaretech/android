package com.inficare.membershipdemo.interfaces;

import java.io.FileNotFoundException;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ViewAnimator;

import com.edmodo.cropper.CropImageView;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.backgroundtask.UploadMemberImageTask;
import com.inficare.membershipdemo.methods.Utility;

public class ImageChangeListner implements OnClickListener {
	Context context;
	String pathReal;
	String cId;
	String fileName;
	ViewAnimator animator;
	ImageView image;
	CropImageView imageCropped;
	Dialog dialog;

	public ImageChangeListner(Dialog dialog, Context context, String path,
			String cId, String fileName, ViewAnimator animator,
			ImageView image, CropImageView imageCropped) {
		this.context = context;
		this.pathReal = path;
		this.cId = cId;
		this.fileName = fileName;
		this.animator = animator;
		this.image = image;
		this.imageCropped = imageCropped;
		this.dialog = dialog;

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.crop_button:
			dialog.dismiss();
			Bitmap bmp = null;
			while (bmp == null) {

				bmp = imageCropped.getCroppedImage();
				Log.e("crop image ..", bmp + "");

			}

			String path = Utility.saveCroppedImage(context, bmp);
			if (Utility.isNetworkAvailable(context)) {
				new UploadMemberImageTask(context, path, cId,
						Utility.getFilename(path), animator, image).execute();
			} else {
//				TagInfo info = GetTagData.getTag(context);
//				Utility.showAlertDialog(context, info.getAlertNoInternet());
				Utility.showAlertDialog(context,context.getResources().getString(R.string.msg_no_internet));
			}

			break;

		case R.id.use_original_button:
			dialog.dismiss();

			if (Utility.isNetworkAvailable(context)) {
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				Bitmap bitmap = null;
				try {
					bitmap = BitmapFactory.decodeStream(
							context.getContentResolver().openInputStream(
									Utility.mImageCaptureUri), null, options);

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Matrix matrix = new Matrix();
				matrix.postRotate(Utility.getImageOrientation(pathReal));
				Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
						bitmap.getWidth(), bitmap.getHeight(), matrix, true);
				String path_camera = Utility.saveCroppedImage(context, rotatedBitmap);
				new UploadMemberImageTask(context, path_camera, cId,
						Utility.getFilename(path_camera), animator, image)
						.execute();
			} else {
//				TagInfo info = GetTagData.getTag(context);
//				Utility.showAlertDialog(context, info.getAlertNoInternet());
				Utility.showAlertDialog(context,context.getResources().getString(R.string.msg_no_internet));
			}

			break;

		default:
			break;
		}

	}

}
