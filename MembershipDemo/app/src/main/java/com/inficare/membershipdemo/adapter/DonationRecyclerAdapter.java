package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hwangjr.rxbus.RxBus;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.pojo.RxBusResultModel;
import com.inficare.membershipdemo.views.DonationDailog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pratyush on 3/12/2018.
 */

public class DonationRecyclerAdapter extends RecyclerView.Adapter<DonationRecyclerAdapter.ViewHolder> {


    private Context mContext;
    private List<String> amountList;
    private int color;
    private int row_index = -1;
    DonationDailog donationDailog;
    public DonationRecyclerAdapter(Context mContext, List<String> amountList, int color,DonationDailog donationDailog) {
        this.mContext = mContext;
        this.amountList = amountList;
        this.color = color;
        this.donationDailog=donationDailog;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.donation_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvAmount.setText("$"+amountList.get(position));


        holder.tvAmount.setOnClickListener(v -> {
//            holder.tvAmount.setBackgroundColor(color);
//            notifyItemChanged(position);
            row_index = position;
            notifyDataSetChanged();
            if(donationDailog!=null)
            donationDailog.dismiss();
            RxBusResultModel rxBusResultModel = new RxBusResultModel();
            rxBusResultModel.setTag(DonationRecyclerAdapter.class.getSimpleName());
            rxBusResultModel.setAmount(amountList.get(position));
            RxBus.get().post(rxBusResultModel);

        });

        if (row_index == position) {
            holder.tvAmount.setBackgroundColor(color);
            holder.tvAmount.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        } else {
            holder.tvAmount.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            holder.tvAmount.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }
    }

    public void swapData(List<String> amountList){
        this.amountList.clear();
        if (this.amountList != null) {
            this.amountList.addAll(amountList);
        }
        notifyDataSetChanged();
    }

    public void setRow_index(int row_index) {
        this.row_index = row_index;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return amountList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_amount)
        TextView tvAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
    public void onDonate(){
        if(donationDailog!=null)
            donationDailog.dismiss();
        RxBusResultModel rxBusResultModel = new RxBusResultModel();
        rxBusResultModel.setTag(DonationRecyclerAdapter.class.getSimpleName());
        rxBusResultModel.setAmount(amountList.get(row_index));
        RxBus.get().post(rxBusResultModel);
    }
}

