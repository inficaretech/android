package com.inficare.membershipdemo.pojo;

public class NotifInfo {

	private String Notifications;

	private String Datetime;

	private int LanguageId;

	public String getNotifications() {
		return Notifications;
	}

	public void setNotifications(String notifications) {
		Notifications = notifications;
	}

	public String getDatetime() {
		return Datetime;
	}

	public void setDatetime(String datetime) {
		Datetime = datetime;
	}

	public int getLanguageId() {
		return LanguageId;
	}

	public void setLanguageId(int languageId) {
		LanguageId = languageId;
	}

}
