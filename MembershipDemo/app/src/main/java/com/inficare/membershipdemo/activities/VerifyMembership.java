package com.inficare.membershipdemo.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.TicketDetailsAdapter;
import com.inficare.membershipdemo.backgroundtask.MemberTaskDownload;
import com.inficare.membershipdemo.databasecreation.CBO;
import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.membership.MemberInfo;
import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.DeviceDetails;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.inficare.membershipdemo.activities.MembershipOptions.SEARCH_MODE_EXPIRED;

public class VerifyMembership extends AppCompatActivity implements View.OnClickListener {
    private ImageView imageMore;
    private LinearLayout imageBack, downloadlayout;
    private Context mContext;
    private TextView downloadCard;
    private LinearLayout innerLayout;
    private FrameLayout upperLayout;
    private ListView listMemberDetails;
    private TicketDetailsAdapter adapter;
    private TextView infoEmail, infomsg, infoForHelp, infoPhone, infoEmailId;
    private ArrayList<MemberInfo> memberInfoList;
    private View toggleView;
    Map<String, String> actualDetatils;
    boolean flagClear = false;
    public static Activity self;
    private ImageView toggleImage;
    private TextView textInfo;
    LinearLayout llEmail;
    private Button btnRenew;
    private Toolbar toolbar;
    int clickedMember;
    List<Configuration> configList;
    Configuration configuration;
    ImageView ivDownload;
    MuseumDemoPreference preferences;
    public static boolean isSavedRecently;
    boolean isBlocked;
    private boolean isInstructionShown;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private TextView tb_title;
    private String cards_successfully_downloaded, downloading_error, no_match_found_message,
            more_than_one_found_message, name, membership_type, zip_code, expire_on;


    public static final String TAG = FAQsActivity.class.getSimpleName();
    private String no_internet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_membership);

        self = this;
        mContext = this;
        memberInfoList = (ArrayList<MemberInfo>) getIntent().getSerializableExtra(Constants.MEMBER_DATA);
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        configuration = configList.get(clickedMember);
        initLanguage();
        findViews();

        //check block or reset
        List<DeviceDetails> deviceDetailsList = configuration.getDeviceDetailsList();
        if (deviceDetailsList != null) {
            for (int i = 0; i < deviceDetailsList.size(); i++) {
                DeviceDetails deviceDetails = deviceDetailsList.get(i);
                if (deviceDetails.getDeviceToken().equalsIgnoreCase(Utility.getDeviceId(this))) {
                    if (deviceDetails.isBlock()) {
                        isBlocked = true;
                    }
                }
            }
        }

        setUi(language);
        setData();
    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);


        try {
            tb_title.setText(languageMap.get(LanguageConstants.VERIFY_MEMBERSHIP_CARD).getString(title));
            downloadCard.setText(languageMap.get(LanguageConstants.DOWNLOAD_MY_CARDS).getString(title));
            infomsg.setText(languageMap.get(LanguageConstants.EXPIRE_DIALOG_MESSAGE).getString(title));
            infoForHelp.setText(languageMap.get(LanguageConstants.FOR_HELP).getString(title));
            infoEmail.setText(languageMap.get(LanguageConstants.EMAIL).getString(title) + " : ");
            infoPhone.setText(languageMap.get(LanguageConstants.OR).getString(title) + " " +
                    languageMap.get(LanguageConstants.CALL_US_ON).getString(title) + " : " +
                    configList.get(clickedMember).getContactNumber()+getConExt());

            btnRenew.setText(languageMap.get(LanguageConstants.RENEW).getString(title));
            cards_successfully_downloaded = languageMap.get(LanguageConstants.CARD_SUCCESSFULLY_DOWNLOADED).getString(title);
            downloading_error = languageMap.get(LanguageConstants.ERROR_DOWNLOADING).getString(title);

            no_match_found_message = languageMap.get(LanguageConstants.NO_MATCH_FOUND_MESSAGE).getString(title);
            more_than_one_found_message = languageMap.get(LanguageConstants.MORE_THAN_ONE_FOUND_MESSAGE).getString(title);

            name = languageMap.get(LanguageConstants.NAME).getString(title);
            membership_type = languageMap.get(LanguageConstants.MEMBERSHIP_TYPE).getString(title);
            zip_code = languageMap.get(LanguageConstants.ZIP_CODE).getString(title);
            expire_on = languageMap.get(LanguageConstants.EXPIRE_ON).getString(title);
            no_internet = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);
            String email = configuration.getEmail();
            if (email != null && email.length() > 5) {
                infoEmailId.setText(email);
            } else {
                llEmail.setVisibility(View.GONE);
                infoPhone.setText(languageMap.get(LanguageConstants.CALL).getString(title) + " : " +
                        configList.get(clickedMember).getContactNumber()+getConExt());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void findViews() {
        downloadCard = (TextView) findViewById(R.id.download_card);
        //goBackLayout = (LinearLayout) findViewById(R.id.goback_layout);
        downloadlayout = (LinearLayout) findViewById(R.id.download_layout);
        innerLayout = (LinearLayout) findViewById(R.id.innerLayout);
        upperLayout = (FrameLayout) findViewById(R.id.upperLayout);
        listMemberDetails = (ListView) findViewById(R.id.member_detail_list);
        infoEmail = (TextView) findViewById(R.id.info_email);
        infomsg = (TextView) findViewById(R.id.info_msg);
        infoForHelp = (TextView) findViewById(R.id.info_forhelp);
        infoPhone = (TextView) findViewById(R.id.info_phone_no);
        infoEmailId = (TextView) findViewById(R.id.info_email_id);
        toggleView = (View) findViewById(R.id.info_msg_view);
        toggleImage = (ImageView) findViewById(R.id.toogle_image);
        textInfo = (TextView) findViewById(R.id.info_link);
        llEmail = (LinearLayout) findViewById(R.id.llEmail);
        btnRenew = findViewById(R.id.btnRenew);
        //ivBack= (ImageView) findViewById(R.id.ivBack);
        ivDownload = (ImageView) findViewById(R.id.ivDownload);
        //ivBack.setColorFilter(Color.parseColor(configuration.getColorCode()), PorterDuff.Mode.SRC_ATOP);
        ivDownload.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        //goBack.setTextColor(Color.parseColor(configuration.getColorCode()));
        ///downloadCard.setTextColor(Color.parseColor(configuration.getColorCode()));
        Utility.changeDrawableColor(this, downloadlayout, configuration.getColorCode());
        Utility.changeDrawableColor(this, btnRenew, configuration.getColorCode());


        downloadlayout.setOnClickListener(this);
        infoPhone.setOnClickListener(this);
        infoEmailId.setOnClickListener(this);
//        View v = findViewById(R.id.camerainclude);
//        Utility.setHeaderLayout(mContext, v);

        findToolbar();
    }

    private void findToolbar() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        tb_title.setSelected(true);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        View tvTopStrip = findViewById(R.id.tvTopStrip);
        tvTopStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        toolbar.setNavigationIcon(upArrow);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setData() {
//        header.setText("Verify Membership Card");
        if (memberInfoList != null) {

            if (memberInfoList.size() == 1) {
                boolean status = Utility.isExpiredCard(memberInfoList.get(0).getValidThroughTimeSpan());
                //Utility.isExpiredCard(memberInfoList.get(0).getValidThroughTimeSpan())
                if (!memberInfoList.get(0).getIsExpire().equals("1")) {
                    listMemberDetails.setVisibility(View.VISIBLE);
                    toggleView.setVisibility(View.GONE);
                    ArrayList<String> data = setMemberData(memberInfoList.get(0));
                    adapter = new TicketDetailsAdapter(mContext, R.layout.inflate_member_details,
                            data, actualDetatils, language, languageMap);
                    listMemberDetails.setAdapter(adapter);
                    // Utility.getListViewSize(listMemberDetails);
                } else {
                    flagClear = true;
                    setResult();
                    if(configuration.getRenewLink()!=null&&!configuration.getRenewLink().isEmpty()){
                        btnRenew.setVisibility(View.VISIBLE);
                        btnRenew.setOnClickListener(this);
                    }

                   /* if(configuration.isTransactionRenew())
                    {
                        btnRenew.setVisibility(View.VISIBLE);
                        btnRenew.setOnClickListener(this);
                    } else {
                        btnRenew.setVisibility(View.GONE);
                    }*/

                    //infomsg.setText("The membership is expired and expired cards can not be downloaded.");
                    infomsg.setText(memberInfoList.get(0).getExpiryMessageText());
                    textInfo.setVisibility(View.VISIBLE);
                    toggleImage.setImageResource(R.drawable.expire);
                    String html = "<linearLayout href=\"http://tickets.museumanywhere.org/Public/load_screen.asp?screen-RenewMembership\">http://tickets.museumanywhere.org/Public/load_screen.asp?screen-RenewMembership</linearLayout>";
                    textInfo.setMovementMethod(LinkMovementMethod.getInstance());
                    /*textInfo.setText(Html.fromHtml(html));*/
                    textInfo.setText("");
                    toggleView.setVisibility(View.VISIBLE);
                    listMemberDetails.setVisibility(View.GONE);
                    downloadlayout.setVisibility(View.INVISIBLE);
                }
            } else if (memberInfoList.size() > 1) {
                flagClear = true;
                setResult();
                infomsg.setText(more_than_one_found_message);
                toggleView.setVisibility(View.VISIBLE);
                listMemberDetails.setVisibility(View.GONE);
                downloadlayout.setVisibility(View.INVISIBLE);

            } else if (memberInfoList.size() == 0) {
                flagClear = true;
                setResult();
                //"No match found please check info entered and try again"
                infomsg.setText(no_match_found_message);
                toggleImage.setImageResource(R.drawable.error);
                toggleView.setVisibility(View.VISIBLE);
                listMemberDetails.setVisibility(View.GONE);
                downloadlayout.setVisibility(View.INVISIBLE);
            }

        } else {
            flagClear = true;
            setResult();
            infomsg.setText(no_match_found_message);
            toggleView.setVisibility(View.VISIBLE);
            listMemberDetails.setVisibility(View.GONE);
            downloadlayout.setVisibility(View.INVISIBLE);
        }
    }

    private ArrayList<String> setMemberData(MemberInfo info) {
        ArrayList<String> listData = new ArrayList<String>();
        listData.add(name);
        //listData.add(configuration.getSearchType()+":");
        listData.add(info.getSearchLabel());
        listData.add(membership_type);
        listData.add(zip_code);

        String date = Utility.convertTimestampToDate(Utility
                .getDateToTimestamp(info.getValidThroughTimeSpan()));
        listData.add(expire_on);

        actualDetatils = new HashMap<String, String>();

        if (info.getSpouseList().size() > 0) {

            StringBuilder sb = new StringBuilder();
            sb.append(info.getFirstName()).append(" " + info.getLastName());
            for (int i = 0; i < info.getSpouseList().size(); i++) {
                sb.append("\n")
                        .append(info.getSpouseList().get(i).getFirstName())
                        .append(" ")
                        .append(info.getSpouseList().get(i).getLastName());
            }
            actualDetatils.put(name, sb.toString());

        } else {
            actualDetatils.put(name,
                    info.getFirstName() + " " + info.getLastName());
        }

        if (info.getsearchType().equals("1")) {
            actualDetatils.put(info.getSearchLabel(), info.getMembershipID());
        } else if (info.getsearchType().equals("2")) {
            actualDetatils.put(info.getSearchLabel(), info.getConstituentID());
        } else if (info.getsearchType().equals("3")) {
            actualDetatils.put(info.getSearchLabel(), info.getNumber());
        }

        if (info.getMembershipleveltype().equals("4529")) {
            actualDetatils.put(membership_type, info.getMembershiplevel()
                    + "/" + "40 Below");
        } else {

            actualDetatils.put(membership_type, info.getMembershiplevel());
        }

        actualDetatils.put(zip_code, info.getZip());
        actualDetatils.put(expire_on, date);
        return listData;

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.llback:
//                onBackPressed();
//                break;
          /*  case R.id.goback_layout:
                // Intent intent = new Intent(mContext, FindMembershipCard.class);
                // startActivity(intent);
                onBackPressed();
                break;*/
            case R.id.info_email_id:

              /*  Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                emailIntent.setType("vnd.android.cursor.item/email");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                        new String[]{"membership@museumanywhere.org"});

                startActivity(Intent.createChooser(emailIntent,
                        "Send mail using..."));*/
                break;

            case R.id.download_layout:

               /* DownloadCardTask card=new DownloadCardTask(this) {
                    @Override
                    public void onTaskFinish(String response) {
                        try {
                            DownloadCard downloadCard=new DownloadCard(response);
                            if(downloadCard.getSuccess()){
                                saveCard();
                            }else {
                                Toast.makeText(VerifyMembership.this,downloadCard.getResult(),Toast.LENGTH_LONG).show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        }

                };
                if(Utility.isNetworkAvailable(this))
                card.execute(Constants.MEMBER_URL_DOWNLOAD_CARD.replace("{mid}",memberInfoList.get(0).getMembershipID()).replace("{organisation_key}",configuration.getMid()));
                else
                Toast.makeText(this,getResources().getString(R.string.msg_no_internet),Toast.LENGTH_LONG).show();*/
                //saveCard();
                if (isBlocked) {
                    Utility.showAlertDialog2(mContext, configuration.getBlock_message(), configuration.getMid(), languageMap);
                    return;
                }
                downloadCardSendToServer();

                break;
            case R.id.info_phone_no:
                Intent in = new Intent(Intent.ACTION_VIEW);
                in.setData(Uri.parse("tel:" +configList.get(clickedMember).getContactNumber()+getConExt()));
                startActivity(in);
                break;
            case R.id.btnRenew:
                openWebPage(configuration.getRenewLink());
               /* Intent intent = new Intent(mContext, RenewMembershipActivity.class);
                intent.putExtra(SEARCH_MODE_EXPIRED, SEARCH_MODE_EXPIRED);
                intent.putExtra(Constants.MEMBER_DATA, memberInfoList);
                startActivity(intent);*/
                break;
            default:
                break;
        }
    }
    public void openWebPage(String url) {
        try {
            Uri webpage = Uri.parse(url);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
            mContext.startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext, "No application can handle this request. Please install a web browser or check your URL.",  Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    private boolean checkForExistance(ArrayList<MemberInfo> memberList) {
        boolean flag = false;
        ArrayList<MemberInfo> info = new ArrayList<>();
        info = CBO.FillCollection(
                DataProvider.getInstance(mContext).getMemberDetails(
                        DBController.createInstance(mContext, false), 0),
                MemberInfo.class);

        MemberList listMember = new MemberList();
        listMember.addAll(info);

        for (int i = 0; i < listMember.size(); i++) {
            if (memberList.get(0).getMembershipID()
                    .equals(listMember.get(i).getMembershipID())) {
                flag = true;
                break;
            }
        }

        return flag;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        FindMembershipCard.flagBack = true;
    }

    private void setResult() {
        Intent i = new Intent();
        i.putExtra(Constants.FLAG_CLEAR, flagClear);
        setResult(1, i);
    }

    public static class BroadcastVerify extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (self != null)
                self.finish();
        }

    }

    private void saveCard() {
        isSavedRecently = true;
        DataProvider.getInstance(mContext).DeleteMemberDetails(
                DBController.createInstance(mContext, true), 0, null);
        boolean insertMain = false,
                insertDec = false;
        for (int i = 0; i < memberInfoList.size(); i++) {
            for (int j = 0; j < memberInfoList.get(i).getSpouseList().size(); j++) {
                insertDec = false;
                insertDec = DataProvider.getInstance(mContext)
                        .insertMemberSpouseDetails(DBController.createInstance(mContext, true),
                                memberInfoList.get(i).getSpouseList().get(j));
            }
            insertMain = DataProvider.getInstance(mContext)
                    .insertMemberDetails(DBController.createInstance(mContext, true),
                            memberInfoList.get(i));

        }
        if (memberInfoList.get(0).getSpouseList().size() > 0) {
            if (insertDec && insertMain) {
                if (memberInfoList.get(0).getSpouseList().size() > 0) {
                    Toast.makeText(mContext,
                            cards_successfully_downloaded, Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(mContext,
                            cards_successfully_downloaded, Toast.LENGTH_LONG).show();

                }

                //Save data for favorite
                preferences.saveBoolean(configList.get(clickedMember).getName(), true);

                Intent intent = new Intent(mContext, MemberShipActivity.class);
                intent.putExtra(Constants.FLAG_STACK_CLEAR, true);
               /* if(!memberInfoList.get(0).getExpiryMessageText().equalsIgnoreCase("-1")){
                    intent.putExtra(MembershipOptions.EXPIRY_MSG,memberInfoList.get(0).getExpiryMessageText());
                }*/

                startActivity(intent);
            } else {
                Toast.makeText(mContext, downloading_error, Toast.LENGTH_LONG)
                        .show();
            }

        } else {
            if (insertMain) {
                if (memberInfoList.get(0).getSpouseList().size() > 0) {
                    Toast.makeText(mContext,
                            cards_successfully_downloaded, Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(mContext,
                            cards_successfully_downloaded, Toast.LENGTH_LONG).show();

                }

                //Save data for favorite
                preferences.saveBoolean(configList.get(clickedMember).getName(), true);

                Intent intent = new Intent(mContext, MemberShipActivity.class);
                intent.putExtra(Constants.FLAG_STACK_CLEAR, true);
               /* if(!memberInfoList.get(0).getExpiryMessageText().equalsIgnoreCase("-1")){
                    intent.putExtra(MembershipOptions.EXPIRY_MSG,memberInfoList.get(0).getExpiryMessageText());
                }*/
                startActivity(intent);


            } else {
                Toast.makeText(mContext, downloading_error, Toast.LENGTH_LONG)
                        .show();
            }
        }


    }

    /**
     * send to server
     */
    private void downloadCardSendToServer() {
        if (memberInfoList != null && memberInfoList.size() > 0) {
            String memberId = "";
            if (memberInfoList.get(0).getsearchType().equals("1")) {
                memberId = memberInfoList.get(0).getMembershipID();
            } else if (memberInfoList.get(0).getsearchType().equals("2")) {
                memberId = memberInfoList.get(0).getConstituentID();
            } else if (memberInfoList.get(0).getsearchType().equals("3")) {
                memberId = memberInfoList.get(0).getNumber();
            }

            if (Utility.isNetworkAvailable(mContext)) {
                Map<String, Object> map = new LinkedHashMap<>();
                map.put("mId", memberId);
                map.put("lName", memberInfoList.get(0).getLastName());
                map.put("organisation_key", configList.get(clickedMember).getMid());
                map.put("udID", Utility.getDeviceToken(mContext));

                map.put("deviceType", "2");
                map.put("deviceName", Utility.getDeviceName());
                map.put("uniqueDeviceID", Utility.getDeviceId(this));
                MemberTaskDownload taskDownload = new MemberTaskDownload(mContext, map) {
                    @Override
                    public void onTaskFinish(String data) {
                        try {
                            JSONObject object = new JSONObject(data);
                            String errorCode = object.getString("ErrorCode");
                            if (errorCode.equalsIgnoreCase("200")) {
                                //
                                downloadImage();
                                saveCard();
                            } else {
                                Utility.showAlertDialog2(mContext, object.getString("Message"), configuration.getMid(), languageMap);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                };

                taskDownload.execute(Constants.DOWNLOAD_CARD_URL);
            } else {
//                        TagInfo infoTag = GetTagData.getTag(mContext);
//                        Utility.showAlertDialog(mContext,
//                                infoTag.getAlertNoInternet());
                Utility.showAlertDialog2(mContext, no_internet, configuration.getMid(), languageMap);
            }
        }

    }

    private void downloadImage() {
        try {
            if (memberInfoList == null) {
                return;
            }
            for (int i = 0; i < memberInfoList.size(); i++) {
                String roamLogoPath = memberInfoList.get(i).getRoamLogoPath();
                String profile_pic = memberInfoList.get(i).getProfileImage();
                String cardUrl = memberInfoList.get(i).getCardBanner();//configuration.getImagePath()+"/"+configuration.getCardBannerImage();

                ImageLoader.getInstance().loadImage(roamLogoPath, null);
                ImageLoader.getInstance().loadImage(profile_pic, null);
                ImageLoader.getInstance().loadImage(cardUrl, null);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private String getConExt(){
        String conExten=configList.get(clickedMember).getContact_ext();
        if(conExten!=null&&!conExten.isEmpty())
        {
            conExten=";"+conExten;
        }else {
            conExten="";
        }
        return conExten;
    }
}
