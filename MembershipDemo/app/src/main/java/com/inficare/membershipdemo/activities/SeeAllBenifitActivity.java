package com.inficare.membershipdemo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.databasecreation.CBO;
import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.Model;
import com.inficare.membershipdemo.pojo.Reciprocal;
import com.inficare.membershipdemo.pojo.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SeeAllBenifitActivity extends AppCompatActivity {
    @BindView(R.id.myWebview)
    WebView myWebview;
    private Context mContext;

    //Toolbar elements
    private Toolbar toolbar;
    private TextView tb_title;
    View topStrip;
    //UI Elements
    private ScrollView sv_container;
    private ProgressDialog dialog;
    int clickedMember;
    List<Configuration> configList;
    private boolean progressVisible = true;
    private String memberID = "all";
    private String organisationID = "";
    LinearLayout linearLayout;
    MuseumDemoPreference preferences;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    public static final String TAG = SeeAllBenifitActivity.class.getSimpleName();
    private String pleaseWait, somethingWentWrong, msg_no_internet;
    private String membership_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all_benifit);
        ButterKnife.bind(this);
        mContext = this;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        setToolbarLayout();
        initLanguage();
        setUi(language);
        findViews();

        myWebview.setVisibility(View.GONE);
        organisationID = configList.get(clickedMember).getMid();

        List<Result> results = getMembershipDetails();
        Log.d("results", results.size() + "");
        if (results.size() > 0) {
            setDataInUI(results);
            //setDataInWebview(results);
            progressVisible = false;
        } else {

        }

        if (Utility.isNetworkAvailable(mContext)) {

            getAllBenefitRetrofitAPI(progressVisible);
        } else {
            Utility.showToast(mContext, msg_no_internet);
        }

    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);

        try {
            tb_title.setText(languageMap.get(LanguageConstants.MEMBERSHIP_BENEFITS).getString(title));
            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            somethingWentWrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);
            membership_type = languageMap.get(LanguageConstants.MEMBERSHIP_TYPE).getString(title);
            msg_no_internet = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void findViews() {
        sv_container = (ScrollView) findViewById(R.id.sv_container);
        topStrip = findViewById(R.id.topStrip);
//        topStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode
// ()));
        topStrip.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
        dialog = Utility.showProgressDialog(mContext, pleaseWait);
    }

    private void setToolbarLayout() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_button));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getAllBenefitRetrofitAPI(final boolean progress) {
//        try {
//            String value = URLEncoder.encode(Utility.myEncrypt(""), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        if (progress)
            dialog.show();
        Call<Model> modelCall = Utility.callRetrofit().getAllBenefitsCall(Utility.getHeaderData()
                , organisationID, Utility.getLanguage(language));
        modelCall.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                if (response.code() == 200) {

                    List<Result> list = response.body().getResult();
                    Gson gson = new Gson();
                    String jsonData = gson.toJson(list);
                    preferences.saveString(MuseumDemoPreference.ALL_BENEFITS + organisationID, jsonData);

                    if (progress) {
                        for (Result data : list) {
                            data.setOrganisationKey(configList.get(clickedMember).getMid());

                            boolean value = DataProvider.getInstance(mContext).insertMemberBenefits
                                    (DBController.createInstance(mContext, true), data);
                            Log.d("value", value + "");
                        }
                        //setDataInUI(list);
                    } else {
                        //replace with new data
                        if (list != null && list.size() > 0) {
                            Result myData = list.get(0);
                            myData.setOrganisationKey(configList.get(clickedMember).getMid());
                            DataProvider.getInstance(mContext)
                                    .deleteMemberBenefits(DBController.createInstance
                                            (mContext, true), myData);
                        }

                        for (Result data : list) {
                            data.setOrganisationKey(configList.get(clickedMember).getMid());
                           /* boolean value = DataProvider.getInstance(mContext)
                                    .updateMembershipBenefits(DBController.createInstance
                                            (mContext, true), data);*/
                            boolean value = DataProvider.getInstance(mContext).insertMemberBenefits
                                    (DBController.createInstance(mContext, true), data);
                            Log.d("value", value + "");
                        }
                    }

                    if (list != null && list.size() > 0) {
                        List<Result> results = getMembershipDetails();
                        if (results.size() > 0) {
                            if (progress)
                                setDataInUI(results);
                            ///setDataInWebview(results);
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        });


    }

    private void setDataInWebview(List<Result> list) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < list.size(); i++) {

           /* String data="<html>\n" +
                    "\n" +
                    "<body >\n" +
                    "replaceMe\n" +
                    "<div onclick=\"alert('hello')\"> <img src=\"file:///android_asset/reciprocal_con.png\">  </div>\n" +
                    "\n" +
                    "</body>\n" +
                    "\n" +
                    "</html>";*/
            String startHtml = "<html>\n" +
                    "\n" +
                    "<body >\n";
            String endHtml = "</body>\n" +
                    "</html>";

            builder.append(startHtml);
            String membershipType = "";
            membershipType = list.get(i).getMembershipleveltype();
            //String sTitle="<p>Membership Type: " + membershipType + " " + list.get(i).getPrice()+"<img src=\"file:///android_asset/reciprocal_con.png\"></p>";
            String sTitle = "<p>Membership Type: " + membershipType + " " + list.get(i).getPrice() + "</p>";

            String button = "<div onclick=\"alert('hello')\"> <img src=\"file:///android_asset/reciprocal_con.png\"></div>";
            builder.append(sTitle);
            builder.append(button);
            String benefits = list.get(i).getBenefits();
            builder.append(benefits);


            //Other Benefits Title

            if (list.get(i).getOtherBenefits() != null) {
                if (!list.get(i).getOtherBenefits().isEmpty()) {

                    String otherBenefitsUpSell = list.get(i).getUpselltext();
                    builder.append(otherBenefitsUpSell);

                    String otherBenefits = list.get(i).getOtherBenefits();
                    builder.append(otherBenefits);
                }
            }

            WebSettings webSettings = myWebview.getSettings();
            webSettings.setJavaScriptEnabled(true);


            builder.append(endHtml);
            //myWebview.loadData(data, "text/html; charset=utf-8", "UTF-8");

            myWebview.loadDataWithBaseURL(null, builder.toString(), "text/html", "utf-8", null);
            //myWebview.loadUrl( builder.toString());
            myWebview.setWebChromeClient(new MyJavaScriptChromeClient());
        }

    }

    private class MyJavaScriptChromeClient extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
//handle Alert event, here we are showing AlertDialog
            /*new AlertDialog.Builder(WebViewApp.this)
                    .setTitle("JavaScript Alert !")
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok,
                            new AlertDialog.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do your stuff
                                    Toast.makeText(WebViewApp.this, "Hello", Toast.LENGTH_SHORT)
                                            .show();

                                    result.confirm();
                                }
                            }).setCancelable(false).create().show();*/

            Intent intent = new Intent(SeeAllBenifitActivity.this, ReciprocalActivity.class);
            startActivity(intent);

            return true;
        }
    }

    private void setDataInUI(List<Result> list) {

        if (this.linearLayout != null) {
            sv_container.removeAllViews();
        }

        LinearLayout a = new LinearLayout(this);
        this.linearLayout = a;
        a.setPadding(10, 5, 5, 10);
        a.setOrientation(LinearLayout.VERTICAL);


        for (int i = 0; i < list.size(); i++) {
            //Adding Membership Title
            TextView tv_membership_type = new TextView(mContext);
            String membershipType = "";
            membershipType = list.get(i).getMembershipleveltype();
//            tv_membership_type.setTextColor(Color.parseColor(configList.get(clickedMember)
//                    .getColorCode()));
            tv_membership_type.setTextColor(ContextCompat.getColor(mContext, R.color.black));

            tv_membership_type.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger
                    (R.integer.benefit_title_text_size_large));
            tv_membership_type.setTypeface(Typeface.DEFAULT_BOLD);
            // tv_membership_type.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            tv_membership_type.setText(membership_type + " : " + membershipType + " " + list.get(i)
                    .getPrice());
            RelativeLayout.LayoutParams paramsIv = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            paramsIv.addRule(RelativeLayout.ALIGN_PARENT_END);

            tv_membership_type.setPadding(0, 0, (int) Utility.convertDPtoPixel(mContext, 20), 0);

            ImageView iv = new ImageView(this);
            iv.setImageResource(R.drawable.reciprocal_con);
            //paramsIv.setMargins(Utility.convertPixelsToDp(10f,this),0,0,0);
            iv.setLayoutParams(paramsIv);
            iv.setClickable(true);
            iv.setColorFilter(Color.parseColor("#000000"));
            final int pos = i;
            List<Reciprocal> reciList = list.get(pos).getReciprocals();
            if (reciList != null && reciList.size() > 0) {
                iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SeeAllBenifitActivity.this, ReciprocalActivity.class);
                        List<Reciprocal> list1 = list.get(pos).getReciprocals();
                        intent.putExtra("reciprocal", (ArrayList<Reciprocal>) list1);
                        startActivity(intent);
                    }
                });
            } else {
                iv.setVisibility(View.GONE);
            }

            RelativeLayout parent = new RelativeLayout(this);
            RelativeLayout.LayoutParams lprams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            parent.setLayoutParams(lprams);


            /*LinearLayout parent = new LinearLayout(this);
            parent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            parent.setOrientation(LinearLayout.HORIZONTAL);*/
            parent.addView(tv_membership_type);
            parent.addView(iv);
            a.addView(parent);


            TextView tv_list_item = new TextView(mContext);
            /*if(list.get(i).getBenefits().contains("</")){
                tv_list_item.setText(Html.fromHtml(list.get(i).getBenefits()));
            }else {*/
            tv_list_item.setText(Html.fromHtml(list.get(i).getBenefits()));//
            // }

            tv_list_item.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger
                    (R.integer.benefit_title_text_size));
            tv_list_item.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            //tv_list_item.setAutoLinkMask(Linkify.ALL);
            Linkify.addLinks(tv_list_item, Linkify.ALL);
            tv_list_item.setLinkTextColor(ContextCompat.getColor(mContext, R.color.link_color));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout
                    .LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, (int) Utility.convertDPtoPixel(mContext, 10), 0, (int) Utility
                    .convertDPtoPixel(mContext, 10));
            tv_list_item.setLayoutParams(params);

            WebView webView = new WebView(mContext);
            webView.setLayoutParams(params);
            //webView.loadData(list.get(i).getBenefits(), "text/html; charset=utf-8", "UTF-8");
            webView.loadDataWithBaseURL("", list.get(i).getBenefits(), "text/html", "UTF-8", "");
            a.addView(webView);

            setWebviewPropertes(webView);


            //Other Benefits Title

            if (list.get(i).getOtherBenefits() != null) {
                if (!list.get(i).getOtherBenefits().isEmpty()) {

                    TextView tv_otherBenefitsTitle = new TextView(mContext);
                    String otherBenefitsTitle = "";
                    otherBenefitsTitle = list.get(i).getUpselltext();
//                    tv_otherBenefitsTitle.setTextColor(Color.parseColor(configList.get
//                            (clickedMember).getColorCode()));

                    tv_otherBenefitsTitle.setTextColor(ContextCompat.getColor(mContext, R.color
                            .black));

                    tv_otherBenefitsTitle.setText(Html.fromHtml(otherBenefitsTitle));//
                    tv_otherBenefitsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources()
                            .getInteger
                                    (R.integer.benefit_title_text_size));

                    View view = new View(mContext);
                    LinearLayout.LayoutParams params_other = new LinearLayout.LayoutParams
                            (LinearLayout.LayoutParams.WRAP_CONTENT, 4);
                    params_other.setMargins(0, (int) Utility.convertDPtoPixel(mContext, 5), 0, (int)
                            Utility.convertDPtoPixel(mContext, 5));
                    view.setLayoutParams(params_other);


//                    view.setBackgroundColor(Color.parseColor(configList.get(clickedMember)
//                            .getColorCode()));
                    view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));


                    WebView webViewOther = new WebView(mContext);
                    webViewOther.setLayoutParams(params);
                    //webViewOther.loadData(otherBenefitsTitle, "text/html; charset=utf-8", "UTF-8");
                    webViewOther.loadDataWithBaseURL("", otherBenefitsTitle, "text/html", "UTF-8", "");
                    a.addView(webViewOther);
                    setWebviewPropertes(webViewOther);
                    a.addView(view);

                    TextView tv_otherBenifitsList = new TextView(mContext);
                    tv_otherBenifitsList.setText(list.get(i).getOtherBenefits());
                    tv_otherBenifitsList.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources()
                            .getInteger
                                    (R.integer.benefit_title_text_size));
                    tv_otherBenifitsList.setTextColor(ContextCompat.getColor(mContext, R.color
                            .black));
                    Linkify.addLinks(tv_otherBenifitsList, Linkify.ALL);
                    tv_otherBenifitsList.setLinkTextColor(ContextCompat.getColor(mContext, R.color.link_color));

                    WebView webViewOtherList = new WebView(mContext);
                    webViewOtherList.setLayoutParams(params);

                    ///webViewOtherList.loadData(list.get(i).getOtherBenefits(), "text/html; charset=utf-8", "UTF-8");
                    webViewOtherList.loadDataWithBaseURL("", list.get(i).getOtherBenefits(), "text/html", "UTF-8", "");
                    a.addView(webViewOtherList);
                    setWebviewPropertes(webViewOtherList);
                }
            }

            View view = new View(mContext);
            LinearLayout.LayoutParams params_border = new LinearLayout.LayoutParams(LinearLayout
                    .LayoutParams.WRAP_CONTENT, 4);
            params_border.setMargins(0, (int) Utility.convertDPtoPixel(mContext, 5), 0, (int)
                    Utility.convertDPtoPixel(mContext, 5));
            view.setLayoutParams(params_border);
//            view.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode
// ()));
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
            a.addView(view);
        }
        sv_container.addView(a);
    }

    private List<Result> getMembershipDetails() {

        ArrayList<Result> data = CBO.FillCollection(DataProvider.getInstance(mContext)
                .getMembershipBenefits(DBController.createInstance(mContext, false),
                        memberID, organisationID), Result.class);
        Log.d("data.size", data.size() + "");

        Gson gson = new Gson();
        String jData = preferences.getString(MuseumDemoPreference.ALL_BENEFITS + organisationID, null);
        if (jData != null) {
            Result[] results = gson.fromJson(jData, Result[].class);
            List<Result> mdata = Arrays.asList(results);
            return mdata;
        }


        return data;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setWebviewPropertes(WebView webView) {
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= 19) {
// chromium, enable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Intent intent = new Intent(SeeAllBenifitActivity.this,MyWebViewActivity.class);
                intent.putExtra(MyWebViewActivity.LINK_URL,url);
                startActivity(intent);

                return true;
            }
        });
    }
}
