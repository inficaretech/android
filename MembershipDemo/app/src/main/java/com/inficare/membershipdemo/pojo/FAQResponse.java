package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 2/20/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FAQResponse {

    @SerializedName("faqs")
    @Expose
    private List<Faqs> faqs = null;
    @SerializedName("Success")
    @Expose
    private String success;

    public List<Faqs> getFaqs() {
        return faqs;
    }

    public void setFaqs(List<Faqs> faqs) {
        this.faqs = faqs;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }


}