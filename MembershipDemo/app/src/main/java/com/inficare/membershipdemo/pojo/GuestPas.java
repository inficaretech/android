package com.inficare.membershipdemo.pojo;

/**
 * Created by Abhishek.jha on 10/17/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GuestPas implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("GuestPassID")
    @Expose
    private String guestPassID;
    @SerializedName("MembershipID")
    @Expose
    private String membershipID;
    @SerializedName("IsUsed")
    @Expose
    private String isUsed;
    @SerializedName("IsGifted")
    @Expose
    private Boolean isGifted;
    @SerializedName("IsGiftedPass")
    @Expose
    private Boolean isGiftedPass;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("ValidUpto")
    @Expose
    private String validUpto;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;
    @SerializedName("ModifiedOn")
    @Expose
    private String modifiedOn;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Discription")
    @Expose
    private String discription;
    @SerializedName("Header")
    @Expose
    private String header;
    @SerializedName("PrimaryName")
    @Expose
    private String primaryName;

    @SerializedName("UsedPurpose")
    @Expose
    private String UsedPurpose;


    private String reserveField1;
    private String reserveField2;
    private String genralAdmissionDesc;
    private String memberPresenceDesc;

    private boolean isChecked;
    /**
     * @return The iD
     */
    public String getID() {
        return iD;
    }

    /**
     * @param iD The ID
     */
    public void setID(String iD) {
        this.iD = iD;
    }

    /**
     * @return The guestPassID
     */
    public String getGuestPassID() {
        return guestPassID;
    }

    /**
     * @param guestPassID The GuestPassID
     */
    public void setGuestPassID(String guestPassID) {
        this.guestPassID = guestPassID;
    }

    /**
     * @return The membershipID
     */
    public String getMembershipID() {
        return membershipID;
    }

    /**
     * @param membershipID The MembershipID
     */
    public void setMembershipID(String membershipID) {
        this.membershipID = membershipID;
    }

    /**
     * @return The isUsed
     */
    public String getIsUsed() {
        return isUsed;
    }

    /**
     * @param isUsed The IsUsed
     */
    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public Boolean getGifted() {
        return isGifted;
    }

    public void setGifted(Boolean gifted) {
        isGifted = gifted;
    }

    public Boolean getGiftedPass() {
        return isGiftedPass;
    }

    public void setGiftedPass(Boolean giftedPass) {
        isGiftedPass = giftedPass;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The Address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The Email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone The Phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return The validUpto
     */
    public String getValidUpto() {
        return validUpto;
    }

    /**
     * @param validUpto The ValidUpto
     */
    public void setValidUpto(String validUpto) {
        this.validUpto = validUpto;
    }

    /**
     * @return The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn The CreatedOn
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return The modifiedOn
     */
    public String getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn The ModifiedOn
     */
    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The Status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * @return The primaryName
     */
    public String getPrimaryName() {
        return primaryName;
    }

    /**
     * @param primaryName The PrimaryName
     */
    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getReserveField1() {
        return reserveField1;
    }

    public void setReserveField1(String reserveField1) {
        this.reserveField1 = reserveField1;
    }

    public String getReserveField2() {
        return reserveField2;
    }

    public void setReserveField2(String reserveField2) {
        this.reserveField2 = reserveField2;
    }

    public String getGenralAdmissionDesc() {
        return genralAdmissionDesc;
    }

    public void setGenralAdmissionDesc(String genralAdmissionDesc) {
        this.genralAdmissionDesc = genralAdmissionDesc;
    }

    public String getMemberPresenceDesc() {
        return memberPresenceDesc;
    }

    public void setMemberPresenceDesc(String memberPresenceDesc) {
        this.memberPresenceDesc = memberPresenceDesc;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getUsedPurpose() {
        return UsedPurpose;
    }

    public GuestPas setUsedPurpose(String usedPurpose) {
        UsedPurpose = usedPurpose;
        return this;
    }
}

