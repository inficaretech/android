package com.inficare.membershipdemo.pojo;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.List;

/**
 * Created by avinash.verma on 4/25/2017.
 */

public class UserProfileDetails implements Serializable {
    private String firstName;
    private String lastname;
    private String email;
    private String contactNumber;
    private String Address;
    private String city;
    private String state;
    private String zipcode;
    private String membershipid;
    private String country;
    private String dob;
    private String message;
    private String donation_amount;
    private String membership_amount ;
    private String Transaction_amount ;
    private String haveDonation ;
    private List<ChildJson> childs ;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getMembershipid() {
        return membershipid;
    }

    public void setMembershipid(String membershipid) {
        this.membershipid = membershipid;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDonation_amount() {
        return donation_amount;
    }

    public void setDonation_amount(String donation_amount) {
        this.donation_amount = donation_amount;
    }

    public String getMembership_amount() {
        return membership_amount;
    }

    public void setMembership_amount(String membership_amount) {
        this.membership_amount = membership_amount;
    }

    public String getTransaction_amount() {
        return Transaction_amount;
    }

    public void setTransaction_amount(String transaction_amount) {
        Transaction_amount = transaction_amount;
    }

    public String getHaveDonation() {
        return haveDonation;
    }

    public void setHaveDonation(String haveDonation) {
        this.haveDonation = haveDonation;
    }

    public List<ChildJson> getChilds() {
        return childs;
    }

    public void setChilds(List<ChildJson> childs) {
        this.childs = childs;
    }
}
