package com.inficare.membershipdemo.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by avinash.verma on 6/27/2017.
 */

public class Reciprocal implements Parcelable,Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("Organisation_key")
    @Expose
    private String organisationKey;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;


    //
    @SerializedName("MembershipLevelID")
    @Expose
    private Integer membershipLevelID;
    @SerializedName("extrnal_link")
    @Expose
    private String extrnalLink;

    @SerializedName("short_title")
    @Expose
    private String shortTitle;
    @SerializedName("discription")
    @Expose
    private Object discription;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("image_absolute_path")
    @Expose
    private String imagePath;

    @SerializedName("is_active")
    @Expose
    private int isActive;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrganisationKey() {
        return organisationKey;
    }

    public void setOrganisationKey(String organisationKey) {
        this.organisationKey = organisationKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Reciprocal() {
    }

    public Integer getMembershipLevelID() {
        return membershipLevelID;
    }

    public void setMembershipLevelID(Integer membershipLevelID) {
        this.membershipLevelID = membershipLevelID;
    }

    public String getExtrnalLink() {
        return extrnalLink;
    }

    public void setExtrnalLink(String extrnalLink) {
        this.extrnalLink = extrnalLink;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public Object getDiscription() {
        return discription;
    }

    public void setDiscription(Object discription) {
        this.discription = discription;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getActive() {
        return isActive;
    }

    public void setActive(int active) {
        isActive = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.organisationKey);
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeValue(this.membershipLevelID);
        dest.writeString(this.extrnalLink);
        dest.writeString(this.shortTitle);

        dest.writeString(this.description);
        dest.writeString(this.imagePath);
        dest.writeInt(this.isActive);
    }

    protected Reciprocal(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.organisationKey = in.readString();
        this.title = in.readString();
        this.url = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.membershipLevelID = (Integer) in.readValue(Integer.class.getClassLoader());
        this.extrnalLink = in.readString();
        this.shortTitle = in.readString();
        this.description = in.readString();
        this.imagePath = in.readString();
        this.isActive = in.readInt();
    }

    public static final Creator<Reciprocal> CREATOR = new Creator<Reciprocal>() {
        @Override
        public Reciprocal createFromParcel(Parcel source) {
            return new Reciprocal(source);
        }

        @Override
        public Reciprocal[] newArray(int size) {
            return new Reciprocal[size];
        }
    };
}
