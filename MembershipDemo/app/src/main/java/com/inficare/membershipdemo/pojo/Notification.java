package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 10/5/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("DeviceType")
    @Expose
    private Object deviceType;
    @SerializedName("DeviceToken")
    @Expose
    private String deviceToken;
    @SerializedName("UniqueDeviceID")
    @Expose
    private String uniqueDeviceID;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("MembershipID")
    @Expose
    private String membershipID;
    @SerializedName("Membershiplevel")
    @Expose
    private String membershiplevel;
    @SerializedName("MembershiplevelID")
    @Expose
    private Object membershiplevelID;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public Object getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Object deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getUniqueDeviceID() {
        return uniqueDeviceID;
    }

    public void setUniqueDeviceID(String uniqueDeviceID) {
        this.uniqueDeviceID = uniqueDeviceID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMembershipID() {
        return membershipID;
    }

    public void setMembershipID(String membershipID) {
        this.membershipID = membershipID;
    }

    public String getMembershiplevel() {
        return membershiplevel;
    }

    public void setMembershiplevel(String membershiplevel) {
        this.membershiplevel = membershiplevel;
    }

    public Object getMembershiplevelID() {
        return membershiplevelID;
    }

    public void setMembershiplevelID(Object membershiplevelID) {
        this.membershiplevelID = membershiplevelID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

}