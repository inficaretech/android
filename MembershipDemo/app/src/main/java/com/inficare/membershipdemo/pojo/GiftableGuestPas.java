package com.inficare.membershipdemo.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by abhishek.kumar on 10/10/2017.
 */

public class GiftableGuestPas {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("GuestPassID")
    @Expose
    private String guestPassID;
    @SerializedName("MembershipID")
    @Expose
    private String membershipID;
    @SerializedName("IsUsed")
    @Expose
    private String isUsed;
    @SerializedName("IsGifted")
    @Expose
    private Boolean isGifted;
    @SerializedName("IsGiftedPass")
    @Expose
    private Boolean isGiftedPass;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("ValidUpto")
    @Expose
    private String validUpto;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;
    @SerializedName("ModifiedOn")
    @Expose
    private String modifiedOn;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Discription")
    @Expose
    private String discription;
    @SerializedName("Header")
    @Expose
    private String header;
    @SerializedName("PrimaryName")
    @Expose
    private String primaryName;
    //
    @SerializedName("ReserveField1")
    @Expose
    private String reserveField1;

    @SerializedName("ReserveField2")
    @Expose
    private String reserveField2;
    @SerializedName("GenralAdmissionDesc")
    @Expose
    private String genralAdmissionDesc;

    @SerializedName("MemberPresenceDesc")
    @Expose
    private String memberPresenceDesc;

    @SerializedName("UsedPurpose")
    @Expose
    private String UsedPurpose;


    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getGuestPassID() {
        return guestPassID;
    }

    public void setGuestPassID(String guestPassID) {
        this.guestPassID = guestPassID;
    }

    public String getMembershipID() {
        return membershipID;
    }

    public void setMembershipID(String membershipID) {
        this.membershipID = membershipID;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public Boolean getIsGifted() {
        return isGifted;
    }

    public void setIsGifted(Boolean isGifted) {
        this.isGifted = isGifted;
    }

    public Boolean getIsGiftedPass() {
        return isGiftedPass;
    }

    public void setIsGiftedPass(Boolean isGiftedPass) {
        this.isGiftedPass = isGiftedPass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getValidUpto() {
        return validUpto;
    }

    public void setValidUpto(String validUpto) {
        this.validUpto = validUpto;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getReserveField1() {
        return reserveField1;
    }

    public void setReserveField1(String reserveField1) {
        this.reserveField1 = reserveField1;
    }

    public String getReserveField2() {
        return reserveField2;
    }

    public void setReserveField2(String reserveField2) {
        this.reserveField2 = reserveField2;
    }

    public String getGenralAdmissionDesc() {
        return genralAdmissionDesc;
    }

    public void setGenralAdmissionDesc(String genralAdmissionDesc) {
        this.genralAdmissionDesc = genralAdmissionDesc;
    }

    public String getMemberPresenceDesc() {
        return memberPresenceDesc;
    }

    public void setMemberPresenceDesc(String memberPresenceDesc) {
        this.memberPresenceDesc = memberPresenceDesc;
    }

    public String getUsedPurpose() {
        return UsedPurpose;
    }

    public GiftableGuestPas setUsedPurpose(String usedPurpose) {
        UsedPurpose = usedPurpose;
        return this;
    }
}
