package com.inficare.membershipdemo.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.activities.GuestPassesActivity;
import com.inficare.membershipdemo.activities.QRCodeActivity;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.GuestPas;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Abhishek.jha on 10/17/2016.
 */

public class GuestPassAdapter extends RecyclerView.Adapter<GuestPassViewHolder> {

    private Context mContext;
    private List<GuestPas> guestPasList;
    private Map<String, Bitmap> bitmapMap;
    //private Map<String,File> bitmapMapFile;
    public static ProgressDialog dialog;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    private boolean isShareClicked;
    private String guest_pass;
    private Map<String, JSONObject> languageMap;
    private String language;
    private String used, shared, ready_to_use, expired, valid_through;
    private String please_wait;


    public GuestPassAdapter(Context context, List<GuestPas> guestPasList, String guest_pass) {

        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);

        language = context.getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, Context.MODE_PRIVATE).getString(configList.get(clickedMember).getMid(), "");
        languageMap = MyLanguage.getInstance().getMyLanguage(context);
        this.mContext = context;
        this.guestPasList = guestPasList;
        bitmapMap = new HashMap<>();
        this.guest_pass = guest_pass;

        String title = "title";
        if (language.equalsIgnoreCase(Constants.ENGLISH)) {

        } else {
            title = title + "_" + language;
        }

        try {
            used = languageMap.get(LanguageConstants.USED).getString(title);
            expired = languageMap.get(LanguageConstants.EXPIRED).getString(title);
            ready_to_use = languageMap.get(LanguageConstants.READY_TO_USE).getString(title);
            shared = languageMap.get(LanguageConstants.SHARED).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {

            valid_through = languageMap.get(LanguageConstants.VALID_THROUGH).getString(title);
            please_wait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }
       /* bitmapMapFile=new HashMap<>();
        for (int i=0;i<guestPasList.size();i++){
          // bitmapMap.put(String.valueOf(i),Utility.generateQRBitmap(guestPasList.get(i)
          .getGuestPassID(), mContext,0));
            File file=saveImage(Utility.generateQRBitmap(guestPasList.get(i).getGuestPassID(),
            mContext,0));
            bitmapMapFile.put(String.valueOf(i),file);

        }*/
        // waitdialog.dismiss();
    }

    @Override
    public GuestPassViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.guest_pass_items,
         null, false);*/
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.guest_pass_items, parent, false);

        GuestPassViewHolder guestPassViewHolder = new GuestPassViewHolder(view);
        return guestPassViewHolder;
    }

    @Override
    public void onBindViewHolder(GuestPassViewHolder holder, final int position) {
        //Guest Pass
        int guest_pass_no = position + 1;
        Utility.changeDrawableColor(mContext, holder.txt_guest_pass, configList
                .get(clickedMember).getColorCode());

        if (guestPasList.get(position).getGiftedPass()) {
            holder.txt_guest_pass.setText(guestPasList.get(position).getHeader());

            if (guestPasList.get(position).getStatus().equals("Ready to use") || guestPasList.get(position).getStatus().equals("Shared")) {
                if (isShareClicked) {
                    holder.myCheckbox.setVisibility(View.VISIBLE);
                    holder.myCheckbox.setOnCheckedChangeListener(null);
                    holder.myCheckbox.setChecked(guestPasList.get(position).isChecked());
                    holder.myCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            guestPasList.get(position).setChecked(isChecked);
                        }
                    });

                } else {
                    holder.myCheckbox.setVisibility(View.GONE);
                }
            } else {
                holder.myCheckbox.setVisibility(View.GONE);
            }
        } else {
            holder.txt_guest_pass.setText(guestPasList.get(position).getHeader() + " " + guest_pass_no);
            holder.myCheckbox.setVisibility(View.GONE);
        }

        holder.tv_description.setText(guestPasList.get(position).getDiscription());
        String usedPurpose=guestPasList.get(position).getUsedPurpose();
        if(usedPurpose!=null&&!usedPurpose.trim().isEmpty()){
            holder.tv_used_purpose.setVisibility(View.VISIBLE);
            String usedLevel=mContext.getResources().getString(R.string.used_purpose);
            holder.tv_used_purpose.setText(usedLevel+" "+usedPurpose);
        }else {
            holder.tv_used_purpose.setVisibility(View.GONE);
        }

        holder.tv_text_qrcode.setText(guestPasList.get(position).getGuestPassID());


        if (GuestPassesActivity.bitmapMapFile != null && GuestPassesActivity.bitmapMapFile.size() > 0) {
            File file = GuestPassesActivity.bitmapMapFile.get(String.valueOf(position));
            if (file != null) {
                final Uri uri = Uri.fromFile(file);
                Picasso.with(mContext).load(uri)
                        .into(holder.qr_code);
                holder.progressBar.setVisibility(View.GONE);
            } else {
                holder.progressBar.setVisibility(View.VISIBLE);
            }

        } else {
            holder.progressBar.setVisibility(View.VISIBLE);
        }

        if (guestPasList.get(position).getGiftedPass() && !guestPasList.get(position).getIsUsed().equalsIgnoreCase("true")) {


            /*holder.iv_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareGiftPass.shareGiftPass(position);
                }
            });*/

        } else {

        }

        String dateandtime = guestPasList.get(position).getValidUpto();
        String[] stringParts = dateandtime.split(" ");
        String dateOnly = stringParts[0];
        holder.bt_valid_through.setText(valid_through + " " + Utility.convertDateFormat(dateOnly));
        holder.bt_ready_to_use.setBackground(mContext.getResources().getDrawable(R.drawable
                .activate_button_sel));
        String status = guestPasList.get(position).getStatus();
        if (status.equalsIgnoreCase("Ready to use")) {
            /*holder.bt_ready_to_use.setText("Ready to use");*/
            holder.bt_ready_to_use.setBackground(mContext.getResources().getDrawable(R.drawable
                    .activate_button_sel));
            holder.imUsedIcon.setVisibility(View.GONE);
            holder.bt_ready_to_use.setText(ready_to_use);

        } else if (status.equalsIgnoreCase("Used")) {
            /*holder.bt_ready_to_use.setText("Used");*/
            holder.bt_ready_to_use.setBackground(mContext.getResources().getDrawable(R.drawable
                    .red_activate_button_sel));
            holder.imUsedIcon.setVisibility(View.VISIBLE);
            holder.imUsedIcon.setBackgroundResource(R.drawable.used_icon);

            String dateandtimeUsed= guestPasList.get(position).getModifiedOn();
            if(!dateandtimeUsed.isEmpty()){
                String[] dateParts = dateandtimeUsed.split(" ");
                String dateUsed = dateParts[0];
                holder.bt_ready_to_use.setText(used+ " " + Utility.convertDateFormat(dateUsed));
            }else {
                holder.bt_ready_to_use.setText(used);
            }


        } else if (status.equalsIgnoreCase("Expired")) {
            /*  holder.bt_ready_to_use.setText("Expired");*/
            holder.imUsedIcon.setVisibility(View.VISIBLE);
            holder.imUsedIcon.setBackgroundResource(R.drawable.expired_icon);
            holder.bt_ready_to_use.setBackground(mContext.getResources().getDrawable(R.drawable.red_activate_button_sel));
            holder.bt_ready_to_use.setText(expired);

        } else if (status.equalsIgnoreCase("Shared")) {

            holder.imUsedIcon.setVisibility(View.GONE);
            holder.bt_ready_to_use.setText(shared);

        } else {
            holder.bt_ready_to_use.setText(status);

            holder.imUsedIcon.setVisibility(View.GONE);
        }

//        holder.bt_ready_to_use.setText(status);

        /*if(guestPasList.get(position).getGifted()){
            holder.tvShared.setVisibility(View.VISIBLE);
        }else {
            holder.tvShared.setVisibility(View.INVISIBLE);
        }*/
        //holder.itemView.setOnClickListener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog = Utility.showProgressDialog(mContext, please_wait);
                dialog.show();
                Intent i = new Intent(mContext, QRCodeActivity.class);
                i.putExtra("guestPassId", guestPasList.get(position).getGuestPassID());
                i.putExtra("usedStatus", guestPasList.get(position).getStatus());
                mContext.startActivity(i);
            }
        });


    }

    @Override
    public int getItemCount() {
        return guestPasList.size();
    }

    public List<GuestPas> getGuestPasList() {
        return guestPasList;
    }


    private File saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/.saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void onShareClicked(boolean is) {
        isShareClicked = is;
        notifyDataSetChanged();
    }
}
