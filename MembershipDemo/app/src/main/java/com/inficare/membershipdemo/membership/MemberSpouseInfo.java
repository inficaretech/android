package com.inficare.membershipdemo.membership;

import java.io.Serializable;

public class MemberSpouseInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8095903108102825638L;

	private String MembershipID;

	private String Membershiplevel;

	private String City;

	private String State;

	private String Zip;

	private String FirstName;

	private String LastName;

	private String MemberSinceTimeSpan;

	private String ValidThroughTimeSpan;

	private String Number;

	private String ProfileImage;

	private String ConstituentID;
	private int HaveLogoPath;
	private String RoamLogoPath;
	private String Membersince;
	private String CardBanner;
	private String searchType;
	private String SearchLabel;
	private String NoOfChildren;
	private String MembershiplevelID;

	public String getMembershiplevelID() {
		return MembershiplevelID;
	}

	public void setMembershiplevelID(String membershiplevelID) {
		MembershiplevelID = membershiplevelID;
	}

	public String getRoamLogoPath() {
		return RoamLogoPath;
	}

	public void setRoamLogoPath(String roamLogoPath) {
		RoamLogoPath = roamLogoPath;
	}

	public int getHaveLogoPath() {
		return HaveLogoPath;
	}

	public void setHaveLogoPath(int haveLogoPath) {
		HaveLogoPath = haveLogoPath;
	}
	//private String Standing;
	private String IsExpire;

	public String getMembershipID() {
		return MembershipID;
	}

	public void setMembershipID(String membershipID) {
		MembershipID = membershipID;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getZip() {
		return Zip;
	}

	public void setZip(String zip) {
		Zip = zip;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getMemberSinceTimeSpan() {
		return MemberSinceTimeSpan;
	}

	public void setMemberSinceTimeSpan(String memberSinceTimeSpan) {
		MemberSinceTimeSpan = memberSinceTimeSpan;
	}

	public String getValidThroughTimeSpan() {
		return ValidThroughTimeSpan;
	}

	public void setValidThroughTimeSpan(String validThroughTimeSpan) {
		ValidThroughTimeSpan = validThroughTimeSpan;
	}

	public String getMembershiplevel() {
		return Membershiplevel;
	}

	public void setMembershiplevel(String membershiplevel) {
		Membershiplevel = membershiplevel;
	}

	public String getNumber() {
		return Number;
	}

	public void setNumber(String number) {
		Number = number;
	}

	public String getProfileImage() {
		return ProfileImage;
	}

	public void setProfileImage(String profileImage) {
		ProfileImage = profileImage;
	}

	public String getConstituentID() {
		return ConstituentID;
	}

	public void setConstituentID(String constituentID) {
		ConstituentID = constituentID;
	}

	public String getIsExpire() {
		return IsExpire;
	}

	public void setIsExpire(String isExpire) {
		IsExpire = isExpire;
	}

	public String getMembersince() {
		return Membersince;
	}

	public void setMembersince(String membersince) {
		Membersince = membersince;
	}

	public String getCardBanner() {
		return CardBanner;
	}

	public void setCardBanner(String cardBanner) {
		CardBanner = cardBanner;
	}

	public String getsearchType() {
		return searchType;
	}

	public void setsearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSearchLabel() {
		return SearchLabel;
	}

	public void setSearchLabel(String searchLabel) {
		SearchLabel = searchLabel;
	}

	public String getNoOfChildren() {
		return NoOfChildren;
	}

	public void setNoOfChildren(String noOfChildren) {
		NoOfChildren = noOfChildren;
	}
}
