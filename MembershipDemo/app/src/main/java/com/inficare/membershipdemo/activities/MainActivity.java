package com.inficare.membershipdemo.activities;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.CustomGrid;
import com.inficare.membershipdemo.adapter.MainAdapter;
import com.inficare.membershipdemo.backgroundtask.FCMMessageReceiverService;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.AppUpdateManager;
import com.inficare.membershipdemo.methods.GPSTracker;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.MyResponse;
import com.inficare.membershipdemo.pojo.VersionControl;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imgBtnOcean, imgBtnNCMuseum;
    Toolbar toolbar;
    List<Configuration> configurationList;

    int clickedMember;
    TextView toolbar_title;
    ImageView ivList;
    ListView list;
    MuseumDemoPreference preferences;
    private static final String TAG_LIST = "list";
    private static final String TAG_GRID = "grid";
    MainAdapter adapter;
    GridView grid;
    CustomGrid adapterGrid;
    FloatingActionButton fab1, fab2, fab3;
    FloatingActionMenu menu;
    boolean isSorted;
    boolean isPermissionDeny;
    boolean isActionMenuOpened;
    private SwipeRefreshLayout swipeToRefresh;
    private boolean isDataLoading;

    private String no_internet, toast_access_location, coming_soon;
    private String language = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);
        ///checkAppVersion();
        createNotificationChannel();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        ivList = (ImageView) findViewById(R.id.ivList);
        list = (ListView) findViewById(R.id.listView);
        grid = (GridView) findViewById(R.id.gridview);
        ivList.setVisibility(View.VISIBLE);
        ivList.setTag(TAG_GRID);
        swipeToRefresh = findViewById(R.id.swipeToRefresh);
        setFabButtons();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        preferences = MuseumDemoPreference.getInstance(this);

        //getConfig();
        callRetroResponse();
        //sendNotification("Title","body");
        String eData = Utility.myEncrypt("akshay kumar");
        Log.i("Encrypt", eData);
        Log.i("Encrypt", Utility.myDecript(eData));

    }


    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, MembershipOptions.class);
        switch (v.getId()) {
            case R.id.imgBtnOcean:
                startActivity(i);
                break;
            case R.id.imgBtnNCMuseum:
                startActivity(i);
                break;
            case R.id.ivList:
                changeView();
                break;
            case R.id.fab1:
                mSortAtoZ();
                menu.close(false);
                break;
            case R.id.fab2:
                if (Build.VERSION.SDK_INT < 23) {
                    enableGPS();
                }
                requestStoragePermission(false);
                menu.close(false);
                break;
            case R.id.fab3:
                mSortFavorite();
                menu.close(false);
                break;
        }
    }

    private void changeView() {
        String tag = (String) ivList.getTag();
        if (tag.equalsIgnoreCase(TAG_GRID)) {
            ivList.setImageResource(R.drawable.grid_view);
            ivList.setTag(TAG_LIST);
            // createViewGrid(configurationList);
            grid.setNumColumns(1);
            adapterGrid = null;
            adapterGrid = new CustomGrid(MainActivity.this, configurationList, 0);
            grid.setAdapter(adapterGrid);


        } else {
            ivList.setImageResource(R.drawable.list_view);
            ivList.setTag(TAG_GRID);
            grid.setNumColumns(2);
            adapterGrid = null;
            adapterGrid = new CustomGrid(MainActivity.this, configurationList, 1);
            grid.setAdapter(adapterGrid);
            //createView(configurationList);
        }

    }


    private void callRetroResponse() {

        if (Utility.isNetworkAvailable(this)) {
            boolean progress = true;
            String data = preferences.getString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, null);
            if (data != null && data.length() > 10) {
                progress = false;
                configurationList = MemberParser.parsConfig(MainActivity.this);

                if (adapterGrid == null) {
                    adapterGrid = new CustomGrid(MainActivity.this, configurationList, 1);
                    grid.setAdapter(adapterGrid);
                } else {
                    adapterGrid.setData(configurationList);
                }

                ivList.setOnClickListener(MainActivity.this);
                fab1.setOnClickListener(MainActivity.this);
                fab2.setOnClickListener(MainActivity.this);
                fab3.setOnClickListener(MainActivity.this);
                requestStoragePermission(isPermissionDeny);
            }
            getConfigRetrofitCall(progress);
        } else {
            String data = preferences.getString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, null);
            if (data != null && data.length() > 10) {
                configurationList = MemberParser.parsConfig(MainActivity.this);

                adapterGrid = new CustomGrid(MainActivity.this, configurationList, 1);
                grid.setAdapter(adapterGrid);

                ivList.setOnClickListener(MainActivity.this);
                fab1.setOnClickListener(MainActivity.this);
                fab2.setOnClickListener(MainActivity.this);
                fab3.setOnClickListener(MainActivity.this);
                requestStoragePermission(isPermissionDeny);
            }
            Toast.makeText(this, getResources().getString(R.string.msg_no_internet), Toast
                    .LENGTH_LONG).show();
        }


        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {

                    MuseumDemoPreference preference = MuseumDemoPreference.getInstance
                            (MainActivity.this);
                    preference.saveInt(MuseumDemoPreference.CLICKED_MEMBER, configurationList.get
                            (position).getItemIndex());
                    Intent i = new Intent(MainActivity.this, MembershipOptions.class);
                    if (!configurationList.get(position).getMuseumStatus().equalsIgnoreCase("0")) {
                        startActivity(i);
                    } else {
                        Toast.makeText(MainActivity.this, "Coming Soon...", Toast.LENGTH_LONG)
                                .show();
                    }

                } catch (Exception e) {

                }

            }
        });

    }

    ProgressDialog dialog = null;

    private void getConfigRetrofitCall(final boolean progress) {
        if (isDataLoading) {
            return;
        }
        isDataLoading = true;
        if (progress) {
            dialog = Utility.showProgressDialog(this, getString(R.string.msg_wait));
            dialog.show();
        }

//        Call<ResponseBody> bodyCall = Utility.callRetrofit().getconfiguration2(Utility
//                .getDeviceId(this));

        Call<ResponseBody> bodyCall = Utility.callRetrofit3().getconfiguration(Utility.getDeviceId(this));

        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                isDataLoading = false;
                swipeToRefresh.setRefreshing(false);
                if (response.code() == HttpURLConnection.HTTP_OK) {

                    String respo = "";
                    try {
                        respo = response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.d("respo", respo + "");
//                    Log.d("texts n label", response.body());

                    preferences.saveString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, respo);

                    configurationList = MemberParser.parsConfig(MainActivity.this);
                    if (adapterGrid == null) {
                        adapterGrid = new CustomGrid(MainActivity.this, configurationList, 1);
                        if (progress) {
                            grid.setAdapter(adapterGrid);
                        } else {
                            adapterGrid.notifyDataSetChanged();
                        }
                    } else {
                        adapterGrid.setData(configurationList);
                    }
                    ivList.setOnClickListener(MainActivity.this);
                    fab1.setOnClickListener(MainActivity.this);
                    fab2.setOnClickListener(MainActivity.this);
                    fab3.setOnClickListener(MainActivity.this);
                    requestStoragePermission(isPermissionDeny);
                    ///downloadImage(configurationList);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                isDataLoading = false;
                swipeToRefresh.setRefreshing(false);
            }
        });
    }


    public void requestStoragePermission(boolean isPermissionDeny) {

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission
                    .ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                if (isPermissionDeny) {
                    return;
                }
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission
                        .ACCESS_FINE_LOCATION}, 0);
            } else {
                sortByLocation();
                mSortLoc();
            }
        } else {
            sortByLocation();
            mSortLoc();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length == 0) {
            return;
        }
        if (requestCode == 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                sortByLocation();
                mSortLoc();
            } else {
                isPermissionDeny = true;
                Toast.makeText(this, getResources().getString(R.string
                        .please_allow_membership_app_to_access_location), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sortByLocation() {
        /*if(isSorted){
            return;
        }*/
        try {
            GPSTracker tracker = new GPSTracker(this);
            for (int i = 0; i < configurationList.size(); i++) {
                double dis = distance(tracker.getLatitude(), tracker.getLongitude(), Double
                        .parseDouble(configurationList.get(i).getLat()), Double.parseDouble
                        (configurationList.get(i).getLng()));
                configurationList.get(i).setLocation(dis);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        isSorted = true;

    }

    private void mSortLoc() {
        try {
            Collections.sort(configurationList, new Comparator<Configuration>() {
                public int compare(Configuration o1, Configuration o2) {
                    if (o1.getLocation() == o2.getLocation())
                        return 0;
              /*  if(o1.getMuseumStatus().equalsIgnoreCase("0"))
                    return 1;*/
                    return o1.getLocation() < o2.getLocation() ? -1 : 1;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        adapterGrid.notifyDataSetChanged();
    }

    private void mSortAtoZ() {
        Collections.sort(configurationList, new Comparator<Configuration>() {
            public int compare(Configuration o1, Configuration o2) {
                if (o1.getName().equalsIgnoreCase(o2.getName()))
                    return 0;
               /* if(o1.getMuseumStatus().equalsIgnoreCase("0"))
                    return 1;*/
                return o1.getName().compareTo(o2.getName());
            }
        });

        adapterGrid.notifyDataSetChanged();
    }

    private void mSortFavorite() {
        Collections.sort(configurationList, new Comparator<Configuration>() {
            public int compare(Configuration o1, Configuration o2) {

                return o1.isFavorite() ? -1 : 1;
            }
        });

        adapterGrid.notifyDataSetChanged();
    }

    public double distance(double lat1, double lon1, double lat2, double lon2) {
//        double theta = lon1 - lon2;
//        double dist = Math.sin(deg2rad(lat1))
//                * Math.sin(deg2rad(lat2))
//                + Math.cos(deg2rad(lat1))
//                * Math.cos(deg2rad(lat2))
//                * Math.cos(deg2rad(theta));
//        dist = Math.acos(dist);
//        dist = rad2deg(dist);
//        dist = dist * 60 * 1.1515;
//        return (dist);
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 0.8684;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void setFabButtons() {

        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) findViewById(R.id.fab3);

        menu = (FloatingActionMenu) findViewById(R.id.menu);
        menu.setClosedOnTouchOutside(true);
        menu.setIconAnimated(false);
        menu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if (opened) {
                    menu.getMenuIconView().setImageResource(R.drawable.cross_icon);
                    isActionMenuOpened = true;
                } else {
                    menu.getMenuIconView().setImageResource(R.drawable.filter_icon);
                    isActionMenuOpened = false;
                }

            }
        });
    }

    private void enableGPS() {
        final LocationManager manager = (LocationManager) getSystemService(Context
                .LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }

    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, please enable it to sort?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog,
                                        @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings
                                .ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            ///callRetroVersionConfog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!isActionMenuOpened) {
            //callRetroResponse();
        }

        if (VerifyMembership.isSavedRecently) {
            VerifyMembership.isSavedRecently = false;
            //refresh activity
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        swipeToRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        swipeToRefresh.setRefreshing(true);
                        getConfigRetrofitCall(false);
                    }
                }
        );
    }


    private void downloadImage(List<Configuration> configurationList) {
        try {
            if (configurationList == null) {
                return;
            }
            for (int i = 0; i < configurationList.size(); i++) {
                String uri = configurationList.get(i).getImagePath() + "/" + configurationList.get(i).getLogo();
                ImageLoader.getInstance().loadImage(uri, null);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void callRetroVersionConfog() throws Exception {

        if (Utility.isNetworkAvailable(this)) {

            String lDate = preferences.getString(AppUpdateManager.LAST_UPDATE_ALERT_DATE, null);
            if (lDate != null) {
                if (!Utility.is24hInterval(lDate)) {
                    return;
                }
            }

            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int currentAppVersionCode = pInfo.versionCode;
            Call<MyResponse> bodyCall = Utility.callRetrofit3().getVersionConfig(String.valueOf(currentAppVersionCode), "2");

            bodyCall.enqueue(new Callback<MyResponse>() {
                @Override
                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {

                    if (response.code() == HttpURLConnection.HTTP_OK) {
                        try {
                            MyResponse myResponse = response.body();
                            if (!myResponse.getSuccess()) {
                                return;
                            }
                            VersionControl control = myResponse.getVersionControl();
                            if (control.getHavingVersionControl() == 1) {
                                boolean force = control.getIsForceUpdate();
                                String version = control.getVersion();
                                int intV = Integer.parseInt(version);
                                String description = control.getDescription();
                                AppUpdateManager.checkAppVersion(MainActivity.this, intV, description, force);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<MyResponse> call, Throwable t) {

                }
            });
        }
    }
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();

           // Uri soundUri=Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.notification_sound);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(FCMMessageReceiverService.CHANNEL_ID, name, importance);
            channel.setDescription(description);

            channel.enableLights(true);
            channel.enableVibration(true);
            //channel.setSound(soundUri, attributes);
            // Register the channel with the system; you can'MatcherNode change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
