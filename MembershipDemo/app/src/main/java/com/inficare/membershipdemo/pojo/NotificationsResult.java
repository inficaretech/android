package com.inficare.membershipdemo.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by akshay.kumar on 10/5/2017.
 */

public class NotificationsResult {
    @SerializedName("Success")
    @Expose
    private String success;
    @SerializedName("Result")
    @Expose
    private Result result;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }


}
