package com.inficare.membershipdemo.backgroundtask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.networks.HttpMethod;

import org.json.JSONObject;

import java.util.Collections;
import java.util.Map;

/**
 * Created by akshay.kumar on 3/27/2017.
 */

public abstract class MyAsyncPost extends AsyncTask<String, Void, String> {

    private Context mContext;

    private static final String CN = "MemberTask";

    private ProgressDialog dialog;
    Map<String, Object> parameter;
    public MyAsyncPost(Context context,Map<String, Object> parameter) {
        mContext = context;
        this.parameter=parameter;
        createDialog(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.show();

    }

    @Override
    protected String doInBackground(String... params) {


        String  responsedata = HttpMethod.getInstance(mContext).postData(params[0], parameter);
               // HttpMethod.TYPE.GET, params[0]);


        return responsedata;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        try {
            dialog.dismiss();
            if (result.contains("Success")) {
                JSONObject json = new JSONObject(result);
                String status=  json.getString("Success");
                if(status.equals("true")){
                    onTaskFinish(result);
                }
            }else {
                Toast.makeText(mContext,result,Toast.LENGTH_LONG).show();
            }
//
        } catch (Exception e) {
            Toast.makeText(mContext,"Something went wrong please try again later",Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }


    }

    public void createDialog(final Context context) {
//		TagInfo infoTag = (Utility.getTagForLangChange(SharedManager
//				.getInstance(context).getInt(Constants.PREV_LANG, 0), context));

//		if (!Constants.langChange) {
//			dialog = Utility.showProgressDialog(mContext, context
//					.getResources().getString(R.string.msg_wait));
//		} else {

//			if (infoTag != null) {
//				dialog = Utility.showProgressDialog(mContext,
//						infoTag.getAlertWait());
//			} else {
        dialog = Utility.showProgressDialog(mContext, context
                .getResources().getString(R.string.msg_wait));
        //}

        //}

    }

    public abstract void onTaskFinish(String response);

}
