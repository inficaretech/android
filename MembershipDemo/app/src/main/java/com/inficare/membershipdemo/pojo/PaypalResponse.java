package com.inficare.membershipdemo.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by avinash.verma on 4/25/2017.
 */

public class PaypalResponse {
    @SerializedName("client")
    @Expose
    private Client client;
    @SerializedName("response")
    @Expose
    private Responses response;
    @SerializedName("response_type")
    @Expose
    private String responseType;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Responses getResponse() {
        return response;
    }

    public void setResponse(Responses response) {
        this.response = response;
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
}
