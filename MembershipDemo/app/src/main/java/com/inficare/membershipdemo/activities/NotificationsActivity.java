package com.inficare.membershipdemo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.NotificationsAdapter;
import com.inficare.membershipdemo.databaseprovider.SQLDataProvider;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.Notification;
import com.inficare.membershipdemo.pojo.NotificationsResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotificationsActivity extends AppCompatActivity {
    public static final String NOTIFICATION_SAVED_DATA = "notification";
    private static final String TAG = NotificationsActivity.class.getSimpleName();
    ListView lvNotifications;
    private String number = "";
    TextView tvTopStrip, tvNoNotifications;
    int clickedMember;
    List<Configuration> configList;
    private ProgressDialog dialog;
    String mid;
    MuseumDemoPreference preferences;
    private boolean progressVisible = true;
    String memberId;
    private boolean isInstructionShown;
    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private Context mContext;
    private TextView toolbar_title;
    private String title;

    boolean isOpenedFromNotification;
  boolean isPush;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        mContext = this;

        preferences = MuseumDemoPreference.getInstance(this);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        isPush=getIntent().getBooleanExtra("isPush",false);
        String pushMid=getIntent().getStringExtra("mid");
        if(pushMid!=null){
            if(pushMid.isEmpty()){
                Toast.makeText(this,"Something went wrong!",Toast.LENGTH_LONG).show();
                return;
            }
          mid=  getIntent().getStringExtra("mid");
            isOpenedFromNotification=true;
          for (int i=0;i<configList.size();i++){
              String orgKey=configList.get(i).getMid();
              if(orgKey.equalsIgnoreCase(mid)){
                 clickedMember=i;
                 break;
              }
          }
        }else {
            mid = configList.get(clickedMember).getMid();
        }


        initLanguage();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.toolbar_title));
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        lvNotifications = (ListView) findViewById(R.id.lvNotifications);
        tvNoNotifications = (TextView) findViewById(R.id.tvNoNotifications);
        tvTopStrip = (TextView) findViewById(R.id.tvTopStrip);
        //tvNoNotifications.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));

        toolbar_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tvTopStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        /*MyAsyncTask task=new MyAsyncTask(this) {
            @Override
            public void onTaskFinish(String response) {
             List<Notifications> list= MemberParser.parseNotifications(response);
              *//*    NotificationsAdapter adapter=new NotificationsAdapter(NotificationsActivity.this,R.layout.notifications_item,list);
                lvNotifications.setAdapter(adapter);
                if(list!=null||list.size()>0){
                    tvNoNotifications.setVisibility(View.GONE);
                }*//*
            }
        };
        String  memberId=SQLDataProvider.getMemberId(this);
        if(Utility.isNetworkAvailable(this)) {
            if (memberId != null)
                task.execute(Constants.GET_NOTIFICATIONS_URL + "?UniqueDeviceID=" + Utility.getDeviceId(this) + "&memId=" + memberId + "&" + Constants.ORGANISATION_KEY + "=" + configList.get(clickedMember).getMid());
        }
*/
        if(isPush){
            memberId = SQLDataProvider.getMemberIdNoti(this,clickedMember);
        }else {
            memberId = SQLDataProvider.getMemberId(this);
        }

        getSavedData();
        if (Utility.isNetworkAvailable(this)) {

            getData(progressVisible, language);
        }

        setUi(language);
    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);

        title = Utility.getTitle(language);

    }

    private void setUi(String language) {

        try {
            toolbar_title.setText(languageMap.get(LanguageConstants.NOTIFICATIONS).getString(title));
            tvNoNotifications.setText(languageMap.get(LanguageConstants.NO_NOTIFICATION_AS_YET).getString(title));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void getData(final boolean progressVisible, String language) {


        try {

            if (memberId == null) {
                return;
            }
            if (progressVisible) {

                if (this.language.equalsIgnoreCase(Constants.ENGLISH)) {
                    dialog = Utility.showProgressDialog(this, languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...");

                } else if (this.language.equalsIgnoreCase(Constants.FRENCH)) {
                    dialog = Utility.showProgressDialog(this, languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...");

                } else {

                }
                dialog.show();
            }

            Call<NotificationsResult> memberReportCall = Utility.callRetrofit2().getNotifications(mid, Utility.getDeviceId(this), memberId);
            memberReportCall.enqueue(new Callback<NotificationsResult>() {

                @Override
                public void onResponse(Call<NotificationsResult> call, retrofit2.Response<NotificationsResult>
                        response) {

                    if (progressVisible)
                        dialog.dismiss();

                    List<Notification> notificationList = response.body().getResult().getNotifications();

                    if (notificationList == null) {
                        if (progressVisible) {
                            try {
                                Toast.makeText(NotificationsActivity.this, languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    } else {

                        NotificationsAdapter adapter = new NotificationsAdapter(NotificationsActivity.this, R.layout.notifications_item, notificationList);
                        lvNotifications.setAdapter(adapter);
                        if (notificationList.size() > 0) {
                            tvNoNotifications.setVisibility(View.GONE);
                        }
                        Gson gson = new Gson();
                        String jsonData = gson.toJson(notificationList);
                        preferences.saveString(NOTIFICATION_SAVED_DATA.concat(mid).concat(memberId), jsonData);
                    }

                }

                @Override
                public void onFailure(Call<NotificationsResult> call, Throwable t) {
                    /*dialog.dismiss();*/
                    if (progressVisible)
                        try {
                            Toast.makeText(NotificationsActivity.this, languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void getSavedData() {
        if (memberId == null) {
            return;
        }
        List<Notification> notificationList = null;
        String jsonData = preferences.getString(NOTIFICATION_SAVED_DATA.concat(mid).concat(memberId), null);
        if (jsonData != null) {
            Type listType = new TypeToken<ArrayList<Notification>>() {
            }.getType();

            notificationList = new Gson().fromJson(jsonData, listType);

            if (notificationList != null && notificationList.size() > 0) {
                tvNoNotifications.setVisibility(View.GONE);
                NotificationsAdapter adapter = new NotificationsAdapter(NotificationsActivity.this, R.layout.notifications_item, notificationList);
                lvNotifications.setAdapter(adapter);
                progressVisible = false;
            }
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isInstructionShown &&clickedMember!=-1&& configList.get(clickedMember).getMid().equalsIgnoreCase
                ("TXVzZXVtQW55V2hlcmU=")) {
            Intent intent = new Intent(this, InstActivityMain.class);
            intent.putExtra(InstActivityMain.INST_DATA, getResources().getString(R.string.push_notifications_appear));
            startActivity(intent);
            isInstructionShown = true;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(isOpenedFromNotification){
            Intent intent = new Intent(this,  MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}
