
package com.inficare.membershipdemo.databasecreation;

import android.content.Context;

import net.sqlcipher.database.SQLiteDatabase;

public class DBController {
    public static final String CN = "DBController";

    public static DBController createInstance(Context context, boolean writeable) {
        SQLiteDatabase.loadLibs(context);
        DBHelper helper = DBHelper.getInstance(context);
        DBController me = new DBController();
        me.database = writeable ? helper.getWritableDatabase("inficare") : helper
                .getReadableDatabase("inficare");
        return me;
    }

    protected SQLiteDatabase database;

    public SQLiteDatabase getDatabase() {
        return database;
    }

    public void startTransaction() {
        if (!this.database.inTransaction())
            this.database.beginTransaction();
    }

    public void endTransaction() {
        if (this.database.inTransaction())
            this.database.endTransaction();
    }

    public void comit() {
        if (this.database.inTransaction())
            this.database.setTransactionSuccessful();
    }

    public void rollback() {
        // Do nothing
    }

    /**
     * Close database connection and all started transactions (If any)
     */
    public void close() { //Because use single instance. can'MatcherNode close
        //        if (this.database.inTransaction()) {
        //            endTransaction();
        //        }
        //        this.database.close();
        //
        //        SQLiteDatabase.releaseMemory();
    }
}
