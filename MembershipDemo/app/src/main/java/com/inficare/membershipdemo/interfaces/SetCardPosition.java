package com.inficare.membershipdemo.interfaces;

import android.widget.ImageView;
import android.widget.ViewAnimator;

public interface SetCardPosition {

	public void setPosition(int pos, ViewAnimator animator, ImageView image,
							boolean flag,int type);

}
