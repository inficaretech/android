package com.inficare.membershipdemo.methods;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.activities.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by akshay.kumar on 19-04-2018.
 */

public class AppUpdateManager {
    public static String LAST_UPDATE_ALERT_DATE="updateAlert";
  private static MuseumDemoPreference preferences;


    public static void checkAppVersion(Context mContext,int newVersion,String title,boolean isForce){
        try {
            PackageInfo pInfo = null;
            if(preferences==null)
                preferences = MuseumDemoPreference.getInstance(mContext);

            pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            int currentAppVersionCode = pInfo.versionCode;
            //int newVersion=41;
            if(currentAppVersionCode<newVersion){
                String lDate=preferences.getString(LAST_UPDATE_ALERT_DATE,null);
                if(lDate==null){
                    appUpdateAlert(mContext,title,isForce);
                    updateCheckedDate();
                }else {
                    if (Utility.is24hInterval(lDate)) {
                        appUpdateAlert(mContext,title,isForce);
                        updateCheckedDate();
                    }

                }

                Log.i("AppVersion","Update available!");
            }else {
                Log.i("AppVersion","Update not available!");
            }

           /* *//*String playStoreVersionCode = FirebaseRemoteConfig.getInstance().getString(
                    "android_latest_version_code");
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            int currentAppVersionCode = pInfo.versionCode;
            Log.i("AppVersion","payStore v:"+playStoreVersionCode);
            Log.i("AppVersion","current v:"+currentAppVersionCode);*//*
      *//*  if(playStoreVersionCode>currentAppVersionCode){
//Show update popup or whatever best for you
        }*//*
            FirebaseRemoteConfig mFirebaseRemoteConfig=  FirebaseRemoteConfig.getInstance() ;
            mFirebaseRemoteConfig.fetch(30l)
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.i("AppVersion","Fetch Succeeded");
                                // After config data is successfully fetched, it must be activated before newly fetched
                                // values are returned.
                                mFirebaseRemoteConfig.activateFetched();
                                PackageInfo pInfo = null;
                                try {

                                    pInfo = MainActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
                                    int currentAppVersionCode = pInfo.versionCode;

                                    String playstoreVersion = mFirebaseRemoteConfig.getString("android_latest_version_code");
                                    Log.i("AppVersion","payStore version :"+playstoreVersion);
                                    int pVersion=Integer.parseInt(playstoreVersion);
                                    if(currentAppVersionCode<pVersion){
                                      String lDate=preferences.getString(LAST_UPDATE_ALERT_DATE,null);
                                      if(lDate==null){
                                          appUpdateAlert();
                                      }else {
                                          if(Utility.is24hInterval(lDate)){
                                              appUpdateAlert();
                                              Calendar c = Calendar.getInstance();
                                              Date currentDate = c.getTime();
                                              SimpleDateFormat dateFormat = new SimpleDateFormat(
                                                      "yyyy-MM-dd");
                                              String oldDate = dateFormat.format(currentDate);
                                              preferences.saveString(LAST_UPDATE_ALERT_DATE,oldDate);
                                          }

                                      }


                                        Log.i("AppVersion","Update available!");
                                    }else {
                                        Log.i("AppVersion","Update not available!");
                                    }
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }

                            } else {
                                Log.i("AppVersion","Fetch Failed");
                            }

                        }
                    });
*/
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private static void appUpdateAlert(Context mContext,String title,boolean isForce) {

        if(preferences==null)
            preferences = MuseumDemoPreference.getInstance(mContext);

        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(title)
                .setCancelable(false)
                .setPositiveButton(mContext.getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog,
                                        @SuppressWarnings("unused") final int id) {
                        appUpdate(mContext);
                    }
                });
                if(!isForce){
                    builder.setNegativeButton(mContext.getResources().getString(R.string.dismiss), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused")
                        final int id) {
                            dialog.cancel();
                        }
                    });
                }

        final AlertDialog alert = builder.create();
        alert.show();
        alert.setCancelable(false);
    }

    public static void appUpdate(Context mContext){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=com.inficare.membershipdemo"));
        mContext.startActivity(intent);
    }


    private static void updateCheckedDate(){
        Calendar c = Calendar.getInstance();
        Date currentDate = c.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        String oldDate = dateFormat.format(currentDate);
        preferences.saveString(LAST_UPDATE_ALERT_DATE,oldDate);
    }
}
