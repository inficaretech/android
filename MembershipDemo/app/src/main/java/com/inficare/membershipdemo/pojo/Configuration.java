package com.inficare.membershipdemo.pojo;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by akshay.kumar on 11/4/2016.
 */

public class Configuration implements Serializable {
    private String id, name, description, mid, createdAt, updateAt, logo, bannerImage,
            cardBannerImage, cardImage, email, lat, lng, address, imagePath, colorCode,
            contactNumber, museumStatus, pageLink, pageLinkLevel, barcode_member_type, donationAmounts, donationLabel, NotificationFrequency;
    boolean isFloorPlan, isGuestPass, isNotification, isFaq, isPageLink, isGift, isRenew,
            isReciprocal, isChildCount, IsTransactionBuy, IsTransactionRenew, IsTransactionGift, IsTransactionDonate;
    double location;
    int itemIndex;
    boolean isFavorite, isProfilePhoto, isMemberidHide, IsCardRestric;
    String blurCardImage, searchType, keybord, block_message, childCountLabel, IsEnvelopBypass, CustomDonationText, languageId;
    private boolean isImageLoaded;
    private String RenewLink;
    private String contact_ext;
    private boolean IsAddressLine;
    private String passSearchType="";
    private int isImgCentrAlign;
    private boolean isBenefitBtnShow=true;
    private boolean passPurposeStatus;

    public static final String MEMBERSHIP_ID = "Membership Id";
    public static final String CONSTITUENT_ID = "Constituent Id";
    public static final String NUMERIC = "Numeric";
    public List<DeviceDetails> deviceDetailsList;
    public List<Language> languageList;

    public Configuration(JSONObject obj) {
        try {
            id = "0";//obj.getString("id");
            name = obj.getString("name");
            description = obj.getString("description");
            if (mid == null) {
                mid = obj.getString("mid");
            }

            isFloorPlan = obj.getString("IsfloorPlan").equals("1");
            isReciprocal = obj.getString("IsReciprocal").equals("1");
            isGuestPass = obj.getString("IsguestPass").equals("1");
            isNotification = obj.getString("IsNotification").equals("1");
            isFaq = obj.getString("Isfaq").equals("1");
            isPageLink = obj.getString("Ispagelink").equals("1");
//            isGift = obj.getString("IsGift").equals("1");
//            isRenew = obj.getString("IsRenew").equals("1");

            if (obj.has("IsMemberidHide"))
                isMemberidHide = obj.getString("IsMemberidHide").equals("1");
            else
                isMemberidHide = false;

            if (obj.has("IsProfilePhoto"))
                isProfilePhoto = obj.getString("IsProfilePhoto").equals("1");
            if (obj.has("IsChildCount"))
                isChildCount = obj.getString("IsChildCount").equals("1");
            if (obj.has("child_count_label"))
                childCountLabel = obj.getString("child_count_label");

            if (obj.has("barcode_member_type"))
                barcode_member_type = obj.getString("barcode_member_type");
            else
                barcode_member_type = "0";

            createdAt = obj.getString("created_at");
            updateAt = obj.getString("updated_at");
            logo = obj.getString("logo");
            bannerImage = obj.getString("banner_image");
            cardBannerImage = obj.getString("card_banner_image");
            cardImage = obj.getString("card_image");
            email = obj.getString("email");
            lat = obj.getString("lat");
            lng = obj.getString("lang");
            address = obj.getString("address");
            imagePath = obj.getString("imagepath");
            colorCode = obj.getString("color_code");
            contactNumber = obj.getString("contact_number");
            museumStatus = obj.getString("museum_status");
            pageLink = obj.getString("page_link");
            pageLinkLevel = obj.getString("page_link_label");
            blurCardImage = imagePath + "/" + obj.getString("blur_card_image");
            searchType = obj.getString("search_type");
            keybord = obj.getString("keyboard");
            if (obj.has("block_message"))
                block_message = obj.getString("block_message");

            if (obj.has("DonationAmounts")) {
                donationAmounts = obj.getString("DonationAmounts");
            }
            if (obj.has("DonationLabel")) {
                donationLabel = obj.getString("DonationLabel");
            }
            if (obj.has("IsEnvelopBypass")) {
                IsEnvelopBypass = obj.getString("IsEnvelopBypass");
            }

            if (obj.has("IsTransactionBuy")) {
                IsTransactionBuy = obj.getBoolean("IsTransactionBuy");
            }

            if (obj.has("IsTransactionRenew")) {
                IsTransactionRenew = obj.getBoolean("IsTransactionRenew");
            }
            if (obj.has("IsTransactionGift")) {
                IsTransactionGift = obj.getBoolean("IsTransactionGift");
            }
            if (obj.has("IsTransactionDonate")) {
                IsTransactionDonate = obj.getBoolean("IsTransactionDonate");
            }

            if (obj.has("NotificationFrequency")) {
                NotificationFrequency = obj.getString("NotificationFrequency");
            }

            if (obj.has("CustomDonationText")) {
                CustomDonationText = obj.getString("CustomDonationText");
            }
            if (obj.has("language_id")) {
                languageId = obj.getString("language_id");
            }
            if (obj.has("RenewLink")) {
                RenewLink = obj.getString("RenewLink");
            }
            if (obj.has("contact_ext")) {
                contact_ext = obj.getString("contact_ext");
            }
            if (obj.has("IsAddressLine")) {
                IsAddressLine = obj.getBoolean("IsAddressLine");
            }
            if (obj.has("pass_search_type")) {
                passSearchType = obj.getString("pass_search_type");
            }
            if (obj.has("IsImgCentrAlign")) {
                isImgCentrAlign = obj.getInt("IsImgCentrAlign");
            }
            if (obj.has("IsBenefitBtnShow")) {
                isBenefitBtnShow = obj.getBoolean("IsBenefitBtnShow");
            }
            if (obj.has("PassPurposeStatus")) {
                passPurposeStatus = obj.getBoolean("PassPurposeStatus");
            }
           /* IsCardRestric=obj.getString("IsCardRestric").equalsIgnoreCase("1");
            if(IsCardRestric) {*/
            if (obj.has("device_details")) {
                JSONArray array = obj.getJSONArray("device_details");
                List<DeviceDetails> deviceDetailsList = new ArrayList<>();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject objDevice = array.getJSONObject(i);
                    DeviceDetails deviceDetails = new DeviceDetails(objDevice.getString
                            ("MembershipID"), objDevice.getString("UniqueDeviceID"), objDevice
                            .getString("DeviceType"), objDevice.getString("IsBlock")
                            .equalsIgnoreCase("1"), objDevice.getString("IsReset")
                            .equalsIgnoreCase("1"));
                    deviceDetailsList.add(deviceDetails);
                }
                this.setDeviceDetailsList(deviceDetailsList);
            }

            if (obj.has("languages")) {
                JSONArray array = obj.getJSONArray("languages");
                List<Language> languageList = new ArrayList<>();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject objDevice = array.getJSONObject(i);
                    Language language = new Language(objDevice.getInt("id")
                            , objDevice.getString("title"), objDevice
                            .getString("short_code"), objDevice.getString("Organisation_key")
                    );
                    languageList.add(language);
                }
                this.setLanguageList(languageList);
            }
            /// }
            //configurationList.add();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getMid() {
        return mid;
    }

    public String getBarcode_member_type() {
        return barcode_member_type;
    }

    public boolean isReciprocal() {
        return isReciprocal;
    }

    public boolean isFloorPlan() {
        return isFloorPlan;
    }

    public boolean isGuestPass() {
        return isGuestPass;
    }

    public boolean isNotification() {
        return isNotification;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdateAt() {
        return updateAt;
    }

    public String getLogo() {
        return logo;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public String getCardBannerImage() {
        return cardBannerImage;
    }

    public String getCardImage() {
        return cardImage;
    }

    public String getEmail() {
        return email;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getColorCode() {
        return colorCode;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getMuseumStatus() {
        return museumStatus;
    }

    public double getLocation() {
        return location;
    }

    public String getPageLink() {
        return pageLink;
    }

    public boolean isFaq() {
        return isFaq;
    }

    public boolean isPageLink() {
        return isPageLink;
    }

    public boolean isGift() {
        return isGift;
    }

    public boolean isRenew() {
        return isRenew;
    }

    public void setLocation(double location) {
        this.location = location;
    }

    public int getItemIndex() {
        return itemIndex;
    }

    public void setItemIndex(int itemIndex) {
        this.itemIndex = itemIndex;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getPageLinkLevel() {
        return pageLinkLevel;
    }

    public String getBlurCardImage() {
        return blurCardImage;
    }

    public boolean isProfilePhoto() {
        return isProfilePhoto;
    }

    public String getSearchType() {
        return searchType;
    }

    public boolean isMemberidHide() {
        return isMemberidHide;
    }

    public String getKeybord() {
        return keybord;
    }

    public List<DeviceDetails> getDeviceDetailsList() {
        return deviceDetailsList;
    }

    public void setDeviceDetailsList(List<DeviceDetails> deviceDetailsList) {
        this.deviceDetailsList = deviceDetailsList;
    }

    public String getBlock_message() {
        return block_message;
    }

    public boolean isChildCount() {
        return isChildCount;
    }

    public String getChildCountLabel() {
        return childCountLabel;
    }

    public boolean isImageLoaded() {
        return isImageLoaded;
    }

    public void setImageLoaded(boolean imageLoaded) {
        isImageLoaded = imageLoaded;
    }

    public String getDonationAmounts() {
        return donationAmounts;
    }

    public void setDonationAmounts(String donationAmounts) {
        this.donationAmounts = donationAmounts;
    }

    public String getDonationLabel() {
        return donationLabel;
    }

    public void setDonationLabel(String donationLabel) {
        this.donationLabel = donationLabel;
    }

    public String getIsEnvelopBypass() {
        return IsEnvelopBypass;
    }

    public boolean isTransactionBuy() {
        return IsTransactionBuy;
    }

    public boolean isTransactionRenew() {
        return IsTransactionRenew;
    }

    public boolean isTransactionGift() {
        return IsTransactionGift;
    }

    public boolean isTransactionDonate() {
        return IsTransactionDonate;
    }

    public List<String> getNotificationFrequency() {

        List<String> arr = null;
        if (NotificationFrequency != null) {
            arr = Arrays.asList(NotificationFrequency.split(","));
        }

        return arr;
    }

    public String getCustomDonationText() {
        return CustomDonationText;
    }

    public List<Language> getLanguageList() {
        return languageList;
    }

    public void setLanguageList(List<Language> languageList) {
        this.languageList = languageList;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getRenewLink() {
        return RenewLink;
    }

    public String getContact_ext() {
        return contact_ext;
    }

    public boolean getIsAddressLine() {
        return IsAddressLine;
    }

    public String getPassSearchType() {
        return passSearchType;
    }

    public int getIsImgCentrAlign() {
        return isImgCentrAlign;
    }

    public boolean isBenefitBtnShow() {
        return isBenefitBtnShow;
    }

    public boolean isPassPurposeStatus() {
        return passPurposeStatus;
    }
}
