package com.inficare.membershipdemo.interfaces;

/**
 * Created by abhishek.kumar on 10/11/2017.
 */

public interface ShareGiftPass {

    void shareGiftPass(int position);

}
