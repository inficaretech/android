package com.inficare.membershipdemo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Abhishek.jha on 10/19/2016.
 */

public class AdminActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private Button bt_qr_scan;
    private Button bt_report;
    View topStrip;
    //Typeface Elements

    //Toolbar elements
    private Toolbar toolbar;
    private TextView tb_title;
    int clickedMember;
    List<Configuration> configList;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    public static final String TAG = AdminActivity.class.getSimpleName();
    private String scan_barcode, report;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        mContext = this;
        MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        //setFonts();
        setToolbarLayout();
        findViews();
        initLanguage();
        setUi(language);

    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);



        try {
            tb_title.setText(languageMap.get(LanguageConstants.ADMIN).getString(title));
            bt_qr_scan.setText(languageMap.get(LanguageConstants.SCAN_BARCODE).getString(title));
            bt_report.setText(languageMap.get(LanguageConstants.REPORT).getString(title));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    /*private void setFonts() {
        faceheader = Utility.setFaceTypeHeader(mContext);
        faceRegular = Typeface.createFromAsset(getAssets(),
                "fonts/Raleway-Medium.ttf");
    }*/

    private void setToolbarLayout() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_button));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void findViews() {
        bt_qr_scan = (Button) findViewById(R.id.bt_qr_scan);
        bt_qr_scan.setOnClickListener(this);
        bt_report = (Button) findViewById(R.id.bt_report);
        bt_report.setOnClickListener(this);
        topStrip = findViewById(R.id.topStrip);
        topStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        Utility.changeDrawableColor(this, bt_qr_scan, configList.get(clickedMember).getColorCode());
        Utility.changeDrawableColor(this, bt_report, configList.get(clickedMember).getColorCode());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_qr_scan:
                startActivity(new Intent(mContext, QRScannerActivity.class));
                break;
            case R.id.bt_report:
                startActivity(new Intent(mContext, ReportActivity.class));
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
