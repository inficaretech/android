package com.inficare.membershipdemo.methods;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MuseumDemoPreference {
    private static MuseumDemoPreference instance;
    private static SharedPreferences sharedPreferences;

    public final static String AGREEMENT_KEY = "Agree";
    public static final String CONFIGURATION_DATA_JSON="Configuration_data_json";
    public static final String CLICKED_MEMBER="clicked_member";

    public static final String APP_UPGRADE_KEY="onAppUpgrade";
    public static final String ALL_BENEFITS="AllBenefits";
    public static final String RECIPROCAL_BY_LEVEL_ID="reciprocalByLevelID";

    public static MuseumDemoPreference getInstance(Context context) {
        if (instance == null) {
            instance = new MuseumDemoPreference();
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return instance;
    }


    public void saveString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void saveInt(String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void saveFloat(String key, float value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public void saveBoolean(String key, Boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String getString(String key, String defaulValue) {
        return sharedPreferences.getString(key, defaulValue);
    }

    public int getInt(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public float getFloat(String key, float defaultValue) {
        return sharedPreferences.getFloat(key, defaultValue);
    }

    public void removeData(String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();
    }

}
