package com.inficare.membershipdemo.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrgReciprocal implements Parcelable,Serializable {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("MembershipLevelID")
    @Expose
    private Integer membershipLevelID;
    @SerializedName("extrnal_link")
    @Expose
    private String extrnalLink;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("short_title")
    @Expose
    private String shortTitle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image_path")
    @Expose
    private String imagePath;
    @SerializedName("image_absolute_path")
    @Expose
    private String imageAbsolutePath;
    @SerializedName("Organisation_key")
    @Expose
    private String organisationKey;
    @SerializedName("is_active")
    @Expose
    private Integer isActive;
    @SerializedName("create_at")
    @Expose
    private String createAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMembershipLevelID() {
        return membershipLevelID;
    }

    public void setMembershipLevelID(Integer membershipLevelID) {
        this.membershipLevelID = membershipLevelID;
    }

    public String getExtrnalLink() {
        return extrnalLink;
    }

    public void setExtrnalLink(String extrnalLink) {
        this.extrnalLink = extrnalLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageAbsolutePath() {
        return imageAbsolutePath;
    }

    public void setImageAbsolutePath(String imageAbsolutePath) {
        this.imageAbsolutePath = imageAbsolutePath;
    }

    public String getOrganisationKey() {
        return organisationKey;
    }

    public void setOrganisationKey(String organisationKey) {
        this.organisationKey = organisationKey;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.membershipLevelID);
        dest.writeString(this.extrnalLink);
        dest.writeString(this.title);
        dest.writeString(this.shortTitle);
        dest.writeString(this.description);
        dest.writeString(this.imagePath);
        dest.writeString(this.imageAbsolutePath);
        dest.writeString(this.organisationKey);
        dest.writeValue(this.isActive);
        dest.writeString(this.createAt);
    }

    public OrgReciprocal() {
    }

    protected OrgReciprocal(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.membershipLevelID = (Integer) in.readValue(Integer.class.getClassLoader());
        this.extrnalLink = in.readString();
        this.title = in.readString();
        this.shortTitle = in.readString();
        this.description = in.readString();
        this.imagePath = in.readString();
        this.imageAbsolutePath = in.readString();
        this.organisationKey = in.readString();
        this.isActive = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createAt = in.readString();
    }

    public static final Creator<OrgReciprocal> CREATOR = new Creator<OrgReciprocal>() {
        @Override
        public OrgReciprocal createFromParcel(Parcel source) {
            return new OrgReciprocal(source);
        }

        @Override
        public OrgReciprocal[] newArray(int size) {
            return new OrgReciprocal[size];
        }
    };
}


