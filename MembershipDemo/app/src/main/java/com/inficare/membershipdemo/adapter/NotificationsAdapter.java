package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.activities.MyWebViewActivity;
import com.inficare.membershipdemo.methods.EnhancedLinkMovementMethod;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Notification;
import com.inficare.membershipdemo.pojo.Notifications;

import java.util.List;

/**
 * Created by akshay.kumar on 11/11/2016.
 */

public class NotificationsAdapter extends ArrayAdapter<Notification> {

    private Context mContext;

    private int mResource;

    private List<Notification> notifications;

    private Typeface facedescription;

    public NotificationsAdapter(Context context, int resource, List<Notification> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
        notifications = objects;
        facedescription = Utility.setFaceTypeContent(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(mResource, null);
        TextView message = (TextView) convertView.findViewById(R.id.tvMessage);
        TextView date = (TextView) convertView.findViewById(R.id.tvDate);
        if (!notifications.get(position).getMessage().equals(null)) {
           /// message.setText(notifications.get(position).getMessage());
        }
        if (!notifications.get(position).getCreatedDate().equals(null)) {
            String sDate=Utility.gmtToLocalDate(notifications.get(position).getCreatedDate());
            if(sDate!=null)
            date.setText(sDate);
        }
        message.setTypeface(facedescription);
        date.setTypeface(facedescription);
        //
        String text = notifications.get(position).getMessage();
        Spannable spannable = new SpannableString( Html.fromHtml(text) );
        Linkify.addLinks(spannable, Linkify.WEB_URLS);

        URLSpan[] spans = spannable.getSpans(0, spannable.length(), URLSpan.class);
        for (URLSpan urlSpan : spans) {
            LinkSpan linkSpan = new LinkSpan(urlSpan.getURL());
            int spanStart = spannable.getSpanStart(urlSpan);
            int spanEnd = spannable.getSpanEnd(urlSpan);
            spannable.setSpan(linkSpan, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.removeSpan(urlSpan);
        }
        message.setMovementMethod(LinkMovementMethod.getInstance());
        message.setText(spannable, TextView.BufferType.SPANNABLE);
        return convertView;
    }

    private class LinkSpan extends URLSpan {
        private LinkSpan(String url) {
            super(url);
        }

        @Override
        public void onClick(View view) {
            String url = getURL();
            if (url != null) {
                mContext.startActivity(new Intent(mContext,MyWebViewActivity.class).putExtra(MyWebViewActivity.LINK_URL,url));

            }
        }
    }
}



