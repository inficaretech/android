package com.inficare.membershipdemo.methods;

import android.content.Context;

import com.inficare.membershipdemo.membership.MemberParser;

import org.json.JSONObject;

import java.util.Map;

public class MyLanguage {
    private static MyLanguage myLanguage=null;
    private MyLanguage(){}

    public static MyLanguage getInstance(){
        if (myLanguage==null){
            myLanguage=new MyLanguage();
        }
        return myLanguage;
    }

    public Map<String,JSONObject> getMyLanguage(Context context){
        MemberParser memberParser=MemberParser.createInstance(context);
       return memberParser.getLabelnTextMap(context);
    }
}
