package com.inficare.membershipdemo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.Constants;

import java.util.concurrent.Future;

public class SplashActivity extends AppCompatActivity implements ImageView.OnClickListener {

    private int SPLASH_TIME_OUT = 3000;
    ImageView ivAnywhere, ivInficare;
    mRunnable runnable;
    Future longRunningTaskFuture;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;
        ivAnywhere = (ImageView) findViewById(R.id.ivAnywhere);
        ivInficare = (ImageView) findViewById(R.id.ivInficare);
        ivAnywhere.setOnClickListener(this);
        ivInficare.setOnClickListener(this);
        initializeFirebase();

        SharedPreferences prefs = mContext.getSharedPreferences(Constants.APPRATER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        Long date_firstLaunch = prefs.getLong(Constants.DATE_FIRST_LAUNCH, 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong(Constants.DATE_FIRST_LAUNCH, date_firstLaunch);
            Log.d("SplashActivity", "date_firstlaunch: " + date_firstLaunch);
        }
        editor.commit();

        runnable = new mRunnable();
        new Handler().postDelayed(runnable, SPLASH_TIME_OUT);
/*
        ExecutorService threadPoolExecutor = Executors.newSingleThreadExecutor();
// submit task to threadpool:
        longRunningTaskFuture = threadPoolExecutor.submit(runnable);*/

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.ivAnywhere:
                //longRunningTaskFuture.cancel(true);
                runnable.kill();
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://museumanywhere.com"));
                startActivity(intent);
                finish();

                break;
            case R.id.ivInficare:
                //longRunningTaskFuture.cancel(true);
                runnable.kill();
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.inficaretech.com"));
                startActivity(intent);
                finish();
                break;
        }
    }

    class mRunnable implements Runnable {
        private boolean killMe = false;

        @Override
        public void run() {
            if (!killMe) {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }

        public void kill() {
            killMe = true;
        }
    }

    public void initializeFirebase() {
        if (FirebaseApp.getApps(this).isEmpty()) {
            FirebaseApp.initializeApp(this, FirebaseOptions.fromResource(this));
        }
        final FirebaseRemoteConfig config = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        config.setConfigSettings(configSettings);
    }
}
