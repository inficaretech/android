package com.inficare.membershipdemo.databasecreation;

import android.database.Cursor;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CBO {

    private final static String TAG = "CBO";

    private static Field[] GetFieldInfo(Class<?> objType) {
        List<Field> fields = getAllFields(new ArrayList<Field>(), objType);
        Field[] objFields = fields.toArray(new Field[fields.size()]);
        return objFields;
    }

    public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        for (Field field : type.getDeclaredFields()) {
            fields.add(field);
        }

        if (type.getSuperclass() != null) {
            fields = getAllFields(fields, type.getSuperclass());
        }

        return fields;
    }

    private static int[] GetColumnIndexs(Field[] objFields, Cursor cur) {
        int[] arrColumnIndexs = new int[objFields.length + 1];
        int intField = 0;
        if ((cur != null)) {
            for (intField = 0; intField <= objFields.length - 1; intField++) {
                arrColumnIndexs[intField] = -1;
                try {
                    arrColumnIndexs[intField] = cur
                            .getColumnIndex((objFields[intField]).getName());
                } catch (Exception e) {
                    // Util.Debug(TAG, "Exception in GetColumIndex:" +
                    // e.getMessage());
                }
            }
        }

        return arrColumnIndexs;
    }

    private static void refCursorSetValueToMethod(Cursor cur,
                                                  Field[] objFields, int[] arrColumnIndexs,
                                                  Object objObject,
                                                  int intField, Method method) throws
            IllegalAccessException,
            InvocationTargetException {
        Object value = null;
        if (objFields[intField].getType().equals(int.class)) {
            value = cur.getInt(arrColumnIndexs[intField]);
        } else if (objFields[intField].getType().equals(long.class)) {
            value = cur.getLong(arrColumnIndexs[intField]);
        } else if (objFields[intField].getType().equals(String.class)) {
            value = cur.getString(arrColumnIndexs[intField]);
        } else if (objFields[intField].getType().equals(byte[].class)) {
            value = cur.getBlob(arrColumnIndexs[intField]);
        } else if (objFields[intField].getType().equals(boolean.class)) {
            value = cur.getInt(arrColumnIndexs[intField]) == 1 ? true : false;
        } else if (objFields[intField].getType().equals(double.class)) {
            value = cur.getDouble(arrColumnIndexs[intField]);
        }
        method.invoke(objObject, value);
    }

    private static boolean checkCursorValue(Cursor cur, Field[] objFields,
                                            int[] arrColumnIndexs, int intField) {
        Object value = null;
        if (objFields[intField].getType().equals(int.class)) {
            value = cur.getInt(arrColumnIndexs[intField]);
        } else if (objFields[intField].getType().equals(long.class)) {
            value = cur.getLong(arrColumnIndexs[intField]);
        } else if (objFields[intField].getType().equals(String.class)) {
            value = cur.getString(arrColumnIndexs[intField]);
        } else if (objFields[intField].getType().equals(byte[].class)) {
            value = cur.getBlob(arrColumnIndexs[intField]);
        } else if (objFields[intField].getType().equals(boolean.class)) {
            value = cur.getInt(arrColumnIndexs[intField]) == 1 ? true : false;
        } else if (objFields[intField].getType().equals(double.class)) {
            value = cur.getDouble(arrColumnIndexs[intField]);
        }
        if (value != null) {
            return false;
        } else {
            return true;
        }
    }

    private static Object CreateObject(Class<?> objType, Cursor cur,
                                       Field[] objFields, int[] arrColumnIndexs) {
        Object objObject = null;
        try {
            objObject = objType.newInstance();
            int intField = 0;

            for (intField = 0; intField < objFields.length; intField++) {
                try {
                    Method method = objType.getMethod("set"
                                    + objFields[intField].getName(),
                            objFields[intField].getType());
                    if (arrColumnIndexs[intField] != -1) {
                        if (checkCursorValue(cur, objFields, arrColumnIndexs,
                                intField)) {
                            method.invoke(objObject, (Object) null);
                        } else {
                            refCursorSetValueToMethod(cur, objFields,
                                    arrColumnIndexs, objObject, intField,
                                    method);
                        }
                    }
                } catch (Exception e) {
                    // Util.Debug(TAG, "Exception in Create Object:" +
                    // e.getMessage());
                }
            }
        } catch (Exception e) {
            // Util.Debug(TAG, "Exception in Create Object 2:" +
            // e.getMessage());
        }

        return objObject;
    }

    public static Object FillObject(Cursor cur, Class<?> objType) {
        Object objFillObject = null;

        Field[] objFields = GetFieldInfo(objType);
        int[] arrColumnIndexs = GetColumnIndexs(objFields, cur);

        if (cur != null && cur.moveToFirst()) {
            objFillObject = CreateObject(objType, cur, objFields,
                    arrColumnIndexs);
        } else {
            objFillObject = null;
        }

        if ((cur != null)) {
            cur.close();
        }

        return objFillObject;
    }

    // public static ModifiedInfo FillModified(Cursor cursor)
    // {
    // ModifiedInfo info=new ModifiedInfo();
    //
    // if (cursor.getCount()> 0) {
    // cursor.moveToFirst();
    // info.setDate(cursor.getString(cursor
    // .getColumnIndex(ModifiedController.MOD_DATE)));
    // info.setLanguageId(cursor.getString(cursor
    // .getColumnIndex(ModifiedController.MOD_LANG)));
    // info.setTableName(cursor.getString(cursor
    // .getColumnIndex(ModifiedController.TABLE_NAME)));
    // return info;
    // }
    // else
    // return null;
    //
    // }
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static ArrayList FillCollection(Cursor cur, Class<?> objType) {
        ArrayList objFillCollection = new ArrayList();
        if (cur == null) {
            return objFillCollection;
        }

        Object objFillObject = null;
        Field[] objFields = GetFieldInfo(objType);

        int[] arrColumnIndexs = GetColumnIndexs(objFields, cur);

        if (cur.moveToFirst()) {
            do {
                objFillObject = CreateObject(objType, cur, objFields,
                        arrColumnIndexs);
                objFillCollection.add(objFillObject);
            } while (cur.moveToNext());
        }

        if ((cur != null)) {
            cur.close();
        }

        return objFillCollection;
    }


}
