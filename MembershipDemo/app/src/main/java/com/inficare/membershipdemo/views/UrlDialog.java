package com.inficare.membershipdemo.views;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.Reciprocal;

import java.util.List;

/**
 * Created by avinash.verma on 6/28/2017.
 */

public abstract class UrlDialog extends DialogFragment {

    private RecyclerView rcv_urlList;
    private Context mContext;
    private List<Reciprocal> reciprocalList;
    private TextView btnYes, tv_title;
    private TextView btnNo;

    private List<Configuration> configurationsList;

    private int clickedMember;

    private MuseumDemoPreference preferences;

    interface onSubmitListener {
        void setOnSubmitListener(String arg);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        // In order to not be too narrow, set the window size based on the screen resolution:
//        final int screen_width = context.getResources().getDisplayMetrics().widthPixels;
//        final int new_window_width = screen_width * 90 / 100;
//        WindowManager.LayoutParams layout = getWindow().getAttributes();
//        layout.width = Math.max(layout.width, new_window_width);
//        getWindow().setAttributes(layout);
//    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getActivity();

        Bundle bundle = getArguments();

        reciprocalList = bundle.getParcelableArrayList("list");


        final Dialog dialog = new Dialog(mContext);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setContentView(R.layout.url_dialog);
//        dialog.getWindow().setBackgroundDrawable(
//                new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        /*rcv_urlList = (RecyclerView) dialog.findViewById(R.id.rcv_urlList);
        rcv_urlList.setLayoutManager(new LinearLayoutManager(mContext));
        rcv_urlList.setAdapter(new UrlRecyAdapter(mContext, reciprocalList));*/

        configurationsList = MemberParser.parsConfig(mContext);

        preferences = MuseumDemoPreference.getInstance(mContext);

        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        tv_title = dialog.findViewById(R.id.tv_title);
        tv_title.setText(configurationsList.get(clickedMember).getDonationLabel());
        btnYes = (TextView) dialog.findViewById(R.id.btnYes);
        btnNo = (TextView) dialog.findViewById(R.id.btnNo);

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onNo();

            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onYes();

            }
        });


//        mButton = (Button) dialog.findViewById(R.id.button1);
//        mEditText = (EditText) dialog.findViewById(R.id.editText1);
//        mEditText.setText(text);
//        mButton.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                mListener.setOnSubmitListener(mEditText.getText().toString());
//                dismiss();
//            }
//        });
        return dialog;
    }

    public abstract void onYes();

    public abstract void onNo();
}
