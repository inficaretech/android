package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 2/23/2018.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChildMember {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ExpiryMessage")
    @Expose
    private Object expiryMessage;
    @SerializedName("ProfileImage")
    @Expose
    private String profileImage;
    @SerializedName("MemberShipCategory")
    @Expose
    private Object memberShipCategory;
    @SerializedName("MemberShipSubCategory")
    @Expose
    private Object memberShipSubCategory;
    @SerializedName("IsExpire")
    @Expose
    private String isExpire;
    @SerializedName("MemberSinceTimeSpan")
    @Expose
    private String memberSinceTimeSpan;
    @SerializedName("ValidThroughTimeSpan")
    @Expose
    private String validThroughTimeSpan;
    @SerializedName("ChildMembers")
    @Expose
    private List<Object> childMembers = null;
    @SerializedName("MembershiplevelID")
    @Expose
    private String membershiplevelID;
    @SerializedName("MembershipNameOnCard")
    @Expose
    private Object membershipNameOnCard;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("NoOfCardToPrint")
    @Expose
    private Object noOfCardToPrint;
    @SerializedName("RoamLogoPath")
    @Expose
    private String roamLogoPath;
    @SerializedName("HaveLogoPath")
    @Expose
    private Boolean haveLogoPath;
    @SerializedName("CardBanner")
    @Expose
    private String cardBanner;
    @SerializedName("searchType")
    @Expose
    private Integer searchType;
    @SerializedName("searchLabel")
    @Expose
    private String searchLabel;
    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("MembershipID")
    @Expose
    private String membershipID;
    @SerializedName("ConstituentID")
    @Expose
    private String constituentID;
    @SerializedName("PrimaryMember")
    @Expose
    private String primaryMember;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private String middleName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("MembersConstituentGender")
    @Expose
    private String membersConstituentGender;
    @SerializedName("Membersince")
    @Expose
    private String membersince;
    @SerializedName("Expirationdate")
    @Expose
    private String expirationdate;
    @SerializedName("Membershiplevel")
    @Expose
    private String membershiplevel;
    @SerializedName("Membershipleveltype")
    @Expose
    private String membershipleveltype;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Noofchildren")
    @Expose
    private String noofchildren;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Zip")
    @Expose
    private String zip;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Number")
    @Expose
    private String number;
    @SerializedName("CardNumber")
    @Expose
    private Object cardNumber;
    @SerializedName("SpouseLookupID")
    @Expose
    private String spouseLookupID;
    @SerializedName("Datechanged")
    @Expose
    private String datechanged;
    @SerializedName("Picture")
    @Expose
    private String picture;
    @SerializedName("Dateadded")
    @Expose
    private String dateadded;
    @SerializedName("LastUpdated")
    @Expose
    private Object lastUpdated;
    @SerializedName("FilePath")
    @Expose
    private String filePath;
    @SerializedName("EntityKey")
    @Expose
    private Object entityKey;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public Object getExpiryMessage() {
        return expiryMessage;
    }

    public void setExpiryMessage(Object expiryMessage) {
        this.expiryMessage = expiryMessage;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Object getMemberShipCategory() {
        return memberShipCategory;
    }

    public void setMemberShipCategory(Object memberShipCategory) {
        this.memberShipCategory = memberShipCategory;
    }

    public Object getMemberShipSubCategory() {
        return memberShipSubCategory;
    }

    public void setMemberShipSubCategory(Object memberShipSubCategory) {
        this.memberShipSubCategory = memberShipSubCategory;
    }

    public String getIsExpire() {
        return isExpire;
    }

    public void setIsExpire(String isExpire) {
        this.isExpire = isExpire;
    }

    public String getMemberSinceTimeSpan() {
        return memberSinceTimeSpan;
    }

    public void setMemberSinceTimeSpan(String memberSinceTimeSpan) {
        this.memberSinceTimeSpan = memberSinceTimeSpan;
    }

    public String getValidThroughTimeSpan() {
        return validThroughTimeSpan;
    }

    public void setValidThroughTimeSpan(String validThroughTimeSpan) {
        this.validThroughTimeSpan = validThroughTimeSpan;
    }

    public List<Object> getChildMembers() {
        return childMembers;
    }

    public void setChildMembers(List<Object> childMembers) {
        this.childMembers = childMembers;
    }

    public String getMembershiplevelID() {
        return membershiplevelID;
    }

    public void setMembershiplevelID(String membershiplevelID) {
        this.membershiplevelID = membershiplevelID;
    }

    public Object getMembershipNameOnCard() {
        return membershipNameOnCard;
    }

    public void setMembershipNameOnCard(Object membershipNameOnCard) {
        this.membershipNameOnCard = membershipNameOnCard;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Object getNoOfCardToPrint() {
        return noOfCardToPrint;
    }

    public void setNoOfCardToPrint(Object noOfCardToPrint) {
        this.noOfCardToPrint = noOfCardToPrint;
    }

    public String getRoamLogoPath() {
        return roamLogoPath;
    }

    public void setRoamLogoPath(String roamLogoPath) {
        this.roamLogoPath = roamLogoPath;
    }

    public Boolean getHaveLogoPath() {
        return haveLogoPath;
    }

    public void setHaveLogoPath(Boolean haveLogoPath) {
        this.haveLogoPath = haveLogoPath;
    }

    public String getCardBanner() {
        return cardBanner;
    }

    public void setCardBanner(String cardBanner) {
        this.cardBanner = cardBanner;
    }

    public Integer getSearchType() {
        return searchType;
    }

    public void setSearchType(Integer searchType) {
        this.searchType = searchType;
    }

    public String getSearchLabel() {
        return searchLabel;
    }

    public void setSearchLabel(String searchLabel) {
        this.searchLabel = searchLabel;
    }

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getMembershipID() {
        return membershipID;
    }

    public void setMembershipID(String membershipID) {
        this.membershipID = membershipID;
    }

    public String getConstituentID() {
        return constituentID;
    }

    public void setConstituentID(String constituentID) {
        this.constituentID = constituentID;
    }

    public String getPrimaryMember() {
        return primaryMember;
    }

    public void setPrimaryMember(String primaryMember) {
        this.primaryMember = primaryMember;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMembersConstituentGender() {
        return membersConstituentGender;
    }

    public void setMembersConstituentGender(String membersConstituentGender) {
        this.membersConstituentGender = membersConstituentGender;
    }

    public String getMembersince() {
        return membersince;
    }

    public void setMembersince(String membersince) {
        this.membersince = membersince;
    }

    public String getExpirationdate() {
        return expirationdate;
    }

    public void setExpirationdate(String expirationdate) {
        this.expirationdate = expirationdate;
    }

    public String getMembershiplevel() {
        return membershiplevel;
    }

    public void setMembershiplevel(String membershiplevel) {
        this.membershiplevel = membershiplevel;
    }

    public String getMembershipleveltype() {
        return membershipleveltype;
    }

    public void setMembershipleveltype(String membershipleveltype) {
        this.membershipleveltype = membershipleveltype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoofchildren() {
        return noofchildren;
    }

    public void setNoofchildren(String noofchildren) {
        this.noofchildren = noofchildren;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Object getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Object cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getSpouseLookupID() {
        return spouseLookupID;
    }

    public void setSpouseLookupID(String spouseLookupID) {
        this.spouseLookupID = spouseLookupID;
    }

    public String getDatechanged() {
        return datechanged;
    }

    public void setDatechanged(String datechanged) {
        this.datechanged = datechanged;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDateadded() {
        return dateadded;
    }

    public void setDateadded(String dateadded) {
        this.dateadded = dateadded;
    }

    public Object getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Object lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Object getEntityKey() {
        return entityKey;
    }

    public void setEntityKey(Object entityKey) {
        this.entityKey = entityKey;
    }

}