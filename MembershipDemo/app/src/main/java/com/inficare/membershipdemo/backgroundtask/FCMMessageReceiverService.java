package com.inficare.membershipdemo.backgroundtask;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.inficare.membershipdemo.R;

import com.inficare.membershipdemo.activities.MainActivity;
import com.inficare.membershipdemo.activities.NotificationsActivity;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;

import java.util.Map;
import java.util.Random;

public class FCMMessageReceiverService extends FirebaseMessagingService {
    android.support.v4.app.NotificationCompat.Builder notificationBuilder;

   public static String CHANNEL_ID="11";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //super.onMessageReceived(remoteMessage);
        Log.d("fcm", "received notification");

      /*  if(remoteMessage!=null&&remoteMessage.getNotification()!=null)
        sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());*/
      //sendNotification(remoteMessage.getData());
        sendNotificationWithVoice(remoteMessage.getData());

    }

    private void sendNotification(String title, String messageBody) {

        if (title != null) {

                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT);
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                    R.mipmap.app_icon);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.notification_icon)
                        .setLargeIcon(bitmap)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(1, notificationBuilder.build());

            /*else if (key == 3) {
                Intent intent = new Intent(this, MembershipOptions.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.app_icon)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(3, notificationBuilder.build());

            } else if (key == 5) {
                Intent intent = new Intent(this, MembershipOptions.class);
                intent.putExtra("userId",hostId+"");
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.app_icon)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(5, notificationBuilder.build());
            }*/

        }

    }
    private void sendNotification(Map<String, String> data) {
        try {
            if(data==null)
                return;
            String title, messageBody,sound;
            title=data.get("title");
            messageBody=data.get("body");
            sound=data.get("sound");
            if (title != null) {

                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT);
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                        R.mipmap.app_icon);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                /*if(sound!=null&&sound.equalsIgnoreCase("EcardAlert.aiff")){
                    defaultSoundUri=Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.notification_sound);
                    Log.i("defaultSoundUri",defaultSoundUri.toString());
                }*/
                NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.notification_icon)
                        .setLargeIcon(bitmap)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(1, notificationBuilder.build());

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void sendNotificationWithVoice(Map<String, String> data) {
        try{
            if(data==null)
                return;
            String title, messageBody,sound,Organisation_key;
            title=data.get("title");
            messageBody=data.get("body");
            sound=data.get("sound");
            Organisation_key=data.get("Organisation_key");
            Log.d("fcm Organisation_key", Organisation_key);
            // Create an explicit intent for an Activity in your app
            Intent intent = new Intent(this,  NotificationsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            if(Organisation_key==null){
                Organisation_key="";
            }
            intent.putExtra("mid",Organisation_key);
            intent.putExtra("isPush",true);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            notificationBuilder = new NotificationCompat.Builder(this,CHANNEL_ID)
                    .setSmallIcon(R.mipmap.notification_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                            R.mipmap.app_icon))

                    .setContentTitle(title)
                    //.setContentText(title)
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    //.setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            //.addAction(0, "Open", pendingIntent);
            //
            if(sound!=null&&sound.equalsIgnoreCase("EcardAlert.aiff")){
                try {
                    Uri notification = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notification_sound);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                notificationBuilder.setSound(defaultSoundUri);
            }
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            Random rand = new Random();

            int n = rand.nextInt(100) + 1;

            notificationManager.notify(n, notificationBuilder.build());
        }catch (Exception e){
            e.printStackTrace();
        }


    }

}