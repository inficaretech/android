package com.inficare.membershipdemo.backgroundtask;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.networks.HttpMethod;

import org.json.JSONObject;

/**
 * Created by akshay.kumar on 1/24/2017.
 */

public abstract class DownloadCardTask extends AsyncTask<String, Void, String> {
    private Context mContext;

    private static final String CN = "MemberTask";

    private ProgressDialog dialog;

    public DownloadCardTask(Context context) {
        mContext = context;
        createDialog(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.show();

    }

    @Override
    protected String doInBackground(String... params) {


        String  responsedata = HttpMethod.getInstance(mContext).doOperation(
                HttpMethod.TYPE.GET, params[0]);


        return responsedata;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        dialog.dismiss();

        try {
            if (result.contains("success")) {
                onTaskFinish(result);
            }else {
                Toast.makeText(mContext,result,Toast.LENGTH_LONG).show();
            }
//
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void createDialog(final Context context) {

        dialog = Utility.showProgressDialog(mContext, context
                .getResources().getString(R.string.msg_wait));

    }

    public abstract void onTaskFinish(String response);
}
