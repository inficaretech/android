package com.inficare.membershipdemo.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class InstructionActivity extends AppCompatActivity implements View.OnClickListener{
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    TextView tv_call_us_on,tv_call_us_on_text,tv_emailon_title,tv_emailon_title_text;
    ImageView ivCrass;

    LinearLayout ll_emailon,llParent;
    private Typeface faceHeader, faceRegular;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);

        preferences = MuseumDemoPreference.getInstance(this);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        faceHeader = Utility.setFaceTypeHeader(this);
        faceRegular = Utility.setFaceTypeContent(this);

        findView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_call_us_on_text:
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + tv_call_us_on_text.getText()));
                startActivity(callIntent);
                break;
            case R.id.tv_emailon_title_text:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{tv_emailon_title_text.getText().toString().trim()});//some@email.address
                /*intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, "mail body");*/
                startActivity(Intent.createChooser(intent, ""));
                break;
            case R.id.llParent:
                finish();
                break;
        }
    }

    private void findView(){
        tv_call_us_on = (TextView) findViewById(R.id.tv_call_us_on);

        tv_call_us_on_text = (TextView) findViewById(R.id.tv_call_us_on_text);
        tv_call_us_on.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tv_call_us_on_text.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
       /// tv_call_us_on_text.setLinkTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tv_call_us_on.setTypeface(faceRegular);
        tv_call_us_on_text.setTypeface(faceHeader);
        tv_call_us_on_text.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));

        tv_call_us_on_text.setOnClickListener(this);
        //Linkify.addLinks(tv_call_us_on_text, Linkify.ALL);
        //tv_call_us_on_text.setLinkTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tv_call_us_on_text.setText(configList.get(clickedMember).getContactNumber()+getConExt());

        tv_emailon_title = (TextView) findViewById(R.id.tv_emailon_title);
        tv_emailon_title.setTypeface(faceRegular);
        tv_emailon_title_text = (TextView) findViewById(R.id.tv_emailon_title_text);
        tv_emailon_title_text.setTypeface(faceHeader);
        //tv_emailon_title_text.setOnClickListener(this);
        tv_emailon_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tv_emailon_title_text.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tv_emailon_title_text.setLinkTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tv_emailon_title_text.setText(configList.get(clickedMember).getEmail());
        tv_emailon_title_text.setLinkTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));

        ll_emailon= (LinearLayout) findViewById(R.id.ll_emailon);
        if(configList.get(clickedMember).getEmail()==null||configList.get(clickedMember).getEmail().trim().length()==0){
            ll_emailon.setVisibility(View.GONE);
        }

        llParent= (LinearLayout) findViewById(R.id.llParent);
        llParent.setOnClickListener(this);
        //ivCrass=findViewById(R.id.ivCrass);

    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private String getConExt(){
        String conExten=configList.get(clickedMember).getContact_ext();
        if(conExten!=null&&!conExten.isEmpty())
        {
            conExten=";"+conExten;
        }else {
            conExten="";
        }
        return conExten;
    }

}
