package com.inficare.membershipdemo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.FloorAdapter;
import com.inficare.membershipdemo.backgroundtask.CacheImg;
import com.inficare.membershipdemo.backgroundtask.MyAsyncTask;
import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.membership.MemberController;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.FloorPlan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FloorMap extends AppCompatActivity implements OnItemClickListener,
        OnClickListener, SubsamplingScaleImageView.OnImageEventListener {
    private FloorAdapter adapter;
    private ListView list;
    private SubsamplingScaleImageView image;
    //private ArrayList<FloorPlan> listData;
    private Context context;
    private Typeface faceheader;
    private ViewAnimator animator;
    private ProgressBar progressBar;
    TextView tvTopStrip, tvTapToZoom, tvNoFloorPlan;
    List<File> file;
    private ProgressDialog pd;
    int clickedMember;
    List<Configuration> configList;
    private String memberID;
    private boolean progressVisible = true;
    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private TextView toolbar_title;
    private String toast_access_external_storage, pleaseWait, no_floor_plan_yet, no_internet_found;


    public static final String TAG = FloorMap.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_floorplan);
        context = this;


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.toolbar_title));
        faceheader = Utility.setFaceTypeHeader(context);
        list = (ListView) findViewById(R.id.list_floor);
        tvTapToZoom = (TextView) findViewById(R.id.tvTapToZoom);
        tvNoFloorPlan = (TextView) findViewById(R.id.tv_no_floor_plan);
        image = (SubsamplingScaleImageView) findViewById(R.id.img_floor);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        animator = (ViewAnimator) findViewById(R.id.animator);

        MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(this);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        memberID = configList.get(clickedMember).getMid();

        initLanguage();

        String title = "title";
        if (language.equalsIgnoreCase(Constants.ENGLISH)) {

        } else {
            title = title + "_" + language;
        }
        try {
            pd = Utility.showProgressDialog(this, languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...");
        } catch (JSONException e) {
            e.printStackTrace();
        }




        /*if (Build.VERSION.SDK_INT >= 23) {
                if (checkCallingPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
				PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
					.WRITE_EXTERNAL_STORAGE}, 1);
				}else {
				getFloorMap();
			}
		}*/

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTopStrip = (TextView) findViewById(R.id.tvTopStrip);
        tvTopStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tvTapToZoom.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        setUi(language);

        List<FloorPlan> results = getFloorMapFromDatabase();
        Log.d("results", results.size() + "");
        tvNoFloorPlan.setVisibility(View.VISIBLE);

        tvNoFloorPlan.setText(no_floor_plan_yet);
        if (results.size() > 0) {
            tvTapToZoom.setVisibility(View.VISIBLE);
            file = new ArrayList<>();
            for (FloorPlan floorPlan : results) {
                file.add(new File(floorPlan.getImagesdcardpath()));
            }
            setData(results, file);
            progressVisible = false;
            tvNoFloorPlan.setVisibility(View.GONE);


        } else {
            tvNoFloorPlan.setVisibility(View.VISIBLE);

            tvNoFloorPlan.setText(no_floor_plan_yet);
//            if (Utility.isNetworkAvailable(context))
//                getFloorMap();
//            else
//                Utility.showNoInternetConnectionToast(context);

        }

        if (Utility.isNetworkAvailable(context)) {

            getFloorMap();
        } else {
            Utility.showToast(context, no_internet_found);
        }
       /* else
            Utility.showNoInternetConnectionToast(context);*/


    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(context, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(context);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);

        try {

            tvTapToZoom.setText(languageMap.get(LanguageConstants.TAP_TO_ZOOM).getString(title));
            toolbar_title.setText(languageMap.get(LanguageConstants.FLOOR_MAP).getString(title));

            toast_access_external_storage = languageMap.get(LanguageConstants.STORAGE_ACCESS_MESSAGE).getString(title) + ".";
            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            no_floor_plan_yet = languageMap.get(LanguageConstants.NO_FLOOR_PLAN).getString(title);
            no_internet_found = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void setData(List<FloorPlan> listData, List<File> file) {
        try {

            ///pd.show();
            if (listData != null) {
               /* if (listData.size() == 1) {
                    list.setVisibility(View.GONE);
                }*/
            }

            adapter = new FloorAdapter(getApplicationContext(),
                    R.layout.floor_inflate, listData);
            list.setAdapter(adapter);

            ///
            //Utility.getListViewSize(list);
            list.setOnItemClickListener(this);
            if (listData == null) {

            } else {

                if (!(listData.size() == 0)) {
                    progressBar.setVisibility(View.GONE);
                    if (file.get(0).exists()) {
                        image.setImage(ImageSource.uri(file.get(0).getAbsolutePath()));
                        image.setOnImageEventListener(this);
                    } else {
                        image.setImage(ImageSource.resource(R.drawable.no_image2));
                    }
                    //ImageLoader.getInstance().displayImage(listData.get(0).getImagePath(),image);
                }
            }

        } catch (Exception e) {

        }
    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        if (file.get(arg2).exists()) {

            image.setImage(ImageSource.uri(file.get(arg2).getAbsolutePath()));
            image.setOnImageEventListener(this);
            ///pd.show();
        }


    }


    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.langChange) {
        }

    }

    private void getFloorMap() {
        try {

            MyAsyncTask task = new MyAsyncTask(this, progressVisible) {
                @Override
                public void onTaskFinish(String response) {
                    try {
                        List<FloorPlan> list = new ArrayList<>();
                        JSONObject object1 = new JSONObject(response);
                        String imagePath = object1.getString("imagepath");
                        JSONArray array = object1.getJSONArray("Result");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            FloorPlan plan = new FloorPlan(object.toString(), imagePath);
                            list.add(plan);
                        }
                        //List<FloorPlan> list=plan.getFloorPlanList();
                        if (list.size() > 0) {
                            tvTapToZoom.setVisibility(View.VISIBLE);
                            tvNoFloorPlan.setVisibility(View.GONE);

                        } else {
                            tvNoFloorPlan.setVisibility(View.VISIBLE);
                            tvNoFloorPlan.setText(no_floor_plan_yet);
                        }
                        downloadFloor(list);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            };
            String url = "";
            url = Constants.GET_FLOOR_PLAN_URL.replace("[mid]", memberID) + "&short_code=" + Utility.getLanguage(language);
            task.execute(url);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void downloadFloor(final List<FloorPlan> list) {
        if (progressVisible && list.size() > 0)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
        List<String> urlList = new ArrayList<>();
        for (FloorPlan plan : list) {
            urlList.add(plan.getImagePath());
        }
        if (urlList.size() > 0) {
            CacheImg task = new CacheImg(this, urlList) {
                @Override
                public void onTaskFinished(List<File> file) {
                    try {
                        if (progressVisible) {
                            FloorMap.this.file = file;

                            for (int i = 0; i < list.size(); i++) {
                                list.get(i).setImagesdcardpath(file.get(i).getAbsolutePath());

                                boolean value = DataProvider.getInstance(context).insertFloorImage
                                        (DBController.createInstance(context, true), list.get(i));
                                Log.d("floormap", value + "");
                            }
                            //setData(list, file);
                        } else {
                            DataProvider.getInstance(context).deleteFloorMap(DBController.createInstance(context, true), memberID);
                            for (int i = 0; i < list.size(); i++) {
                                list.get(i).setImagesdcardpath(file.get(i).getAbsolutePath());

                              /*  boolean value = DataProvider.getInstance(context)
                                        .updateFloorImage(DBController.createInstance(context, true),
                                                list.get(i));*/
                                boolean value = DataProvider.getInstance(context).insertFloorImage
                                        (DBController.createInstance(context, true), list.get(i));
                                Log.d("floormap", value + "");
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (file != null && file.size() > 0) {
                        setData(list, file);
                    }

                }
            };
            task.execute();
        }

    }

    @Override
    public void onReady() {

    }

    @Override
    public void onImageLoaded() {
        if (pd.isShowing()) {
            pd.dismiss();
        }
    }

    @Override
    public void onPreviewLoadError(Exception e) {

    }

    @Override
    public void onImageLoadError(Exception e) {

    }

    @Override
    public void onTileLoadError(Exception e) {

    }

    @Override
    public void onPreviewReleased() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length == 0) {
            return;
        }
        //For Gallery
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getFloorMap();
            } else {
                Toast.makeText(this, toast_access_external_storage,
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private List<FloorPlan> getFloorMapFromDatabase() {

//        ArrayList<FloorPlan> data = CBO.FillCollection(DataProvider.getInstance(context)
//                .getFloorImage(DBController.createInstance(context, false), memberID), FloorPlan
//                .class);
//
        Cursor cursor = DataProvider.getInstance(context)
                .getFloorImage(DBController.createInstance(context, true), memberID);
        List<FloorPlan> data = new ArrayList<>();

        if (cursor != null && cursor.getCount() > 0) {

            cursor.moveToFirst();
            do {
//                String name = cursor.getString(1);
//                //overlay.addGeoPoint( new GeoPoint( (int)(lat*1E6),  (int)(lon*1E6)));
//                System.out.println(name);
//                Log.d("str", cursor.getString(1));

                FloorPlan floorPlan = new FloorPlan();

                floorPlan.setId(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_ID)));
                floorPlan.setImage_name(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_IMAGENAME)));
                floorPlan.setTitle(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_TITLE)));
                floorPlan.setImage_size(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_IMAGE_SIZE)));
                floorPlan.setImage_type(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_IMAGETYPE)));
                floorPlan.setCreated_at(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_CREATED_AT)));
                floorPlan.setUpdated_at(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_UPDATED_AT)));
                floorPlan.setImagesdcardpath(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_IMAGE_SD_CARD_PATH)));
                floorPlan.setImageBasePath(cursor.getString(cursor.getColumnIndex(MemberController
                        .FLOORMAP_IMAGEBASE_PATH)));
                Log.d("str", cursor.getString(1));
                data.add(floorPlan);


            } while (cursor.moveToNext());
        }


        if (cursor != null)
            cursor.close();
        Log.d("data.size", data.size() + "");
        return data;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
