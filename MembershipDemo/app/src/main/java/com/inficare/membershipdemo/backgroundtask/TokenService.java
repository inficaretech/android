package com.inficare.membershipdemo.backgroundtask;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;

public class TokenService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.w("notification", refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
     Log.i("MyDebug","token:"+token);
        MuseumDemoPreference.getInstance(TokenService.this).saveString(Constants.DEVICE_TOKEN_FCM,token);
    }
}