package com.inficare.membershipdemo.methods;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.ColorInt;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.backgroundtask.MemberTaskDownload;
import com.inficare.membershipdemo.databasecreation.DBHelper;
import com.inficare.membershipdemo.interfaces.RetrofitInterface;
import com.inficare.membershipdemo.pojo.MatcherNode;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by neha.saini on 5/31/2016.
 */
public class Utility {


    public static Uri mImageCaptureUri;

    public static Typeface setFaceTypeHeader(Context ctx) {
        Typeface faceheader = null;
        faceheader = Typeface.createFromAsset(ctx.getAssets(),
                Constants.FONT_BOLD);
        return faceheader;

    }

    public static Typeface setFaceTypeContent(Context ctx) {
        Typeface faceheader = null;
        faceheader = Typeface.createFromAsset(ctx.getAssets(),
                Constants.FONT_REGULAR);
        return faceheader;

    }

    public static Typeface setFaceTypeMedium(Context ctx) {
        Typeface faceheader = null;
        faceheader = Typeface.createFromAsset(ctx.getAssets(),
                Constants.FONT_MEDIUM);
        return faceheader;

    }

    public static ProgressDialog showProgressDialog(Context mContext, String Message) {

        ProgressDialog waitdialog = new ProgressDialog(mContext);
        waitdialog.setIndeterminate(true);
//        waitdialog.setCancelable(false);
        waitdialog.setCancelable(true);
        waitdialog.setMessage(Message);

        return waitdialog;

    }

    public static ProgressDialog showProgressDialogCustom(Context mContext, String Message,
                                                          @ColorInt int color) {

        ProgressDialog waitdialog = new ProgressDialog(mContext);
//        ProgressBar spinner = new android.widget.ProgressBar(mContext, null, android.R.attr
//                .progressBarStyle);
//        spinner.getIndeterminateDrawable().setColorFilter(color, android.graphics.PorterDuff
//                .Mode.MULTIPLY);

//        ProgressBar spinner = new android.widget.ProgressBar(
//                mContext, null, android.R.attr.progressBarStyle);
//        waitdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color
//                .TRANSPARENT));
//        spinner.getIndeterminateDrawable().setColorFilter(color, android.graphics.PorterDuff
//                .Mode.MULTIPLY);

//        Drawable bgDrawable = splashProgressBar.getProgressDrawable();
//        bgDrawable.setColorFilter(Color.BLUE, android.graphics.PorterDuff.Mode.MULTIPLY);
//        splashProgressBar.setProgressDrawable(bgDrawable);

 /*       ProgressBar prog = (ProgressBar) waitdialog.findViewById(android.R.id.progress);
        prog.getIndeterminateDrawable().setColorFilter(new LightingColorFilter(Color.BLUE,
                0xFFFFFF));

        waitdialog.setContentView(prog);*/
        waitdialog.setIndeterminate(true);
        waitdialog.setCancelable(false);
        waitdialog.setMessage(Message);

        return waitdialog;

    }

    public static boolean validateSearchByName(Context context, EditText fName,
                                               EditText lName, EditText zip, EditText phone) {
        boolean flag = false;
        boolean fieldsOK = validate(new EditText[]{fName, lName, zip, phone});
        if (!fieldsOK) {
            Utility.showAlertDialog(context, "Please fill the details");

        } else {

            if (fName.getText().length() == 0) {
                Utility.showAlertDialog(context, "Please enter first name");
            } else if (lName.getText().length() == 0) {
                Utility.showAlertDialog(context, "Please enter last name");
            } else if (zip.getText().length() == 0) {
                Utility.showAlertDialog(context, "Please enter  zip code");
            } else if (phone.getText().length() == 0) {
                Utility.showAlertDialog(context, "Please enter phone number");
            } else {
                flag = true;
            }

        }
        return flag;

    }


    public static boolean validateSearchByMember(Context context,
                                                 EditText memberId, EditText lastname, String searchType,
                                                 String mid, Map<String, JSONObject> languageMap) {
        boolean flag = false;
        boolean fieldsOK = validate(new EditText[]{memberId, lastname});
        String language = context.getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, MODE_PRIVATE).getString(mid, "");
        String please_fill_details = "";
        String please_enter = "";
        String last_name = "";

        String title = "title";
        if (language.equalsIgnoreCase(Constants.ENGLISH)) {

        } else {
            title = title + "_" + language;
        }

        try {
            please_fill_details = languageMap.get(LanguageConstants.PLEASE_FILL_DETAILS).getString(title);
            please_enter = languageMap.get(LanguageConstants.PLEASE_ENTER).getString(title);
            last_name = languageMap.get(LanguageConstants.LAST_NAME).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (!fieldsOK) {
            Utility.showAlertDialog2(context, please_fill_details, mid, languageMap);

        } else {

            if (memberId.getText().length() == 0) {
                Utility.showAlertDialog2(context, please_enter, mid, languageMap);
            } else if (lastname.getText().length() == 0) {
                Utility.showAlertDialog2(context, please_enter + " " + last_name, mid, languageMap);
            } else {
                flag = true;
            }

        }

        return flag;

    }

    private static boolean validate(EditText[] fields) {
        boolean flag = false;
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().length() <= 0) {
                flag = false;
            } else {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static boolean emailValidate(Context mContext, EditText editText) {

        if (editText.getText().toString().trim() == null && !editText.getText().toString().trim()
                .isEmpty()) {
//            Utility.showToast(mContext, mContext.getString(R.string.toast_email_invalid));
            return false;
        } else
            return Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString().trim()).matches();
    }

    public static boolean isValidEmail(Context mContext, String string) {

        if (string == null || string.trim().isEmpty()) {
//            Utility.showToast(mContext, mContext.getString(R.string.toast_email_invalid));
            return false;
        } else
            return Patterns.EMAIL_ADDRESS.matcher(string).matches();
    }

    public static void showAlertDialog(Context context, String message) {
        try {
            String ok;

            ok = "Ok";

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setPositiveButton(ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            if (alert != null) {
                alert.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showAlertDialog2(Context context, String message, String membershipId, Map<String, JSONObject> languageMap) {
        try {
            SharedPreferences langPreferences = context.getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, MODE_PRIVATE);
            String language = langPreferences.getString(membershipId, "");
            String ok = "";

            String title = "title";
            if (language.equalsIgnoreCase(Constants.ENGLISH)) {

            } else {
                title = title + "_" + language;
            }

            try {
                ok = languageMap.get(LanguageConstants.OK).getString(title);


            } catch (JSONException e) {
                e.printStackTrace();
            }


            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message).setCancelable(false)
                    .setPositiveButton(ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            if (alert != null) {
                alert.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*public static boolean isNetworkAvailable(Context activity) {

        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }*/

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*public static boolean isNetworkAvailable(Context context) {
        if (hasActiveInternetConnection(context)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com")
                .openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                //Log.e(LOG_TAG, "Error checking internet connection", e);
            }
        } else {
          //  Log.d(LOG_TAG, "No network available!");
        }
        return false;
    }*/

    public static Dialog initializeDialog(Context mContext, int layout) {

        Dialog dialog = new Dialog(mContext);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(layout);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        return dialog;

    }


    // Used on Donate activity
    public static void selectFromCamera(Context context) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "Image File name");
        mImageCaptureUri = context.getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        try {
            getCameraAngle(mImageCaptureUri, context);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentPicture.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        ((Activity) context).startActivityForResult(intentPicture,
                Tags.PICK_FROM_CAMERA);

    }

    public static int getCameraAngle(Uri uri, Context ctx) throws IOException {
        int angle = 0;

        if (ctx.getResources().getConfiguration().orientation != Configuration
                .ORIENTATION_LANDSCAPE) {
            angle = 0;

        } else {
            angle = 0;
        }
        return angle;

    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = context.getContentResolver().query(contentUri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            return cursor.getString(columnIndex);
        } catch (Exception e) {
            return contentUri.getPath();
        }

    }

    //Generate QRCode and returns bitmap
    public static Bitmap generateQRBitmap(String content, Context context, int flag) {
        Bitmap bitmap = null;
        int width = 0, height = 0;
        if (flag == 0) {
            width = 70;
            height = 70;
        } else {
            width = 256;
            height = 256;
        }
        QRCodeWriter writer = new QRCodeWriter();
        try {
            Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.put(EncodeHintType.MARGIN, 0); /* default = 4 */
            BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height, hints);
            //256, 256
           /* int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();*/
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    //bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                    // guest_pass_background_color
                    bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : context
                            .getResources().getColor(R.color.white));//R.color.guest_pass_background_color
                    //Color.WHITE
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap generateQRBitmapWithWightBg(String content, Context context, int flag) {
        Bitmap bitmap = null;
        int width = 0, height = 0;
        if (flag == 0) {
            width = 70;
            height = 70;
        } else {
            width = 256;
            height = 256;
        }
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height);
            //256, 256
           /* int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();*/
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    //bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                    // guest_pass_background_color
                    bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : context
                            .getResources().getColor(R.color.white));//R.color.guest_pass_background_color
                    //Color.WHITE
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static String convertDateFormat(String strDate) {
        DateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat outputFormat = new SimpleDateFormat("MMM dd yyyy");
        Date date = null;
        try {
            date = inputFormat.parse(strDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        return outputDateStr;
    }

    public static String convertDate(String inputDateStr) {
        //10/28/2016 12:00:00 AM
        DateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
        //DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        //String inputDateStr="2013-06-24";
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        return outputDateStr;
    }

    public static void setscreenDescription(Dialog dialog) {

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
    }

    public static boolean isExpiredCard(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date strDate = null;
        Date dateCurrent = null;
        try {
            strDate = sdf.parse(date);
            Calendar c = Calendar.getInstance();


            // SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = sdf.format(c.getTime());
            dateCurrent = sdf.parse(formattedDate);
            System.out.println("Current date => " + dateCurrent);

            if (dateCurrent.compareTo(strDate) <= 0) {
                System.out.println("Current strDate => " + strDate);
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {

            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        // if (System.currentTimeMillis() <= strDate.getTime()) {
        return false;

    }

    public static boolean isNotificationFrequency(String toyBornTime, List<String> frequency) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd");

        try {
            if (frequency == null) {
                return false;
            }
            //toyBornTime="2018-03-20";
            Date oldDate = dateFormat.parse(toyBornTime);
            System.out.println(oldDate);

            Calendar c = Calendar.getInstance();
            Date currentDate = c.getTime();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (oldDate.after(currentDate)) {

                Log.e("oldDate", "is previous date");
                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                        + " hours: " + hours + " days: " + days);
                for (int i = 0; i < frequency.size(); i++) {
                    long fre = Long.parseLong(frequency.get(i));
                    if (fre >= Math.abs(days)) {
                        return true;
                    }
                }


            }

            // Log.e("toyBornTime", "" + toyBornTime);

        } catch (ParseException e) {

            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean is24hInterval(String date) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        try {
            //toyBornTime="2018-03-20";
            Date oldDate = dateFormat.parse(date);
            System.out.println(oldDate);

            Calendar c = Calendar.getInstance();
            Date currentDate = c.getTime();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (oldDate.before(currentDate)) {

                Log.e("oldDate", "is previous date");
                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                        + " hours: " + hours + " days: " + days);
                if (Math.abs(days) >= 1) {
                    return true;
                }


            }

            // Log.e("toyBornTime", "" + toyBornTime);

        } catch (ParseException e) {

            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String convertTimestampToDate(long timeStamp) {
        DateFormat objFormatter = new SimpleDateFormat("MM-dd-yyyy");
        objFormatter.setTimeZone(TimeZone.getDefault());
        Calendar objCalendar = Calendar.getInstance(TimeZone.getDefault());
        objCalendar.setTimeInMillis(timeStamp * 1000);// edit
        String result = objFormatter.format(objCalendar.getTime());
        objCalendar.clear();
        return result;
    }

    public static long getDateToTimestamp(String Date) {
        Calendar c = Calendar.getInstance();
        // System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        // String currentDate = df.format(c.getTime());
        Date currentDateObj = null;
        try {
            currentDateObj = df.parse(Date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        long currentDateTimeStamp = currentDateObj.getTime() / 1000L;
        return currentDateTimeStamp;

    }

    public static String convertTimestampToDateForMember(long timeStamp) {
        DateFormat objFormatter = new SimpleDateFormat("MMM dd, yyyy");
        objFormatter.setTimeZone(TimeZone.getDefault());
        Calendar objCalendar = Calendar.getInstance(TimeZone.getDefault());
        objCalendar.setTimeInMillis(timeStamp * 1000);// edit
        String result = objFormatter.format(objCalendar.getTime());
        objCalendar.clear();
        return result;
    }

    public static void showImageFromUrl(String url, final ImageView imgView,
                                        final ViewAnimator animator) {

        ImageLoader.getInstance().displayImage(url, imgView,
                new ImageLoadingListener() {

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        animator.setDisplayedChild(1);
                        Log.e("Started ", "");
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {
                        imgView.setImageResource(R.drawable.avatar_icon);
                        animator.setDisplayedChild(0);
                        Log.e("failed ", "");
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view,
                                                  Bitmap loadedImage) {
                        animator.setDisplayedChild(0);
                        Log.e("completed ", "");
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        imgView.setImageResource(R.drawable.avatar_icon);
                        animator.setDisplayedChild(0);
                        Log.e("Cancelled ", "");
                    }
                });

    }

    public static void setscreen(Dialog dialog) {
        if (dialog != null) {
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }

    }


    public static void selectFromGallery(Context context) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (pickPhoto.resolveActivity(context.getPackageManager()) != null) {
            ((Activity) context).startActivityForResult(pickPhoto,
                    Tags.PICK_FROM_GALLERY);
        } else {
            Toast.makeText(context, "No Gallery Found", Toast.LENGTH_LONG).show();

        }

    }


    public static Dialog initializeDialogforDescription1(Context mContext,
                                                         int layout) {
        Dialog dialog = new Dialog(mContext, R.style.DialogSlideAnim);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(layout);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

       /* WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(lp);*/

        return dialog;

    }


    public static void setscreenCard(Dialog dialog) {

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
    }


    public static int getImageOrientation(String imagePath) {
        int rotate = 0;
        try {

            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public static String getFilename(String url) {

        String[] separated = url.split("/");
        String filename = null;
        for (int i = 0; i < separated.length; i++) {
            if (i == separated.length - 1) {
                filename = separated[i];
            }
        }
        return filename;
    }

    public static String saveCroppedImage(Context context, Bitmap bmp) {
        String filepath = null;
        String root = Environment.getExternalStorageDirectory()
                + "/.CroppedImages";
        File myDir = new File(root);
        if (myDir.exists()) {
            deleteDirectory(myDir);
            myDir.mkdirs();
        } else {
            myDir.mkdirs();
        }

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String fname = ts + ".jpg";
        filepath = root + "/" + fname;
        File file = new File(myDir, fname);
        // if (file.exists())
        // file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            filepath = null;
            e.printStackTrace();
        }
        return filepath;
    }

    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    public static void hideKeyboard(Activity activity) {

        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager)
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }


    }

    public static void hideKeyboadOnView(Context context, View view) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(
                view.getWindowToken(), 0);

       /* inputManager.hideSoftInputFromWindow(activity.getCurrentFocus()
                        .getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);*/
    }

    public static String myDecript(String str) {
        String outD = "";
        //This is how to use CryptLib.java
        try {
            // String str="Fi9Z4Ot2MBppKLoctk4cJ5sqBvLK9QvO6MlMrJ8EgSB0RSbN
            // /iPDSB3u2xqbl4z2aELCX3KZQGFW6RRBvV9LkRJqxiaIgOCbPPtSHsTcAMMi6uRMwJPFoGFURp5mrOdMHSYCCdFg/NBSZkg2MsJ83VjBwpQ7clQ1roGHrp72GJkNJfQ3RmqLDiFRmgiKjiv1mVSEbMrsIl87sCOuyAdbHnaYSXKakJzxPO8SMgEo2td2BsEEo3sQdlk3Ay/epqHLlSk+PFUw7zZue0RRTYi7yq4IjQCx6rUUnOiKfI06WFnhHsXIKvrhebW9+3B8yKoMDxVGpaWjiC1X5Mq7fSWkLpS8eReZBbErouZpi9qbg6/WZ2CD2ziCJAmAjRmXSfENAq26dNtG1Oc4yNQ5oChWv+z730F20Vb7lsdourCvD30DLPD6DktuuyEMAiyNXkS32i86D6b8Fy5O39U/8p6+DKOcULt6qST3WopHVWqca0wD2cNxfMpCvet6dHMR59G02q12+nxyAFSf+m6PRUmXQcRwm4hdvvQ/6DzKJSFWjrtSJX+X0zU0XdTpLTLN5s0nvc8cEbmL7tgmrbAugaJzEEV28oXWykWq1yzm5G/rRA2pxHC8XjNTJ7hYOYGQbV6Tf7yvfIEKZhnaqyH1XXPwDWdmoPt+oewNXcizkn69V5kQn1hUoEVFh7rWth1H3F6MrNQ5u8QukK7mzO1EDrzEZ1lBiAXY5AsVcVvjaGY1GAs73RtoohQUMcjN4zUfpDJOLsqck/4C8LXXuqApL6Ev7OCkgl+vhBT0eg1atz7Hm4tOC8sAPdjuULWpqgGXOev716+wSJxSed941oS2H2HuErZHXd7KNctLMNjI47i+xfXX5F2euWs1hIWyCVODdih2zrwOWfSqRBJmcXaDNTfMCdsFXgsofYB9lY/DZrjCDJCGFQOw/Vb16jj6jMtE3isa002lh8c1Q1KGCGDzakh0ejX+HV7z0fm7097dpZlM74BpfQgp6Iz9heySEFSU/JeNhA2Bto1BYRuLbFDVwhw2y5iYLov67U1p1l0oiyfB9ULI7Qw02NX73epe+PvWm/nRZVtZBcwTLBOZ21W4U5m+qi8JJL8zfcQ8VuERTYxmiQUL1GKHRetypmGBXF3J2YsbyIvRGr7052xM9VQ4CTrBHc7SIByioBLnnhGsLisdozrlEzxZWTKsCIdPF9CaeizSt2FHDsN2ViUFdheiAvlaQSdRwDgbvFO5XskmqGKS0QSX0px5RIbXfnuYmtAX1tMI0AbzVMapaDhXAyIMtoxAhEbEdy7yLd7f+zdpdEH1KTv50NR4pcmd8vqFAI1tePuanlt+d28dM2WCGQ4Snrdc+E8xa6HHhaSkZ9wkiMIaGW9kc5mgvMb6xinGQuoAU2i5CsQEgs9GqucBn75j8zYvTaVuL4pxxmYYkFmXw0gL/fzq9PYO7awkyvW7gixnChx1oFb0sFjwfcAwR2KGtfls1PE4d19zrBZLlS2ZJcH+xxluSRZsAC458Acy7xHZ3+MFUxMn03vg86IbIrTGzsv2+4xLoHcPacVa+U4zQCq1UYsrVHVE116L8F67aeuK59D2FL5/WEL3q62q+jsjabEJEmfLMeIZ9H7Hmh/IjeCQgkCBjetke1ZaO+rS+ymEFIJBf5i+x0k2VOu7TNK0GGlVp3dBiK90SM/cwk6KtQqzH0Qr1VgUkujjmUS7g8GJCILKUf89eqlOzFgyTMw4Bl0rk735ZlG4zkf6TrKX5TS5um0lyzu0RxVZLfY2i57t7FNchhNKSa0zOGjSi4JoPbfTnj+ZO93BjfV7c+Jpp9ZErJJsCDxnS0XHmnkS5JNYzxAdb/aOZ+udriOgxFw4+E/n/36dHuvViwPV+SGgT3eOcAjAhMQny2y+4Cn34pc8iLb7H5eje0ypczbhJXnIbBjfvnWvFL3hl4lI4Rtom6Zq/eTBZzFtRuAJ9qYVhVsYq4oBGBKnnB6T3JiMni37jFX/D3dStVocN3oyZYdsNyZtGrJAg3i5OxCC9CZ0BLpxNs60ExAlJ0c3w+9BA5Lzzg+/2wxUeI2tVRjnvV2RBRTY1AvxhTT5/AE7bdz8xKV0Z5p8KXAkVdeXqklNSkeCeVIUUzqFG1QvUDqg9n8x+X/gFuuGkwwbzStdrstl6K9CBbcLTHjP5SEAS/q4OPecTkuP98eIpz/p/2KcyvSTaw5CMw5kok6rDQ1OugtPGlY6JqbWNhYXRQypX7WppNQiRsAKxsRXM9ImbvqmZExygb5r7SzeHoVqae6wbv/kdRUJ2EELTwO3vC+KM1Ue/c2omDre82k9XnKx+aCeJOhJFEvvNXCfxbozaRMYE7OtcG6PMyxV/MFqsADAQ3iU5ZlKW+cz+MIRCGDHkyEfeA06HuLxEwn6LkxMJ/rj0H6D8T3Mx/ccLdzDpUwHoO5SFDraZDMzzzJN1fpj6D8HNa1umvQrl4tV99Kyd/fnMx9rY6NgYtc3nvChikJuybQfIyHpuiojyVbPIYV6CNPD9BjBkTU/8r3Mnjq+io6yyW2II9YCR+tayDOBjPkEtvA+Lo8LFaVY06A7sipgzFEakgfk8ULVad0d/GY18fMuywJymb68s6RGrqC9yZUxHMkdozrqH8hDTHBrVKOi6zNKegQ5b7d+j0oU1VLGAsaV9reQugz111DRli7xy4omQjyCIgPAVFBMCW4+wu1eNbR4mUeGYW0M0Dp1GXMZ366d8JYc2uPjhGYGZvQbWFk4aRgoVRLJigZXbIVku+NREF0AQy9ryx7D7K4xqHuCIF7uYWeOueaLF4VAXFeS7JHkLnxuzxN/fWog6oXfkYWMCf0N9Kqa3mRzkURrflQZQcA1X1Mi0pD+XVn/iHtAHOPfYhsR9iOomEEYRtWhqDzsZufwA1xTEFpcbFt+XEBGTsGmXcK1Fvup9Qkf827OZ19Eyt0NRVSYtgt8avgRuYYfx8rvBNslund3kDDfhKV1TmUOkF1ueQQ0FuPzrUimVWgBB+BtIh9liMMzpSIlR5RriA0BuBLWw6OJZwOShPkyV3PU43RVQE8SudILSeaLIyGF8UGKRBAl/rS6wlKjCt9C04Q75N0b2eEIGMqkvNCYWGbLRXA4g99QTuTLzTsP8u1DG3W6/8pUcwmnB9em2agSbS3aZV/UQsYDfaL5gk2ZFRrAolQfMLoynrxO/F1apPpToVKSJvx+eVQCVxkqJnVe1YQfAbx7FVwpM/ysL8Y9DOZ6ab2KzviYSB+2qUGlt0IQC9RZrGEbii18iLsc8GbI1pkMwKmIuHH8ACh1L83/zmTIQgFMdbJ+PFTiocc/hrU5rho9MiYmOYtjB0FJrsYpUEZ8VQ/t0ddPddMkV5CswkgnWkpPzsjacMbcrUz+Bl1rVbrLrxlAfAVIPJdVO4oY19DrN02OJXCxliXX6X1aisDzI14R/8ratY+n1+WKcecyMmDRB5foZJHPMfIQp2+pPqwdtbJ7gAPY32GOMYZ+xbAuqOok1eP7pgoEb1qbgNCuqzkI0jYU4+NF77uTnF1NWscLLxb/8KIfWV3qjnGzoirASVea/r/RldfaZX/QYiIYwBhXaGjb/69i0S2Jjs8W/yi1VqQ93yp357hapg0APl445ANOUN2jAKOJIDOiwCqeGY074nIBPkw18TH2/ZAF7uWn8S2AXSZbFK08OFCFhHRA4jsJNqQvswJS6rAiv4OAUXnp9DIhdkYTshbnolWLyFdmDoq+Cglw6CxAo9cg5IesNBEIz5xFEHbTiyPt49R78GOdSOojR0f1gV8TF/l0WB03+8+pU+Iq9LmO137qd5K//jV4L6j1df9Fbs4aApNNq9e42FBJp3ITNo4yvW7ZC6WNm/o0JKfj1LOXOMbmUAYhAGJd+Z4ig0uhjOhC1ifw4ViCooG7iv1ZO1+X9UOSx9dOox3irgPSXBpOqVcorxmMUMMmDdJmtv7EBU1x1QqrAyoGNzHeDc7MH9doFyMkoqCCiI8fzdDvhCDzzveqEW+/Qwnvs+hy1w4NMmn6r627D6vI04g7LQ66X3CvMci7BOjbzRsXNBPoys/X8F5o4+YDZHaqV+oNSlycpnkgeK0GmkMk9MrysHnENAlecEzz4eHRYSLxzj+CbHldbNwROdtQe9dFmWIjs9hUHHHJRIps+1RG/NWaVYFPBtRYZymScXsEKOO8IbFy68EsX0r0UPhFGbC/b85EygLahMCpkzOBfjk2ASom16Q9RCInd2rqflNX2DIx1bW4vP3QgQ63LDSlqPZRobhwHfQ9hKfJNAOnQ6P4RM8/JUzRFcP8pQ3DeN8v4GxaCcaJms2jboPW+DVrhnZsJH3kVrrsuXrggzPBqC3KiFqEeC8ulg==";
            CryptLib _crypt = new CryptLib();
            String output = "";
            String plainText = "This is the text to be encrypted.";
            String key = CryptLib.SHA256("./Inficare2017!!", 31); //32 bytes = 256 bit
            String iv = "9b23e69523d2c846";//CryptLib.generateRandomIV(16); //16 bytes = 128 bit
            //output = _crypt.encrypt(plainText, key, iv); //encrypt
            outD = _crypt.decrypt(str, key, iv); //decrypt
            System.out.println("decrypted text=" + outD);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return outD;
    }

    public static String myEncrypt(String str) {
        String output = "";
        //This is how to use CryptLib.java
        try {
            CryptLib _crypt = new CryptLib();
            String plainText = "This is the text to be encrypted.";
            String key = CryptLib.SHA256("./Inficare2017!!", 31); //32 bytes = 256 bit
            String iv = "9b23e69523d2c846";//CryptLib.generateRandomIV(16); //16 bytes = 128 bit
            output = _crypt.encrypt(str, key, iv); //encrypt
            System.out.println("encrypted text=" + output);
            //outD = _crypt.decrypt(str, key,iv); //decrypt
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return output;
    }

    //Retrofit Method
    public static RetrofitInterface callRetrofit() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging).build();
// add your other interceptors …
// add logging as last interceptor
//        httpClient.addInterceptor(logging);  // <-- this is the important line!
//        String credentials = Credentials.basic("admin", "admin");


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();

        RetrofitInterface anInterface = retrofit.create(RetrofitInterface.class);

        return anInterface;
    }

    //Retrofit Method
    public static RetrofitInterface callRetrofit2() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder().readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.GET_FAQ_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();

        RetrofitInterface anInterface = retrofit.create(RetrofitInterface.class);

        return anInterface;
    }

    public static RetrofitInterface callRetrofit3() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
//                .cache(new Cache(cacheDir, cacheSize))
                .addInterceptor(logging).build();

//        httpClient.dispatcher().executorService().shutdown();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .build();

        RetrofitInterface anInterface = retrofit.create(RetrofitInterface.class);

        return anInterface;
    }


    public static String credential() {
        return Credentials.basic("Inficare", "./!nfic@re");
    }


    public static void showToast(final Context context, String text) {
//        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 50, 50);
        toast.show();
    }

    public static void showNoInternetConnectionToast(final Context context) {
//        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        Toast toast = Toast.makeText(context, context.getString(R.string.msg_no_internet), Toast
                .LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 50, 50);
        toast.show();
    }

    public static float convertDPtoPixel(Context mContext, int dpValue) {

        Resources r = mContext.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, r
                .getDisplayMetrics
                        ());
        return px;
    }

    public static String getDeviceToken(Context mContext) {
        return MuseumDemoPreference.getInstance(mContext).getString(Constants.DEVICE_TOKEN_FCM,
                "not_found");
    }

    public static void printLog(String tag, String message) {
        Log.d(tag, message);
    }


    public static void changeDrawableColor2(Context context, View inflatedView, int res) {

        StateListDrawable gradientDrawable = (StateListDrawable) inflatedView.getBackground();
        DrawableContainer.DrawableContainerState drawableContainerState = (DrawableContainer
                .DrawableContainerState) gradientDrawable.getConstantState();
        Drawable[] children = drawableContainerState.getChildren();
        GradientDrawable selectedItem = (GradientDrawable) children[0];
        GradientDrawable unselectedItem = (GradientDrawable) children[1];

        unselectedItem.setColor(res);
        selectedItem.setColor(ContextCompat.getColor(context, R.color.red));

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void setEdittextLineColor(int color, EditText editText) {

        ColorStateList colorStateList = ColorStateList.valueOf(color);
        int[] stateSet = editText.isFocused() ? new int[]{android.R.attr.state_selected} :
                new int[]{};
        colorStateList.getColorForState(stateSet, color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            editText.setBackgroundTintList(colorStateList);
        }

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                switch (view.getId()) {
                    case R.id.et_phone_no:
                        if (b) {
                            editText.setBackgroundTintList(colorStateList);
                        } else {
                            editText.setBackgroundTintList(colorStateList);
                        }
                        break;
                    case R.id.et_email:
                        if (b) {
                            editText.setBackgroundTintList(colorStateList);
                        } else {
                            editText.setBackgroundTintList(colorStateList);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    public static void changeDrawableColor(Context context, View inflatedView, String colorCode) {
        try {
            Typeface faceHeader = Utility.setFaceTypeHeader(context);
            try {
                ((Button) inflatedView).setTypeface(faceHeader);
            } catch (Exception e) {
                e.printStackTrace();
            }
            StateListDrawable gradientDrawable = (StateListDrawable) inflatedView.getBackground();
            DrawableContainer.DrawableContainerState drawableContainerState = (DrawableContainer
                    .DrawableContainerState) gradientDrawable.getConstantState();
            Drawable[] children = drawableContainerState.getChildren();
            GradientDrawable selectedItem = (GradientDrawable) children[0];
            GradientDrawable unselectedItem = (GradientDrawable) children[1];

            int res = Color.parseColor(colorCode);
            int r = Color.red(res);
            int g = Color.green(res);
            int b = Color.blue(res);
            int a = Color.alpha(res);
       /* if(r<155)
        {
           r=r+155;
        }else {
            r=r-155;
        }*/
            String newRes1 = String.format("#%02x%02x%02x%02x", a, r, g, b);
            //a= (int) (a-a*0.35);
            a = a / 2;
            String newRes2 = String.format("#%02x%02x%02x%02x", a, r, g, b, a);
            int resGradient1 = Color.parseColor(newRes1);
            int resGradient2 = Color.parseColor(newRes2);

            int colors[] = {resGradient2, resGradient1};
            unselectedItem.setColors(colors);
            selectedItem.setColor(resGradient1);
            unselectedItem.setStroke((int) convertDPtoPixel(context, 2), resGradient1);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = (int) (px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public void stcodb(Context context) {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + context.getPackageName() + "/databases/" +
                        DBHelper.DATABASE_NAME;
                String backupDBPath = "backupname.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
    }

    public static String getHeaderData() {
        return Credentials.basic("admin", "admin");
    }

    private static class MyAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public static String getDeviceId(Context context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure
                .ANDROID_ID);
        return deviceId;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        } else {
            return manufacturer + " " + model;
        }
    }

    public static void setInputTextLayoutColor(EditText editText, @ColorInt int color) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            changeEditTextUnderlineColor(editText, color);
        }
//        changeEditTextUnderlineColor(editText, color);
//        setCursorColor(editText, color);
        if (editText.getParent() instanceof TextInputLayout) {
            TextInputLayout til = (TextInputLayout) editText.getParent();
            try {
                Field fDefaultTextColor = TextInputLayout.class.getDeclaredField
                        ("mDefaultTextColor");
                fDefaultTextColor.setAccessible(true);
                fDefaultTextColor.set(til, new ColorStateList(new int[][]{{0}}, new int[]{color}));

                Field fFocusedTextColor = TextInputLayout.class.getDeclaredField
                        ("mFocusedTextColor");
                fFocusedTextColor.setAccessible(true);
                fFocusedTextColor.set(til, new ColorStateList(new int[][]{{0}}, new int[]{color}));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public static void changeEditTextUnderlineColor(final EditText editText, @ColorInt final int
            color) {

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    editText.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_IN);
                else
                    editText.getBackground().clearColorFilter();

            }
        });

    }

    public static void setCursorColor(EditText view, @ColorInt int color) {
        try {
            // Get the cursor resource id
            Field field = TextView.class.getDeclaredField("mCursorDrawableRes");
            field.setAccessible(true);
            int drawableResId = field.getInt(view);

            // Get the editor
            field = TextView.class.getDeclaredField("mEditor");
            field.setAccessible(true);
            Object editor = field.get(view);

            // Get the drawable and set a color filter
            Drawable drawable = ContextCompat.getDrawable(view.getContext(), drawableResId);
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
            Drawable[] drawables = {drawable, drawable};

            // Set the drawables
            field = editor.getClass().getDeclaredField("mCursorDrawable");
            field.setAccessible(true);
            field.set(editor, drawables);
        } catch (Exception ignored) {
        }
    }

    public static void syncDataOnAppUpgrade(final Context context, Map<String, Object> map) {
        PackageInfo packageInfo = null;
        int versionCode = 0;

        final String orgKey = map.get("organisation_key").toString();

        final MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(context);
        String saveOrgKey = preferences.getString(orgKey, "");

        if (saveOrgKey.equalsIgnoreCase(orgKey)) {
            return;
        }

        try {
            packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);

            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (versionCode == 20) {
            if (Utility.isNetworkAvailable(context)) {

                MemberTaskDownload taskDownload = new MemberTaskDownload(context, map) {
                    @Override
                    public void onTaskFinish(String data) {
                        try {
                            JSONObject object = new JSONObject(data);
                            String errorCode = object.getString("ErrorCode");
                            if (errorCode.equalsIgnoreCase("200")) {
                                preferences.saveString(orgKey, orgKey);
                            } else {
                                //Utility.showAlertDialog(context, object.getString("Message"));

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                };

                taskDownload.execute(Constants.DOWNLOAD_CARD_URL);
            } else {
//                        TagInfo infoTag = GetTagData.getTag(mContext);
//                        Utility.showAlertDialog(mContext,
//                                infoTag.getAlertNoInternet());
                Utility.showAlertDialog(context, context.getResources().getString(R.string
                        .msg_no_internet));
            }
        }
    }


    public static void changeMenuIconDrawable(Toolbar toolbar, Context context, @ColorInt int
            color) {
//        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.menu);
//        toolbar.setOverflowIcon(drawable);
        Drawable drawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.menu,
                null);

/*
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable.mutate(), color);
            toolbar.setOverflowIcon(drawable);
        }*/
        Log.d("colorcode", color + "");
        if (drawable != null) {
            Drawable newIcon = drawable.mutate();
            DrawableCompat.setTint(drawable.mutate(), Color.parseColor("#FF4500"));
//            newIcon.setColorFilter(Color.parseColor("#FF4500"), PorterDuff.Mode.MULTIPLY);
//            toolbar.setOverflowIcon(newIcon);
        }
//        setOverflowButtonColor(context,color);
//        drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
//        toolbar.setOverflowIcon(drawable);
    }


    public static void changeMenuIconDrawable(MenuItem menu, Context context, @ColorInt int
            color) {
        Drawable drawable = menu.getIcon();
//        toolbar.setOverflowIcon(drawable);

        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable.mutate(), color);
            menu.setIcon(drawable);
        }
      /*  Log.d("colorcode", color + "");
        if (drawable != null) {
            Drawable newIcon = drawable.mutate();
            DrawableCompat.setTint(drawable.mutate(), Color.parseColor("#FF4500"));
//            newIcon.setColorFilter(Color.parseColor("#FF4500"), PorterDuff.Mode.MULTIPLY);
//            toolbar.setOverflowIcon(newIcon);
        }*/
//        setOverflowButtonColor(context,color);
//        drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
//        toolbar.setOverflowIcon(drawable);
    }

    public static void setOverflowButtonColor(final Context activity, @ColorInt final int
            color) {
        final String overflowDescription = activity.getString(R.string
                .abc_action_menu_overflow_description);
        final ViewGroup decorView = (ViewGroup) ((Activity) activity).getWindow().getDecorView();
        final ViewTreeObserver viewTreeObserver = decorView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final ArrayList<View> outViews = new ArrayList<View>();
                decorView.findViewsWithText(outViews, overflowDescription,
                        View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
                if (outViews.isEmpty()) {
                    return;
                }
                AppCompatImageView overflow = (AppCompatImageView) outViews.get(0);
                overflow.setColorFilter(color);
//                removeOnGlobalLayoutListener(decorView,this);
            }
        });
    }

    public static Spanned changeFontColorForHTML(String text, int mycolor) {
        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: " +
                "url(\"file:///android_asset/font/description_font.ttf\")}body {font-family: " +
                "MyFont;font-size: medium;text-align: justify;}</style></head><body>";
        String pas = "</body></html>";
        String myHtmlString = pish + text + pas;

        return Html.fromHtml("<font color='" + mycolor + "'>" + text + "</font>");
    }


    public static void setEditTextFocusWithEnd(EditText editText) {
        editText.requestFocus();
        editText.setSelection(editText.length());
    }

    public static String gmtToLocalDate(String sDate) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(sDate);
            df.setTimeZone(TimeZone.getDefault());
            String formattedDate = df.format(date);
            return formattedDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File renameFileToNew(File location, String fileName, String extention) {
        File file = new File(location, fileName + extention);

        if (file.exists()) {
            File to;

            for (int i = 1; i < 1000; i++) {

                to = new File(location, fileName + "(" + i + ")" + extention);
                if (!to.exists()) {
                    return to;
                }
            }
        }

        return file;
    }

    public static String getFileNameFromUrl(final String url) {
        final String fileName = url.substring(url.lastIndexOf('/') + 1,
                url.length());
        return fileName;
    }

    public static void sendFileToMailIntent(Context context, final String filePath, String
            subject) {
        // For More Android Methods www.androidwarriors.com
        Uri path = Uri.fromFile(new File(filePath));
        //This is added to show only Email apps - part 1
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        // set the type to 'email'
        emailIntent.setType("vnd.android.cursor.dir/email");

        //This is added to show only Email apps - part 2
        String recepientEmail = ""; // either set to destination email or leave empty
        emailIntent.setData(Uri.parse("mailto:" + recepientEmail));

        // the attachment
        emailIntent.putExtra(Intent.EXTRA_STREAM, path);
        // the mail subject
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "" + subject);
        if (emailIntent.resolveActivityInfo(context.getPackageManager(), 0) != null) {
//            context.startActivity(emailIntent);
            context.startActivity(Intent.createChooser(emailIntent, "Send email..."));

        } else {
            // if you reach this place, it means there is no any Email Applicaition Found
            Utility.showToast(context, "No Email application found! Please download one");
        }
    }

    public static String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String formattedDate = null;
        try {
            Calendar c = Calendar.getInstance();


            // SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            formattedDate = sdf.format(c.getTime());
            System.out.println("Current date => " + formattedDate);


        } catch (Exception e) {

            e.printStackTrace();
        }
        // if (System.currentTimeMillis() <= strDate.getTime()) {
        return formattedDate;

    }

    public static String genRef(String orgName) {

        String[] nameArr = orgName.split(" ");
        if (nameArr.length == 2) {
            orgName = nameArr[0].substring(0, 2) + nameArr[1].substring(0, 2);
        } else if (nameArr.length == 3) {
            orgName = nameArr[0].substring(0, 2) + nameArr[1].substring(0, 1) + nameArr[2].substring(0, 1);
        } else if (nameArr.length == 4) {
            orgName = nameArr[0].substring(0, 1) + nameArr[1].substring(0, 1) + nameArr[2].substring(0, 1) + nameArr[3].substring(0, 1);
        } else if (nameArr.length > 4) {
            orgName = nameArr[0].substring(0, 1) + nameArr[1].substring(0, 1) + nameArr[2].substring(0, 1) + nameArr[3].substring(0, 1);
        } else {
            orgName = orgName.substring(0, 4);
        }
        String cuDate = getCurrentDate();
        Random random = new Random();
        int id = random.nextInt(100000);
        orgName = orgName + "-" + cuDate + "-" + id;
        return orgName.toUpperCase();
    }

    public static String getLanguage(String language) {

        if (language.equalsIgnoreCase(Constants.ENGLISH)) {
            return "";
        } else {
            return language;
        }
    }

    public static String getTitle(String language) {
        String title = "title";

        if (language.equalsIgnoreCase(Constants.ENGLISH) || language.isEmpty()) {

        } else {
            title = title + "_" + language;
        }
        return title;
    }


    public static String getCurrentLanguage(Context context, String mid) {
        return context.getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, MODE_PRIVATE).getString(mid, "");
    }

    public static <T> T getAppVersion(Context context,int type){
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            if(type==0){
                return (T) pInfo.versionName;
            }else {
                return (T) new Integer(pInfo.versionCode);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String parseEmailFromHtml(String link){
        String email=null;
        String data[]=link.split(":");
        if(data.length>1){
            String part2[]=data[1].split("\\s+");
            if(part2.length>0){
                email= part2[0];
            }
        }
        return email;
    }
    public static String parsePhoneFromHtml(String link){
        String phone=null;
        String data[]=link.split(":");
        if(data.length>1){
            phone=data[1];
        }
        return phone;
    }
    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static List<MatcherNode> extractNumber(String data){
        List<MatcherNode>matcherNodes=new ArrayList<>();
        try{
            if(data.contains("tel:"))
                return matcherNodes;
            String regex = "[0-9]{10}|[0-9-().+\\s]{15}|[0-9-().+\\s]{13}";

            Pattern pattern = Pattern.compile(regex);

            Matcher matcher = pattern.matcher(data);
            // System.out.println(email +" : "+ matcher.matches());
            while(matcher.find()) {
                MatcherNode matcherNode=new MatcherNode(matcher.start(),(matcher.end()-1),matcher.group());
                matcherNodes.add(matcherNode);
                System.out.print(matcher.group());
                System.out.println( " "+matcher.start()+","+(matcher.end()-1));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return matcherNodes;
    }
}
