package com.inficare.membershipdemo.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.edmodo.cropper.CropImageView;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.zxing.BarcodeFormat;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.CardAdapter;
import com.inficare.membershipdemo.backgroundtask.MemberTaskInitial;
import com.inficare.membershipdemo.backgroundtask.UploadMemberImageTask;
import com.inficare.membershipdemo.databasecreation.CBO;
import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.interfaces.ImageChangeListner;
import com.inficare.membershipdemo.interfaces.SetCardPosition;
import com.inficare.membershipdemo.membership.MemberController;
import com.inficare.membershipdemo.membership.MemberInfo;
import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Tags;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import me.xiaopan.barcodescanner.EncodeUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MemberShipActivity extends AppCompatActivity implements View.OnClickListener,
        SetCardPosition {

    transient private Typeface faceheader;
    transient private Typeface faceRegular;
    private TextView header;
    private static Context ctx;
    private Dialog dialog;
    private ImageView envalope;
    ImageView dummyImage, ivLogoOnEnvelop;
    ImageView imageCross;
    ViewPager pager;
    CardAdapter adapter;
    boolean flagMultipleTap = false;
    private int pos;
    MemberList list;
    MemberList list2;
    ViewAnimator animator;
    ImageView profimage;
    CropImageView imageCrop;
    View header_line;
    String expiryDate = "";
    String expiryMsg = "";
    transient private PendingAction pendingAction = PendingAction.LOGIN;
    //  transient private UiLifecycleHelper uiHelper;
    boolean flag_fb = false;
    private static final String PENDING_ACTION_BUNDLE_KEY = "com.example" +
            ".samplefbshare:PendingAction";
    private boolean flag_userdialog;
    private Dialog fb_option_dialog;
    String getid;
    private boolean flag_remove_acnt;
    private String please_wait = "Please wait...";

    public enum PendingAction {
        LOGIN, POST_PHOTO
    }

    CallbackManager callbackManager;
    private Toolbar toolbar;
    private TextView tb_title;
    private int TYPE_GALLERY = 1;
    private int TYPE_CAMERA = 2;
    private int TYPE_FB = 3;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    private LinearLayout ll_renew;
    private TextView tv_renew;
    private boolean isInstructionShown;
    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private TextView toolbar_title;
    private boolean isDataUpdate=false;
    private Context mContext;
    private boolean isCardIsOpen;

    public static final String TAG = MemberShipActivity.class.getSimpleName();
    private String gallery_access_message, camera_access_message, storage_access_message, no_internet_message, ok, notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookSdkInitialize();
        setContentView(R.layout.activity_member_ship);
        preferences = MuseumDemoPreference.getInstance(this);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        ctx = this;
        mContext = this;

        initLanguage();
        boolean flag = getIntent().getBooleanExtra(Constants.FLAG_STACK_CLEAR,
                false);
        if (flag) {
            sendBroadcast(new Intent(ctx, FindMembershipCard.BroadcastFind.class));
            sendBroadcast(new Intent(ctx, VerifyMembership.BroadcastVerify.class));
        }
        getView();
        initMenu();

        setUi(language);

        String expireMsgText = "";
        String membershipIdText = "";
        String LastNameText = "";
        String numberText = "";
        Cursor cursor = DataProvider.getInstance(ctx).getMemberDetails(DBController
                .createInstance(ctx, false), 0);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            expireMsgText = cursor.getString(10);
            expiryDate = cursor.getString(12);
            cursor.moveToNext();
        }
        String expiryMsg = getIntent().getStringExtra(MembershipOptions.EXPIRY_MSG);

        /*if (expiryMsg != null && (!expiryMsg.isEmpty())) {
            ShowDialog(expiryMsg);
        } else {
            //Toast.makeText(ctx,expireMsgText,Toast.LENGTH_LONG).show();

        }*/
        cursor.close();

        envelopByPass();
        /*String pass = configList.get(clickedMember).getIsEnvelopBypass();
        if (pass != null && pass.equalsIgnoreCase("1")) {
            isCardIsOpen=true;
            envelopByPass();
        }*/

      /*  try {
            if(!isDataUpdate){
                isDataUpdate=true;
                checkExpiryMsg(language);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);


        try {
            tb_title.setText(languageMap.get(LanguageConstants.SHOW_MEMBERSHIP_CARD).getString(title));
            gallery_access_message = languageMap.get(LanguageConstants.GALLERY_ACCESS_MESSAGE).getString(title);
            camera_access_message = languageMap.get(LanguageConstants.CAMERA_ACCESS_MESSAGE).getString(title);
            storage_access_message = languageMap.get(LanguageConstants.STORAGE_ACCESS_MESSAGE).getString(title);
            no_internet_message = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);
            notification = languageMap.get(LanguageConstants.NOTIFICATION).getString(title);
            ok = languageMap.get(LanguageConstants.OK).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    class DownloadFbPic extends AsyncTask<String, Void, Bitmap> {
        String userId;

        ProgressDialog pDialog;

        public DownloadFbPic(String userId) {
            this.userId = userId;
            pDialog = new ProgressDialog(ctx);
            pDialog.setMessage("PleaseWait..");
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String url = "http://graph.facebook.com/" + userId
                    + "/picture?type=large";
            Bitmap bmp = downloadBitmap(url);
            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            String path = Utility.saveCroppedImage(ctx, result);
            pDialog.dismiss();
            if (result == null) {
                Utility.showAlertDialog(ctx, "Something went wrong");

            } else {
                new UploadMemberImageTask(ctx, path, list.get(pos)
                        .getConstituentID(), Utility.getFilename(path), animator,
                        profimage).execute();
            }

        }

    }


    private Bitmap downloadBitmap(String url) {
        // initilize the default HTTP client object
        /*
        final DefaultHttpClient client = new DefaultHttpClient();

        // forming linearLayout HttoGet request
        final HttpGet getRequest = new HttpGet(url);
        try {

            HttpResponse response = client.execute(getRequest);

            // check 200 OK for success
            final int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode
                        + " while retrieving bitmap from " + url);
                return null;

            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    // getting contents from the stream
                    inputStream = entity.getContent();

                    // decoding stream data back into image Bitmap that android
                    // understands
                    final Bitmap bitmap = BitmapFactory
                            .decodeStream(inputStream);

                    return bitmap;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (Exception e) {
            // You Could provide linearLayout more explicit error message for IOException
            getRequest.abort();
            return null;
            // Log.e("ImageDownloader", "Something went wrong while"
            // + " retrieving bitmap from " + url + e.toString());
        }
*/
        return null;
    }

    public void ShowDialog(String expireMsgText) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set title
        alertDialogBuilder.setTitle(notification);
        // set dialog message
        alertDialogBuilder
                .setMessage(expireMsgText)
                .setCancelable(false)
                .setPositiveButton(ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        if(isCardIsOpen)
                        envelopByPass();
                    }
                });
                /*.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private void facebookSdkInitialize() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new
                FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {


                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        response.getError();
                                        try {
                                            if (android.os.Build.VERSION.SDK_INT > 9) {
                                                StrictMode.ThreadPolicy policy = new StrictMode
                                                        .ThreadPolicy.Builder().permitAll().build();
                                                StrictMode.setThreadPolicy(policy);
                                                String profilePicUrl = object.getJSONObject
                                                        ("picture")
                                                        .getJSONObject("data").getString("url");

                                                URL fb_url = new URL(profilePicUrl);//small |
                                                // noraml | large
                                                HttpsURLConnection conn1 = (HttpsURLConnection)
                                                        fb_url
                                                                .openConnection();
                                                HttpsURLConnection.setFollowRedirects(true);
                                                conn1.setInstanceFollowRedirects(true);
                                                Bitmap fb_img = BitmapFactory.decodeStream
                                                        (conn1.getInputStream());

                                                String path = Utility.saveCroppedImage(ctx, fb_img);
                                                new UploadMemberImageTask(ctx, path, list.get(pos)
                                                        .getConstituentID(), Utility.getFilename
                                                        (path),
                                                        animator,
                                                        profimage).execute();

//                                        profimage.setImageBitmap(fb_img);
//                                        animator.setDisplayedChild(0);
                                            }
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,picture");
                        request.setParameters(parameters);
                        request.executeAsync();


                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {

                    }
                });
    }


    private void getView() {
        faceheader = Utility.setFaceTypeHeader(ctx);
        faceRegular = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Medium.ttf");
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        ///toolbar.setNavigationIcon(upArrow);

        RelativeLayout rlEnvelop=findViewById(R.id.rlEnvelop);
        rlEnvelop.setVisibility(View.GONE);

        //toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_button));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        final LinearLayout llback = (LinearLayout) findViewById(R.id.llback);
//        final ImageView imgmore = (ImageView) findViewById(R.id.imgmore);
        // membershipEnv = (TextView) findViewById(R.id.membership_env);
        // imgmore.setVisibility(View.GONE);
        dummyImage = (ImageView) findViewById(R.id.envalope2);
        envalope = (ImageView) findViewById(R.id.envalope1);
        envalope.setOnClickListener(this);
//        header = (TextView) findViewById(R.id.txtHeading);
//        header.setTypeface(faceheader, Typeface.BOLD);
        // membershipEnv.setTypeface(faceRegular);
        // header.setText("MEMBERSHIP");
//        llback.setOnClickListener(this);
//        imgmore.setOnClickListener(this);
//        View v = findViewById(R.id.camerainclude);
//        Utility.setHeaderLayout(this, v);
        header_line = findViewById(R.id.header_line);
        header_line.setBackgroundColor(Color.parseColor(configList.get(clickedMember)
                .getColorCode()));
        ivLogoOnEnvelop = (ImageView) findViewById(R.id.ivLogoOnEnvelop);
       /* Picasso.with(this)
                .load(configList.get(clickedMember).getImagePath()+"/"+configList.get
                (clickedMember).getLogo())
                   *//* .placeholder(R.drawable.logo)*//*
                .into(ivLogoOnEnvelop);
*/
        List<String> urlList = new ArrayList<>();
        urlList.add(configList.get(clickedMember).getImagePath() + "/" + configList.get
                (clickedMember).getLogo());
       /* DownloadImage task=new DownloadImage(this,urlList) {
            @Override
            public void onTaskFinished(List<File> file) {
                if(file!=null) {
                    try {
                        ivLogoOnEnvelop.setImageURI(Uri.fromFile(file.get(0)));
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

            }
        };
        task.execute();*/
        ImageLoader.getInstance().displayImage(urlList.get(0), ivLogoOnEnvelop);
        downloadImage();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.envalope1:
               /* if (!Utility.isExpiredCard(expiryDate)) {
                    ExpiryDialog ex = new ExpiryDialog(this, 0);
                    ex.setCancelable(false);
                    ex.show();
                    return;
                }*/

                if (!flagMultipleTap) {
                    isCardIsOpen=true;
                    flagMultipleTap = true;
                    dummyImage.setVisibility(View.VISIBLE);
                    Animation animation1 = AnimationUtils.loadAnimation(
                            getApplicationContext(), R.anim.slide_out_top);
                    // getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    dummyImage.startAnimation(animation1);

                    // Show status bar

                    animation1.setAnimationListener(new Animation.AnimationListener() {

                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                            dummyImage.setVisibility(View.INVISIBLE);
                            dialog = Utility.initializeDialogforDescription1(ctx,
                                    R.layout.popup_card);
                            findTicketViews(dialog);
                            dialog.show();
                            Utility.setscreenCard(dialog);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    imageCross.setVisibility(View.VISIBLE);
                                    Animation fadeIn = new AlphaAnimation(0, 1);
                                    fadeIn.setInterpolator(new DecelerateInterpolator()); // add
                                    // this
                                    fadeIn.setDuration(500);
                                    imageCross.startAnimation(fadeIn);
                                    flagMultipleTap = false;
                                    // getWindow().clearFlags(
                                    // WindowManager.LayoutParams.FLAG_FULLSCREEN);
                                }
                            }, 600);


                        }
                    });

                }

                break;
            case R.id.cross_card:
                dialog.dismiss();
                isCardIsOpen=false;
                onBackPressed();
                break;
            case R.id.arrow_left:
                int pos = pager.getCurrentItem();
                if (pos > 0) {
                    pager.setCurrentItem(pos - 1);
                }
                break;
            case R.id.arrow_right:
                int pos1 = pager.getCurrentItem();
                if (pos1 < adapter.getCount() - 1) {
                    pager.setCurrentItem(pos1 + 1);
                }
                break;

            case R.id.crop_button:
                // Bitmap bmp = image.getCroppedImage();
                // String path = Utility.saveCroppedImage(ctx, bmp);
                // new UploadMemberImageTask(ctx, path, list.get(this.pos)
                // .getConstitID(), Utility.getFilename(path), animator,
                // profimage).execute();
                break;

            case R.id.use_original_button:

                break;
//            case R.id.img_popup_cross:
//                fb_option_dialog.dismiss();
//                break;
//            case R.id.txt_countinue:
//                fb_option_dialog.dismiss();
//                if (getid != null) {
//                    new DownloadFbPic(getid).execute();
//                }
//                break;
//            case R.id.txt_remove_account:
//                fb_option_dialog.dismiss();
//                final Session sessionclear = Session.getActiveSession();
//                if (sessionclear != null) {
//                    sessionclear.closeAndClearTokenInformation();
//                    sessionclear.close();
//                }
//                flag_remove_acnt = true;
//                performPublish(PendingAction.LOGIN, true);
//                break;
            default:
                break;
        }
    }

    private void envelopByPass() {
        dummyImage.setVisibility(View.INVISIBLE);
        dialog = Utility.initializeDialogforDescription1(ctx,
                R.layout.popup_card);
        findTicketViews(dialog);
        dialog.show();
        Utility.setscreenCard(dialog);
        //
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                }
                return true;
            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               /// imageCross.setVisibility(View.VISIBLE);
                Animation fadeIn = new AlphaAnimation(0, 1);
                fadeIn.setInterpolator(new DecelerateInterpolator()); // add
                // this
                fadeIn.setDuration(500);
                imageCross.startAnimation(fadeIn);
                flagMultipleTap = false;
                // getWindow().clearFlags(
                // WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }
        }, 600);
    }

    private void initMenu() {

//        morePopup = new MorePopup(this, this);
//        morePopup.setBackgroundPosition(9);
//        morePopup.setSelectedId(R.id.rl_member);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //   uiHelper.onResume();
        if (!isInstructionShown && clickedMember!=-1&&configList.get(clickedMember).getMid().equalsIgnoreCase
                ("TXVzZXVtQW55V2hlcmU=")) {
            Intent intent = new Intent(this, InstActivityMain.class);
            intent.putExtra(InstActivityMain.INST_DATA, getResources().getString(R.string.once_downloaded_card_stay));
            startActivity(intent);
            isInstructionShown = true;
        }
    }

    private Bitmap getBarcodeBitmap(String content) {
        Bitmap bmp = null;
        try {
            bmp = EncodeUtils.encode(content, BarcodeFormat.CODE_39, null, 250,
                    100, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;

    }

    private void findTicketViews(Dialog dialog) {
        imageCross = (ImageView) dialog.findViewById(R.id.cross_card);
        imageCross.setVisibility(View.INVISIBLE);
        // ImageView barcodeImage = (ImageView) dialog
        // .findViewById(R.id.barcode_image);
        if (list != null) {
            list.clear();
        }

        list = setData();


        Collections.reverse(list);
        pager = (ViewPager) dialog.findViewById(R.id.mid_layout);
        try {
            adapter = new CardAdapter(ctx, list, this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pager.setAdapter(adapter);

        final ImageView arrow_left = (ImageView) dialog
                .findViewById(R.id.arrow_left);
        Button btnRenew=dialog.findViewById(R.id.btnRenew);
        Utility.changeDrawableColor(mContext, btnRenew, configList.get(clickedMember).getColorCode());
        renew(btnRenew);
        final ImageView arrow_right = (ImageView) dialog
                .findViewById(R.id.arrow_right);
        if (list != null) {
            if (list.size() == 1) {
                arrow_left.setVisibility(View.INVISIBLE);
                arrow_right.setVisibility(View.INVISIBLE);
            } else {
                arrow_left.setVisibility(View.INVISIBLE);
                arrow_right.setVisibility(View.VISIBLE);
            }

        }
        arrow_left.setOnClickListener(this);
        arrow_right.setOnClickListener(this);
        pager.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageSelected(int pos) {
                if (pos == 0) {
                    arrow_left.setVisibility(View.INVISIBLE);
                } else {
                    arrow_left.setVisibility(View.VISIBLE);
                }

                if (pos == adapter.getCount() - 1) {
                    arrow_right.setVisibility(View.INVISIBLE);
                } else {
                    arrow_right.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

        imageCross.setOnClickListener(this);
    }

    private MemberList setData() {

        @SuppressWarnings("unchecked")
        ArrayList<MemberInfo> info = CBO.FillCollection(
                DataProvider.getInstance(ctx).getMemberDetails(DBController.createInstance(ctx,
                        false), 0), MemberInfo.class);

        MemberList listMember = new MemberList();

        listMember.addAll(info);

        //
        // for (int i = 0; i < listMember.size(); i++) {
        // for (int j = 0; j < listMember.get(i).getSpouseList().size(); j++) {
        // MemberInfo infoSpouse = new MemberInfo();
        // infoSpouse.setFirstName(listMember.get(i).getSpouseList()
        // .get(j).getFirstName());
        // infoSpouse.setLastName(listMember.get(i).getSpouseList().get(j)
        // .getLastName());
        // infoSpouse.setPhone(listMember.get(i).getSpouseList().get(j)
        // .getPhone());
        // infoSpouse.setMemberSinceTimeSpan(listMember.get(i)
        // .getSpouseList().get(j).getMemberSinceTimeSpan());
        // infoSpouse.setValidThroughTimeSpan(listMember.get(i)
        // .getSpouseList().get(j).getValidThroughTimeSpan());
        // infoSpouse.setMembershipID(listMember.get(i).getSpouseList()
        // .get(j).getMembershipID());
        // infoSpouse.setMembershipType(listMember.get(i).getSpouseList()
        // .get(j).getMembershipType());
        // infoSpouse.setMemberImage(listMember.get(i).getSpouseList()
        // .get(j).getMemberImage());
        //
        // infoSpouse.setCity(listMember.get(i).getSpouseList().get(j)
        // .getCity());
        // infoSpouse.setState(listMember.get(i).getSpouseList().get(j)
        // .getState());
        // infoSpouse.setZip(listMember.get(i).getSpouseList().get(j)
        // .getZip());
        // listMember.add(infoSpouse);
        //
        // }
        //
        // }
        // MemberInfo info1 = new MemberInfo();
        // info1.setName("Rachel and K.C. Warner");
        // info1.setAddress("Columbia Falls, MT 59912");
        // info1.setProfilePic(R.drawable.images);
        // info1.setSince("July 28, 2015");
        // info1.setThrough("June 28, 2016");
        // info1.setType("FAMILY MEMBER");
        // info1.setBarCodeNo(88028252);
        //
        // MemberInfo info2 = new MemberInfo();
        // info2.setName("Dr. Marco Aprili");
        // info2.setAddress("Columbia Falls, MT 59912");
        // info2.setProfilePic(R.drawable.prof2);
        // info2.setSince("Aug 25, 2015");
        // info2.setThrough("June 25, 2016");
        // info2.setType("FAMILY MEMBER");
        // info2.setBarCodeNo(88028031);
        //
        // MemberInfo info3 = new MemberInfo();
        // info3.setName("Michael asmar");
        // info3.setAddress("Columbia Falls, MT 59912");
        // info3.setProfilePic(R.drawable.prof3);
        // info3.setSince("Jan 24, 2015");
        // info3.setThrough("June 24, 2016");
        // info3.setType("FAMILY MEMBER");
        // info3.setBarCodeNo(88048071);
        //
        // MemberInfo info4 = new MemberInfo();
        // info4.setName("Scott Forstall");
        // info4.setAddress("Columbia Falls, MT 59912");
        // info4.setProfilePic(R.drawable.prof6);
        // info4.setSince("Mar 15, 2015");
        // info4.setThrough("June 15, 2016");
        // info4.setType("FAMILY MEMBER");
        // info4.setBarCodeNo(88128091);
        //
        // MemberInfo info5 = new MemberInfo();
        // info5.setName("F. Daniel Siciliano");
        // info5.setAddress("Columbia Falls, MT 59912");
        // info5.setProfilePic(R.drawable.prof5);
        // info5.setSince("Apr 12, 2015");
        // info5.setThrough("June 12, 2016");
        // info5.setType("FAMILY MEMBER");
        // info5.setBarCodeNo(88038052);
        //
        // listMember.add(info1);
        // listMember.add(info2);
        // listMember.add(info3);
        // listMember.add(info4);
        // listMember.add(info5);

        return listMember;

    }

    @Override
    public void setPosition(int pos, ViewAnimator animator, ImageView image, boolean flag, int
            type) {
        this.pos = pos;
        this.animator = animator;
        profimage = image;
        flag_fb = flag;

        //IF CLICK ON GALLERY,Camera,Facebook TO TAKE PHOTO
        if (type == TYPE_GALLERY) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkCallingPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                            .WRITE_EXTERNAL_STORAGE}, TYPE_GALLERY);
                } else {
                    Utility.selectFromGallery(ctx);
                }

            } else {

                Utility.selectFromGallery(ctx);
            }
        } else if (type == TYPE_CAMERA) {
            if (Build.VERSION.SDK_INT >= 23) {
                if ((checkCallingPermission(Manifest.permission.CAMERA) != PackageManager
                        .PERMISSION_GRANTED) || (checkCallingPermission(Manifest.permission
                        .WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                            .CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, TYPE_CAMERA);
                } else {
                    Utility.selectFromCamera(ctx);
                }

            } else {
                Utility.selectFromCamera(ctx);
            }
        } else if (type == TYPE_FB) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkCallingPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                            .WRITE_EXTERNAL_STORAGE}, TYPE_FB);
                } else {
                    if (flag) {
                        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList
                                ("public_profile"));
                    }
                }
            } else {
                if (flag) {
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList
                            ("public_profile"));
                }
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!(resultCode == Activity.RESULT_CANCELED)) {

            if (requestCode == Tags.PICK_FROM_CAMERA) {
                Uri selectedImageUri = null;
                SendImage();
                /*if (data == null) {

                    SendImage();

                } else {

                    selectedImageUri = data.getData();
                    Utility.mImageCaptureUri = selectedImageUri;
                }*/
            } else if (requestCode == Tags.PICK_FROM_GALLERY) {
                if (data != null) {
                    Utility.mImageCaptureUri = data.getData();

                    SendImage();
                }

            } else if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }

        }
//comment now for facebook use later 31-5-2016
//        uiHelper.onActivityResult(requestCode, resultCode, data,
//                new FacebookDialog.Callback() {
//
//                    @Override
//                    public void onError(PendingCall pendingCall,
//                                        final Exception error, final Bundle data) {
//                        // Log.e(CN, String.format("Error: %s",
//                        // error.toString()));
//                        Log.e("tag", "Error");
//
//                    }
//
//                    @Override
//                    public void onComplete(PendingCall pendingCall, Bundle data) {
//                        // Log.i(CN, "Success!");
//                        Log.e("tag", "Success");
//                        // getUserID();
//
//                    }
//
//                });
    }

    private void SendImage() {

        final String selectedImagePath = Utility.getRealPathFromURI(
                MemberShipActivity.this, Utility.mImageCaptureUri);
        if (selectedImagePath == null) {

            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                Bitmap bitmap = BitmapFactory.decodeStream(
                        getApplicationContext().getContentResolver()
                                .openInputStream(Utility.mImageCaptureUri),
                        null, options);

                if (bitmap != null) {
                    // imgImage.setImageBitmap(bitmap);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {

            try {
                Dialog d = setDialogForCropping(selectedImagePath);
                Utility.setscreenDescription(d);
                d.show();
            } catch (Exception e) {
                e.printStackTrace();

            }


        }

    }

    private Dialog setDialogForCropping(String path) {

        Dialog dialogCrop = Utility.initializeDialog(ctx, R.layout.crop_layout);
        imageCrop = (CropImageView) dialogCrop.findViewById(R.id.CropImageView);
        dialogCrop.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#B3000000")));
        imageCrop.setFixedAspectRatio(true);
        Button cropBtn = (Button) dialogCrop.findViewById(R.id.crop_button);
        Button useBtn = (Button) dialogCrop
                .findViewById(R.id.use_original_button);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        cropBtn.setTypeface(faceheader);
        useBtn.setTypeface(faceheader);
        Matrix matrix = new Matrix();
        matrix.postRotate(Utility.getImageOrientation(path));
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        imageCrop.setImageBitmap(rotatedBitmap);

        cropBtn.setOnClickListener(new ImageChangeListner(dialogCrop, ctx,
                path, list.get(this.pos).getConstituentID(), Utility
                .getFilename(path), animator, profimage, imageCrop));
        useBtn.setOnClickListener(new ImageChangeListner(dialogCrop, ctx, path,
                list.get(this.pos).getConstituentID(), Utility.getFilename(path),
                animator, profimage, imageCrop));
        return dialogCrop;

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

    }

    private void handlePendingAction() {
        final PendingAction previouslyPendingAction = pendingAction;
        pendingAction = PendingAction.LOGIN;
        switch (previouslyPendingAction) {

            case LOGIN:
                // final Session sessionclear = Session.getActiveSession();
                // if (sessionclear != null) {
                // sessionclear.closeAndClearTokenInformation();
                // sessionclear.close();
                // }
                loginFB();
                break;
            default:
                break;
        }
    }

    private void loginFB() {
//        if (Session.getActiveSession() != null
//                && Session.getActiveSession().isOpened()) {
//            flag_userdialog = true;
//            getUserID();
//
//        } else {
//
//            final Session session = Session.getActiveSession();
//
//            if (!session.isOpened() && !session.isClosed()) {
//                session.openForRead(new Session.OpenRequest(this)
//                        .setCallback(this));
//
//            } else {
//                Session.openActiveSession(this, true, this);
//
//            }
//        }

    }

    private void performPublish(final PendingAction action,
                                final boolean allowNoSession) {
//        final Session session = Session.getActiveSession();
//        if (allowNoSession) {
//            pendingAction = action;
//            handlePendingAction();
//        }
    }

    long value = 0;
    long threshold = 20;

//    @Override
//    public void call(Session session, SessionState state, Exception exception) {
//        Log.e("tag", "Oncall");
//        Log.e("tag1", "" + state.isOpened());
//        Log.e("tag2", "Oncall" + flag_fb);
//
//        long tempValue = System.currentTimeMillis();
//        if (state.isOpened() && flag_fb) {
//            Log.e("called", "true");
//            if (tempValue - value > threshold) {
//                value = tempValue;
//                getUserID();
//            }
//
//        }
//
//    }

    // private void setSharedPrefFb(final String id, final String name) {
    // SharedManager.getInstance(mContext).saveString(
    // Constants.TWITTER_USERID, null);
    //
    // SharedManager.getInstance(mContext).saveString(Constants.FB_USERID, id);
    // SharedManager.getInstance(mContext).saveString(Constants.ENTER_WITHOUT,
    // Constants.ENTER_WITHOUT_EMPTY);
    // SharedManager.getInstance(mContext).saveString(Constants.Fb_Name, name);
    // Log.e("FB LOGIN", "2");
    //
    // if (socialflag) {
    // socialflag = false;
    //
    // if (Utility.isNetworkAvailable(mContext)) {
    //
    // new TriviaTask().execute();
    // } else {
    // finish();
    // startActivity(new Intent(getApplicationContext(),
    // TabsActivity.class));
    // }
    //
    // }
    //
    // }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //uiHelper.onSaveInstanceState(outState);
        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
        super.onSaveInstanceState(outState);
    }

    // @Override
    // protected void onActivityResult(int requestCode, int resultCode, Intent
    // data) {
    // super.onActivityResult(requestCode, resultCode, data);
    // uiHelper.onActivityResult(requestCode, resultCode, data,
    // new FacebookDialog.Callback() {
    //
    // @Override
    // public void onError(PendingCall pendingCall,
    // final Exception error, final Bundle data) {
    // Log.e(CN, String.format("Error: %s", error.toString()));
    //
    // }
    //
    // @Override
    // public void onComplete(PendingCall pendingCall, Bundle data) {
    // Log.i(CN, "Success!");
    //
    // }
    //
    // });
    // }

    @SuppressWarnings("deprecation")
    private void getUserID() {
/*
        Request.executeMeRequestAsync(Session.getActiveSession(),
                new Request.GraphUserCallback() {
                    public void onCompleted(GraphUser user, Response response) {
                        Log.d("GraphUserCallback", "GraphUserCallback");
                        if (response != null) {
                            if (user != null) {
                                getid = user.getId();
                                SharedManager.getInstance(ctx).saveString(
                                        Constants.FB_USERID, getid);
                                if (flag_remove_acnt) {
                                    SharedManager.getInstance(ctx).saveBoolean(
                                            Constants.CHANGE_ACNT_FLAG, true);
                                    flag_remove_acnt = false;
                                }
                                if (flag_userdialog) {
                                    flag_userdialog = false;

                                    fb_option_dialog = Utility
                                            .initializeDialog(ctx,
                                                    R.layout.fb_option_popup);
                                    findViewsForDialog(
                                            fb_option_dialog,
                                            user.getFirstName() + " "
                                                    + user.getLastName());
                                    fb_option_dialog.show();
                                    // TagInfo info = GetTagData.getTag(ctx);
                                    // final AlertDialog.Builder builder = new
                                    // AlertDialog.Builder(
                                    // ctx);
                                    // builder.setTitle("Alert");
                                    // builder.setMessage("You are logged in as "
                                    // + user.getFirstName()
                                    // + " "
                                    // + user.getLastName()
                                    // +
                                    // " and picture will be brought from that account");
                                    // builder.setPositiveButton(
                                    // info.getOk(),
                                    // new DialogInterface.OnClickListener() {
                                    //
                                    // @Override
                                    // public void onClick(
                                    // DialogInterface dialog,
                                    // int which) {
                                    // dialog.dismiss();
                                    // if (getid != null) {
                                    // new DownloadFbPic(getid)
                                    // .execute();
                                    // }
                                    // }
                                    // });
                                    //
                                    // builder.setNegativeButton(
                                    // info.getCancel(),
                                    // new DialogInterface.OnClickListener() {
                                    //
                                    // @Override
                                    // public void onClick(
                                    // DialogInterface dialog,
                                    // int which) {
                                    //
                                    // final Session sessionclear = Session
                                    // .getActiveSession();
                                    // if (sessionclear != null) {
                                    // sessionclear
                                    // .closeAndClearTokenInformation();
                                    // sessionclear.close();
                                    // }
                                    // performPublish(
                                    // PendingAction.LOGIN,
                                    // true);
                                    // dialog.cancel();
                                    // }
                                    // });
                                    // builder.show();

                                } else {
                                    if (user.getId() != null) {
                                        new DownloadFbPic(user.getId())
                                                .execute();
                                    }
                                }
                                // Toast.makeText(ctx, "" + user.getId(), 1000)
                                // .show();
                                String url = "http://graph.facebook.com/"
                                        + user.getId() + "/picture?type=large";
                                // boolean flag = DataProvider.getInstance(ctx)
                                // .updateMemberProfImage(
                                // DBController.createInstance(
                                // ctx, true),
                                // list.get(pos).getConstitID(),
                                // url);
                                // Utility.showImageFromUrl(url, profimage,
                                // animator);
                                Log.e("UserId", getid);

                            }

                        }
                    }

                });

        */

    }

/*
    private void findViewsForDialog(Dialog dialog, String userName) {

        ImageView img_cross = (ImageView) dialog
                .findViewById(R.id.img_popup_cross);
        TextView txt_header = (TextView) dialog.findViewById(R.id.popup_header);
        TextView txt_msg = (TextView) dialog.findViewById(R.id.text_msg);
        TextView click_countinue = (TextView) dialog
                .findViewById(R.id.txt_countinue);
        TextView click_delete = (TextView) dialog
                .findViewById(R.id.txt_remove_account);
        txt_header.setText("Alert");

        click_countinue.setText("Continue");
        click_delete.setText("Change");
        txt_header.setTypeface(faceheader);
        txt_msg.setTypeface(faceRegular);
        click_countinue.setTypeface(faceRegular);
        click_delete.setTypeface(faceRegular);

        txt_msg.setText("You are logged in as "
                + userName
                + " and picture will be brought from that account.Do you want to continue or do
                you want to change logged in account");
        img_cross.setOnClickListener(this);
        click_countinue.setOnClickListener(this);
        click_delete.setOnClickListener(this);

    }
    */


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length == 0) {
            return;
        }
        //For Gallery
        if (requestCode == TYPE_GALLERY) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Utility.selectFromGallery(ctx);
            } else {
                Toast.makeText(this, gallery_access_message, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == TYPE_CAMERA) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Utility.selectFromCamera(ctx);
            } else {
                Toast.makeText(this, camera_access_message, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == TYPE_FB) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (Utility.isNetworkAvailable(ctx)) {
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList
                            ("public_profile"));
                } else {
                    Utility.showAlertDialog2(ctx, no_internet_message, language, languageMap);
                }
            } else {
                Toast.makeText(this, storage_access_message, Toast
                        .LENGTH_LONG).show();
            }
        }
    }

    private void downloadImage() {
        List<String> urlList = new ArrayList<>();
        final List<Configuration> list = MemberParser.parsConfig(this);
        for (Configuration config : list) {
            urlList.add(config.getImagePath() + "/" + config.getCardImage());
        }
        /*Picasso.with(this)
                .load(urlList.get(clickedMember))
                .placeholder(R.drawable.logo)
                .into(dummyImage);*/
       /* if(urlList.size()>0){
            DownloadImage task=new DownloadImage(this,urlList) {
                @Override
                public void onTaskFinished(List<File> file) {
                    try {
                        dummyImage.setImageURI(Uri.fromFile(file.get(0)));
                    }catch (Exception e){
                        e.printStackTrace();
                    }



                }
            };
            task.execute();
        }*/
        //ImageSize targetSize = new ImageSize((int)Utility.convertDPtoPixel(this,4000), (int)
        // Utility.convertDPtoPixel(this,1000));
        ImageLoader.getInstance().displayImage(urlList.get(clickedMember), dummyImage);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }




    private void checkExpiryMsg(String lang) throws Exception {
        String id = "";
        String expireMsgText = "";
        String membershipIdText = "";
        String LastNameText = "";
        String numberText = "";
        String constituentId = "";
        String searchType = "";
        //String expiryDate = "";
        Cursor cursor = DataProvider.getInstance(mContext).getMemberDetails(DBController
                .createInstance(mContext, false), 0);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            expireMsgText = cursor.getString(cursor.getColumnIndex(MemberController
                    .MEMBER_EXPIRE_MESSAGE));
            membershipIdText = cursor.getString(cursor.getColumnIndex(MemberController.MEMBER_ID));
            LastNameText = cursor.getString(cursor.getColumnIndex(MemberController
                    .MEMBER_LASTNAME));
            numberText = cursor.getString(cursor.getColumnIndex(MemberController.MEMBER_NUMBER));
            expiryDate = cursor.getString(cursor.getColumnIndex(MemberController.MEMBER_THROUGH));
            constituentId = cursor.getString(cursor.getColumnIndex(MemberController
                    .MEMBER_CONSTITUENTID));
            searchType = cursor.getString(cursor.getColumnIndex(MemberController.SEARCH_TYPE));
            if (searchType == null) {
                searchType = "1";
            }
            if (searchType.equals("1")) {
                if (membershipIdText == null) {
                    membershipIdText = "";
                }
                if (!membershipIdText.isEmpty()) {
                    id = membershipIdText;
                    break;
                }
            } else if (searchType.equals("2")) {
                if (constituentId == null) {
                    constituentId = "";
                }
                if (!constituentId.isEmpty()) {
                    id = constituentId;
                    break;
                }
            } else if (searchType.equals("3")) {
                if (numberText == null) {
                    numberText = "";
                }
                if (!numberText.isEmpty()) {
                    id = numberText;
                    break;
                }
            }
            cursor.moveToNext();
        }
        cursor.close();

        String passSearchType=configList.get(clickedMember).getPassSearchType();
        if(!passSearchType.isEmpty()){
            if(passSearchType.equalsIgnoreCase("1")||passSearchType.equalsIgnoreCase("4")){
                if(!membershipIdText.isEmpty())
                  id=membershipIdText;
            }else {
                if(!constituentId.isEmpty())
                id=constituentId;
            }
        }
        //Toast.makeText(mContext,"expireMsgText: "+expireMsgText,Toast.LENGTH_LONG).show();
        if (!LastNameText.equals("")) {
            //For on upgrade
            final Map<String, Object> map = new LinkedHashMap<>();
            map.put("mId", id);//membershipIdText
            map.put("lName", LastNameText);
            map.put("organisation_key", configList.get(clickedMember).getMid());
            map.put("udID", Utility.getDeviceToken(mContext));
            map.put("deviceType", "2");
            map.put("deviceName", Utility.getDeviceName());
            map.put("uniqueDeviceID", Utility.getDeviceId(this));


            if (Utility.isNetworkAvailable(mContext)) {
                new MemberTaskInitial(mContext, lang, please_wait) {
                    @Override
                    public void onTaskFinished(MemberList list) {
                        if (list.size() > 0) {
                            MemberShipActivity.this.list = list;

                            if (!list.get(0).getExpiryMessageText().equalsIgnoreCase("-1")) {

                                expiryMsg = list.get(0).getExpiryMessageText();
                                ///showDialog(list.get(0).getExpiryMessageText());
                            } else {
                                expiryMsg = "";
                            }
                            if (expiryMsg != null && (!expiryMsg.isEmpty())) {
                                /*if(dialog!=null&&dialog.isShowing())
                                    dialog.dismiss();*/
                                ShowDialog(expiryMsg);
                            }
                            saveCard(list);
                            if (map != null) {
                                Utility.syncDataOnAppUpgrade(mContext, map);
                            }
                        }
                    }
                }.execute(Constants.MODE_NORM,
                        LastNameText, id);//membershipIdText
            } else {

            }
        } else {
            //Toast.makeText(mContext,expireMsgText,Toast.LENGTH_LONG).show();
            //ShowDialog(expireMsgText);
        }

    }
    private void saveCard(List<MemberInfo> memberInfoList) {
        DataProvider.getInstance(mContext).DeleteMemberDetails(
                DBController.createInstance(mContext, true), 0, null);
        boolean insertMain = false,
                insertDec = false;
        for (int i = 0; i < memberInfoList.size(); i++) {
            for (int j = 0; j < memberInfoList.get(i).getSpouseList().size(); j++) {
                insertDec = false;
                insertDec = DataProvider.getInstance(mContext)
                        .insertMemberSpouseDetails(DBController.createInstance(mContext, true),
                                memberInfoList.get(i).getSpouseList().get(j));
            }
            insertMain = DataProvider.getInstance(mContext)
                    .insertMemberDetails(DBController.createInstance(mContext, true),
                            memberInfoList.get(i));

        }


    }

    private void renew(Button btnRenew){
        if(list==null&&list.size()==0){
            return;
        }
        List<String> listFrequency= configList.get(clickedMember).getNotificationFrequency();
        if (!Utility.isExpiredCard(list.get(0).getValidThroughTimeSpan())||(Utility.isNotificationFrequency(list.get(0).getValidThroughTimeSpan(),listFrequency))) {

            String renewUrl=configList.get(clickedMember).getRenewLink();
            if(renewUrl!=null&&!renewUrl.isEmpty()){
                btnRenew.setVisibility(View.VISIBLE);
                btnRenew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(renewUrl!=null&&!renewUrl.isEmpty()){
                            openWebPage(renewUrl);
                        }

                    }
                });
            }else {
                btnRenew.setVisibility(View.GONE);
            }

        } else {
            btnRenew.setVisibility(View.GONE);

        }
      ///  btnRenew.setVisibility(View.VISIBLE);
    }

    public void openWebPage(String url) {
        try {
            Uri webpage = Uri.parse(url);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, webpage);
            mContext.startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext, "No application can handle this request. Please install a web browser or check your URL.",  Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
}
