package com.inficare.membershipdemo.methods;

import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.inficare.membershipdemo.dialogFragment.RateDialogFragment;


public class AppRater {
    public static final String TAG = "AppRater";
    private final static String APP_TITLE = "eMembership";// App Name
    public final static String APP_PNAME = "com.inficare.membershipdemo";// Package Name

    private final static long DAYS_UNTIL_PROMPT = 7776000000L; //90 days
    private final static int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches
    private static FragmentManager manager;

    public static void app_launched(Context mContext, FragmentManager fragmentManager) {
        manager = fragmentManager;

        SharedPreferences prefs = mContext.getSharedPreferences(Constants.APPRATER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        /*if (prefs.getBoolean(Constants.DONT_SHOW_AGAIN, false)) {

            Log.d(TAG, "app_launched: already rated");
            return;
        }
*/

        // Increment launch counter
//        int launch_count = prefs.getInt(Constants.LAUNCH_COUNT, 0) + 1;
//        editor.putInt(Constants.LAUNCH_COUNT, launch_count);

//        Log.d(TAG, "launch_count: " + launch_count);

//        int laterCount = prefs.getInt(Constants.LATER_COUNT, 0);

//        Log.d(TAG, "laterCount: " + laterCount);


        // Get date of first launch
        Long date_firstLaunch = prefs.getLong(Constants.DATE_FIRST_LAUNCH, 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong(Constants.DATE_FIRST_LAUNCH, date_firstLaunch);

            Log.d(TAG, "date_firstlaunch: " + date_firstLaunch);

        }


        Log.d(TAG, "90 days: " + DAYS_UNTIL_PROMPT);

        // Wait at least n days before opening
        Log.d(TAG, "System.currentTimeMillis(): " + System.currentTimeMillis());
        Log.d(TAG, "date_firstLaunch: " + date_firstLaunch);
        Log.d(TAG, "date_firstLaunch + 90 days: " + (date_firstLaunch + DAYS_UNTIL_PROMPT));
        Log.d(TAG, "value: " + String.valueOf(System.currentTimeMillis() >= date_firstLaunch + DAYS_UNTIL_PROMPT));
        /*if (launch_count >= LAUNCHES_UNTIL_PROMPT && laterCount < 2) {
            showRateDialog();

        } else if (System.currentTimeMillis() >= date_firstLaunch + ) {
            showRateDialog();

        }*/
        if (System.currentTimeMillis() >= (date_firstLaunch + DAYS_UNTIL_PROMPT)) {

            showRateDialog();
        }
//        (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)

        editor.commit();
    }

    public static void showRateDialog() {

        RateDialogFragment dialogFragment = RateDialogFragment.newInstance();
        dialogFragment.show(manager, "RateDialogFragment");
    }


}