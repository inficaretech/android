package com.inficare.membershipdemo.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by avinash.verma on 9/22/2016.
 */
public class Model {

    @SerializedName("Result")
    @Expose
    private List<Result> result = new ArrayList<Result>();

    @SerializedName("labelntext")
    @Expose
    private List<Labelntext> labelntext = null;

    /**
     * @return The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     * @param result The Result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }
}
