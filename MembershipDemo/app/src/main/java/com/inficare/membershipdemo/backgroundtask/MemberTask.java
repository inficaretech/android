package com.inficare.membershipdemo.backgroundtask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;

import com.inficare.membershipdemo.activities.MembershipOptions;
import com.inficare.membershipdemo.activities.RenewMembershipActivity;
import com.inficare.membershipdemo.activities.VerifyMembership;
import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.networks.HttpMethod;
import com.inficare.membershipdemo.pojo.Configuration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class MemberTask extends AsyncTask<String, Void, String> {

    private Context mContext;

    private static final String CN = "MemberTask";

    private ProgressDialog dialog;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    Map<String, Object> map;
    String searchMode;
    private String language;
    private String info, ok, connection_time_out, please_wait;
    private Map<String, JSONObject> languageMap;


    //added
    public MemberTask(Context context, Map<String, Object> map, String searchMode, String language, Map<String, JSONObject> languageMap) {
        mContext = context;
        this.map = map;
        this.language = language;
        this.languageMap = languageMap;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);
        this.searchMode = searchMode;

        String title = "title";
        if (language.equalsIgnoreCase(Constants.ENGLISH)) {

        } else {
            title = title + "_" + language;
        }
        try {
            info = languageMap.get(LanguageConstants.INFO).getString(title);
            ok = languageMap.get(LanguageConstants.OK).getString(title);
            connection_time_out = languageMap.get(LanguageConstants.CONNECTION_TIME_OUT).getString(title);
            please_wait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        createDialog(mContext);



    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.setCancelable(false);
        dialog.show();

    }

    @Override
    protected String doInBackground(String... params) {

        String responsedata = null;
        responsedata = HttpMethod.getInstance(mContext).postData(params[0], map);

        return responsedata;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        dialog.dismiss();
        Intent intent = null;
        try {
            if (result != null) {
                if (result.equals(Constants.timeoutException)) {
//				TagInfo infoTag = GetTagData.getTag(mContext);
                    //	Utility.showAlertDialog(mContext, infoTag.getConnectionTimout());
                    Utility.showAlertDialog2(mContext, connection_time_out, configList.get(clickedMember).getMid(), languageMap);

                } else {
                    if (!result.contains("MembershipID")) {
                        JSONObject json = new JSONObject(result);
                        result = json.getString("Result");///Utility.myDecript(json.getString
/// ("Result"));
                    }
//                     else if (result.contains("Message")) {
//
//                        JSONObject json = new JSONObject(result);
//                        Utility.showToast(mContext, json.get("Message").toString());
//
//                    }
                    MemberList list = MemberParser.createInstance(mContext)
                            .getMemberDetails(result);
//                    dialog.dismiss();

                    if (searchMode != null && searchMode.equalsIgnoreCase(MembershipOptions
                            .SEARCH_MODE_EXPIRED)) {
                        intent = new Intent(mContext, RenewMembershipActivity.class);
                    } else {
                        intent = new Intent(mContext, VerifyMembership.class);
//                        intent = new Intent(mContext, RenewMembershipActivity.class);
                    }
                    intent.putExtra(Constants.MEMBER_DATA, list);
                    ((Activity) mContext).startActivityForResult(intent, 1);
                }

            } else {
                //dialog.dismiss();
//                MemberList list = null;
                MemberList list = MemberParser.createInstance(mContext)
                        .getMemberDetails(result);
                //                dialog.dismiss();
                if (searchMode == null || !searchMode.equalsIgnoreCase(MembershipOptions
                        .SEARCH_MODE_EXPIRED)) {
                    intent = new Intent(mContext, VerifyMembership.class);
//                    intent = new Intent(mContext, RenewMembershipActivity.class);
                    intent.putExtra(Constants.MEMBER_DATA, list);
                    ((Activity) mContext).startActivityForResult(intent, 1);
                }
            }
        } catch (Exception e) {
            try {
                if (result != null) {
                    JSONObject json = new JSONObject(result);
                    if (json.has("Message")) {
                        intent = new Intent(mContext, VerifyMembership.class);
                        ((Activity) mContext).startActivityForResult(intent, 1);
                        //showDialog(json.getString("Message"));
                    }

                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }

            e.printStackTrace();

        }


    }

    public void createDialog(final Context context) {
//		TagInfo infoTag = (Utility.getTagForLangChange(SharedManager
//				.getInstance(context).getInt(Constants.PREV_LANG, 0), context));

//		if (!Constants.langChange) {
//			dialog = Utility.showProgressDialog(mContext, context
//					.getResources().getString(R.string.msg_wait));
//		} else {

//			if (infoTag != null) {
//				dialog = Utility.showProgressDialog(mContext,
//						infoTag.getAlertWait());
//			} else {
        dialog = Utility.showProgressDialog(mContext, please_wait);
        //}

        //}

    }

    public void showDialog(String expireMsgText) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        // set title
        alertDialogBuilder.setTitle(info);
        // set dialog message
        alertDialogBuilder
                .setMessage(expireMsgText)
                .setCancelable(true)
                .setPositiveButton(ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                /*.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

}
