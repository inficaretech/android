package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.activities.MyWebViewActivity;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.HeaderInfo;
import com.inficare.membershipdemo.pojo.MatcherNode;
import com.inficare.membershipdemo.pojo.QuestionsInfo;

import java.util.List;

/**
 * Created by akshay.kumar on 2/20/2017.
 */
public class ExpListAdapter extends BaseExpandableListAdapter {
    private int quizSize = 5;
    private Context context;
    private List<HeaderInfo> deptList;
    Drawable minus_icon,plus_icon;
    View view;

    HolderChild holderChild;
    int clickedMember;
    List<Configuration> configList;
    private ExpandableListView listView;
    /**
     * @param context
     * @param deptList    uded for single data
     */
    public ExpListAdapter(Context context, List<HeaderInfo> deptList,ExpandableListView listView) {
        this.context = context;
        this.deptList = deptList;
        this.listView=listView;
        quizSize=deptList.size();
        MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(context);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);

        plus_icon = context.getResources().getDrawable(R.drawable.plus_icon);
        minus_icon = context.getResources().getDrawable(R.drawable.plus_icon);
//        plus_icon.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()), PorterDuff.Mode.SRC_ATOP);
//        minus_icon.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()), PorterDuff.Mode.SRC_ATOP);
//
        plus_icon.setColorFilter(ContextCompat.getColor(context, R.color.black),
                PorterDuff.Mode.SRC_ATOP);
        minus_icon.setColorFilter(ContextCompat.getColor(context, R.color.black),
                PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        List<QuestionsInfo> questionList = deptList.get(groupPosition).getQuestionList();
        return questionList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public class HolderChild {
        TextView tvQuestion;

    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {
        //HolderChild holderChild = null;
        final QuestionsInfo questions = (QuestionsInfo) getChild(groupPosition, childPosition);
        if (view == null) {
            holderChild = new HolderChild();
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.list_item, null);
            holderChild.tvQuestion= (TextView) view.findViewById(R.id.tvQuestion);
            view.setTag(holderChild);
        } else {
            holderChild = (HolderChild) view.getTag();
        }
        //holderChild.tvQuestion.setText(Html.fromHtml(questions.getAnswer()));
         String data=questions.getAnswer().trim();
        List<MatcherNode> matcherNodes=Utility.extractNumber(data);
        for(MatcherNode node:matcherNodes){
            data=data.replace(node.getData(),"<a href=tel:"+node.getData().trim()+">"+node.getData()+"</a>");
        }
        setTextViewHTML(holderChild.tvQuestion,data,-1,false);
//        holderChild.tvQuestion.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        ///holderChild.tvQuestion.setTextColor(ContextCompat.getColor(context, R.color.black));

        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        List<QuestionsInfo> questionList = deptList.get(groupPosition).getQuestionList();
        return questionList.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return deptList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return deptList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public class Holder {
        TextView tvTitle;
        ImageView help_group_indicator;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        HeaderInfo headerInfo = (HeaderInfo) getGroup(groupPosition);
        Holder holder = null;
        if (view == null) {
            holder = new Holder();
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.list_group, null);
            holder.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            holder.help_group_indicator = (ImageView) view.findViewById(R.id.help_group_indicator);

            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        //holder.tvTitle.setText(Html.fromHtml(headerInfo.getName()));
        setTextViewHTML(holder.tvTitle,headerInfo.getName().trim(),groupPosition,true);
//        holder.tvTitle.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        ///holder.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.black));

        if(isLastChild){
            minus_icon.setBounds(0, 0, minus_icon.getIntrinsicWidth(), minus_icon.getIntrinsicHeight());
            holder.help_group_indicator.setImageResource(R.drawable.black_minus_icon);
        }else {
            holder.help_group_indicator.setImageResource(R.drawable.black_add_icon);

        }

        Log.i("expend",isLastChild+" "+groupPosition);
        return view;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span)
    {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                // Do something with span.getURL() to handle the link click...
                Intent intent=null;
                String link=span.getURL();
                if(link==null)
                    return;
               if(link.startsWith("mailto:")){
                  link=Utility.parseEmailFromHtml(link);
                if(Utility.isValidEmail(context,link)){

                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{link});
                    i.putExtra(Intent.EXTRA_SUBJECT, "");
                    i.putExtra(Intent.EXTRA_TEXT   , "");
                    try {
                        context.startActivity(Intent.createChooser(i, ""));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                }
               } else if(link.startsWith("tel:")){
                   link = Utility.parsePhoneFromHtml(link);
                   if(link.endsWith(".")){
                       link=link.replace(".","");
                   }
                   if(Utility.isValidMobile(link)){
                       Intent i = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", link, null));
                       context.startActivity(i);
                   }
                }else {
                    intent = new Intent(context,MyWebViewActivity.class);
                    intent.putExtra(MyWebViewActivity.LINK_URL,span.getURL());
                    context.startActivity(intent);
                }
            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    protected void setTextViewHTML(TextView text, String html,int pos,boolean isHeader)
    {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for(URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        if(isHeader){
            if(urls!=null&&urls.length>0){
                if(listView!=null)
                    listView.expandGroup(pos);
                text.setMovementMethod(LinkMovementMethod.getInstance());
            }else {
                text.setMovementMethod(null);
            }
        }else {
            text.setMovementMethod(LinkMovementMethod.getInstance());
        }

        text.setText(strBuilder);

    }


}
