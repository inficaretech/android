package com.inficare.membershipdemo.views;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import java.util.List;


/**
 * Created by akshay.kumar on 8/31/2016.
 */
public class ExpiryDialog extends Dialog implements View.OnClickListener {
    private TextView infoEmail, infomsg, infoForHelp, infoPhone, infoEmailId;
    private TextView textInfo;
    private Button btnOk;
    LinearLayout llEmail;
    Context context;
    private Typeface faceHeader, faceDescription;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    int flag;

    public ExpiryDialog(Context context,int flag) {
        super(context);
        this.context = context;
        faceDescription = Utility.setFaceTypeContent(context);

        preferences = MuseumDemoPreference.getInstance(context);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);
        this.flag=flag;
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // In order to not be too narrow, set the window size based on the screen resolution:
        final int screen_width = context.getResources().getDisplayMetrics().widthPixels;
        final int new_window_width = screen_width * 90 / 100;
        WindowManager.LayoutParams layout = getWindow().getAttributes();
        layout.width = Math.max(layout.width, new_window_width);
        getWindow().setAttributes(layout);
    }

    private void init() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.expiry_dailog);
        infoEmail = (TextView) findViewById(R.id.info_email);
        infomsg = (TextView) findViewById(R.id.info_msg);
        infoForHelp = (TextView) findViewById(R.id.info_forhelp);
        infoPhone = (TextView) findViewById(R.id.info_phone_no);
        infoEmailId = (TextView) findViewById(R.id.info_email_id);
        textInfo = (TextView) findViewById(R.id.info_link);
        llEmail= (LinearLayout) findViewById(R.id.llEmail);
        btnOk=(Button) findViewById(R.id.btnOk);
        infoEmailId.setTypeface(faceDescription);
        infoEmail.setTypeface(faceDescription);
        infomsg.setTypeface(faceDescription);
        infoForHelp.setTypeface(faceDescription);
        infoPhone.setTypeface(faceDescription);
        infoPhone.setTypeface(faceDescription);
        infoEmailId.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        infoPhone.setOnClickListener(this);
        textInfo.setVisibility(View.VISIBLE);
        infomsg.setText("The membership is expired and expired cards can not be downloaded. Touch link to renew");
        String html = "<a href=\"http://tickets.museumanywhere.org/Public/load_screen.asp?screen-RenewMembership\">http://tickets.museumanywhere.org/Public/load_screen.asp?screen-RenewMembership</a>";
        textInfo.setMovementMethod(LinkMovementMethod.getInstance());
        textInfo.setText(Html.fromHtml(""/*html*/));
        String email=configList.get(clickedMember).getEmail();
        if(email!=null&&email.length()>5){
            infoEmailId.setText(email);
            infoPhone.setText("or call "+configList.get(clickedMember).getContactNumber());
        }else {
            llEmail.setVisibility(View.GONE);
            infoPhone.setText("call "+configList.get(clickedMember).getContactNumber());
        }
    }

    public void setMessage(String message) {
        // txt_msg.setText(message);
    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.info_phone_no:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("tel:"+configList.get(clickedMember).getContactNumber()));//"tel:(907)-929-9228"
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                /*if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }*/
                context.startActivity(intent);
                break;
            case R.id.info_email_id:
                 intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + configList.get(clickedMember).getEmail()));
                //intent.putExtra(Intent.EXTRA_SUBJECT, "your_subject");
                //intent.putExtra(Intent.EXTRA_TEXT, "your_text");
                context.startActivity(intent);
                break;
            case R.id.btnOk:
                if(flag!=1)
                {
                    ((Activity)context).finish();
                }else {
                    dismiss();
                }


                break;

        }
    }

}
