package com.inficare.membershipdemo.pojo;


import java.util.List;

/**
 * Created by akshay.kumar on 6/1/2016.
 */
public class HeaderInfo {
    private String name;
    private boolean isExpanded;
    private List<QuestionsInfo> list;

    public HeaderInfo(String name, List<QuestionsInfo> list){
       this.name=name;
        this.list=list;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<QuestionsInfo> getQuestionList() {
        return list;
    }
    public void getQuestionList(List<QuestionsInfo> list) {
        this.list = list;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public List<QuestionsInfo> getList() {
        return list;
    }

    public void setList(List<QuestionsInfo> list) {
        this.list = list;
    }
}
