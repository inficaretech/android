package com.inficare.membershipdemo.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.activities.QRCodeActivity;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.GuestPas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Abhishek.jha on 10/17/2016.
 */

public class GuestPassAdapter2  extends BaseAdapter {

    private Context mContext;
    private List<GuestPas> guestPasList;
    private Map<String,Bitmap> bitmapMap;
    public static ProgressDialog dialog;
    private static LayoutInflater inflater=null;
    public GuestPassAdapter2(Context context, List<GuestPas> guestPasList){
        this.mContext = context;
        this.guestPasList = guestPasList;
        bitmapMap=new HashMap<>();
        for (int i=0;i<guestPasList.size();i++){
            ///bitmapMap.put(String.valueOf(i),Utility.generateQRBitmap(guestPasList.get(i).getGuestPassID(), mContext));
        }
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return guestPasList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;
        if(convertView==null)
        rowView = inflater.inflate(R.layout.guest_pass_items, null);
        else
            rowView=convertView;
        Holder holder=new Holder(rowView);
        int guest_pass_no = position+1;
        holder.txt_guest_pass.setText("GUEST PASS "+guest_pass_no);//guestPasList.get(position).getID()
        holder.tv_text_qrcode.setText(guestPasList.get(position).getGuestPassID());
        //holder.qr_code.setImageBitmap(Utility.generateQRBitmap(guestPasList.get(position).getGuestPassID(), mContext));
        ///holder.qr_code.setImageBitmap(bitmapMap.get(String.valueOf(position)));
        String dateandtime = guestPasList.get(position).getValidUpto();
        String [] stringParts = dateandtime.split(" ");
        String dateOnly = stringParts[0];
        holder.bt_valid_through.setText("Valid through "+ Utility.convertDateFormat(dateOnly));
        if(guestPasList.get(position).getStatus().equals("Active")){
            holder.bt_ready_to_use.setText("Ready to use");
            holder.bt_ready_to_use.setBackground(mContext.getResources().getDrawable(R.drawable.activate_button_sel));
        }else{
            holder.bt_ready_to_use.setText("Used");
            holder.bt_ready_to_use.setBackground(mContext.getResources().getDrawable(R.drawable.red_activate_button_sel));
        }
//holder.itemView.setOnClickListener
        holder.qr_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog = Utility.showProgressDialog(mContext, mContext.getResources().getString(R.string.msg_wait));
                dialog.show();
                Intent i = new Intent(mContext, QRCodeActivity.class);
                i.putExtra("guestPassId",guestPasList.get(position).getGuestPassID());
                mContext.startActivity(i);
            }
        });
        return rowView;
    }

  /*  @Override
    public GuestPassViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        *//*View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.guest_pass_items, null, false);*//*
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.guest_pass_items, parent, false);

        GuestPassViewHolder guestPassViewHolder = new GuestPassViewHolder(view);
        return guestPassViewHolder;
    }

    @Override
    public void onBindViewHolder(GuestPassViewHolder holder, final int position) {
        //Guest Pass
        int guest_pass_no = position+1;
        holder.txt_guest_pass.setText("GUEST PASS "+guest_pass_no);//guestPasList.get(position).getID()
        holder.tv_text_qrcode.setText(guestPasList.get(position).getGuestPassID());
        //holder.qr_code.setImageBitmap(Utility.generateQRBitmap(guestPasList.get(position).getGuestPassID(), mContext));
        holder.qr_code.setImageBitmap(bitmapMap.get(String.valueOf(position)));
        String dateandtime = guestPasList.get(position).getValidUpto();
        String [] stringParts = dateandtime.split(" ");
        String dateOnly = stringParts[0];
        holder.bt_valid_through.setText("Valid through "+ Utility.convertDateFormat(dateOnly));
        if(guestPasList.get(position).getStatus().equals("Active")){
            holder.bt_ready_to_use.setText("Ready to use");
            holder.bt_ready_to_use.setBackground(mContext.getResources().getDrawable(R.drawable.activate_button_sel));
        }else{
            holder.bt_ready_to_use.setText("Used");
            holder.bt_ready_to_use.setBackground(mContext.getResources().getDrawable(R.drawable.red_activate_button_sel));
        }
//holder.itemView.setOnClickListener
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog = Utility.showProgressDialog(mContext, mContext.getResources().getString(R.string.msg_wait));
                dialog.show();
                Intent i = new Intent(mContext, QRCodeActivity.class);
                i.putExtra("guestPassId",guestPasList.get(position).getGuestPassID());
                mContext.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return guestPasList.size();
    }*/

    public class Holder
    {
        TextView txt_guest_pass;
        ImageView qr_code;
        TextView tv_text_qrcode;
        Button bt_ready_to_use;
        Button bt_valid_through;
        public Holder(View itemView){
            txt_guest_pass = (TextView)itemView.findViewById(R.id.txt_guest_pass);
            tv_text_qrcode = (TextView)itemView.findViewById(R.id.tv_text_qrcode);
            qr_code = (ImageView)itemView.findViewById(R.id.qr_code);
            bt_ready_to_use = (Button)itemView.findViewById(R.id.bt_ready_to_use);
            bt_valid_through = (Button)itemView.findViewById(R.id.bt_valid_through);
        }
    }
}
