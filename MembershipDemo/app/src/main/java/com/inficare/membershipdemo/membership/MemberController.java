package com.inficare.membershipdemo.membership;

import android.content.Context;

public class MemberController {

    private static final String CN = "MemberController";

    private Context _context;

    private static MemberController _instance;

    public final static String MEMBER_ID = "MembershipID";

    public final static String MEMBER_TYPE = "Membershiplevel";

    public final static String MEMBER_CITY = "City";

    public final static String MEMBER_STATE = "State";

    public final static String MEMBER_ZIP = "Zip";

    public final static String MEMBER_FIRSTNAME = "FirstName";

    public final static String MEMBER_LASTNAME = "LastName";

    public final static String MEMBER_SINCE = "MemberSinceTimeSpan";

    public final static String MEMBER_THROUGH = "ValidThroughTimeSpan";

    public final static String MEMBER_NUMBER = "Number";

    public final static String PROFILE_IMAGE = "ProfileImage";

    public final static String MEMBER_CONSTITUENTID = "ConstituentID";

    public final static String MEMBER_SUB_CAT = "Membershipleveltype";

    public final static String MEMBER_ISEXPIRE = "IsExpire";
    public final static String MEMBER_EXPIRE_MESSAGE = "ExpireMessageText";
    public final static String ROAM_LOGO_PATH = "RoamLogoPath";
    public final static String HAVE_LOGO_PATH = "HaveLogoPath";
    public final static String MEMBERSINCE = "Membersince";

    public final static String TABLE_NAME = "MemberDetail";

    public final static String ORG_KEY = "OrgKey";
    public final static String CARD_BANNER = "CardBanner";
    public final static String  SEARCH_TYPE = "searchType";
    public final static String  SEARCH_LABEL = "SearchLabel";
    public final static String  NO_OF_CHILDREN = "NoOfChildren";
    public final static String MEMBERSHIP_LEVEL_ID = "MembershiplevelID";



    public MemberController(Context context) {
        this._context = context;
    }

    public static MemberController getInstance(Context context) {
        if (_instance == null) {
            _instance = new MemberController(context);
        }

        return _instance;
    }

    public final static String CREATE_TABLE = (new StringBuilder())
            .append("CREATE TABLE ").append(TABLE_NAME).append(" (")
            .append(MEMBER_ID).append(" TEXT , ").append(MEMBER_TYPE)
            .append(" TEXT , ").append(MEMBER_CITY).append(" TEXT , ")
            .append(MEMBER_STATE).append(" TEXT , ").append(MEMBER_ZIP)
            .append(" TEXT , ").append(MEMBER_FIRSTNAME).append(" TEXT , ")
            .append(MEMBER_CONSTITUENTID).append(" TEXT , ").append(MEMBER_LASTNAME)
            .append(" TEXT , ").append(MEMBER_SUB_CAT).append(" TEXT , ")
            .append(MEMBER_ISEXPIRE).append(" TEXT , ").append(MEMBER_EXPIRE_MESSAGE).append(" " +
                    "TEXT , ")
            .append(MEMBER_SINCE).append(" TEXT , ").append(MEMBER_THROUGH).append(" TEXT , ")
            .append(MEMBER_NUMBER).append(" TEXT , ").append(HAVE_LOGO_PATH).append(" int , ").append(ROAM_LOGO_PATH).append(" TEXT , ")
            .append(MEMBERSINCE).append(" TEXT , ").append(PROFILE_IMAGE)
            .append(" TEXT, ").append(ORG_KEY).append(" TEXT, ").append(CARD_BANNER)
            .append(" TEXT, ").append(SEARCH_TYPE)
            .append(" TEXT, ").append(SEARCH_LABEL)
            .append(" TEXT, ").append(NO_OF_CHILDREN).append(" TEXT ")
            .append(" TEXT, ").append(MEMBERSHIP_LEVEL_ID).append(" TEXT );").toString();


    public final static String TABLE_MEMBERSHIP_BENEFIT = "MembershipBenefitTable";


    public final static String MEMBERSHIP_LEVEL_TYPE = "Membershipleveltype";


    public final static String PRICE = "Price";

    public final static String DESCRIPTION = "Description";

    public final static String UPSELLTEXT = "Upselltext";

    public final static String BENEFITS = "Benefits";

    public final static String OTHERBENEFITS = "OtherBenefits";

    public final static String MY_MEMBERSHIP_ID = "MyMembershipID";

    public final static String ORGANISATIONKEY = "OrganisationKey";


    public final static String CREATE_TABLE_MEMBERSHIP_BENIFIT = (new StringBuilder())
            .append("CREATE TABLE ").append(TABLE_MEMBERSHIP_BENEFIT).append(" (")
            .append(MEMBERSHIP_LEVEL_TYPE).append(" TEXT , ")
            .append(MEMBERSHIP_LEVEL_ID).append(" TEXT , ")
            .append(PRICE).append(" TEXT , ")
            .append(DESCRIPTION).append(" TEXT , ")
            .append(UPSELLTEXT).append(" TEXT , ")
            .append(BENEFITS).append(" TEXT , ")
            .append(MY_MEMBERSHIP_ID).append(" TEXT , ")
            .append(ORGANISATIONKEY).append(" TEXT , ")
            .append(OTHERBENEFITS).append(" TEXT );").toString();


    public final static String TABLE_MEMBERSHIP_FLOORMAP = "MembershipFloorMapTable";


    public final static String FLOORMAP_ID = "id";
    public final static String FLOORMAP_TITLE = "title";
    public final static String FLOORMAP_IMAGENAME = "image_name";
    public final static String FLOORMAP_IMAGETYPE = "image_type";
    public final static String FLOORMAP_IMAGE_SIZE = "image_size";
    public final static String FLOORMAP_MID = "mid";
    public final static String FLOORMAP_CREATED_AT = "created_at";
    public final static String FLOORMAP_UPDATED_AT = "updated_at";
    public final static String FLOORMAP_IMAGEBASE_PATH = "imageBasePath";
    public final static String FLOORMAP_IMAGE_SD_CARD_PATH = "imagesdcardpath";


    public final static String CREATE_TABLE_MEMBERSHIP_FLOORMAP = (new StringBuilder())
            .append("CREATE TABLE ").append(TABLE_MEMBERSHIP_FLOORMAP).append(" (")
            .append(FLOORMAP_ID).append(" TEXT , ")
            .append(FLOORMAP_TITLE).append(" TEXT , ")
            .append(FLOORMAP_IMAGENAME).append(" TEXT , ")
            .append(FLOORMAP_IMAGETYPE).append(" TEXT , ")
            .append(FLOORMAP_IMAGE_SIZE).append(" TEXT , ")
            .append(FLOORMAP_MID).append(" TEXT , ")
            .append(FLOORMAP_CREATED_AT).append(" TEXT , ")
            .append(FLOORMAP_IMAGEBASE_PATH).append(" TEXT , ")
            .append(FLOORMAP_IMAGE_SD_CARD_PATH).append(" TEXT , ")
            .append(FLOORMAP_UPDATED_AT).append(" TEXT );").toString();

	/*public final static String CREATE_TABLE = (new StringBuilder())
            .append("CREATE TABLE ").append(TABLE_NAME).append(" (")
			.append(MEMBER_ID).append(" TEXT , ").append(MEMBER_TYPE)
			.append(" TEXT , ").append(MEMBER_CITY).append(" TEXT , ")
			.append(MEMBER_STATE).append(" TEXT , ").append(MEMBER_ZIP)
			.append(" TEXT , ").append(MEMBER_FIRSTNAME).append(" TEXT , ")
			.append(MEMBER_CONSTITUENTID).append(" TEXT , ").append(MEMBER_LASTNAME)
			.append(" TEXT , ").append(MEMBER_SUB_CAT).append(" TEXT , ")
			.append(MEMBER_ISEXPIRE).append(" TEXT , ").append(MEMBER_EXPIRE_MESSAGE).append("
			TEXT , ")
			.append(MEMBER_SINCE).append(" TEXT , ").append(MEMBER_THROUGH).append(" TEXT , ")
			.append(MEMBER_NUMBER).append(" TEXT , ").append(CLICKED_INDEX).append(" int , ")
			.append(HAVE_LOGO_PATH).append(" int , ").append(ROAM_LOGO_PATH).append(" TEXT , ")
			.append(MEMBERSINCE).append(" TEXT , ").append(PROFILE_IMAGE)
			.append(" TEXT );").toString();*/

}
