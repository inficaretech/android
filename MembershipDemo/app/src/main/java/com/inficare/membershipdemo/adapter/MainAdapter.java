package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by akshay.kumar on 2/17/2017.
 */

public class MainAdapter extends ArrayAdapter<Configuration> {

    private Context mContext;

    private int mResource;

    private List<Configuration> configurations;

    private Typeface facedescription;

    public MainAdapter(Context context, int resource, List<Configuration> configurations) {
        super(context, resource, configurations);
        mContext = context;
        mResource = resource;
        this.configurations = configurations;
        facedescription = Utility.setFaceTypeContent(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(mResource, null);
        ImageView ivImage = (ImageView) convertView.findViewById(R.id.ivImage);
        ImageLoader.getInstance().displayImage(configurations.get(position).getImagePath() + "/" + configurations.get(position).getLogo(), ivImage);
        return convertView;
    }
}
