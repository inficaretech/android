package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 5/27/2016.
 */
public class QuestionsInfo {
    private int id;
    String answer;

    public QuestionsInfo(int id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
