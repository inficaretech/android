package com.inficare.membershipdemo.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.hwangjr.rxbus.RxBus;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.LanguageRecyclerAdapter;
import com.inficare.membershipdemo.backgroundtask.MemberTaskInitial;
import com.inficare.membershipdemo.databasecreation.CBO;
import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.dialogFragment.LanguageDialogFragment;
import com.inficare.membershipdemo.interfaces.PurchaseType;
import com.inficare.membershipdemo.membership.MemberController;
import com.inficare.membershipdemo.membership.MemberInfo;
import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.AppRater;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.DeviceDetails;
import com.inficare.membershipdemo.pojo.Labelntext;
import com.inficare.membershipdemo.pojo.LanguageResponse;
import com.inficare.membershipdemo.pojo.Reciprocal;
import com.inficare.membershipdemo.pojo.ReciprocalResult;
import com.inficare.membershipdemo.pojo.RxBusResultModel;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MembershipOptions extends AppCompatActivity implements View.OnClickListener {

    public static final String EXPIRY_MSG = "ExpiryMsg";
    public static final String SEARCH_MODE_EXPIRED = "Expired";
    public static final String TAG = MembershipOptions.class.getSimpleName();
    private static final String ONE = "1";
    private static final String YOUR_CARD_IS_EXPIRED = "Your card is expired please renew it";
    View tvTopStrip;
    LinearLayout ll_emailon;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    MemberList list;
    String expiryDate = "";
    String expiryMsg = "";
    boolean isInstructionShown;
    private ImageView bannerImage;
    private CardView card_view;
    private Button showCard, findCard, bt_member_benifits, bt_buy_renew_gift_membership;
    private TextView tv_info1, tv_info2, tv_call_us_on, tv_call_us_on_text, tv_locate_us_text;
    private TextView tv_emailon_title, tv_emailon_title_text;
    private ImageView iv_locate_us;
    private Typeface faceHeader;//faceRegular
    private Context mContext;
    private TextView header;
    private LinearLayout llback, ll_locate_us;
    private Toolbar toolbar;
    private TextView tb_title;
    private int colorInt;
    private boolean renew;
    private String language;
    private boolean firstTime;
    //    private int languageId;
    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private Map<String, JSONObject> languageMap;
    private Labelntext labelntext;
    private Menu menu;
    private boolean showLanguageDialog;
    private String url_not_valid, no_internet, something_went_wrong, no_card_found;
    private ProgressDialog dialog;
    private String please_wait = "Please wait...";
    private String language_changed_successfully = "Language Successfully Changed";
    ProgressDialog dialog1;
    private List<DeviceDetails> deviceDetails;

    public static Drawable changeDrawableColor(int drawableRes, String colorRes, Context context) {
        //Convert drawable res to bitmap
        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), drawableRes);
        final Bitmap resultBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                bitmap.getWidth() - 1, bitmap.getHeight() - 1);
        final Paint p = new Paint();
        final Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, p);

        //Create new drawable based on bitmap
        final Drawable drawable = new BitmapDrawable(context.getResources(), resultBitmap);
        drawable.setColorFilter(new
                PorterDuffColorFilter(Color.parseColor(colorRes), PorterDuff.Mode.MULTIPLY));
        return drawable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_membership_options);
        mContext = this;

        RxBus.get().register(this);
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }

        Log.d("clickedMember", "onCreate: " + clickedMember);
        configList = MemberParser.parsConfig(this);

        deviceDetails = configList.get(clickedMember).getDeviceDetailsList();
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);

        colorInt = Color.parseColor(configList.get(clickedMember)
                .getColorCode());


//        android.content.res.Configuration config = getBaseContext().getResources().getConfiguration();

        /*String lang = settings.getString("LANG", "");
        language = lang;
        isChecked = settings.getBoolean("isChecked", false);
        if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }*/

        //changBgColor();
        PackageInfo info;

        try {
            info = getPackageManager().getPackageInfo(this.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }


        findViews();


//        tv_info1.setText(configList.get(clickedMember).getDescription());

        /*if (Utility.isNetworkAvailable(mContext)) {
            getConfigRetrofitCall(true, language, false);
        } else {

            setUi(language);

        }*/
//        setUi(language);


        /*if (Utility.isNetworkAvailable(mContext)) {
            setUi(language);

        } else {*/

        initLanguage();


        downloadImage();

        //check block or reset
        List<DeviceDetails> deviceDetailsList = configList.get(clickedMember)
                .getDeviceDetailsList();
        if (deviceDetailsList != null) {
            for (int i = 0; i < deviceDetailsList.size(); i++) {
                DeviceDetails deviceDetails = deviceDetailsList.get(i);
                if (deviceDetails.getDeviceToken().equalsIgnoreCase(Utility.getDeviceId(this))) {
                    if (deviceDetails.isBlock() || deviceDetails.isReset()) {
                        DataProvider.getInstance(mContext).DeleteMemberDetailsOnBlock(
                                DBController.createInstance(mContext, true));
                    }
                }
            }
        }

        ///checkPermissions();
        //Change move to next screen
        try {
            checkExpiryMsg(language);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //AppRater.app_launched(this, getFragmentManager());


    }

    @Subscribe
    public void rxLanguageCallBack(RxBusResultModel rxBusResultModel) {
        if (rxBusResultModel != null && rxBusResultModel.getTag().equalsIgnoreCase(LanguageRecyclerAdapter.class.getSimpleName())) {
            HashMap<String, String> map = new HashMap<>();
            map.put(Constants.UDID, Utility.getDeviceId(this));
            map.put(Constants.ORGANISATION_KEY, configList.get(clickedMember).getMid());
            map.put(Constants.LANGUAGE_ID, rxBusResultModel.getLanguageID());

            callChooseLanguageRetrofit(rxBusResultModel, map, true);

        }
    }

    private void callChooseLanguageRetrofit(RxBusResultModel rxBusResultModel, HashMap<String, String> map, boolean progress) {
        Call<LanguageResponse> bodyCall = Utility.callRetrofit3().chooseLanguage(map);

        if (progress) {
            dialog = Utility.showProgressDialog(this, please_wait);
            dialog.show();
        }

        bodyCall.enqueue(new Callback<LanguageResponse>() {
            @Override
            public void onResponse(Call<LanguageResponse> call, Response<LanguageResponse> response) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;

                }
                if (response.code() == HttpURLConnection.HTTP_OK) {
                    try {

                        if (response.body().getSuccess()) {
                            language = rxBusResultModel.getLanguage();

                            editor.putString(configList.get(clickedMember).getMid(), language);
                            editor.commit();
//                            setUi(language);
//                            setConfiguration(menu);
                            Utility.showToast(mContext, language_changed_successfully);
                            getConfigRetrofitCall(true, rxBusResultModel.getLanguage(), true);
                            try {
                                //Change moved to next screen
                                ///checkExpiryMsg(Utility.getLanguage(rxBusResultModel.getLanguage()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {


                            Utility.showToast(mContext, something_went_wrong);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<LanguageResponse> call, Throwable t) {
                if (dialog != null && dialog.isShowing()) {

                    dialog.dismiss();
                    dialog = null;

                }

                Utility.showToast(mContext, something_went_wrong);

            }
        });

    }

    private void initLanguage() {
        if (configList.get(clickedMember).getLanguageList() != null) {
             langPreferences = getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, MODE_PRIVATE);
            editor = langPreferences.edit();

            editor.putString(configList.get(clickedMember).getMid(), configList.get(clickedMember).getLanguageId());
            editor.commit();
            firstTime = langPreferences.getBoolean(configList.get(clickedMember).getMid() + Constants.FIRST_TIME, false);

        /*for (int i = 0; i < deviceDetails.size(); i++) {
            Log.d(TAG, "position: " + i + " " + deviceDetails.get(i).getDeviceToken());
            Log.d(TAG, "position: " + i + " " + deviceDetails.get(i).getLanguageId());
            if (deviceDetails.get(i).getDeviceToken().equalsIgnoreCase(Utility.getDeviceId(mContext))) {
                editor.putString(configList.get(clickedMember).getMid(), deviceDetails.get(i).getLanguageId());
                editor.commit();
                setUi(deviceDetails.get(i).getLanguageId());
            }
        }*/
            language = langPreferences.getString(configList.get(clickedMember).getMid(), "");
//        languageId = langPreferences.getInt(configList.get(clickedMember).getMid() + Constants.LANGUAGE_ID, -1);
            Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);

            if (configList.get(clickedMember).getLanguageList().size() > 1) {
                showLanguageDialog = true;
            } else {
                showLanguageDialog = false;
            }

           /* if (language.isEmpty() && !firstTime) {
                if (showLanguageDialog) {
                    editor.putBoolean(configList.get(clickedMember).getMid() + Constants.FIRST_TIME, true);

                    showLanguageDialog(false);
                }
            } else {
                setUi(language);
            }*/
           //change
            setUi(language);
            if (language == null) {


                editor.putString(configList.get(clickedMember).getMid(), "");
                Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " :en");
                editor.commit();
            }

        /*if (languageId == -1) {
            editor.putInt(configList.get(clickedMember).getMid() + Constants.LANGUAGE_ID, 1);
            Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " :1");
            editor.commit();
        }*/
        }

    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);


        try {
            showCard.setText(languageMap.get(LanguageConstants.SHOW_MY_MEMBERSHIP_CARDS).getString(title));
            findCard.setText(languageMap.get(LanguageConstants.FIND_MY_MEMBERSHIP_CARDS).getString(title));
            bt_member_benifits.setText(languageMap.get(LanguageConstants.MEMBERSHIP_BENEFITS).getString(title));
            bt_buy_renew_gift_membership.setText(languageMap.get(LanguageConstants.BUYRENEWGIFT_MEMBERSHIP).getString(title));

            tv_call_us_on.setText(languageMap.get(LanguageConstants.CALL_US_ON).getString(title) + " :");
            tv_locate_us_text.setText(languageMap.get(LanguageConstants.LOCATE_US).getString(title));
            tv_emailon_title.setText(languageMap.get(LanguageConstants.OR).getString(title) + " " + languageMap.get(LanguageConstants.EMAIL).getString(title) + " :");

            no_internet = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);
            something_went_wrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);
            no_card_found = languageMap.get(LanguageConstants.NO_CARDS_IN_VALUT).getString(title);
            language_changed_successfully = languageMap.get(LanguageConstants.LANGUAGE_CHANGED_SUCCESSFULLY).getString(title);
            please_wait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            tv_info1.setText(configList.get(clickedMember).getDescription());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        /*} else {

            showCard.setText(languageMap.get(LanguageConstants.SHOW_MY_MEMBERSHIP_CARDS).getTitle());
            findCard.setText(languageMap.get(LanguageConstants.FIND_MY_MEMBERSHIP_CARDS).getTitle());
            bt_member_benifits.setText(languageMap.get(LanguageConstants.MEMBERSHIP_BENEFITS).getTitle());
            bt_buy_renew_gift_membership.setText(languageMap.get(LanguageConstants.BUYRENEWGIFT_MEMBERSHIP).getTitle());


            tv_call_us_on.setText(languageMap.get(LanguageConstants.CALL_US_ON).getTitle() + " :");
            tv_locate_us_text.setText(languageMap.get(LanguageConstants.LOCATE_US).getTitle());
            tv_emailon_title.setText(languageMap.get(LanguageConstants.OR).getTitle() + " " + languageMap.get(LanguageConstants.EMAIL).getTitle() + " :");

            no_internet = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getTitle();
            something_went_wrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getTitle();
            no_card_found = languageMap.get(LanguageConstants.NO_CARDS_IN_VALUT).getTitle();
            language_changed_successfully = languageMap.get(LanguageConstants.LANGUAGE_CHANGED_SUCCESSFULLY).getTitle();


        }*/
    }

    private void findViews() {
        /*faceHeader = Utility.setFaceTypeHeader(mContext);
        faceRegular = Utility.setFaceTypeContent(mContext);*/

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        bt_buy_renew_gift_membership = findViewById(R.id.bt_buy_renew_gift_membership);

        tb_title.setSelected(true);
        tb_title.setText(configList.get(clickedMember).getName());//MemberShip
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        bannerImage = (ImageView) findViewById(R.id.membership_banner_image);
        showCard = (Button) findViewById(R.id.show_card);
        findCard = (Button) findViewById(R.id.find_card);
        bt_member_benifits = (Button) findViewById(R.id.bt_member_benifits);

        Utility.changeDrawableColor(this, showCard, configList.get
                (clickedMember).getColorCode());
        Utility.changeDrawableColor(this, findCard, configList.get
                (clickedMember).getColorCode());
        Utility.changeDrawableColor(this, bt_member_benifits, configList.get
                (clickedMember).getColorCode());
        Utility.changeDrawableColor(this, bt_buy_renew_gift_membership, configList.get
                (clickedMember).getColorCode());

        if(configList.get(clickedMember).isBenefitBtnShow()){
            bt_member_benifits.setVisibility(View.VISIBLE);
        }else {
            bt_member_benifits.setVisibility(View.GONE);
        }
        if (configList.get(clickedMember).isTransactionBuy() || configList.get(clickedMember).isTransactionRenew() || configList.get(clickedMember).isTransactionGift()) {
            bt_buy_renew_gift_membership.setEnabled(true);
        } else {
          /*  bt_buy_renew_gift_membership.setEnabled(false);
            bt_buy_renew_gift_membership.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel_grey));*/
            bt_buy_renew_gift_membership.setVisibility(View.GONE);
        }

        ///bt_member_benifits.setTypeface(faceHeader);
        bt_member_benifits.setOnClickListener(this);
        bt_buy_renew_gift_membership.setOnClickListener(this);

       /* showCard.setTypeface(faceHeader);
        findCard.setTypeface(faceHeader);*/
        findCard.setOnClickListener(this);
        showCard.setOnClickListener(this);

        ll_emailon = (LinearLayout) findViewById(R.id.ll_emailon);
        tv_info1 = (TextView) findViewById(R.id.tv_info1);
        /*tv_info2 = (TextView)findViewById(R.id.tv_info2);*/
//        tv_info1.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tv_info1.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        /*tv_info2.setTypeface(faceRegular);*/
        tv_call_us_on = (TextView) findViewById(R.id.tv_call_us_on);
        tv_call_us_on_text = (TextView) findViewById(R.id.tv_call_us_on_text);
//        tv_call_us_on.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode
// ()));
//        tv_call_us_on_text.setTextColor(Color.parseColor(configList.get(clickedMember)
//                .getColorCode()));
        tv_call_us_on.setTextColor(ContextCompat.getColor(mContext, R.color
                .black));
        tv_call_us_on_text.setTextColor(ContextCompat.getColor(mContext, R.color
                .black));
       // tv_call_us_on_text.setLinkTextColor(ContextCompat.getColor(mContext, R.color.black));

        tv_locate_us_text = (TextView) findViewById(R.id.tv_locate_us_text);
        iv_locate_us = (ImageView) findViewById(R.id.iv_locate_us);
//        iv_locate_us.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()
//        ), PorterDuff.Mode.SRC_ATOP);
        iv_locate_us.setColorFilter(ContextCompat.getColor(mContext, R.color.black), PorterDuff
                .Mode.SRC_ATOP);
        ll_locate_us = (LinearLayout) findViewById(R.id.ll_locate_us);
        ll_locate_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationIntent();
            }
        });
//        tv_locate_us_text.setTextColor(Color.parseColor(configList.get(clickedMember)
//                .getColorCode()));
        tv_locate_us_text.setTextColor(ContextCompat.getColor(mContext, R.color
                .black));
//        tv_call_us_on_text.setTextColor(Color.parseColor(configList.get(clickedMember)
//                .getColorCode()));
        tv_call_us_on_text.setTextColor(ContextCompat.getColor(mContext, R.color
                .black));

        tv_call_us_on_text.setOnClickListener(this);
        //Linkify.addLinks(tv_call_us_on_text, Linkify.ALL);
       // tv_call_us_on_text.setLinkTextColor(ContextCompat.getColor(mContext, R.color.black));

        tv_call_us_on_text.setText(configList.get(clickedMember).getContactNumber()+getConExt());
        //Color.parseColor("#2f6699")

        tv_emailon_title = (TextView) findViewById(R.id.tv_emailon_title);
        tv_emailon_title_text = (TextView) findViewById(R.id.tv_emailon_title_text);
        //tv_emailon_title_text.setOnClickListener(this);
//        tv_emailon_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode
//                ()));
//        tv_emailon_title_text.setTextColor(Color.parseColor(configList.get(clickedMember)
//                .getColorCode()));
        tv_emailon_title.setTextColor(ContextCompat.getColor(mContext, R.color
                .black));
        tv_emailon_title_text.setTextColor(ContextCompat.getColor(mContext, R.color
                .black));
        tv_emailon_title_text.setLinkTextColor(ContextCompat.getColor(mContext, R.color.black));

        tvTopStrip = findViewById(R.id.tvTopStrip);
        tvTopStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode
                ()));

        tv_emailon_title_text.setText(configList.get(clickedMember).getEmail());
//        tv_emailon_title_text.setLinkTextColor(Color.parseColor(configList.get(clickedMember)
//                .getColorCode()));
        tv_emailon_title_text.setLinkTextColor(ContextCompat.getColor(mContext, R.color
                .black));

        if (configList.get(clickedMember).getEmail() == null || configList.get(clickedMember)
                .getEmail().trim().length() == 0) {
            ll_emailon.setVisibility(View.GONE);
        }
        card_view = findViewById(R.id.card_view);
    }

    private void locationIntent() {
        try {
            double latitude = Double.parseDouble(configList.get(clickedMember).getLat());
            double longitude = Double.parseDouble(configList.get(clickedMember).getLng());
            String label = configList.get(clickedMember).getName();
            String uriBegin = "geo:" + latitude + "," + longitude;
            String query = latitude + "," + longitude + "(" + label + ")";
            String encodedQuery = Uri.encode(query);
            String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
            Uri uri = Uri.parse(uriString);
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String maplLabel = "Museum Anywere";
//        final Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                Uri.parse("geo:0,0?q=" + "38.9975574" + "," + "-77.4493774" + "&z=16 (" +
//                        maplLabel + ")"));
//        startActivity(intent);


       /* double latitude = 38.9975574;
        double longitude = -77.4493774;*/


    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
       if(bannerImage!=null){
           int w = bannerImage.getWidth();
           final float ASPECT = 1.5f;
           int h = (int) (w / ASPECT);
           ViewGroup.LayoutParams lp = bannerImage.getLayoutParams();
           lp.height = h;
           bannerImage.setLayoutParams(lp);

           if (!isInstructionShown &&clickedMember!=-1&&configList.get(clickedMember).getMid().equalsIgnoreCase
                   ("TXVzZXVtQW55V2hlcmU=")) {
               Intent intent = new Intent(this, InstructionActivity.class);
               startActivity(intent);
               isInstructionShown = true;
           }
       }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.find_card:
                Intent gotoFind = new Intent(mContext, FindMembershipCard.class);
                startActivity(gotoFind);
                break;
            case R.id.show_card:
                MemberList list = setData();
                if (list.size() > 0) {
                    checkExpiry();
                   /* Intent gotoSHowCard = new Intent(mContext, MemberShipActivity.class);
                    startActivity(gotoSHowCard);*/
                } else {
                    Utility.showAlertDialog2(mContext, no_card_found, configList.get(clickedMember).getMid(), languageMap);
                }
                break;
            case R.id.bt_member_benifits:
                Intent memberBenifitIntent = new Intent(mContext, MembersBenifitsActivity.class);
                startActivity(memberBenifitIntent);
                break;
            case R.id.bt_buy_renew_gift_membership:
                /*Intent buyRenewGiftMembershipIntent = new Intent(mContext, BuyRenewGiftMembershipActivity.class);
                buyRenewGiftMembershipIntent.putExtra(Constants.COLOUR, colorInt);
                startActivity(buyRenewGiftMembershipIntent);*/
                break;
            case R.id.tv_call_us_on_text:
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + tv_call_us_on_text.getText()));
                startActivity(callIntent);
          /*      break;*//*
            case R.id.tv_emailon_title_text:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{tv_emailon_title_text.getText()
                        .toString().trim()});//some@email.address
                *//*intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
                intent.putExtra(Intent.EXTRA_TEXT, "mail body");*//*
                startActivity(Intent.createChooser(intent, ""));
                break;*/
        }

    }

    private MemberList setData() {

        @SuppressWarnings("unchecked")
        ArrayList<MemberInfo> info = CBO.FillCollection(
                DataProvider.getInstance(mContext).getMemberDetails(
                        DBController.createInstance(mContext, false), 0),
                MemberInfo.class);
        MemberList listMember = new MemberList();
        listMember.addAll(info);

        return listMember;

    }

    public void showDialog(String expireMsgText) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set title
        alertDialogBuilder.setTitle("Notification");
        // set dialog message
        alertDialogBuilder
                .setMessage(expireMsgText)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                /*.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        setConfiguration(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Intent intent;
        switch (item.getItemId()) {
            case R.id.notifications:
                intent = new Intent(this, NotificationsActivity.class);
                startActivity(intent);
                return true;
            case R.id.floorPlan:
                intent = new Intent(this, FloorMap.class);
                startActivity(intent);
                return true;
            case R.id.faqs:
                intent = new Intent(this, FAQsActivity.class);
                startActivity(intent);
                return true;
            case R.id.privacy_policy:
                String url = configList.get(clickedMember).getPageLink();
                if (url != null) {
                    /*intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);*/
                    if (URLUtil.isValidUrl(url)) {
                        intent = new Intent(mContext, MyWebViewActivity.class);
                        intent.putExtra(MyWebViewActivity.LINK_URL, url);
                        startActivity(intent);
                    } else {
                        Toast.makeText(mContext, something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

                Log.i("Link", "page link");
                return true;
            case R.id.gift:
               /* intent = new Intent(this, MembershipPlanActivity.class);
                intent.putExtra("purchaseType", PurchaseType.GIFT);*/
                return true;
            case R.id.renew:
                MemberList list = setData();
                if (list.size() > 0) {
                    intent = new Intent(mContext, RenewMembershipActivity.class);
                    intent.putExtra(SEARCH_MODE_EXPIRED, SEARCH_MODE_EXPIRED);
                    startActivity(intent);
                } else {
                    intent = new Intent(mContext, FindMembershipCard.class);
                    intent.putExtra(SEARCH_MODE_EXPIRED, SEARCH_MODE_EXPIRED);
                    startActivity(intent);
                }

               /* intent = new Intent(mContext, FindMembershipCard.class);
                intent.putExtra(SEARCH_MODE_EXPIRED, SEARCH_MODE_EXPIRED);
                startActivity(intent);*/

                return true;
            case R.id.buy:
               /* intent = new Intent(this, MembershipPlanActivity.class);
                intent.putExtra("purchaseType", PurchaseType.BUY);
                startActivity(intent);*/
               /* type = "Renew";
                intent = new Intent(this, MembershipPlanActivity.class);
                intent.putExtra("type", type);*/

                return true;
            case R.id.donation:
                list = setData();
                if (list == null) {
                    return true;
                }
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        String memberId = list.get(i).getMembershipID();
                        if (memberId != null) {
                            break;
                        }
                    }

                   /* intent = new Intent(this, DonationActivity.class);
//                intent.putExtra("purchaseType", PurchaseType.BUY);
                    intent.putExtra("memberId", configList.get(0).getMid());
                    intent.putExtra("purchaseType", PurchaseType.DONATION);
                    Log.d("memberId", "onOptionsItemSelected: " + configList.get(0).getMid());
                    startActivity(intent);*/
               /* type = "Renew";
                intent = new Intent(this, MembershipPlanActivity.class);
                intent.putExtra("type", type);*/
                    return true;
                } else {
                    Toast.makeText(this, "Please Download card first to donate!", Toast.LENGTH_LONG).show();
                    return true;
                }

            case R.id.reciprocal:
//                intent = new Intent(this, ReciprocalActivity.class);
////                intent.putExtra("purchaseType", PurchaseType.BUY);
//                startActivity(intent);
               /* type = "Renew";
                intent = new Intent(this, MembershipPlanActivity.class);
                intent.putExtra("type", type);*/
                intent = new Intent(mContext, ReciprocalActivity.class);
                startActivity(intent);
                /*if (Utility.isNetworkAvailable(mContext)) {
                    getReciprocalCall(configList.get(clickedMember).getMid());
                } else {
                    Utility.showToast(mContext, getString(R.string.msg_no_internet));
                }*/
                return true;

            case R.id.language:

                /*isChecked = !item.isChecked();
                item.setChecked(isChecked);
                Log.d(TAG, "onOptionsItemSelected: " + isChecked);

                if (isChecked) {
                    editor.putString(configList.get(clickedMember).getMid(), "fr");
                    editor.putBoolean("isChecked", isChecked).commit();
                    item.setTitle("French");
                    language = "fr";
                    Log.d(TAG, "onclick: "+configList.get(clickedMember).getMid() + configList.get(clickedMember).getName()+" :fr");

//                    setLangRecreate("fr");
                } else {
                    editor.putString(configList.get(clickedMember).getMid(), "en");
                    editor.putBoolean("isChecked", isChecked).commit();
//                    setLangRecreate("en");
                    item.setTitle("English");
                    language = "en";

                    Log.d(TAG, "onclick: "+configList.get(clickedMember).getMid() + configList.get(clickedMember).getName()+" :en");

                }


                editor.commit();
                setConfiguration(menu);*/

                showLanguageDialog(true);


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showLanguageDialog(boolean showCheckedLanguage) {
        LanguageDialogFragment languageDialogFragment = LanguageDialogFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.LANGUAGE_ID, showCheckedLanguage);
        languageDialogFragment.setArguments(bundle);
        languageDialogFragment.show(getSupportFragmentManager(), "language fragment");
    }

    private void setLangRecreate(String langval) {
        android.content.res.Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        recreate();
    }

    /**
     * Dynamically
     * Modify menu items here
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    private void setConfiguration(Menu menu) {
        List<Configuration> configurationList;
        MenuItem item8 = menu.findItem(R.id.action_new).setVisible(true);
        Utility.changeMenuIconDrawable(item8, mContext, Color.parseColor(configList.get
                (clickedMember).getColorCode()));
        MenuItem[] menuItems = new MenuItem[10];
        try {
            configurationList = MemberParser.parsConfig(this);
            if (configurationList.size() > 0) {

                //enable floor plan
                MenuItem item1 = menu.findItem(R.id.floorPlan).setVisible(configurationList.get
                        (clickedMember).isFloorPlan());
                MenuItem item2 = menu.findItem(R.id.notifications).setVisible(configurationList
                        .get(clickedMember).isNotification());
                MenuItem item3 = menu.findItem(R.id.faqs).setVisible(configurationList.get
                        (clickedMember).isFaq());


                menuItems[0] = item1;
                menuItems[1] = item2;
                menuItems[2] = item3;

            } else {
                menu.findItem(R.id.floorPlan).setVisible(false);
                menu.findItem(R.id.notifications).setVisible(false);
                menu.findItem(R.id.faqs).setVisible(false);
            }

            MenuItem item5 = menu.findItem(R.id.gift).setVisible(configurationList.get(clickedMember).isTransactionGift());
            MenuItem item6 = menu.findItem(R.id.buy).setVisible(configurationList.get(clickedMember).isTransactionBuy());
            MenuItem item7 = menu.findItem(R.id.renew).setVisible(configurationList.get(clickedMember).isTransactionRenew());
            MenuItem itemDonation = menu.findItem(R.id.donation).setVisible(configurationList.get(clickedMember).isTransactionDonate());
            MenuItem itemLanguage = menu.findItem(R.id.language).setVisible(configurationList.get(clickedMember).getLanguageList().size() > 1);
            MenuItem itemPageLink = menu.findItem(R.id.privacy_policy).setVisible(configurationList.get(clickedMember).isPageLink());

            MenuItem item9 = menu.findItem(R.id.reciprocal).setVisible(configurationList.get
                    (clickedMember).isReciprocal());
            // MenuItem item9 = menu.findItem(R.id.reciprocal).setVisible(true);
            menuItems[3] = itemPageLink;
            menuItems[4] = item5;
            menuItems[5] = item6;
            menuItems[6] = item7;
            menuItems[7] = item9;
            menuItems[8] = itemDonation;
            menuItems[9] = itemLanguage;

            itemPageLink.setTitle(configurationList.get(clickedMember).getPageLinkLevel());


//            itemLanguage.setChecked(isChecked);
//
//            if (language.equalsIgnoreCase("en")) {
//                itemLanguage.setTitle("English");
//
//            } else {
//                itemLanguage.setTitle("French");
//
//            }
            Utility.changeMenuIconDrawable(menuItems[1], mContext, colorInt);
            changeMenuItemColor(menuItems[1]);

            Utility.changeMenuIconDrawable(menuItems[2], mContext, colorInt);
            changeMenuItemColor(menuItems[2]);
            Utility.changeMenuIconDrawable(menuItems[3], mContext, colorInt);
            changeMenuItemColor(menuItems[3]);
            Utility.changeMenuIconDrawable(menuItems[5], mContext, colorInt);
            changeMenuItemColor(menuItems[5]);
            Utility.changeMenuIconDrawable(menuItems[4], mContext, colorInt);
            changeMenuItemColor(menuItems[4]);
            Utility.changeMenuIconDrawable(menuItems[6], mContext, colorInt);
            changeMenuItemColor(menuItems[6]);
            Utility.changeMenuIconDrawable(menuItems[7], mContext, colorInt);
            changeMenuItemColor(menuItems[7]);
            Utility.changeMenuIconDrawable(menuItems[8], mContext, colorInt);
            changeMenuItemColor(menuItems[8]);
            Utility.changeMenuIconDrawable(menuItems[9], mContext, colorInt);
            changeMenuItemColor(menuItems[9]);


            String title = "title";
            if (language.equalsIgnoreCase(Constants.ENGLISH)) {

            } else {
                title = title + "_" + language;
            }

            try {

                menuItems[0].setTitle(languageMap.get(LanguageConstants.FLOOR_PLAN).getString(title));
                menuItems[1].setTitle(languageMap.get(LanguageConstants.NOTIFICATIONS).getString(title));
                menuItems[2].setTitle(languageMap.get(LanguageConstants.FAQS).getString(title));
                ///menuItems[3].setTitle(languageMap.get(LanguageConstants.OUR_WEBSITE).getString(title));

                item9.setTitle(languageMap.get(LanguageConstants.RECIPROCALS).getString(title));

                item6.setTitle(languageMap.get(LanguageConstants.BUY_NEW).getString(title));
                item7.setTitle(languageMap.get(LanguageConstants.RENEW).getString(title));
                item5.setTitle(languageMap.get(LanguageConstants.GIFT).getString(title));
                itemDonation.setTitle(languageMap.get(LanguageConstants.DONATE).getString(title));
                itemLanguage.setTitle(languageMap.get(LanguageConstants.LANGUAGE).getString(title));

            } catch (JSONException e) {
                e.printStackTrace();
            }


            changeMenuItemColor(menuItems);
            changeMenuIconColor(menuItems);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void downloadImage() {
        List<String> urlList = new ArrayList<>();

        for (Configuration config : configList) {
            urlList.add(config.getImagePath() + "/" + config.getBannerImage());
        }
        String imagePath = configList.get(clickedMember).getImagePath();
        String bannerUrl = imagePath + "/" + configList.get(clickedMember).getBannerImage();
        ImageLoader.getInstance().displayImage(bannerUrl, bannerImage);

    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            //do your check here
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission
                        .WRITE_EXTERNAL_STORAGE}, 1);
            }

        }
    }

    private void changeMenuItemColor(MenuItem[] menuItems) {
        for (int i = 0; i < menuItems.length; i++) {
            SpannableString s = new SpannableString(menuItems[i].getTitle());
            s.setSpan(new ForegroundColorSpan(colorInt), 0, s.length(), 0);
            menuItems[i].setTitle(s);
            Drawable drawable = menuItems[i].getIcon();
            if (drawable != null) {
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable.mutate(), Color.parseColor(configList.get
                        (clickedMember).getColorCode()));
                menuItems[i].setIcon(drawable);
            }

        }

    }

    private void changeMenuItemColor(MenuItem menuItems) {
        SpannableString s = new SpannableString(menuItems.getTitle());
        s.setSpan(new ForegroundColorSpan(colorInt), 0, s.length(), 0);
        menuItems.setTitle(s);

    }


    private void changeMenuIconColor(MenuItem[] menuItems) {
        for (int i = 0; i < menuItems.length; i++) {
            Drawable drawable = menuItems[i].getIcon();
            if (drawable != null) {
                drawable = DrawableCompat.wrap(drawable);
                DrawableCompat.setTint(drawable.mutate(), Color.parseColor(configList.get
                        (clickedMember)
                        .getColorCode()));
                menuItems[i].setIcon(drawable);
            }
        }

    }


    private void checkExpiryMsg(String lang) throws Exception {
        String id = "";
        String expireMsgText = "";
        String membershipIdText = "";
        String LastNameText = "";
        String numberText = "";
        String constituentId = "";
        String searchType = "";
        //String expiryDate = "";
        Cursor cursor = DataProvider.getInstance(mContext).getMemberDetails(DBController
                .createInstance(mContext, false), 0);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            expireMsgText = cursor.getString(cursor.getColumnIndex(MemberController
                    .MEMBER_EXPIRE_MESSAGE));
            membershipIdText = cursor.getString(cursor.getColumnIndex(MemberController.MEMBER_ID));
            LastNameText = cursor.getString(cursor.getColumnIndex(MemberController
                    .MEMBER_LASTNAME));
            numberText = cursor.getString(cursor.getColumnIndex(MemberController.MEMBER_NUMBER));
            expiryDate = cursor.getString(cursor.getColumnIndex(MemberController.MEMBER_THROUGH));
            constituentId = cursor.getString(cursor.getColumnIndex(MemberController
                    .MEMBER_CONSTITUENTID));
            searchType = cursor.getString(cursor.getColumnIndex(MemberController.SEARCH_TYPE));
            if (searchType == null) {
                searchType = "1";
            }
            if (searchType.equals("1")) {
                if (membershipIdText == null) {
                    membershipIdText = "";
                }
                if (!membershipIdText.isEmpty()) {
                    id = membershipIdText;
                    break;
                }
            } else if (searchType.equals("2")) {
                if (constituentId == null) {
                    constituentId = "";
                }
                if (!constituentId.isEmpty()) {
                    id = constituentId;
                    break;
                }
            } else if (searchType.equals("3")) {
                if (numberText == null) {
                    numberText = "";
                }
                if (!numberText.isEmpty()) {
                    id = numberText;
                    break;
                }
            }
            cursor.moveToNext();
        }
        cursor.close();
        //Toast.makeText(mContext,"expireMsgText: "+expireMsgText,Toast.LENGTH_LONG).show();
        if (!LastNameText.equals("")) {
            //For on upgrade
            final Map<String, Object> map = new LinkedHashMap<>();
            map.put("mId", id);//membershipIdText
            map.put("lName", LastNameText);
            map.put("organisation_key", configList.get(clickedMember).getMid());
            map.put("udID", Utility.getDeviceToken(mContext));
            map.put("deviceType", "2");
            map.put("deviceName", Utility.getDeviceName());
            map.put("uniqueDeviceID", Utility.getDeviceId(this));


            if (Utility.isNetworkAvailable(mContext)) {
                new MemberTaskInitial(mContext, lang, please_wait) {
                    @Override
                    public void onTaskFinished(MemberList list) {
                        if (list.size() > 0) {
                            MembershipOptions.this.list = list;

                            if (!list.get(0).getExpiryMessageText().equalsIgnoreCase("-1")) {

                                expiryMsg = list.get(0).getExpiryMessageText();
                                ///showDialog(list.get(0).getExpiryMessageText());
                            } else {
                                expiryMsg = "";
                            }
                            saveCard(list);
                            if (map != null) {
                                Utility.syncDataOnAppUpgrade(mContext, map);
                            }
                        }
                    }
                }.execute(Constants.MODE_NORM,
                        LastNameText, id);//membershipIdText
            } else {
                //Utility.showAlertDialog(mContext,getResources().getString(R.string
                // .msg_no_internet));
                Toast.makeText(mContext, no_internet,
                        Toast.LENGTH_LONG).show();

            }
        } else {
            //Toast.makeText(mContext,expireMsgText,Toast.LENGTH_LONG).show();
            //ShowDialog(expireMsgText);
        }

    }

    private void checkExpiry() {
        if (list != null) {

          /*  //if (configList.get(clickedMember).isTransactionRenew()) {
                List<String> listFrequency = configList.get(clickedMember).getNotificationFrequency();
                if ((!Utility.isExpiredCard(list.get(0).getValidThroughTimeSpan()) || ((Utility.isNotificationFrequency(list.get(0).getValidThroughTimeSpan(), listFrequency))))) {
                    Intent intent = new Intent(mContext, RenewMembershipActivity.class);
                    startActivity(intent);
                    return;
                }
           // }*/

            String isExpired = list.get(0).getIsExpire();
            if (isExpired.equalsIgnoreCase(ONE)) {

                               /* ExpiryDialog ex = new ExpiryDialog(mContext,1);
                                ex.setCancelable(false);
                                ex.show();*/
//                showDialog(list.get(0).getExpiryMessageText());
                Intent gotoSHowCard = new Intent(mContext, MemberShipActivity.class);
                gotoSHowCard.putExtra(EXPIRY_MSG, expiryMsg);
                startActivity(gotoSHowCard);
            } else {
                Intent gotoSHowCard = new Intent(mContext, MemberShipActivity.class);
                gotoSHowCard.putExtra(EXPIRY_MSG, expiryMsg);
                startActivity(gotoSHowCard);
            }
        } else {
            if (expiryDate != null && !expiryDate.isEmpty()) {
                if (!Utility.isExpiredCard(expiryDate)) {
                    /*showDialog(YOUR_CARD_IS_EXPIRED);
                    showCard.setEnabled(false);*/
                   /* ExpiryDialog ex = new ExpiryDialog(mContext,1);
                    ex.setCancelable(false);
                    ex.show();*/

//                    showDialog(list.get(0).getExpiryMessageText());

                    Intent gotoSHowCard = new Intent(mContext, MemberShipActivity.class);
                    gotoSHowCard.putExtra(EXPIRY_MSG, expiryMsg);
                    startActivity(gotoSHowCard);
                } else {
                    Intent gotoSHowCard = new Intent(mContext, MemberShipActivity.class);
                    startActivity(gotoSHowCard);
                }
            } else {
                Intent gotoSHowCard = new Intent(mContext, MemberShipActivity.class);
                startActivity(gotoSHowCard);
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* Intent intent=new Intent(this,InstructionActivity.class);
        startActivity(intent);*/
       /* if (!isInstructionShown && configList.get(clickedMember).getMid().equalsIgnoreCase
                ("TXVzZXVtQW55V2hlcmU=")) {
            Intent intent = new Intent(this, InstructionActivity.class);
            startActivity(intent);
            isInstructionShown = true;
        }*/
    }


    private void saveCard(List<MemberInfo> memberInfoList) {
        DataProvider.getInstance(mContext).DeleteMemberDetails(
                DBController.createInstance(mContext, true), 0, null);
        boolean insertMain = false,
                insertDec = false;
        for (int i = 0; i < memberInfoList.size(); i++) {
            for (int j = 0; j < memberInfoList.get(i).getSpouseList().size(); j++) {
                insertDec = false;
                insertDec = DataProvider.getInstance(mContext)
                        .insertMemberSpouseDetails(DBController.createInstance(mContext, true),
                                memberInfoList.get(i).getSpouseList().get(j));
            }
            insertMain = DataProvider.getInstance(mContext)
                    .insertMemberDetails(DBController.createInstance(mContext, true),
                            memberInfoList.get(i));

        }


    }

    private void getReciprocalCall(String organizationKey) {
        final ProgressDialog dialog = Utility.showProgressDialog(mContext, "Please wait...");
        dialog.show();
        Call<ReciprocalResult> bodyCall = Utility.callRetrofit3().getReciprocalData
                (organizationKey);
        bodyCall.enqueue(new Callback<ReciprocalResult>() {
            @Override
            public void onResponse(Call<ReciprocalResult> call, Response<ReciprocalResult>
                    response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                if (response.body().getSuccess()) {

//                    loadURLinWebBrowser(response.body().getReciprocals().get(0).getUrl());
//                    loadDialog(response.body().getReciprocals());
                    loadInActivity(response.body().getReciprocals());

                } else {
                    Utility.showToast(mContext, something_went_wrong);

                }
//                Utility.showToast(mContext, "success" + response.code());
            }

            @Override
            public void onFailure(Call<ReciprocalResult> call, Throwable t) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                Utility.showToast(mContext, something_went_wrong);
            }
        });
    }

    private void loadURLinWebBrowser(String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);

    }


    private void loadInActivity(List<Reciprocal> reciprocalList) {

        Intent intent = new Intent(mContext, ReciprocalActivity.class);
        intent.putParcelableArrayListExtra("list", (ArrayList<? extends Parcelable>)
                reciprocalList);
        startActivity(intent);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop: ");

        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: ");

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        RxBus.get().unregister(this);

        Log.d(TAG, "onDestroy: ");
        super.onDestroy();
    }
/*
    private void callRetroResponse() {

        if (Utility.isNetworkAvailable(this)) {
            boolean progress = true;
            String data = preferences.getString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, null);
            if (data != null && data.length() > 10) {
                progress = false;
                configList = MemberParser.parsConfig(mContext);

            }
            getConfigRetrofitCall(progress);
        } else {
            String data = preferences.getString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, null);
            if (data != null && data.length() > 10) {
                configList = MemberParser.parsConfig(mContext);
            }
            Toast.makeText(this, getResources().getString(R.string.msg_no_internet), Toast
                    .LENGTH_LONG).show();
        }

    }*/

    private void getConfigRetrofitCall(final boolean progress, String lang, boolean setMenu) {


        if (progress) {
            dialog1 = Utility.showProgressDialog(this, please_wait);
            dialog1.show();
        }

//        Call<ResponseBody> bodyCall = Utility.callRetrofit().getconfiguration2(Utility
//                .getDeviceId(this));
        Call<ResponseBody> bodyCall;


        bodyCall = Utility.callRetrofit3().getconfiguration(Utility.getDeviceId(this));


        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (dialog1 != null && dialog1.isShowing()) {
                    dialog1.dismiss();
                    dialog1 = null;
                }

                if (response.code() == HttpURLConnection.HTTP_OK) {

                    String respo = "";
                    try {
                        respo = response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.d("respo", respo + "");
//                    Log.d("texts n label", response.body());

                    preferences.saveString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, respo);

                    configList = MemberParser.parsConfig(mContext);

                    setUi(lang);

                    if (setMenu) {
                        setConfiguration(menu);
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (dialog1 != null && dialog1.isShowing()) {
                    dialog1.dismiss();
                    dialog1 = null;
                }
            }
        });
    }

private String getConExt(){
    String conExten=configList.get(clickedMember).getContact_ext();
    if(conExten!=null&&!conExten.isEmpty())
    {
        conExten=";"+conExten;
    }else {
        conExten="";
    }
    return conExten;
}
}
