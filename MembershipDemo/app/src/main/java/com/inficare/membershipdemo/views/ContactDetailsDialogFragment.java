package com.inficare.membershipdemo.views;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.ContactsEmailRecyclerAdapter;
import com.inficare.membershipdemo.adapter.ContactsPhoneRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by pratyush on 3/14/2018.
 */

public class ContactDetailsDialogFragment extends DialogFragment {


    @BindView(R.id.tv_contact_name)
    TextView tvContactName;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.rv_phone)
    RecyclerView rvPhone;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.rv_email)
    RecyclerView rvEmail;
    @BindView(R.id.media_card_view)
    CardView mediaCardView;
    @BindView(R.id.bt_done)
    TextView btDone;
    Unbinder unbinder;
    private Context mContext;
    private List<String> phoneList;
    private List<String> emailList;


    public ContactDetailsDialogFragment() {
        // Required empty public constructor
    }


    public static ContactDetailsDialogFragment newInstance() {
        ContactDetailsDialogFragment fragment = new ContactDetailsDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_contact_details, container, false);
        unbinder = ButterKnife.bind(this, view);

        populateLists();


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvPhone.setLayoutManager(layoutManager);
        rvEmail.setLayoutManager(layoutManager);

        rvPhone.setAdapter(new ContactsPhoneRecyclerAdapter(mContext,phoneList));
        rvEmail.setAdapter(new ContactsEmailRecyclerAdapter(mContext,emailList));



        return view;
    }

    private void populateLists() {
        phoneList = new ArrayList<>();
        emailList = new ArrayList<>();

        for (long i = 8888888; i<8888898; i++){
            phoneList.add(String.valueOf(i)+"1234");
            emailList.add("abc"+i+"@xyz.com");

        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
