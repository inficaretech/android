package com.inficare.membershipdemo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.ExpListAdapter;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.FAQResponse;
import com.inficare.membershipdemo.pojo.Faqs;
import com.inficare.membershipdemo.pojo.HeaderInfo;
import com.inficare.membershipdemo.pojo.QuestionsInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FAQsActivity extends AppCompatActivity {
    public static final String FAQ_SAVED_DATA = "faq";
    ExpandableListView expandableListView;
    ExpListAdapter adapter;
    List<HeaderInfo> headerInfoList;
    List<QuestionsInfo> questionsInfoList;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    private ProgressDialog dialog;
    List<Faqs> faqsList;
    TextView tvTopStrip, tvNoReciprocal;
    Drawable minus_icon, plus_icon;
    String mid;
    private boolean progressVisible = true;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private TextView toolbar_title;

    private Context mContext;

    public static final String TAG = FAQsActivity.class.getSimpleName();
    private String pleaseWait, somethingWentWrong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        mContext = this;

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        preferences = MuseumDemoPreference.getInstance(this);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        mid = configList.get(clickedMember).getMid();

        initLanguage();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.toolbar_title));
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTopStrip = (TextView) findViewById(R.id.tvTopStrip);
        tvNoReciprocal = (TextView) findViewById(R.id.tvNoReciprocal);
//        tvTopStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember)
// .getColorCode()));
        //tvTopStrip.setBackgroundColor(ContextCompat.getColor(this, R.color.black));
        tvTopStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        headerInfoList = new ArrayList<>();
        changeGrpIndicatorColor();
        getSavedData();
        if (Utility.isNetworkAvailable(this)) {
            getData(progressVisible,language);
        }

        setUi(language);

    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);


        try {

            toolbar_title.setText(languageMap.get(LanguageConstants.FAQS).getString(title));
            tvNoReciprocal.setText(languageMap.get(LanguageConstants.NO_FAQS_AS_YET).getString(title));
            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            somethingWentWrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void getData(final boolean progressVisible, String lang) {
        if (progressVisible) {
            dialog = Utility.showProgressDialog(this, pleaseWait);
            dialog.show();
        }

        lang = Utility.getLanguage(lang);

        Call<FAQResponse> memberReportCall = Utility.callRetrofit2().getFAQs(mid, lang);
        memberReportCall.enqueue(new Callback<FAQResponse>() {

            @Override
            public void onResponse(Call<FAQResponse> call, retrofit2.Response<FAQResponse>
                    response) {

                if (progressVisible)
                    dialog.dismiss();

                faqsList = response.body().getFaqs();

                if (faqsList == null) {
                    if (progressVisible)
                        Toast.makeText(FAQsActivity.this, somethingWentWrong, Toast.LENGTH_LONG).show();
                } else {

                    if (faqsList.size() > 0) {
                        headerInfoList.clear();
                        tvNoReciprocal.setVisibility(View.GONE);
                    }
                    for (int i = 0; i < faqsList.size(); i++) {
                        QuestionsInfo info = new QuestionsInfo(1, faqsList.get(i).getAnswer());
                        questionsInfoList = new ArrayList<>();
                        questionsInfoList.add(info);
                        HeaderInfo info1 = new HeaderInfo(faqsList.get(i).getQuestion(),
                                questionsInfoList);
                        headerInfoList.add(info1);
                    }
                    Gson gson = new Gson();
                    String jsonData = gson.toJson(headerInfoList);
                    preferences.saveString(FAQ_SAVED_DATA.concat(mid), jsonData);
                    //if(adapter==null){
                    adapter = new ExpListAdapter(FAQsActivity.this, headerInfoList,expandableListView);
                    expandableListView.setAdapter(adapter);
                    /*}else {
                        adapter.notifyDataSetChanged();
                    }*/

                }

            }

            @Override
            public void onFailure(Call<FAQResponse> call, Throwable t) {
                /*dialog.dismiss();*/
                if (progressVisible)
                    Toast.makeText(FAQsActivity.this, somethingWentWrong,
                            Toast.LENGTH_LONG).show();

            }
        });

    }

    private void changeGrpIndicatorColor() {
        plus_icon = getResources().getDrawable(R.drawable.plus_icon);
        minus_icon = getResources().getDrawable(R.drawable.plus_icon);
//        plus_icon.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
//                PorterDuff.Mode.SRC_ATOP);
//        minus_icon.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
//                PorterDuff.Mode.SRC_ATOP);

        plus_icon.setColorFilter(ContextCompat.getColor(this, R.color.black),
                PorterDuff.Mode.SRC_ATOP);
        minus_icon.setColorFilter(ContextCompat.getColor(this, R.color.black),
                PorterDuff.Mode.SRC_ATOP);

        expandableListView.setGroupIndicator(null);
       /* expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View clickedView, int
            groupPosition, long rowId) {
                 groupIndicator = (ImageView) clickedView.findViewById(R.id.help_group_indicator);
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                    groupIndicator.setImageResource(R.drawable.plus_icon);

                    //headerInfoList.get(groupPosition).setExpanded(false);
                } else {
                    parent.expandGroup(groupPosition);
                    groupIndicator.setImageResource(R.drawable.minus_icon);
                    //headerInfoList.get(groupPosition).setExpanded(true);

                }

                return true;
            }
        });*/
/*
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[] {android.R.attr.state_expanded}, plus_icon);
        states.addState(new int[] {android.R.attr.state_empty},minus_icon);
        expandableListView.setGroupIndicator(states);*/


    }

    private void getSavedData() {
        List<HeaderInfo> headerInfoList = null;
        String jsonData = preferences.getString(FAQ_SAVED_DATA.concat(mid), null);
        if (jsonData != null) {
            Type listType = new TypeToken<ArrayList<HeaderInfo>>() {
            }.getType();

            headerInfoList = new Gson().fromJson(jsonData, listType);

            if (headerInfoList != null && headerInfoList.size() > 0) {
                adapter = new ExpListAdapter(FAQsActivity.this, headerInfoList,expandableListView);
                expandableListView.setAdapter(adapter);
                progressVisible = false;
                tvNoReciprocal.setVisibility(View.GONE);
            }
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
