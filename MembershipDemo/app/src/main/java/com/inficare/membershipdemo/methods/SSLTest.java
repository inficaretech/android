package com.inficare.membershipdemo.methods;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import android.content.Context;

public class SSLTest {

	public static SSLSocketFactory getSSLSocketFactory(Context ctx) {
		// Load CAs from an InputStream
		// (could be from a resource or ByteArrayInputStream or ...)
		CertificateFactory cf = null;
		InputStream caInput = null;
		HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
		try {
			cf = CertificateFactory.getInstance("X.509");
			// From
			// https://www.washington.edu/itconnect/security/ca/load-der.crt
			try {
				caInput = new BufferedInputStream(ctx.getAssets().open(
						"raw/www.saas.inficaresoft.crt"));
			/*	caInput = new BufferedInputStream(ctx.getAssets().open(
						"raw/ssl_certificate.p7b"));*/
			} catch (IOException e) {
				e.printStackTrace();
			}
			// new FileInputStream(

			// "www.saas.inficaresoft.crt"));
		} catch (CertificateException ce) {
			ce.printStackTrace();
		}
		Certificate ca = null;
		try {
			ca = cf.generateCertificate(caInput);
			System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
		} catch (CertificateException ce) {
			ce.printStackTrace();
		} finally {
			try {
				caInput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Create a KeyStore containing our trusted CAs
		String keyStoreType = KeyStore.getDefaultType();
		KeyStore keyStore = null;
		try {
			keyStore = KeyStore.getInstance(keyStoreType);
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		try {
			keyStore.load(null, null);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			keyStore.setCertificateEntry("ca", ca);
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}

		// Create a TrustManager that trusts the CAs in our KeyStore
		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmf = null;
		try {
			tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			tmf.init(keyStore);
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}

		// Create an SSLContext that uses our TrustManager
		SSLContext context = null;
		try {
			context = SSLContext.getInstance("TLS");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			context.init(null, tmf.getTrustManagers(), null);

		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		return context.getSocketFactory();
	}

}
