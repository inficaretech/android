package com.inficare.membershipdemo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.inficare.membershipdemo.R;

/**
 * Created by Abhishek.jha on 10/21/2016.
 */
public class ReportAdapterViewHolder extends RecyclerView.ViewHolder{

    public TextView membership_number, member_name, used_pass, left_pass, valid_from, valid_till;

    public ReportAdapterViewHolder(View itemView) {
        super(itemView);
        membership_number = (TextView)itemView.findViewById(R.id.membership_number);
        member_name = (TextView)itemView.findViewById(R.id.member_name);
        used_pass = (TextView)itemView.findViewById(R.id.used_pass);
        left_pass = (TextView)itemView.findViewById(R.id.left_pass);
        valid_from = (TextView)itemView.findViewById(R.id.valid_from);
        valid_till = (TextView)itemView.findViewById(R.id.valid_till);

    }
}
