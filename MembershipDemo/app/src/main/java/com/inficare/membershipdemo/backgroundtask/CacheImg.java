package com.inficare.membershipdemo.backgroundtask;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.inficare.membershipdemo.methods.SSLTest;
import com.inficare.membershipdemo.networks.HttpMethod;
import com.nostra13.universalimageloader.core.download.ImageDownloader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

public abstract class CacheImg extends AsyncTask<String, Void, List<File>> {
    private Context context;
    private ProgressDialog dialog;
    private static final String RESULT = "result";
    String folder = ".ecard";
    List<String> urlList;
//String accessToken;

    public CacheImg(Context context, List<String> urlList) {
        this.context = context;
        dialog = new ProgressDialog(context);
        this.urlList = urlList;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (dialog != null) {
            dialog.setMessage("Please wait...");
            dialog.setCancelable(true);
            //dialog.show();
        }
    }

    @Override
    protected List<File> doInBackground(String... params) {
        Bitmap bitmap = null;
        InputStream input = null;
        File directory = null;
        List<File> fileList = new ArrayList<>();
        try {

            for (int i = 0; i < urlList.size(); i++) {
                String url = urlList.get(i);
                String[] lastName = url.split("/");

                String fileName = lastName[lastName.length - 1];
                File myPath = new File(context.getCacheDir(), fileName);
                if (!myPath.exists()) {
                    File file = getPath(url, myPath.getPath());
                    fileList.add(file);
                } else {
                    fileList.add(myPath);
                    // return mypath;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        return fileList;
    }

    @Override
    protected void onPostExecute(List<File> result) {
        super.onPostExecute(result);
        if (dialog.isShowing()) {
            dialog.dismiss();
            // Toast.makeText(mContext, " " + result, Toast.LENGTH_LONG).show();
        }
        onTaskFinished(result);
    }

    public abstract void onTaskFinished(List<File> file);

    /**
     * @param imageurl  remote image url
     * @param localpath your sd card path
     * @return file object
     */
    private File getPath(String imageurl, String localpath) {
        String filepath = null;
        File file = null;
        InputStream inputStream = null;
        HttpURLConnection urlConnection=null;

        try {
            URL url = new URL(imageurl);
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(false);
                urlConnection.setConnectTimeout(20000);
                urlConnection.connect();
                inputStream = urlConnection.getInputStream();
            } catch (SocketTimeoutException e) {
                Log.i("SocketTimeoutException:","floor:"+e);
                return file;
            } catch (Exception e) {


                Log.i("SocketTimeoutException:","floor:"+e);
                return file;
            }
            if (localpath != null) {
                file = new File(localpath);

                FileOutputStream fileOutput = new FileOutputStream(
                        file);

                int totalSize = urlConnection.getContentLength();
                int downloadedSize = 0;
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                if (inputStream != null) {
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        fileOutput.write(buffer, 0, bufferLength);
                        downloadedSize += bufferLength;
                        // Log.i("Progress:", "downloadedSize:" +
                        // downloadedSize
                        // + "totalSize:" + totalSize);
                    }
                }

                fileOutput.close();
                ///  if (downloadedSize == totalSize)
                /// filepath = downloadImage.getPath();


            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("ImageDownload", "Exception in DownloadRunnable" + e);
        }
        Log.i("filepath:", " " + file.getPath());
        return file;

    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
    // always verify the host - dont check for certificate
    final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    public static String encodeCredentials() {
        try {
            String userpass="";
            String auth = Base64.encodeToString(userpass.getBytes("UTF-8"), Base64.NO_WRAP);
            return auth;
        } catch (Exception ignored) {
            Log.e("eMembership", ignored.getMessage(), ignored);
        }
        return "";
    }
}
