package com.inficare.membershipdemo.databaseprovider;

import android.content.Context;
import android.database.Cursor;

import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.membership.MemberInfo;
import com.inficare.membershipdemo.membership.MemberSpouseInfo;
import com.inficare.membershipdemo.pojo.FloorPlan;
import com.inficare.membershipdemo.pojo.Result;

public abstract class DataProvider {

    public static final String DEBUG_TAG = "DataProvider";
    private static DataProvider instance;

    public static DataProvider getInstance(Context context) {
        if (instance == null) {
            instance = new SQLDataProvider(context);
        }
        return instance;
    }

	/*------------------------MemberShip----------------------*/

    abstract public boolean insertMemberDetails(DBController myDB, MemberInfo info);

    abstract public boolean insertMemberSpouseDetails(DBController myDB, MemberSpouseInfo info);

    abstract public void DeleteMemberDetails(DBController myDB, int langId, String mId);

    abstract public void DeleteMemberDetailsOnBlock(DBController myDB);

    abstract public Cursor getMemberDetails(DBController myDB, int langId);
    abstract public Cursor getMemberDetailsNotification(DBController myDB, int click);

    abstract public boolean updateMemberProfImage(DBController myDB, String recId, String path);

    abstract public boolean insertMemberBenefits(DBController myDB, Result result);

    abstract public Cursor getMembershipBenefits(DBController myDB, String memID, String
            organizationID);

    abstract public boolean updateMembershipBenefits(DBController myDB, Result result);

    abstract public boolean insertFloorImage(DBController myDB, FloorPlan result);

    abstract public Cursor getFloorImage(DBController myDB, String memID);

    abstract public boolean updateFloorImage(DBController myDB, FloorPlan result);
    abstract public void deleteMemberBenefits(DBController myDB, Result result);
    abstract public void deleteFloorMap(DBController myDB,String memID);
}
