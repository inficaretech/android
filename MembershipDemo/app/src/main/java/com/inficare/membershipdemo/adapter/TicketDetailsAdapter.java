package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.Utility;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class TicketDetailsAdapter extends ArrayAdapter<String> {

    private ArrayList<String> listData;

    private int mResource;

    private Context mContext;

    private Typeface faceDescription;

    private Map<String, String> actualValues;

    private String language;
    private Map<String, JSONObject> languageMap;

    public TicketDetailsAdapter(Context context, int resource,
                                ArrayList<String> objects, Map<String, String> actualValues, String language, Map<String, JSONObject> languageMap) {
        super(context, resource, objects);
        listData = objects;
        mResource = resource;
        mContext = context;
        this.actualValues = actualValues;
        this.language = language;
        this.languageMap = languageMap;
        faceDescription = Utility.setFaceTypeContent(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        String label="";
        
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext)
                    .inflate(mResource, null);
            viewHolder.memberDetails = (TextView) convertView
                    .findViewById(R.id.details_member);
            viewHolder.memberDtailsValue = (TextView) convertView
                    .findViewById(R.id.details_member_value);
            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();


        viewHolder.memberDetails.setText(listData.get(position)+ " : ");
        viewHolder.memberDtailsValue.setText(this.actualValues.get(listData
                .get(position)));
        viewHolder.memberDtailsValue.setTypeface(faceDescription);
        viewHolder.memberDetails.setTypeface(faceDescription);

        return convertView;
    }

    private class ViewHolder {

        TextView memberDetails, memberDtailsValue;

    }
}
