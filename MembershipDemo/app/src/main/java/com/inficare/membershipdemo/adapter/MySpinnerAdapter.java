package com.inficare.membershipdemo.adapter;

import java.util.List;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.methods.Utility;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MySpinnerAdapter extends ArrayAdapter<String> {
	// Initialise custom font, for example:

	// (In reality I used a manager which caches the Typeface objects)
	// Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);
	Typeface font;

	private Context context;

	public MySpinnerAdapter(Context context, int resource, List<String> items) {
		super(context, resource, items);
		this.context = context;
		font= Utility.setFaceTypeContent(context);
	}

	// Affects default (closed) state of the spinner
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView view = (TextView) super.getView(position, convertView, parent);
		view.setTypeface(font);
		view.setTextSize(17f);
		return view;
	}

	// Affects opened state of the spinner
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		TextView view = (TextView) super.getView(position, convertView, parent);
		view.setTextColor(context.getResources()
				.getColor(R.color.heading_color));
		view.setTextSize(17f);
		view.setBackgroundResource(R.drawable.spinner_selector);
		view.setTypeface(font);
		return view;
	}
}