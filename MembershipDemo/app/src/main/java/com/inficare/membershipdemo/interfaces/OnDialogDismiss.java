package com.inficare.membershipdemo.interfaces;

/**
 * Created by akshay.kumar on 3/13/2018.
 */

public interface OnDialogDismiss {
  public void  onDismiss(String data);
}
