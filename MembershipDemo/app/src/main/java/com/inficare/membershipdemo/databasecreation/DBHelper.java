package com.inficare.membershipdemo.databasecreation;

import android.content.Context;
import android.util.Log;


import com.inficare.membershipdemo.membership.MemberController;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "Museum.db";

    public static int DATABASE_VERSION = 9;//8

    private static DBHelper mInstance;

    public Context mContext;

    private static final String CN = "DBHelper";

    public synchronized static DBHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DBHelper(context);
        }
        return mInstance;
    }

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context;
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                    int version) {

        super(context, name, factory, version);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(MemberController.CREATE_TABLE);
        sqLiteDatabase.execSQL(MemberController.CREATE_TABLE_MEMBERSHIP_BENIFIT);
        sqLiteDatabase.execSQL(MemberController.CREATE_TABLE_MEMBERSHIP_FLOORMAP);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MemberController.TABLE_NAME);
        Log.i("onUpgrade","DB onUpgrade");
		onCreate(sqLiteDatabase);*/
        Log.i("myLog", "onUpgrade-version:" + newVersion);
        Log.i("myLog", "Previous-version:" + oldVersion);
//        if (DATABASE_VERSION == 4) {
//            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
//                    MemberController.ORG_KEY + " text");
//        }
        // If you need to add a column
        if (newVersion == 8 && oldVersion == 4) {
            db.execSQL(MemberController.CREATE_TABLE_MEMBERSHIP_BENIFIT);
            db.execSQL(MemberController.CREATE_TABLE_MEMBERSHIP_FLOORMAP);

            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.CARD_BANNER + " text");

            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.SEARCH_TYPE + " text");
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.SEARCH_LABEL + " text");
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.NO_OF_CHILDREN + " text");
        } else if (newVersion == 8 && oldVersion == 5) {
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.CARD_BANNER + " text");

            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.SEARCH_TYPE + " text");
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.SEARCH_LABEL + " text");
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.NO_OF_CHILDREN + " text");
        } else if (newVersion == 8 && oldVersion == 6) {
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.SEARCH_TYPE + " text");
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.SEARCH_LABEL + " text");
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.NO_OF_CHILDREN + " text");
        }else if (newVersion == 8 && oldVersion == 7) {
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.NO_OF_CHILDREN + " text");
        }else if (newVersion == 9 && oldVersion == 8) {
            db.execSQL("ALTER TABLE " + MemberController.TABLE_NAME + " ADD COLUMN " +
                    MemberController.MEMBERSHIP_LEVEL_ID + " text");
        }

    }

	/*@Override
    public void onCreate(SQLiteDatabase db) {
		db.execSQL(MemberController.CREATE_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}*/
}
