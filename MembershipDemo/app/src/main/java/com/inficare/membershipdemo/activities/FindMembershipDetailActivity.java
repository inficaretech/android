package com.inficare.membershipdemo.activities;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inficare.membershipdemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FindMembershipDetailActivity extends AppCompatActivity {

    @BindView(R.id.tb_title)
    TextView tbTitle;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;
    @BindView(R.id.tvTopStrip)
    View tvTopStrip;
    @BindView(R.id.tv_membership_title)
    TextView tvMembershipTitle;
    @BindView(R.id.et_membershipid)
    EditText etMembershipid;
    @BindView(R.id.til_membershipid)
    TextInputLayout tilMembershipid;
    @BindView(R.id.et_lastname)
    EditText etLastname;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.etMessage)
    EditText etMessage;
    @BindView(R.id.tilMessage)
    TextInputLayout tilMessage;
    @BindView(R.id.ll_below_container)
    LinearLayout llBelowContainer;
    @BindView(R.id.ll_container)
    LinearLayout llContainer;
    @BindView(R.id.media_card_view)
    CardView mediaCardView;
    @BindView(R.id.bt_next)
    TextView btNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_membership_detail);
        ButterKnife.bind(this);


    }
}
