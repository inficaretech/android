package com.inficare.membershipdemo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.inficare.membershipdemo.R;


/**
 * Created by Abhishek.jha on 10/17/2016.
 */
public class GuestPassViewHolder extends RecyclerView.ViewHolder{

    TextView txt_guest_pass;
    TextView tv_description;
    TextView tv_used_purpose;
    ImageView qr_code,imUsedIcon;
    TextView tv_text_qrcode;
    Button bt_ready_to_use;
    Button bt_valid_through;
    CheckBox myCheckbox;
    ProgressBar progressBar;

    public GuestPassViewHolder(View itemView) {
        super(itemView);
        txt_guest_pass = (TextView)itemView.findViewById(R.id.txt_guest_pass);
        tv_description = (TextView)itemView.findViewById(R.id.tv_description);
        tv_used_purpose = itemView.findViewById(R.id.tv_used_purpose);
        tv_text_qrcode = (TextView)itemView.findViewById(R.id.tv_text_qrcode);
        qr_code = (ImageView)itemView.findViewById(R.id.qr_code);
        bt_ready_to_use = (Button)itemView.findViewById(R.id.bt_ready_to_use);
        bt_valid_through = (Button)itemView.findViewById(R.id.bt_valid_through);
        imUsedIcon=(ImageView)itemView.findViewById(R.id.imUsedIcon);
        myCheckbox=itemView.findViewById(R.id.myCheckbox);
        progressBar=itemView.findViewById(R.id.progressBar);
    }
}
