package com.inficare.membershipdemo.interfaces;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by akshay.kumar on 2/8/2018.
 */

public abstract class  MyImageLoader extends ImageLoader{
Map<String,Integer> map=new HashMap<>();
    public void mLoadImage(String uri,final int position){
        map.put(uri,position);
        ImageLoader.getInstance().loadImage(uri, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Integer mPos=map.get(imageUri);
                if(mPos!=null){
                    onImgLoaded(mPos,imageUri,loadedImage);
                }

            }
        });
    }

    public abstract void onImgLoaded(int position,String uri,Bitmap loadedImage);

}
