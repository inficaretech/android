package com.inficare.membershipdemo.pojo;

/**
 * Created by Abhishek.jha on 10/21/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Report implements Serializable {

    @SerializedName("MembershipId")
    @Expose
    private String membershipId;
    @SerializedName("PrimaryName")
    @Expose
    private String primaryName;
    @SerializedName("TotalPass")
    @Expose
    private Integer totalPass;
    @SerializedName("UsedPass")
    @Expose
    private Integer usedPass;
    @SerializedName("RemainsPass")
    @Expose
    private Integer remainsPass;
    @SerializedName("ValidUpto")
    @Expose
    private String validUpto;
    @SerializedName("ValidFrom")
    @Expose
    private String validFrom;

    /**
     * @return The membershipId
     */
    public String getMembershipId() {
        return membershipId;
    }

    /**
     * @param membershipId The MembershipId
     */
    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    /**
     * @return The primaryName
     */
    public String getPrimaryName() {
        return primaryName;
    }

    /**
     * @param primaryName The PrimaryName
     */
    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    /**
     * @return The totalPass
     */
    public Integer getTotalPass() {
        return totalPass;
    }

    /**
     * @param totalPass The TotalPass
     */
    public void setTotalPass(Integer totalPass) {
        this.totalPass = totalPass;
    }

    /**
     * @return The usedPass
     */
    public Integer getUsedPass() {
        return usedPass;
    }

    /**
     * @param usedPass The UsedPass
     */
    public void setUsedPass(Integer usedPass) {
        this.usedPass = usedPass;
    }

    /**
     * @return The remainsPass
     */
    public Integer getRemainsPass() {
        return remainsPass;
    }

    /**
     * @param remainsPass The RemainsPass
     */
    public void setRemainsPass(Integer remainsPass) {
        this.remainsPass = remainsPass;
    }

    /**
     * @return The validUpto
     */
    public String getValidUpto() {
        return validUpto;
    }

    /**
     * @param validUpto The ValidUpto
     */
    public void setValidUpto(String validUpto) {
        this.validUpto = validUpto;
    }

    /**
     * @return The validFrom
     */
    public String getValidFrom() {
        return validFrom;
    }

    /**
     * @param validFrom The ValidFrom
     */
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

}
