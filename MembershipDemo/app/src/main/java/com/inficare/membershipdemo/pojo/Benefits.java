package com.inficare.membershipdemo.pojo;

import com.google.gson.annotations.SerializedName;

public class Benefits {
    @SerializedName("Benefit")
    private String Benefit;

    public String getBenefit() {
        return Benefit;
    }

    public void setBenefit(String Benefit) {
        this.Benefit = Benefit;
    }
}
