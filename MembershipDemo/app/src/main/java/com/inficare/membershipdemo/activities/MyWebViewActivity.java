package com.inficare.membershipdemo.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyWebViewActivity extends AppCompatActivity {

    WebView myWebview;
    TextView tvTopStrip, tvNoNotifications;
    int clickedMember;
    List<Configuration> configList;

    MuseumDemoPreference preferences;
    String url;
    public static String LINK_URL = "linkUrl";
    @BindView(R.id.myProgressbar)
    ProgressBar myProgressbar;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_web_view);
        ButterKnife.bind(this);
        mContext=this;
        preferences = MuseumDemoPreference.getInstance(this);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);

        url = getIntent().getStringExtra(LINK_URL);
        if (isDocExtension(url)){
            String googleDocs = "https://docs.google.com/viewer?url=";
            String pdfUrl = googleDocs + url;
            url=pdfUrl;
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.toolbar_title));
        TextView toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        toolbar_title.setText(url);
        toolbar_title.setSelected(true);
        toolbar_title.setSingleLine(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvTopStrip = findViewById(R.id.tvTopStrip);
        myWebview = findViewById(R.id.myWebview);
        if (url != null) {
            myWebview.loadUrl(url);
        }
        toolbar_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tvTopStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        myWebview.getSettings().setJavaScriptEnabled(true);
        myWebview.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
                myProgressbar.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (isDocExtension(url)) {
                    String googleDocs = "https://docs.google.com/viewer?url=";
                    String pdfUrl = googleDocs + url;
                    view.loadUrl(pdfUrl);
                } else {
                    if (url.startsWith("mailto:")) {
                        url = Utility.parseEmailFromHtml(url);
                        if (Utility.isValidEmail(mContext, url)) {

                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{url});
                            i.putExtra(Intent.EXTRA_SUBJECT, "");
                            i.putExtra(Intent.EXTRA_TEXT, "");
                            try {
                                mContext.startActivity(Intent.createChooser(i, ""));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(mContext, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }else if(url.startsWith("tel:")){
                        url = Utility.parsePhoneFromHtml(url);
                        if(Utility.isValidMobile(url)){
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", url, null));
                            startActivity(intent);
                        }
                    } else {
                        view.loadUrl(url);
                    }

                }
                return true;
            }
        });


    }

private boolean isDocExtension(String url){
        if(url==null)
            return false;
   return (url.endsWith(".pdf")||url.endsWith(".doc")||url.endsWith(".docx")||url.endsWith(".xls")||url.endsWith(".ppt")||url.endsWith(".pptx"));
}
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
