package com.inficare.membershipdemo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.inficare.membershipdemo.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pratyush on 3/14/2018.
 */

public class ContactsPhoneRecyclerAdapter extends RecyclerView.Adapter<ContactsPhoneRecyclerAdapter.ViewHolder> {


    private Context mContext;
    private List<String> phoneList;

    public ContactsPhoneRecyclerAdapter(Context mContext, List<String> phoneList) {
        this.mContext = mContext;
        this.phoneList = phoneList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_phone_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvPhoneNo.setText(phoneList.get(position));
    }

    @Override
    public int getItemCount() {
        return phoneList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_phone_no)
        TextView tvPhoneNo;
        @BindView(R.id.rb_phone)
        RadioButton rbPhone;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
