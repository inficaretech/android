package com.inficare.membershipdemo.pojo;

import com.inficare.membershipdemo.interfaces.PurchaseType;

import java.io.Serializable;

/**
 * Created by akshay.kumar on 3/15/2018.
 */

public class IntentData implements Serializable {

   private PaymentDetailJSON paymentDetailJSON;
   private PurchaseType purchaseType;
   private Result result;
   private String stringData;
   private boolean booleanData;

    public PaymentDetailJSON getPaymentDetailJSON() {
        return paymentDetailJSON;
    }

    public void setPaymentDetailJSON(PaymentDetailJSON paymentDetailJSON) {
        this.paymentDetailJSON = paymentDetailJSON;
    }

    public PurchaseType getPurchaseType() {
        return purchaseType;
    }

    public void setPurchaseType(PurchaseType purchaseType) {
        this.purchaseType = purchaseType;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getStringData() {
        return stringData;
    }

    public void setStringData(String stringData) {
        this.stringData = stringData;
    }

    public boolean isBooleanData() {
        return booleanData;
    }

    public void setBooleanData(boolean booleanData) {
        this.booleanData = booleanData;
    }
}
