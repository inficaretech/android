package com.inficare.membershipdemo.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.GuestPassAdapter;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import me.xiaopan.barcodescanner.EncodeUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Abhishek.jha on 10/20/2016.
 */

public class QRCodeActivity extends AppCompatActivity {

    private Context mContext;
    //Toolbar elements
    private Toolbar toolbar;
    private TextView tb_title;
    View topStrip;
    //declare UI Variables
    private ImageView qrcode_image, ivUsedIcon;
    private TextView tvBarCode;
    private String guestPassId = "";
    private String usedStatus = "";
    int clickedMember;
    List<Configuration> configList;
    public static boolean isBacked;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    public static final String TAG = GuestPassesActivity.class.getSimpleName();
    private String pleaseWait, somethingWentWrong, msg_no_internet;
    private String membership_type, guest_pass, no_sharable_pass_available, please_select_atleast_one_pass;
    private String title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        mContext = this;
        MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        guestPassId = getIntent().getStringExtra("guestPassId");
        usedStatus = getIntent().getStringExtra("usedStatus");
        setToolbarLayout();
        initLanguage();
        setUi(language);


        // findViews();
    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);

        title = Utility.getTitle(language);

    }

    private void setUi(String language) {


        try {
            tb_title.setText(languageMap.get(LanguageConstants.BARCODE).getString(title));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void findViews() {
        qrcode_image = (ImageView) findViewById(R.id.qrcode_image);

        ivUsedIcon = (ImageView) findViewById(R.id.ivUsedIcon);
        tvBarCode = (TextView) findViewById(R.id.tvBarCode);
        if (usedStatus.equalsIgnoreCase("Used")) {
            ivUsedIcon.setVisibility(View.VISIBLE);
            ivUsedIcon.setBackgroundResource(R.drawable.used_icon);
        } else if (usedStatus.equalsIgnoreCase("Expired")) {
            ivUsedIcon.setVisibility(View.VISIBLE);
            ivUsedIcon.setBackgroundResource(R.drawable.expired_icon);
        } else {
            ivUsedIcon.setVisibility(View.GONE);
        }
        tvBarCode.setText(guestPassId);
        topStrip = findViewById(R.id.topStrip);
        topStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        if (!guestPassId.equals("")) {
            //qrcode_image.setImageBitmap(Utility.generateQRBitmap(guestPassId, mContext,1));
            qrcode_image.setImageBitmap(getBarcodeBitmap(guestPassId));
        }
    }


    private void setToolbarLayout() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_button));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        findViews();
        if (GuestPassAdapter.dialog != null && GuestPassAdapter.dialog.isShowing()) {
            GuestPassAdapter.dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isBacked = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public Bitmap getBarcodeBitmap(String content) {
        Bitmap bmp = null;
        try {
            bmp = EncodeUtils.encode(content, BarcodeFormat.CODE_39, null, 1500,
                    300, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
