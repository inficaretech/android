package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 3/27/2017.
 */

public class DeviceDetails {
    String MembershipID,DeviceToken,DeviceType;
    boolean IsBlock,IsReset;

    public DeviceDetails(String membershipID, String deviceToken, String deviceType,   boolean isBlock, boolean isReset) {
        MembershipID = membershipID;
        DeviceToken = deviceToken;
        DeviceType = deviceType;
        IsBlock = isBlock;
        IsReset = isReset;
    }

    public String getMembershipID() {
        return MembershipID;
    }

    public void setMembershipID(String membershipID) {
        MembershipID = membershipID;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public boolean isBlock() {
        return IsBlock;
    }

    public void setBlock(boolean block) {
        IsBlock = block;
    }

    public boolean isReset() {
        return IsReset;
    }

    public void setReset(boolean reset) {
        IsReset = reset;
    }
}
