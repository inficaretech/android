package com.inficare.membershipdemo.pojo;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by akshay.kumar on 11/22/2016.
 */

public class MyLocalDate {
    String date,time;


    public MyLocalDate(String dt){

        long offSet=getGMTOffset2();
        long h=offSet/60;
        long m=offSet%60;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
          /*  SimpleDateFormat sdfOut = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");*/
            Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getDefault());
            c.setTime(sdf.parse(dt));
            c.add(Calendar.HOUR_OF_DAY, (int)h);
            c.add(Calendar.MINUTE, (int)m);// number of days to add
             //String date = sdfOut.format(c.getTime());  // dt is now the new date
            String t = sdf.format(c.getTime());
            this.date=date;
            this.time=t;
        }catch (Exception e){
            e.printStackTrace();
        }
    }
  /*  public String getDate() {
        return date;
    }*/

    public String getTime() {
        return time;
    }


    private static String getGMT2(){
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("EEE, MMM d, hh:mm:ss z yyyy");
        //date.setTimeZone(TimeZone.getTimeZone("GMT"));
        String localTime = date.format(currentLocalTime);

        return localTime;
    }

    public long getGMTOffset2(){
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int mGMTOffset = mTimeZone.getRawOffset();
        //String str=TimeUnit.HOURS.convert(mGMTOffset, TimeUnit.MILLISECONDS)+" : "+TimeUnit.MINUTES.convert(mGMTOffset, TimeUnit.MILLISECONDS)+"";
        long tOffSet= TimeUnit.MINUTES.convert(mGMTOffset, TimeUnit.MILLISECONDS);
        if(mTimeZone.inDaylightTime( new Date() )){
            float dstOffset =TimeUnit.MINUTES.convert( mTimeZone.getDSTSavings(), TimeUnit.MILLISECONDS);
            tOffSet+=dstOffset;
            Log.i("MyDebug","inDaylightTime:"+dstOffset);
        }

        return tOffSet;
    }


}
