package com.inficare.membershipdemo.pojo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by akshay.kumar on 3/20/2018.
 */

public class MyJsonArray implements Serializable{
   private JSONObject jsonObject;

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}
