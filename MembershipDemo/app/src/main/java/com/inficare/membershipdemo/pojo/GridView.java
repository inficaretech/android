package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 2/17/2017.
 */

public class GridView {
    String url;
    int sortAtoZ,sortLocation;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getSortAtoZ() {
        return sortAtoZ;
    }

    public void setSortAtoZ(int sortAtoZ) {
        this.sortAtoZ = sortAtoZ;
    }

    public int getSortLocation() {
        return sortLocation;
    }

    public void setSortLocation(int sortLocation) {
        this.sortLocation = sortLocation;
    }
}
