package com.inficare.membershipdemo.membership;

import android.content.Context;

import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.pojo.Labelntext;
import com.inficare.membershipdemo.pojo.MyLocalDate;
import com.inficare.membershipdemo.pojo.Notifications;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MemberParser {

    private Context _context;

    private static MemberParser instance;
    private static Map<String, JSONObject> labelntextMap;

    private static final String SPOUSE = "ChildMembers";

    private MuseumDemoPreference preferences;

    private MemberParser(Context context) {

        _context = context;
    }

    public static MemberParser createInstance(Context context) {

        if (instance == null)
            instance = new MemberParser(context);

        return instance;

    }

    public MemberList getMemberDetails(String json) {
        MemberList list = new MemberList();
        try {
            JSONArray jArray = new JSONArray(json);

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject obj = jArray.getJSONObject(i);
                MemberInfo info = new MemberInfo();
                info.setMembershipID(obj.getString(MemberController.MEMBER_ID));
                info.setProfileImage(obj.getString("ProfileImage"));
                info.setFirstName(obj.getString(MemberController.MEMBER_FIRSTNAME));
                info.setLastName(obj.getString(MemberController.MEMBER_LASTNAME));
                info.setMembershiplevel(obj.getString("Membershiplevel"));
                info.setNumber(obj.getString("Number"));
                info.setConstituentID(obj.getString("ConstituentID"));
                info.setIsExpire(obj.getString("IsExpire"));
                //info.setIsExpire("1");
                //Added ExpiryMessageText 6-10-2016
                info.setExpiryMessageText(obj.getString("ExpiryMessage"));
                info.setMembershipleveltype(obj.getString("Membershipleveltype"));
                if (obj.has("HaveLogoPath")) {
                    info.setHaveLogoPath(obj.getBoolean("HaveLogoPath") ? 1 : 0);
                    info.setRoamLogoPath(obj.getString("RoamLogoPath"));
                }
                if (obj.has("searchType")) {
                    info.setsearchType(obj.getString("searchType"));
                }
                if (obj.has("searchLabel")) {
                    info.setSearchLabel(obj.getString("searchLabel"));
                }
                if (obj.has("Noofchildren")) {
                    info.setNoOfChildren(obj.getString("Noofchildren"));
                } else {
                    info.setNoOfChildren("");
                }
                if (obj.has("Country")) {
                    info.setCountry(obj.getString("Country"));
                } else {
                    info.setCountry("");
                }

                if (obj.has("EmailAddress")) {
                    info.setEmail(obj.getString("EmailAddress"));
                } else {
                    info.setEmail("");
                }
                if (obj.has("MembershiplevelID")) {
                    info.setMembershiplevelID(obj.getString("MembershiplevelID"));
                }
                info.setMembersince(obj.getString("Membersince"));
                info.setCity(obj.getString(MemberController.MEMBER_CITY));
                info.setState(obj.getString(MemberController.MEMBER_STATE));
                info.setZip(obj.getString(MemberController.MEMBER_ZIP));
                info.setMemberSinceTimeSpan(obj
                        .getString(MemberController.MEMBER_SINCE));
                info.setValidThroughTimeSpan(obj
                        .getString(MemberController.MEMBER_THROUGH));
                //info.setValidThroughTimeSpan("2016-09-08");
                info.setCardBanner(obj.getString(MemberController.CARD_BANNER));

                JSONArray spousearray = obj.getJSONArray(SPOUSE);
                MemberSpouseList listSpouse = new MemberSpouseList();
                for (int j = 0; j < spousearray.length(); j++) {
                    JSONObject objSpouse = spousearray.getJSONObject(j);
                    MemberSpouseInfo infoSpouse = new MemberSpouseInfo();
                    infoSpouse.setProfileImage(objSpouse
                            .getString("ProfileImage"));
                    infoSpouse.setFirstName(objSpouse
                            .getString(MemberController.MEMBER_FIRSTNAME));
                    infoSpouse.setLastName(objSpouse
                            .getString(MemberController.MEMBER_LASTNAME));

                    infoSpouse.setMembershipID(objSpouse.getString(MemberController.MEMBER_ID));
                    // for new api
                    infoSpouse.setMembershiplevel(objSpouse
                            .getString("Membershiplevel"));
                    infoSpouse.setNumber(objSpouse.getString("Number"));
                    infoSpouse.setConstituentID(objSpouse
                            .getString("ConstituentID"));

                    infoSpouse.setMembersince(obj.getString("Membersince"));
                    //
                    infoSpouse.setCity(objSpouse
                            .getString(MemberController.MEMBER_CITY));
                    infoSpouse.setState(objSpouse
                            .getString(MemberController.MEMBER_STATE));
                    infoSpouse.setZip(objSpouse
                            .getString(MemberController.MEMBER_ZIP));

                    infoSpouse.setMemberSinceTimeSpan(objSpouse
                            .getString(MemberController.MEMBER_SINCE));
                    infoSpouse.setValidThroughTimeSpan(objSpouse
                            .getString(MemberController.MEMBER_THROUGH));

                    if (objSpouse.has("HaveLogoPath")) {
                        infoSpouse.setHaveLogoPath(obj.getBoolean("HaveLogoPath") ? 1 : 0);
                        infoSpouse.setRoamLogoPath(obj.getString("RoamLogoPath"));
                    }

                    if (obj.has("searchType")) {
                        infoSpouse.setsearchType(obj.getString("searchType"));
                    }
                    if (obj.has("searchLabel")) {
                        infoSpouse.setSearchLabel(obj.getString("searchLabel"));
                    }
                    if (obj.has("Noofchildren")) {
                        infoSpouse.setNoOfChildren(obj.getString("Noofchildren"));
                    } else {
                        infoSpouse.setNoOfChildren("");
                    }
                    if (obj.has("MembershiplevelID")) {
                        infoSpouse.setMembershiplevelID(obj.getString("MembershiplevelID"));
                    }
                    infoSpouse.setCardBanner(objSpouse.getString(MemberController.CARD_BANNER));
                    listSpouse.add(infoSpouse);
                }
                info.setSpouseList(listSpouse);
                list.add(info);
            }
        } catch (JSONException e) {
            list = null;
            e.printStackTrace();
        }
        return list;

    }


    public Map<String, JSONObject> getLabelntext(String json) {


        if (labelntextMap == null) {
            labelntextMap = new HashMap<>();
        }
        // Map<String,Labelntext> labelntextMap=new HashMap<>();
        try {

            JSONObject object = new JSONObject(json);
            JSONArray labelntextArr = object.getJSONArray("labelntext");

            for (int i = 0; i < labelntextArr.length(); i++) {
                JSONObject obj = labelntextArr.getJSONObject(i);
                String key = obj.getString("key");
                labelntextMap.put(key, obj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return labelntextMap;

    }


    public Map<String, JSONObject> getLabelnTextMap(Context mContext) {
        if (labelntextMap != null) {
            return labelntextMap;
        } else {

            MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(mContext);
            String configJson = preferences.getString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, null);

            labelntextMap = getLabelntext(configJson);
        }
        return labelntextMap;
    }


    public static List<Notifications> parseNotifications(String json) {
        String id, deviceType, message, createdDate;
        List<Notifications> list = new ArrayList<>();
        MyLocalDate myLocalDate;
        try {
            JSONObject object = new JSONObject(json);
            JSONObject result = object.getJSONObject("Result");
            JSONArray array = result.getJSONArray("Notifications");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object1 = array.getJSONObject(i);
                id = object1.getString("ID");
                deviceType = object1.getString("DeviceType");
                message = object1.getString("Message");
                createdDate = object1.getString("CreatedDate");

                myLocalDate = new MyLocalDate(createdDate);
                String time = myLocalDate.getTime();
                if (time != null) {
                    createdDate = time;
                }
                Notifications notifications = new Notifications(id, deviceType, message, createdDate);
                list.add(notifications);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;

    }

    public static List<com.inficare.membershipdemo.pojo.Configuration> parsConfig(Context mContext) {
        List<com.inficare.membershipdemo.pojo.Configuration> configurationList = new ArrayList<>();
        try {
            MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(mContext);
            String configJson = preferences.getString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, null);
            JSONObject object = new JSONObject(configJson);
            JSONArray arr = object.getJSONArray("Result");
            if (arr.length() > 0) {
                arr = sortJsonArr(arr);
            }
            for (int i = 0; i < arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                com.inficare.membershipdemo.pojo.Configuration configuration = new com.inficare.membershipdemo.pojo.Configuration(obj);
                configuration.setItemIndex(i);
                boolean isFavorite = preferences.getBoolean(configuration.getName(), false);
                if (isFavorite)
                    configuration.setFavorite(true);
                configurationList.add(configuration);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return configurationList;
    }

    public static List<Labelntext> parsLabelnText(Context mContext) {
        List<Labelntext> labelntextList = new ArrayList<>();
        try {
            MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(mContext);
            String configJson = preferences.getString(MuseumDemoPreference.CONFIGURATION_DATA_JSON, null);
            JSONObject object = new JSONObject(configJson);
            JSONArray labelntextArr = object.getJSONArray("labelntext");
			/*if(labelntextArr.length()>0)
			{
                labelntextArr=sortJsonArr(labelntextArr);
			}*/
            for (int i = 0; i < labelntextArr.length(); i++) {
                JSONObject obj = labelntextArr.getJSONObject(i);
                Labelntext labelntext = new Labelntext(obj);
                labelntextList.add(labelntext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return labelntextList;
    }


    private static JSONArray sortJsonArr(JSONArray jsonArr) {
        JSONArray sortedJsonArray = null;
        try {
            sortedJsonArray = new JSONArray();

            List<JSONObject> jsonValues = new ArrayList<JSONObject>();
            for (int i = 0; i < jsonArr.length(); i++) {
                jsonValues.add(jsonArr.getJSONObject(i));
            }
            Collections.sort(jsonValues, new Comparator<JSONObject>() {
                //You can change "Name" with "ID" if you want to sort by ID
                private static final String KEY_NAME = "name";

                @Override
                public int compare(JSONObject a, JSONObject b) {
                    String valA = new String();
                    String valB = new String();

                    try {
                        valA = (String) a.get(KEY_NAME);
                        valB = (String) b.get(KEY_NAME);
                    } catch (JSONException e) {
                        //do something
                    }

                    return valA.compareTo(valB);
                    //if you want to change the sort order, simply use the following:
                    //return -valA.compareTo(valB);
                }
            });

            for (int i = 0; i < jsonArr.length(); i++) {
                sortedJsonArray.put(jsonValues.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sortedJsonArray;
    }
}
