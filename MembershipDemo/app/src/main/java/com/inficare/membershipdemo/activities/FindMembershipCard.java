package com.inficare.membershipdemo.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.MySpinnerAdapter;
import com.inficare.membershipdemo.backgroundtask.MemberTask;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FindMembershipCard extends Activity implements View.OnClickListener, AdapterView
        .OnItemSelectedListener {

    private EditText membershipNo, firstName, lastNameTop, lastNameBottom,
            zipCode, phoneNo;
    private TextView headerMember, headerName, header, type;
    private Spinner spinner_type;
    private Button find, findTop;
    private ImageView ivBurredCard;
    private LinearLayout imageBack;
    private Context mContext;
    RelativeLayout layoutType;
    boolean flagAlert;
    Map<String, Integer> types;
    ArrayList<String> typesNew;
    public static boolean flagBack;
    private View cameraView;
    boolean clearFlag;
    public static Activity self;
    private Toolbar toolbar;
    private TextView tb_title;
    private LinearLayout ll, llSearchByDetails;
    View header_view;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    private String searchMode;
    private int colorCode;
    private boolean isInstructionShown;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private static final String TAG = FindMembershipCard.class.getSimpleName();
    private String pleaseWait, somethingWentWrong, search_by;
    private String no_internet_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_membership_card);

        self = this;
        mContext = this;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);

        initLanguage();
        searchMode = getIntent().getStringExtra(MembershipOptions.SEARCH_MODE_EXPIRED);
        colorCode = Color.parseColor(configList.get(clickedMember).getColorCode());
        findViews();

        setUi(language);
    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);


        try {

            if (searchMode != null) {
                tb_title.setText(languageMap.get(LanguageConstants.RENEW).getString(title));
            } else {
                tb_title.setText(languageMap.get(LanguageConstants.FIND_MEMBERSHIP_CARD).getString(title));
            }
            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            somethingWentWrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);
            no_internet_found = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);

            headerMember.setText(languageMap.get(LanguageConstants.SEARCH_BY).getString(title) + " " + configList.get(clickedMember).getSearchType());
            membershipNo.setHint(configList.get(clickedMember).getSearchType() +" *");

            lastNameTop.setHint(languageMap.get(LanguageConstants.LAST_NAME).getString(title) +" *");
            findTop.setText(languageMap.get(LanguageConstants.FIND).getString(title));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        //            please_fill_details = languageMap.get(LanguageConstants.FIND_MEMBERSHIP_CARD).getTitle();


    }


    private void findViews() {
      /*  faceHeader = Utility.setFaceTypeHeader(mContext);
        faceDescription = Utility.setFaceTypeContent(mContext);*/
        ll = (LinearLayout) findViewById(R.id.ll);
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);


        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(colorCode, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.hideKeyboadOnView(FindMembershipCard.this, membershipNo);
                onBackPressed();
            }
        });
        membershipNo = findViewById(R.id.edt_membership);
        if (configList.get(clickedMember).getKeybord().equalsIgnoreCase(Configuration.NUMERIC)) {
            membershipNo.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            membershipNo.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        }

        membershipNo.setHint(configList.get(clickedMember).getSearchType() + " *");
        firstName = (EditText) findViewById(R.id.edt_firstname);
        lastNameTop = (EditText) findViewById(R.id.edt_lastname_top);

        Utility.setInputTextLayoutColor(membershipNo, colorCode);
        Utility.setInputTextLayoutColor(lastNameTop, colorCode);


        lastNameBottom = (EditText) findViewById(R.id.edt_lastname_bottom);
        zipCode = (EditText) findViewById(R.id.edt_zip_code);
        phoneNo = (EditText) findViewById(R.id.edt_phone);
        headerMember = (TextView) findViewById(R.id.Search_by_mem_header);
        headerName = (TextView) findViewById(R.id.Search_by_name_header);
        spinner_type = (Spinner) findViewById(R.id.spinner_type);
        find = (Button) findViewById(R.id.btn_find);
        layoutType = (RelativeLayout) findViewById(R.id.layout_type);
        type = (TextView) findViewById(R.id.text_type);
        findTop = (Button) findViewById(R.id.btn_find_top);
        Utility.changeDrawableColor(this, findTop, configList.get(clickedMember)
                .getColorCode());
        Utility.changeDrawableColor(this, find, configList.get(clickedMember)
                .getColorCode());
        header_view = findViewById(R.id.header_view);
        header_view.setBackgroundColor(Color.parseColor(configList.get(clickedMember)
                .getColorCode()));
        layoutType.setOnClickListener(this);
//        Utility.setInputTextLayoutColor(membershipNo,Color.parseColor(configList.get
// (clickedMember)
//                .getColorCode()));
        find.setOnClickListener(this);
        spinner_type.setOnItemSelectedListener(this);
        findTop.setOnClickListener(this);
        setData();
        llSearchByDetails = (LinearLayout) findViewById(R.id.llSearchByDetails);
        ivBurredCard = (ImageView) findViewById(R.id.ivBurredCard);
        String uri = configList.get(clickedMember).getImagePath() + "/" + configList.get(clickedMember).getLogo();
        /*ImageLoader.getInstance().displayImage(configList.get(clickedMember).getBlurCardImage(),
                ivBurredCard);*/
        ImageLoader.getInstance().displayImage(uri, ivBurredCard);
        if (clickedMember == 1) {
            llSearchByDetails.setVisibility(View.GONE);
            find.setVisibility(View.GONE);
        } else {
            llSearchByDetails.setVisibility(View.GONE);
            find.setVisibility(View.GONE);
        }

        if (searchMode != null && !searchMode.equals("")) {
            ivBurredCard.setVisibility(View.GONE);
        } else {

        }

//        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
//                new WeakReference<EditText>(phoneNo));
//        phoneNo.addTextChangedListener(addLineNumberFormatter);

        phoneNo.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        // scrollView.setOnClickListener(this);
        ll.setOnClickListener(this);

    }

    private void setData() {
        // ArrayList<String> types = new ArrayList<String>();
        types = new LinkedHashMap<String, Integer>();
        types.put(Constants.DEFAULT_TYPE, -1);
        types.put("Associate Family", 6);
        types.put("Associate Individual", 30);
        types.put("Benefactor", 11);
        types.put("BM Discover", 22);
        types.put("BM Experience", 23);
        types.put("BM Explore", 21);
        types.put("Business Discovery", 15);
        types.put("Business Explorer", 14);
        types.put("Business Partner", 20);
        types.put("Business Resolution", 16);
        types.put("Cook Inlet", 18);
        types.put("Contributing", 10);
        types.put("Family", 5);
        types.put("Family Extended", 24);
        types.put("Family Friends", 26);
        types.put("Individual", 1);
        types.put("Individual, Educator", 13);
        types.put("Individual Expansion", 27);
        types.put("Life", 17);
        types.put("Military Family", 7);
        types.put("Military Individual", 3);
        types.put("School Partnership", 29);
        types.put("Senior Family", 8);
        types.put("Senior Individual", 4);
        types.put("Sponsor", 12);
        types.put("Staff", 31);
        types.put("Student", 2);
        types.put("Sustaining", 9);
        types.put("Sustaining Expansion", 28);
        types.put("Volunteer", 19);
        typesNew = new ArrayList<String>();
        for (String key : types.keySet()) {
            typesNew.add(key);
        }
        String str[] = {"Family Member", "Member", "Member Type 1"};
        View view = getLayoutInflater().inflate(R.layout.spinner_item, null);
        TextView text = (TextView) view.findViewById(R.id.spinner_text);
        MySpinnerAdapter dataAdapter = new MySpinnerAdapter(mContext,
                R.layout.spinner_item, typesNew);
        dataAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type.setAdapter(dataAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.llback:
//                onBackPressed();
//                break;
            case R.id.btn_find:
                /*Utility.hideKeyboard(FindMembershipCard.this);
                boolean flagN = Utility.validateSearchByName(mContext, firstName,
                        lastNameBottom, zipCode, phoneNo);
                if (flagN) {
                    if (Utility.isNetworkAvailable(mContext)) {
                        new MemberTask(mContext)
                                .execute(Constants.MODE_ADV, firstName.getText()
                                                .toString(), lastNameBottom.getText()
                                                .toString(), zipCode.getText().toString(),
                                        phoneNo.getText().toString(), String
                                                .valueOf(types.get(type.getText()
                                                        .toString())));
                    } else {
//                        TagInfo infoTag = GetTagData.getTag(mContext);
//                        Utility.showAlertDialog(mContext,
//                                infoTag.getAlertNoInternet());
                        Utility.showAlertDialog(mContext, getResources().getString(R.string
                                .msg_no_internet));
                    }

                }*/
                // boolean flag = Utility.validateMemberSearch(mContext,
                // membershipNo,
                // lastNameTop, firstName, lastNameBottom, zipCode, phoneNo,
                // type.getText().toString());
                // ArrayList<EditText> advanced = new ArrayList<EditText>();
                // advanced.add(firstName);
                // advanced.add(lastNameBottom);
                // advanced.add(phoneNo);
                // advanced.add(zipCode);
                // advanced.add(membershipNo);
                // advanced.add(lastNameTop);
                // if (checkForTextReverse(advanced, type.getText().toString())) {
                // if (flag) {
                // if (Utility.isNetworkAvailable(mContext)) {
                // if (!membershipNo.getText().toString().equals("")) {
                // new MemberTask(mContext).execute(
                // Constants.MODE_NORM, lastNameTop.getText()
                // .toString(), membershipNo.getText()
                // .toString());
                // } else {
                // new MemberTask(mContext).execute(
                // Constants.MODE_ADV, firstName.getText()
                // .toString(), lastNameBottom
                // .getText().toString(), zipCode
                // .getText().toString(), phoneNo
                // .getText().toString(), String
                // .valueOf(types.get(type.getText()
                // .toString())));
                // }
                // } else {
                // TagInfo infoTag = GetTagData.getTag(mContext);
                // Utility.showAlertDialog(mContext,
                // infoTag.getAlertNoInternet());
                //
                // }
                //
                // }
                // } else {
                // Utility.showAlertDialog(mContext, Constants.SECTIONAL_MSG);
                // }

                break;

            case R.id.layout_type:
                spinner_type.performClick();

                break;

            case R.id.btn_find_top:
//                Utility.hideKeyboard(FindMembershipCard.this);
                Utility.hideKeyboadOnView(FindMembershipCard.this, membershipNo);
                boolean flagM = Utility.validateSearchByMember(mContext,
                        membershipNo, lastNameTop, configList.get(clickedMember).getSearchType(), configList.get(clickedMember).getMid(), languageMap);
                if (flagM) {
                    if (Utility.isNetworkAvailable(mContext)) {
                        findTop.setClickable(false);
                        Map<String, Object> map = new LinkedHashMap<>();
                        map.put("mId", membershipNo.getText().toString().trim());
                        map.put("lName", lastNameTop.getText().toString());
                        map.put("organisation_key", configList.get(clickedMember).getMid());
                        map.put("uniqueDeviceId", Utility.getDeviceId(this));
                        new MemberTask(mContext, map, searchMode, language, languageMap).execute(Constants.MEMBER_URL+Utility.getLanguage(language));

                    } else {
//                        TagInfo infoTag = GetTagData.getTag(mContext);
//                        Utility.showAlertDialog(mContext,
//                                infoTag.getAlertNoInternet());
                        Utility.showAlertDialog2(mContext, no_internet_found, configList.get(clickedMember).getMid(), languageMap);
                    }
                }

                break;
            case R.id.ll:
//                Utility.hideKeyboard(FindMembershipCard.this);
                Utility.hideKeyboadOnView(FindMembershipCard.this, membershipNo);
                break;

            default:
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Object item = parent.getItemAtPosition(position);
        type.setText(item.toString());

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null) {
            clearFlag = data.getBooleanExtra(Constants.FLAG_CLEAR, false);
        }

    }

    private boolean callForCheck(boolean flag) {

        boolean flagLocal = false;
        if (flag) {
            ArrayList<EditText> advanced = new ArrayList<EditText>();
            advanced.add(firstName);
            advanced.add(lastNameBottom);
            advanced.add(phoneNo);
            advanced.add(zipCode);
            flagLocal = checkForText(advanced);
        } else {
            ArrayList<EditText> minor = new ArrayList<EditText>();
            minor.add(membershipNo);
            minor.add(lastNameTop);
            flagLocal = checkForText(minor);
        }
        return flagLocal;
    }

    private boolean checkForText(ArrayList<EditText> edtTexts) {
        boolean flag = false;
        for (int i = 0; i < edtTexts.size(); i++) {
            if (!edtTexts.get(i).getText().toString().equals("")) {
                flag = true;
                break;
            }

        }
        return flag;

    }

    private boolean checkForTextReverse(ArrayList<EditText> edtTexts,
                                        String type) {
        boolean flag = false;
        for (int i = 0; i < edtTexts.size(); i++) {
            if (edtTexts.get(i).getText().toString().equals("")) {
                flag = true;
                break;
            }

        }

        if (type.equals(Constants.DEFAULT_TYPE)) {
            flag = true;
        }
        return flag;

    }

    public static class BroadcastFind extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (self != null)
                self.finish();

        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        findTop.setClickable(true);
        if (!isInstructionShown &&clickedMember!=-1&& configList.get(clickedMember).getMid().equalsIgnoreCase
                ("TXVzZXVtQW55V2hlcmU=")) {
            Intent intent = new Intent(this, InstActivityMain.class);
            intent.putExtra(InstActivityMain.INST_DATA, getResources().getString(R.string.on_this_screen_member_can_put));
            startActivity(intent);
            isInstructionShown = true;
        }
    }
}
