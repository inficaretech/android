package com.inficare.membershipdemo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.databasecreation.CBO;
import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.interfaces.PurchaseType;
import com.inficare.membershipdemo.membership.MemberInfo;
import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.Model;
import com.inficare.membershipdemo.pojo.Response;
import com.inficare.membershipdemo.pojo.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.inficare.membershipdemo.activities.MembershipOptions.SEARCH_MODE_EXPIRED;

public class MembersBenifitsActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.bt_buy_new_membership)
    Button btBuyNewMembership;
    @BindView(R.id.bt_renew_membership)
    Button btRenewMembership;
    @BindView(R.id.bt_gift_membership)
    Button btGiftMembership;
    private Context mContext;

    //Typeface Elements
    transient private Typeface faceheader;
    transient private Typeface faceRegular;

    //Toolbar elements
    private Toolbar toolbar;
    private TextView tb_title;


    //    UI Elements
    private Button bt_my_member_benefit, bt_gift_donation;
    private Button bt_see_all;
    private Button bt_guest_passes;
    private Button bt_admin_access;
    private Button btnReciprocal;

    private Button one;
    private Button two;
    private Button three;
    private Button four;
    private Button five;
    View line_view;
    //Membership Id
    String memberId = "";
    //UI Elements
    private ProgressDialog dialog;
    private String number = "";
    int clickedMember;
    List<Configuration> configList;
    private boolean isInstructionShown;
    MuseumDemoPreference preferences;
    String organisationID;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private String pleaseWait, somethingWentWrong;
    private static final String TAG = MembersBenifitsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members_benifits);
        ButterKnife.bind(this);
        mContext = this;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        organisationID = configList.get(clickedMember).getMid();
        initLanguage();
        setFonts();
        setToolbarLayout();
        findViews();
        setUi(language);
        setUIElements();
        getAllBenefitRetrofitAPI();
    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);



        try {
            //tb_title.setText(languageMap.get(LanguageConstants.MEMBERSHIP).getString(title));
            tb_title.setText(languageMap.get(LanguageConstants.MEMBERSHIP_BENEFITS).getString(title));

            bt_my_member_benefit.setText(languageMap.get(LanguageConstants.MY_MEMBERSHIP_BENEFITS).getString(title));
            bt_see_all.setText(languageMap.get(LanguageConstants.SEE_ALL).getString(title));
            btnReciprocal.setText(languageMap.get(LanguageConstants.RECIPROCAL_BENEFITS).getString(title));
            bt_guest_passes.setText(languageMap.get(LanguageConstants.GUEST_PASSES).getString(title));


            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            somethingWentWrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void setUIElements() {
        dialog = Utility.showProgressDialog(mContext, pleaseWait);
    }

    private void setToolbarLayout() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        //tb_title.setText(configList.get(clickedMember).getName());
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        tb_title.setTypeface(faceheader);
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(upArrow);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setFonts() {
        faceheader = Utility.setFaceTypeHeader(mContext);
        faceRegular = Typeface.createFromAsset(getAssets(),
                "fonts/Raleway-Medium.ttf");
    }

    private void findViews() {
        bt_see_all = (Button) findViewById(R.id.bt_see_all);
        bt_see_all.setTypeface(faceRegular);
        bt_see_all.setOnClickListener(this);
        Utility.changeDrawableColor(this, bt_see_all, configList.get(clickedMember).getColorCode());
        bt_my_member_benefit = (Button) findViewById(R.id.bt_my_member_benefit);
        Utility.changeDrawableColor(this, bt_my_member_benefit, configList.get(clickedMember).getColorCode());
        bt_my_member_benefit.setOnClickListener(this);
        bt_my_member_benefit.setTypeface(faceRegular);

        bt_guest_passes = (Button) findViewById(R.id.bt_guest_passes);
        btnReciprocal = findViewById(R.id.btnReciprocal);
        bt_guest_passes.setTypeface(faceRegular);
        btnReciprocal.setTypeface(faceRegular);
        bt_guest_passes.setOnClickListener(this);
        btnReciprocal.setOnClickListener(this);
        Utility.changeDrawableColor(this, bt_guest_passes, configList.get(clickedMember).getColorCode());
        Utility.changeDrawableColor(this, btBuyNewMembership, configList.get(clickedMember).getColorCode());
        Utility.changeDrawableColor(this, btGiftMembership, configList.get(clickedMember).getColorCode());
        Utility.changeDrawableColor(this, btRenewMembership, configList.get(clickedMember).getColorCode());
        Utility.changeDrawableColor(this, btnReciprocal, configList.get(clickedMember).getColorCode());
        /*bt_admin_access = (Button)findViewById(R.id.bt_admin_access);
        bt_admin_access.setTypeface(faceRegular);
        bt_admin_access.setOnClickListener(this);*/

        one = (Button) findViewById(R.id.one);

        one.setOnClickListener(this);
        two = (Button) findViewById(R.id.two);
        two.setOnClickListener(this);
        three = (Button) findViewById(R.id.three);
        three.setOnClickListener(this);
        four = (Button) findViewById(R.id.four);
        four.setOnClickListener(this);
        five = (Button) findViewById(R.id.five);
        five.setOnClickListener(this);
        line_view = findViewById(R.id.line_view);
        line_view.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        bt_gift_donation = findViewById(R.id.bt_gift_donation);
        bt_gift_donation.setOnClickListener(this);
        Utility.changeDrawableColor(this, bt_gift_donation, configList.get(clickedMember).getColorCode());
        bt_gift_donation.setTypeface(faceRegular);

        if (configList.get(clickedMember).isTransactionBuy()) {
            btBuyNewMembership.setEnabled(true);
        } else {
           /* btBuyNewMembership.setEnabled(false);
            btBuyNewMembership.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel_grey));*/
            btBuyNewMembership.setVisibility(View.GONE);
        }

        if (configList.get(clickedMember).isTransactionRenew()) {
            btRenewMembership.setEnabled(true);
        } else {
           /* btRenewMembership.setEnabled(false);
            btRenewMembership.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel_grey));*/
            btRenewMembership.setVisibility(View.GONE);
        }

        if (configList.get(clickedMember).isTransactionGift()) {
            btGiftMembership.setEnabled(true);
        } else {
          /*  btGiftMembership.setEnabled(false);
            btGiftMembership.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel_grey));*/
            btGiftMembership.setVisibility(View.GONE);
        }
        if (configList.get(clickedMember).isReciprocal()) {
            btnReciprocal.setEnabled(true);
        } else {
           /* btnReciprocal.setEnabled(false);
            btnReciprocal.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel_grey));*/
            btnReciprocal.setVisibility(View.GONE);
        }

       /* if (configList.get(clickedMember).isGuestPass()) {
            bt_guest_passes.setVisibility(View.VISIBLE);
            bt_guest_passes.setEnabled(true);
        } else {
          *//*  bt_guest_passes.setEnabled(false);
            bt_guest_passes.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel_grey));*//*
            bt_guest_passes.setVisibility(View.GONE);
        }*/
        bt_guest_passes.setVisibility(View.GONE);
        MemberList list = setData();
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                memberId = list.get(i).getMembershipID();
                if (memberId != null) {
                    break;
                }
            }

            bt_my_member_benefit.setEnabled(true);
            if (configList.get(clickedMember).isTransactionDonate()) {
                bt_gift_donation.setEnabled(true);
            } else {
              /*  bt_gift_donation.setEnabled(false);
                bt_gift_donation.setBackground(getResources().getDrawable(R.drawable
                        .member_card_btn_sel_grey));*/
                bt_gift_donation.setVisibility(View.GONE);
            }


//            bt_my_member_benefit.setBackgroundColor(ContextCompat.getColor(mContext, R.color
//                    .ORANGE));
           /* bt_my_member_benefit.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel));*/
            Utility.changeDrawableColor(this, bt_my_member_benefit, configList.get(clickedMember).getColorCode());


            if (configList.get(clickedMember).isGuestPass()) {
                bt_guest_passes.setVisibility(View.VISIBLE);
                bt_guest_passes.setEnabled(true);
            } else {
                bt_guest_passes.setVisibility(View.GONE);
            }
        } else {
//            bt_my_member_benefit.setBackgroundColor(ContextCompat.getColor(mContext, R.color
// .GREY));
            bt_my_member_benefit.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel_grey));
            bt_my_member_benefit.setEnabled(false);
            //bt_my_member_benefit.setVisibility(View.GONE);
//            Utility.showAlertDialog(mContext, "No cards in the vault!");

            bt_gift_donation.setEnabled(false);
            bt_gift_donation.setBackground(getResources().getDrawable(R.drawable
                    .member_card_btn_sel_grey));
            bt_gift_donation.setVisibility(View.GONE);

        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        number = "";
        if (!isInstructionShown &&clickedMember!=-1&& configList.get(clickedMember).getMid().equalsIgnoreCase
                ("TXVzZXVtQW55V2hlcmU=")) {
            Intent intent = new Intent(this, InstActivityMain.class);
            intent.putExtra(InstActivityMain.INST_DATA, getResources().getString(R.string.member_can_see_benefits_applicable));
            startActivity(intent);
            isInstructionShown = true;
        }
    }

    //http://52.6.141.191/museumecard/api/guest/genrateGuestPass?mid=10021763
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_see_all:
                Intent seeAllIntent = new Intent(mContext, SeeAllBenifitActivity.class);
                startActivity(seeAllIntent);
                break;
            case R.id.bt_my_member_benefit:
                Intent mybenifitIntnet = new Intent(mContext, MyMemberBenefitActivity.class);
                startActivity(mybenifitIntnet);
                break;
            case R.id.bt_guest_passes:
                if (memberId != null) {
                    Intent guestPasses = new Intent(mContext, GuestPassesActivity.class);
                    guestPasses.putExtra("memberId", memberId);
                    startActivity(guestPasses);
                }
                break;
            /*case R.id.bt_admin_access:
                processingAdminAccessCode(v);
                break;*/
            case R.id.one:
                if (number.length() < 5) {
                    number = number + one.getText().toString();
                    checkAndConfirmAdminAccess();
                } else {

                }
                break;
            case R.id.two:
                if (number.length() < 5) {
                    number = number + two.getText().toString();
                    checkAndConfirmAdminAccess();
                } else {

                }
                break;
            case R.id.three:
                if (number.length() < 5) {
                    number = number + three.getText().toString();
                    checkAndConfirmAdminAccess();
                } else {

                }
                break;
            case R.id.four:
                if (number.length() < 5) {
                    number = number + four.getText().toString();
                    checkAndConfirmAdminAccess();
                }
                break;
            case R.id.five:
                if (number.length() < 5) {
                    number = number + five.getText().toString();
                    checkAndConfirmAdminAccess();
                }
                break;
            case R.id.bt_gift_donation:
                /*Intent intent = new Intent(this, DonationActivity.class);
//                intent.putExtra("purchaseType", PurchaseType.BUY);
                intent.putExtra("memberId", configList.get(0).getMid());
                intent.putExtra("purchaseType", PurchaseType.DONATION);
                Log.d("memberId", "onOptionsItemSelected: " + configList.get(0).getMid());
                startActivity(intent);*/
                break;
            case R.id.btnReciprocal:
                Intent intent = new Intent(mContext, ReciprocalActivity.class);
                startActivity(intent);
                break;

        }
    }

    private void checkAndConfirmAdminAccess() {
        if (number.length() == 5) {
            /*Utility.showToast(mContext,"number.length(): "+number.length());*/
            if (Utility.isNetworkAvailable(mContext)) {
                checkAdminAccessCode();
            } else {
                //Utility.showToast(mContext, "Internet not connected!!");
            }
        } else {
            //Utility.showToast(mContext,"number.length()!= 5");
        }
    }

    private void checkAdminAccessCode() {
        //Calling service /api/guest/AdminAccess?key=MA@2020!
        Call<Response> getAdminCode = Utility.callRetrofit().getAdminAccessCode(Utility.getHeaderData(), number, configList.get(clickedMember).getMid());
        getAdminCode.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                number = "";
                if (response.body().getSuccess().equals("true")) {
                    startActivity(new Intent(mContext, AdminActivity.class));//QRScannerActivity
                } else {

                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {

            }
        });


    }

    private MemberList setData() {

        @SuppressWarnings("unchecked")
        ArrayList<MemberInfo> info = CBO.FillCollection(
                DataProvider.getInstance(mContext).getMemberDetails(
                        DBController.createInstance(mContext, false), 0),
                MemberInfo.class);
        MemberList listMember = new MemberList();
        listMember.addAll(info);

        return listMember;


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick({R.id.bt_buy_new_membership, R.id.bt_renew_membership, R.id.bt_gift_membership})
    public void onViewClicked(View view) {

        Intent intent;
        switch (view.getId()) {
            case R.id.bt_buy_new_membership:

              /*  intent = new Intent(this, MembershipPlanActivity.class);
                intent.putExtra("purchaseType", PurchaseType.BUY);
                startActivity(intent);*/
                break;
            case R.id.bt_renew_membership:

                intent = new Intent(mContext, FindMembershipCard.class);
                intent.putExtra(SEARCH_MODE_EXPIRED, SEARCH_MODE_EXPIRED);
                startActivity(intent);
                break;
            case R.id.bt_gift_membership:

                /*intent = new Intent(this, MembershipPlanActivity.class);
                intent.putExtra("purchaseType", PurchaseType.GIFT);
                startActivity(intent);*/
                break;
            default:
                break;
        }
    }


    private void getAllBenefitRetrofitAPI() {

        Call<Model> modelCall = Utility.callRetrofit().getAllBenefitsCall(Utility.getHeaderData()
                , organisationID, Utility.getLanguage(language));
        modelCall.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, retrofit2.Response<Model> response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
                if (response.code() == 200) {

                    List<Result> list = response.body().getResult();
                    Gson gson = new Gson();
                    String jsonData = gson.toJson(list);
                    preferences.saveString(MuseumDemoPreference.ALL_BENEFITS + organisationID, jsonData);

                }
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {
            }
        });


    }
}
