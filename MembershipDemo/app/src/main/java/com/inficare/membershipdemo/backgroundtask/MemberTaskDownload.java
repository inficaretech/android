package com.inficare.membershipdemo.backgroundtask;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.networks.HttpMethod;
import com.inficare.membershipdemo.pojo.Configuration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by akshay.kumar on 3/27/2017.
 */

public abstract class MemberTaskDownload extends AsyncTask<String, Void, String> {

    private Context mContext;

    private static final String CN = "MemberTask";

    private ProgressDialog dialog;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;
    Map<String, Object> map;
    private String language, please_wait;
    private Map<String, JSONObject> languageMap;

    public MemberTaskDownload(Context context, Map<String, Object> map) {
        mContext = context;
        this.map = map;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);
        this.language = mContext.getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, Context.MODE_PRIVATE).getString(configList.get(clickedMember).getMid(), "");
        this.languageMap = MyLanguage.getInstance().getMyLanguage(mContext);

        String title = "title";
        if (this.language.equalsIgnoreCase(Constants.ENGLISH)) {

        } else {
            title = title + "_" + language;
        }

        try {
            please_wait = this.languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        createDialog(mContext);


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.show();

    }

    @Override
    protected String doInBackground(String... params) {

        String responsedata = null;
        responsedata = HttpMethod.getInstance(mContext).postData(params[0], map);

        return responsedata;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        dialog.dismiss();

        try {
            if (result != null) {
                onTaskFinish(result);

            } else {

            }
        } catch (Exception e) {

            e.printStackTrace();

        }


    }

    public void createDialog(final Context context) {
//		TagInfo infoTag = (Utility.getTagForLangChange(SharedManager
//				.getInstance(context).getInt(Constants.PREV_LANG, 0), context));

//		if (!Constants.langChange) {
//			dialog = Utility.showProgressDialog(mContext, context
//					.getResources().getString(R.string.msg_wait));
//		} else {

//			if (infoTag != null) {
//				dialog = Utility.showProgressDialog(mContext,
//						infoTag.getAlertWait());
//			} else {
        dialog = Utility.showProgressDialog(mContext, please_wait);
        //}

        //}

    }

    public void showDialog(String expireMsgText) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        // set title
        alertDialogBuilder.setTitle("Info");
        // set dialog message
        alertDialogBuilder
                .setMessage(expireMsgText)
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                /*.setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });*/
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public abstract void onTaskFinish(String data);
}
