package com.inficare.membershipdemo.databaseprovider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.membership.MemberController;
import com.inficare.membershipdemo.membership.MemberInfo;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.membership.MemberSpouseInfo;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.FloorPlan;
import com.inficare.membershipdemo.pojo.Result;

import java.util.List;
import java.util.Random;


public class SQLDataProvider extends DataProvider {

    private final String CN = "SQLDataProvider";
    public static final String DEBUG_TAG = "SQLDataProvider";
    protected Context context;
    MuseumDemoPreference preferences;
    int clickedMember;
    List<Configuration> configList;

    public SQLDataProvider(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    protected DBController getDBController(boolean writeable) {
        return DBController.createInstance(context, writeable);
    }


    @Override
    public boolean insertMemberDetails(DBController myDB, MemberInfo info) {
        Log.d("ExpiryMessageText: ", info.getExpiryMessageText());
        preferences = MuseumDemoPreference.getInstance(context);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);

        ContentValues values = new ContentValues();
        values.put(MemberController.MEMBER_ID, info.getMembershipID());
        values.put(MemberController.MEMBER_FIRSTNAME, info.getFirstName());
        values.put(MemberController.MEMBER_LASTNAME, info.getLastName());
        values.put(MemberController.MEMBER_CITY, info.getCity());
        values.put(MemberController.MEMBER_STATE, info.getState());
        values.put(MemberController.MEMBER_ZIP, info.getZip());
        values.put(MemberController.MEMBER_SINCE, info.getMemberSinceTimeSpan());
        values.put(MemberController.MEMBER_THROUGH,
                info.getValidThroughTimeSpan());
        values.put(MemberController.MEMBER_NUMBER, info.getNumber());
        values.put(MemberController.MEMBER_TYPE, info.getMembershiplevel());
        values.put(MemberController.PROFILE_IMAGE, info.getProfileImage());
        values.put(MemberController.MEMBER_CONSTITUENTID, info.getConstituentID());
        values.put(MemberController.MEMBER_SUB_CAT,
                info.getMembershipleveltype());
        values.put(MemberController.MEMBER_ISEXPIRE, info.getIsExpire());
        values.put(MemberController.MEMBER_EXPIRE_MESSAGE, info.getExpiryMessageText());
        values.put(MemberController.ORG_KEY, configList.get(clickedMember).getMid());
        values.put(MemberController.CARD_BANNER, info.getCardBanner());

        values.put(MemberController.MEMBERSINCE, info.getMembersince());
        if (info.getHaveLogoPath() == 1) {
            values.put(MemberController.HAVE_LOGO_PATH, 1);
            values.put(MemberController.ROAM_LOGO_PATH, info.getRoamLogoPath());
        } else {
            values.put(MemberController.HAVE_LOGO_PATH, 0);
        }
        values.put(MemberController.SEARCH_TYPE,info.getsearchType());
        values.put(MemberController.SEARCH_LABEL,info.getSearchLabel());
        values.put(MemberController.NO_OF_CHILDREN,info.getNoOfChildren());
        values.put(MemberController.MEMBERSHIP_LEVEL_ID,info.getMembershiplevelID());

        long row = myDB.getDatabase().insert(MemberController.TABLE_NAME, null,
                values);

        if (row > 0)
            return true;
        else
            return false;
    }

    @Override
    public boolean insertMemberSpouseDetails(DBController myDB, MemberSpouseInfo info) {
        preferences = MuseumDemoPreference.getInstance(context);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);

        ContentValues values = new ContentValues();

        values.put(MemberController.MEMBER_ID, info.getMembershipID());
        values.put(MemberController.MEMBER_FIRSTNAME, info.getFirstName());
        values.put(MemberController.MEMBER_LASTNAME, info.getLastName());
        values.put(MemberController.MEMBER_CITY, info.getCity());
        values.put(MemberController.MEMBER_STATE, info.getState());
        values.put(MemberController.MEMBER_ZIP, info.getZip());
        values.put(MemberController.MEMBER_SINCE, info.getMemberSinceTimeSpan());
        values.put(MemberController.MEMBER_THROUGH,
                info.getValidThroughTimeSpan());
        values.put(MemberController.MEMBER_NUMBER, info.getNumber());
        values.put(MemberController.MEMBER_TYPE, info.getMembershiplevel());
        values.put(MemberController.PROFILE_IMAGE, info.getProfileImage());
        values.put(MemberController.MEMBER_CONSTITUENTID, info.getConstituentID());
        values.put(MemberController.MEMBER_ISEXPIRE, info.getIsExpire());
        values.put(MemberController.ORG_KEY, configList.get(clickedMember).getMid());
        values.put(MemberController.CARD_BANNER, info.getCardBanner());
        values.put(MemberController.MEMBERSINCE, info.getMembersince());
        if (info.getHaveLogoPath() == 1) {
            values.put(MemberController.HAVE_LOGO_PATH, 1);
            values.put(MemberController.ROAM_LOGO_PATH, info.getRoamLogoPath());
        } else {
            values.put(MemberController.HAVE_LOGO_PATH, 0);
        }
        values.put(MemberController.SEARCH_TYPE,info.getsearchType());
        values.put(MemberController.SEARCH_LABEL,info.getSearchLabel());
        values.put(MemberController.NO_OF_CHILDREN,info.getNoOfChildren());
        values.put(MemberController.MEMBERSHIP_LEVEL_ID,info.getMembershiplevelID());
        long row = myDB.getDatabase().insert(MemberController.TABLE_NAME, null,
                values);

        if (row > 0)
            return true;
        else
            return false;
    }

    @Override
    public void DeleteMemberDetails(DBController myDB, int langId, String mId) {
        preferences = MuseumDemoPreference.getInstance(context);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);
        String orgKey = configList.get(clickedMember).getMid();
        try {
            String sql = "DELETE  FROM " + MemberController.TABLE_NAME + " where " +
                    MemberController.ORG_KEY + "='" + orgKey + "'";
            ;
            myDB.getDatabase().execSQL(sql);

        } catch (Exception e) {
            Log.d("DeleteMemberDetails", e.getMessage());
        }
    }

    @Override
    public void DeleteMemberDetailsOnBlock(DBController myDB) {
        preferences = MuseumDemoPreference.getInstance(context);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);
        String orgKey = configList.get(clickedMember).getMid();
        try {
            String sql = "DELETE  FROM " + MemberController.TABLE_NAME + " where " +
                    MemberController.ORG_KEY + "='" + orgKey + "'";
            myDB.getDatabase().execSQL(sql);

            //remove for favorite
            configList = MemberParser.parsConfig(context);
            boolean isSaved = preferences.getBoolean(configList.get(clickedMember).getName(),
                    false);
            if (isSaved) {
                preferences.saveBoolean(configList.get(clickedMember).getName(), false);
            }

        } catch (Exception e) {
            Log.d("DeleteMemberDetails", e.getMessage());
        }
    }

    @Override
    public Cursor getMemberDetails(DBController myDB, int langId) {
        Cursor cur = null;
        String search="";
        preferences = MuseumDemoPreference.getInstance(context);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);
        String orgKey = configList.get(clickedMember).getMid();
        try {
            String sql = "SELECT * FROM " + MemberController.TABLE_NAME + " where " +
                    MemberController.ORG_KEY + "='" + orgKey + "'";
            cur = myDB.getDatabase().rawQuery(sql, null);
            Log.d("MyDebug", "cur size: " + cur.getCount());

            if (cur.moveToFirst()){
                do{
                    search= cur.getString(cur.getColumnIndex(MemberController.SEARCH_TYPE));
                    Log.d("MyDebug", "search: " + search);
                }while(cur.moveToNext());
            }

        } catch (Exception e) {

        }
        return cur;
    }


    public Cursor getMemberDetailsNotification(DBController myDB, int clickedMember) {
        Cursor cur = null;
        String search="";
        preferences = MuseumDemoPreference.getInstance(context);
        //clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        configList = MemberParser.parsConfig(context);
        String orgKey = configList.get(clickedMember).getMid();
        try {
            String sql = "SELECT * FROM " + MemberController.TABLE_NAME + " where " +
                    MemberController.ORG_KEY + "='" + orgKey + "'";
            cur = myDB.getDatabase().rawQuery(sql, null);
            Log.d("MyDebug", "cur size: " + cur.getCount());

            if (cur.moveToFirst()){
                do{
                    search= cur.getString(cur.getColumnIndex(MemberController.SEARCH_TYPE));
                    Log.d("MyDebug", "search: " + search);
                }while(cur.moveToNext());
            }

        } catch (Exception e) {

        }
        return cur;
    }

    @Override
    public boolean updateMemberProfImage(DBController myDB, String recId, String path) {
        ContentValues values = new ContentValues();
        values.put(MemberController.PROFILE_IMAGE, path);
        long row = myDB.getDatabase().update(MemberController.TABLE_NAME,
                values, MemberController.MEMBER_CONSTITUENTID + "='" + recId + "'",
                null);
        if (row > 0)
            return true;
        else
            return false;
    }

    @Override
    public boolean insertMemberBenefits(DBController myDB, Result result) {
        ContentValues values = new ContentValues();
       /* Random rand = new Random();
        int  n = rand.nextInt(50) + 1;*/
        values.put(MemberController.MEMBERSHIP_LEVEL_TYPE, result.getMembershipleveltype());
        values.put(MemberController.MEMBERSHIP_LEVEL_ID, result.getMembershiplevelID());
        values.put(MemberController.PRICE, result.getPrice());
        values.put(MemberController.DESCRIPTION, result.getDescription());
        values.put(MemberController.UPSELLTEXT, result.getUpselltext());
        values.put(MemberController.BENEFITS, result.getBenefits());
        values.put(MemberController.OTHERBENEFITS, result.getOtherBenefits());
        values.put(MemberController.ORGANISATIONKEY, result.getOrganisationKey());

        if (result.getMyMembershipID() != null && result.getOrganisationKey() != null) {
            values.put(MemberController.MY_MEMBERSHIP_ID, result.getMyMembershipID());
        } else {
            values.put(MemberController.MY_MEMBERSHIP_ID, "all");
        }

        Log.d("insertMemberBenefits", values + "");
        long row = myDB.getDatabase().insert(MemberController.TABLE_MEMBERSHIP_BENEFIT, null,
                values);
        if (row > 0)
            return true;

        return false;
    }


    @Override
    public Cursor getMembershipBenefits(DBController myDB, String memID, String organizationID) {
        Cursor cursor = null;

        String data = "SELECT * FROM " + MemberController.TABLE_MEMBERSHIP_BENEFIT + " WHERE " +
                "" + MemberController.MY_MEMBERSHIP_ID + " ='" + memID + "' AND " +
                "" + MemberController.ORGANISATIONKEY + " ='" + organizationID + "'";
        try {
            cursor = myDB.getDatabase().rawQuery(data, null);
            Log.d("MyDebug", "cur size: " + cursor.getCount());

        } catch (Exception e) {
        }

        return cursor;
    }

    @Override
    public boolean updateMembershipBenefits(DBController myDB, Result result) {

        ContentValues values = new ContentValues();

        values.put(MemberController.MEMBERSHIP_LEVEL_TYPE, result.getMembershipleveltype());
        values.put(MemberController.MEMBERSHIP_LEVEL_ID, result.getMembershiplevelID());
        values.put(MemberController.PRICE, result.getPrice());
        values.put(MemberController.DESCRIPTION, result.getDescription());
        values.put(MemberController.UPSELLTEXT, result.getUpselltext());
        values.put(MemberController.BENEFITS, result.getBenefits());
        values.put(MemberController.OTHERBENEFITS, result.getOtherBenefits());
        values.put(MemberController.ORGANISATIONKEY, result.getOrganisationKey());

        if (result.getMyMembershipID() != null && result.getOrganisationKey() != null) {
            values.put(MemberController.MY_MEMBERSHIP_ID, result.getMyMembershipID());
        } else {
            values.put(MemberController.MY_MEMBERSHIP_ID, "all");
            result.setMyMembershipID( "all");
        }

        String whereClause = MemberController.MEMBERSHIP_LEVEL_ID + " ='" + result
                .getMembershiplevelID()+ "' AND " + MemberController.ORGANISATIONKEY + " ='" + result
                .getOrganisationKey() + "'";
        Log.d("insertMemberBenefits", values + "");
        long row = myDB.getDatabase().update(MemberController.TABLE_MEMBERSHIP_BENEFIT, values,
                whereClause, null);
        if (row > 0)
            return true;

        return false;
    }

    @Override
    public void deleteMemberBenefits(DBController myDB, Result result){
        String memberId=result.getMyMembershipID();
        if(memberId==null){
            memberId="all";
        }
        String sql = "DELETE  FROM " + MemberController.TABLE_MEMBERSHIP_BENEFIT + " where " +
                MemberController.MY_MEMBERSHIP_ID + " ='" +memberId + "' AND " +   MemberController.ORGANISATIONKEY + "='" + result.getOrganisationKey() + "'";
        myDB.getDatabase().execSQL(sql);
    }

    @Override
    public void deleteFloorMap(DBController myDB,String memID){

        String sql = "DELETE  FROM " + MemberController.TABLE_MEMBERSHIP_FLOORMAP + " where " +
                MemberController.FLOORMAP_MID + " ='" +memID + "'";
        myDB.getDatabase().execSQL(sql);
    }


    @Override
    public boolean insertFloorImage(DBController myDB, FloorPlan result) {
        ContentValues values = new ContentValues();
        values.put(MemberController.FLOORMAP_ID, result.getId() + "");
        values.put(MemberController.FLOORMAP_TITLE, result.getTitle() + "");
        values.put(MemberController.FLOORMAP_IMAGENAME, result.getImage_name() + "");
        values.put(MemberController.FLOORMAP_IMAGETYPE, result.getImage_type() + "");
        values.put(MemberController.FLOORMAP_IMAGE_SIZE, result.getImage_size() + "");
        values.put(MemberController.FLOORMAP_MID, result.getMid() + "");
        values.put(MemberController.FLOORMAP_CREATED_AT, result.getCreated_at() + "");
        values.put(MemberController.FLOORMAP_UPDATED_AT, result.getUpdated_at() + "");
        values.put(MemberController.FLOORMAP_IMAGEBASE_PATH, result.getImageBasePath() + "");
        values.put(MemberController.FLOORMAP_IMAGE_SD_CARD_PATH, result.getImagesdcardpath() + "");
        Log.d("insertMemberBenefits", values + "");

        long row = myDB.getDatabase().insert(MemberController.TABLE_MEMBERSHIP_FLOORMAP, null,
                values);
        if (row > 0)
            return true;

        return false;
    }

    @Override
    public Cursor getFloorImage(DBController myDB, String memID) {

        Cursor cursor = null;

        String data = "SELECT * FROM " + MemberController.TABLE_MEMBERSHIP_FLOORMAP + " WHERE " +
                "" + MemberController.FLOORMAP_MID + " ='" + memID + "'";

//        String data = "SELECT * FROM " + MemberController.TABLE_MEMBERSHIP_FLOORMAP;
        try {
            cursor = myDB.getDatabase().rawQuery(data, null);
            Log.d("MyDebug", "cur size: " + cursor.getCount());

        } catch (Exception e) {
        }
        return cursor;
    }

    @Override
    public boolean updateFloorImage(DBController myDB, FloorPlan result) {
        ContentValues values = new ContentValues();
        values.put(MemberController.FLOORMAP_ID, result.getId() + "");
        values.put(MemberController.FLOORMAP_TITLE, result.getTitle() + "");
        values.put(MemberController.FLOORMAP_IMAGENAME, result.getImage_name() + "");
        values.put(MemberController.FLOORMAP_IMAGETYPE, result.getImage_type() + "");
        values.put(MemberController.FLOORMAP_IMAGE_SIZE, result.getImage_size() + "");
        values.put(MemberController.FLOORMAP_MID, result.getMid() + "");
        values.put(MemberController.FLOORMAP_CREATED_AT, result.getCreated_at() + "");
        values.put(MemberController.FLOORMAP_UPDATED_AT, result.getUpdated_at() + "");
        values.put(MemberController.FLOORMAP_IMAGEBASE_PATH, result.getImageBasePath() + "");
        values.put(MemberController.FLOORMAP_IMAGE_SD_CARD_PATH, result.getImagesdcardpath() + "");

        String whereClause = MemberController.FLOORMAP_ID + " ='" + result
                .getId() + "' AND " + MemberController.FLOORMAP_MID + " ='" + result.getMid() + "'";
        Log.d("updateFloorImage", values + "");
        long row = myDB.getDatabase().update(MemberController.TABLE_MEMBERSHIP_FLOORMAP, values,
                whereClause, null);
        if (row > 0)
            return true;


        return false;
    }

    /***
     * get Direct MemberList
     * @param mContext
     * @return
     */

    public static String getMemberId(Context mContext) {
        String membershipIdText = null;
        Cursor cursor = DataProvider.getInstance(mContext).getMemberDetails(DBController
                .createInstance(mContext, false), 0);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            membershipIdText = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();

        return membershipIdText;
      /*  @SuppressWarnings("unchecked")
        ArrayList<MemberInfo> info = CBO.FillCollection(
                DataProvider.getInstance(mContext).getMemberDetails(
                        DBController.createInstance(mContext, false), 0),
                MemberInfo.class);
        MemberList listMember = new MemberList();
        listMember.addAll(info);


        return listMember;*/

    }

    public static String getMemberIdNoti(Context mContext,int click) {
        String membershipIdText = null;
        Cursor cursor = DataProvider.getInstance(mContext).getMemberDetailsNotification(DBController
                .createInstance(mContext, false), click);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            membershipIdText = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();

        return membershipIdText;
      /*  @SuppressWarnings("unchecked")
        ArrayList<MemberInfo> info = CBO.FillCollection(
                DataProvider.getInstance(mContext).getMemberDetails(
                        DBController.createInstance(mContext, false), 0),
                MemberInfo.class);
        MemberList listMember = new MemberList();
        listMember.addAll(info);


        return listMember;*/

    }

}
