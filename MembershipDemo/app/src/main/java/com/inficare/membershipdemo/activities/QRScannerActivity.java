package com.inficare.membershipdemo.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.ResponseBarcode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Abhishek.jha on 10/19/2016.
 */

public class QRScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    public static String TAG = "QRScannerActivity";
    private ProgressDialog dialog;
    private Context mContext;
    private static final int PERMISSION_REQUEST_CAMERA = 100;
    int clickedMember;
    List<Configuration> configList;
    private String prevQRData = "";

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private String pleaseWait, somethingWentWrong, msg_no_internet;
    private String membership_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        initLanguage();
        setUi(language);
        setUIElements();

        /*setContentView(R.layout.activity_qr_code_scanner);*/
    }

    private void initLanguage() {
        langPreferences = getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, MODE_PRIVATE);
        editor = langPreferences.edit();
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);


        language = langPreferences.getString(configList.get(clickedMember).getMid(), "");
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);

        if (language.isEmpty() || language == null) {
            editor.putString(configList.get(clickedMember).getMid(), "");
            Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " :en");
            editor.commit();
        }


    }

    private void setUi(String language) {

        String title = "title";
        if (language.equalsIgnoreCase(Constants.ENGLISH)) {

        } else {
            title = title + "_" + this.language;
        }


        try {
            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            somethingWentWrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);
            membership_type = languageMap.get(LanguageConstants.MEMBERSHIP_TYPE).getString(title);
            msg_no_internet = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void setUIElements() {
        dialog = Utility.showProgressDialog(mContext, pleaseWait);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 23) {
            checkCameraPermission();
        } else {
            startScanning();
        }
    }

    private void startScanning() {
        mScannerView.setResultHandler(this); // Register ourselves as linearLayout handler for scan results.
        mScannerView.startCamera();
    }

    private void checkCameraPermission() {
        /*if(ContextCompat.checkSelfPermission(mContext))*/
        int permissionCheck = ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.CAMERA);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            startScanning();
        } else {
            ActivityCompat.requestPermissions(QRScannerActivity.this, new String[]{Manifest.permission.CAMERA},
                    PERMISSION_REQUEST_CAMERA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startScanning();
                } else {
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();// Stop camera on pause
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v(TAG, rawResult.getText()); // Prints scan results
        Log.v(TAG, rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)


       if(configList.get(clickedMember).isPassPurposeStatus()){
           myDailog(rawResult);
       }else if (Utility.isNetworkAvailable(mContext)) {

            if (!prevQRData.equals(rawResult.getText())) {
                sendScannedQRCode(rawResult.getText(), "");
                Log.v(TAG, "sendScannedQRCode");
            }

            prevQRData = rawResult.getText();
        } else {
            Utility.showToast(mContext, msg_no_internet);
        }


        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
        //using /api/Guest/UpdatePassStatus?guestPassId=xxxxxx&status=True
    }

    private void sendScannedQRCode(String qrCode,String purpouse) {
        dialog.show();
        //mScannerView.stopCamera();
        Call<ResponseBarcode> sendScannedQRCodeCall = Utility.callRetrofit().sendScannedQRCode(Utility.getHeaderData(), qrCode, "True", configList.get(clickedMember).getMid(),purpouse);
        sendScannedQRCodeCall.enqueue(new Callback<ResponseBarcode>() {
            @Override
            public void onResponse(Call<ResponseBarcode> call, retrofit2.Response<ResponseBarcode> response) {
                dialog.dismiss();
                //mScannerView.resumeCameraPreview(QRScannerActivity.this);
                try {
                    if (response.body().getSuccess().equals("true")) {
                        //Utility.showToast(mContext, response.body().getResult().getGuestPass().get(0).getStatus());
                        Utility.showToast(mContext, response.body().getMessage());
                        finish();
                    } else {
                        Utility.showToast(mContext, response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBarcode> call, Throwable t) {
                dialog.dismiss();
                Utility.showToast(mContext, somethingWentWrong);
                //mScannerView.resumeCameraPreview(QRScannerActivity.this);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

     Dialog mDialog=null;
    private void myDailog(Result rawResult){

        // custom dialog
        if(mDialog==null)
        mDialog = new Dialog(this);

        mDialog.setContentView(R.layout.custom_dialog);
        //dialog.setTitle("Send Purpose");

       EditText etPurpose  = mDialog.findViewById(R.id.etPurpose);

        Button btnSend =  mDialog.findViewById(R.id.btnSend);
        Button btnCancel =  mDialog.findViewById(R.id.btnCancel);

        Utility.changeDrawableColor(this, btnSend, configList.get
                (clickedMember).getColorCode());
        Utility.changeDrawableColor(this, btnCancel, configList.get
                (clickedMember).getColorCode());
        // if button is clicked, close the custom dialog
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                if(etPurpose.getText().toString().trim().isEmpty()){
                    Toast.makeText(mContext,"Please enter purpose",Toast.LENGTH_LONG).show();
                    return;
                }
                if (Utility.isNetworkAvailable(mContext)) {

                    if (!prevQRData.equals(rawResult.getText())) {
                        sendScannedQRCode(rawResult.getText(),etPurpose.getText().toString());
                        Log.v(TAG, "sendScannedQRCode");
                    }

                    prevQRData = rawResult.getText();
                } else {
                    Utility.showToast(mContext, msg_no_internet);
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finish();
            }
        });

        if(!mDialog.isShowing())
        mDialog.show();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        mDialog.getWindow().setLayout((6 * width)/7, (4 * height)/13);
    }
}
