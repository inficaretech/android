package com.inficare.membershipdemo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.databasecreation.CBO;
import com.inficare.membershipdemo.databasecreation.DBController;
import com.inficare.membershipdemo.databaseprovider.DataProvider;
import com.inficare.membershipdemo.membership.MemberInfo;
import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.Model;
import com.inficare.membershipdemo.pojo.Reciprocal;
import com.inficare.membershipdemo.pojo.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyMemberBenefitActivity extends AppCompatActivity {
    private Context mContext;
    View topStrip;
    //Toolbar elements
    private Toolbar toolbar;
    private TextView tb_title;

    //    UI Elements
    private ScrollView sv_container;
    private ProgressDialog dialog;
    int clickedMember;
    List<Configuration> configList;
    private boolean progressVisible = true;
    private String memberID = "";
    private String organisationID = "";
    LinearLayout linearLayout;
    private static String IS_RECIPROCAL = "isReciprocal";
    MuseumDemoPreference preferences;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    public static final String TAG = MyMemberBenefitActivity.class.getSimpleName();
    private String pleaseWait, somethingWentWrong;
    private String membership_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_member_benefit);
        ButterKnife.bind(this);
        mContext = this;
        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        initLanguage();

        setToolbarLayout();
        setUi(language);
        setUIElements();


        organisationID = configList.get(clickedMember).getMid();

        MemberList list = setData();
        if (list.size() > 0) {

            for (int i = 0; i < list.size(); i++) {
                memberID = list.get(i).getMembershipID();
                if (memberID != null) {
                    break;
                }
            }

            List<Result> results = getMembershipDetails();
            Log.d("results", results.size() + "");
            if (results.size() > 0) {
                setDataInUI(results);
                ///setDataInWebview(results);
                progressVisible = false;
            } else {

            }
            getMyMemberBenefitRetrofitAPI(memberID, progressVisible);

        }

    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {
        String title = Utility.getTitle(language);



        try {
            tb_title.setText(languageMap.get(LanguageConstants.MY_MEMBERSHIP_BENEFITS).getString(title));
            pleaseWait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";
            somethingWentWrong = languageMap.get(LanguageConstants.SOMETHING_WENT_WRONG).getString(title);
            membership_type = languageMap.get(LanguageConstants.MEMBERSHIP_TYPE).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setUIElements() {
        sv_container = (ScrollView) findViewById(R.id.sv_container);
        topStrip = findViewById(R.id.topStrip);

        topStrip.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
        dialog = Utility.showProgressDialog(mContext, pleaseWait);
    }

    private void setToolbarLayout() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_button));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private MemberList setData() {

        @SuppressWarnings("unchecked")
        ArrayList<MemberInfo> info = CBO.FillCollection(
                DataProvider.getInstance(mContext).getMemberDetails(
                        DBController.createInstance(mContext, false), 0),
                MemberInfo.class);
        MemberList listMember = new MemberList();
        listMember.addAll(info);


        return listMember;

    }


    private List<Result> getMembershipDetails() {

        ArrayList<Result> data = CBO.FillCollection(DataProvider.getInstance(mContext)
                .getMembershipBenefits(DBController.createInstance(mContext, false),
                        memberID, organisationID), Result.class);
        Log.d("data.size", data.size() + "");
        return data;
    }


    private void getMyMemberBenefitRetrofitAPI(final String mem_id, final boolean progress) {
//        String mem_id = null;
//        try {
//            mem_id = URLEncoder.encode(Utility.myEncrypt(member_id), "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        if (progress)
            dialog.show();
        Call<Model> modelCall = Utility.callRetrofit().myMemberBenifitCall(Utility.getHeaderData
                (), mem_id, organisationID, Utility.getLanguage(language));
        modelCall.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();

                if (response.code() == 200) {
                    List<Result> list = response.body().getResult();

                    if (list != null && list.size() > 0) {
                        for (int i = 0; i < list.size(); i++) {
                            List<Reciprocal> reciprocalList = list.get(i).getReciprocals();
                            if (reciprocalList != null && reciprocalList.size() > 0) {
                                preferences.saveBoolean(IS_RECIPROCAL + i, true);
                            } else {
                                preferences.saveBoolean(IS_RECIPROCAL + i, false);
                            }
                        }

                    }

                    if (progress) {
                        for (Result data : list) {

                            data.setMyMembershipID(mem_id);
                            data.setOrganisationKey(configList.get(clickedMember).getMid());

                            boolean value = DataProvider.getInstance(mContext).insertMemberBenefits
                                    (DBController.createInstance(mContext, true), data);
                            Log.d("value", value + "");
                        }
                        //setDataInUI(list);
                    } else {
                        //replace with new data
                        if (list != null && list.size() > 0) {
                            Result myData = list.get(0);
                            myData.setMyMembershipID(mem_id);
                            myData.setOrganisationKey(configList.get(clickedMember).getMid());
                            DataProvider.getInstance(mContext)
                                    .deleteMemberBenefits(DBController.createInstance
                                            (mContext, true), myData);
                        }

                        for (Result data : list) {
                            data.setMyMembershipID(mem_id);
                            data.setOrganisationKey(configList.get(clickedMember).getMid());
                            boolean value = DataProvider.getInstance(mContext).insertMemberBenefits
                                    (DBController.createInstance(mContext, true), data);
                            Log.d("value", value + "");
                        }
                       /* for (Result data : list) {

                            data.setMyMembershipID(mem_id);
                            data.setOrganisationKey(configList.get(clickedMember).getMid());

                            boolean value = DataProvider.getInstance(mContext)
                                    .updateMembershipBenefits(DBController.createInstance
                                            (mContext, true), data);
                            Log.d("value", value + "");
                        }*/
                    }

                    if (list != null && list.size() > 0) {
                        setDataInUI(list);
                       /* List<Result> results = getMembershipDetails();
                        if (results.size() > 0) {
                            setDataInUI(results);
                            ///setDataInWebview(results);
                        }*/
                    }
                }
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {
                if (dialog != null && dialog.isShowing())
                    dialog.dismiss();
            }
        });

    }

    private void setDataInWebview(List<Result> list) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < list.size(); i++) {
            String membershipType = "";
            membershipType = list.get(i).getMembershipleveltype();
            String sTitle = "<h4>Membership Type: " + membershipType + " " + list.get(i).getPrice() + "</h4>";
            builder.append(sTitle);

            String benefits = list.get(i).getBenefits();
            builder.append(benefits);


            //Other Benefits Title

            if (list.get(i).getOtherBenefits() != null) {
                if (!list.get(i).getOtherBenefits().isEmpty()) {

                    String otherBenefitsUpSell = list.get(i).getUpselltext();
                    builder.append(otherBenefitsUpSell);

                    String otherBenefits = list.get(i).getOtherBenefits();
                    builder.append(otherBenefits);
                }
            }
            //myWebview.loadData(builder.toString(), "text/html; charset=utf-8", "UTF-8");

        }

    }


    private void setDataInUI(List<Result> list) {
        if (this.linearLayout != null) {
            sv_container.removeAllViews();
        }
        LinearLayout a = new LinearLayout(this);
        this.linearLayout = a;
        a.setPadding(10, 5, 5, 10);
        a.setOrientation(LinearLayout.VERTICAL);

        for (int i = 0; i < list.size(); i++) {
            //Adding Membership Title
            TextView tv_membership_type = new TextView(mContext);
            String membershipType = "";
            membershipType = list.get(i).getMembershipleveltype();
//            tv_membership_type.setTextColor(Color.parseColor(configList.get(clickedMember)
//                    .getColorCode()));
            tv_membership_type.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            tv_membership_type.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger
                    (R.integer.benefit_title_text_size_large));
            tv_membership_type.setTypeface(Typeface.DEFAULT_BOLD);
            tv_membership_type.setText(membership_type + " : " + membershipType +
                    " " + list.get(i).getPrice());

            RelativeLayout.LayoutParams paramsIv = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            paramsIv.addRule(RelativeLayout.ALIGN_PARENT_END);
            ImageView iv = new ImageView(this);
            iv.setImageResource(R.drawable.reciprocal_con);
            //paramsIv.setMargins(Utility.convertPixelsToDp(10f,this),0,0,0);
            iv.setLayoutParams(paramsIv);
            iv.setClickable(true);
            iv.setColorFilter(Color.parseColor("#000000"));
            final int pos = i;

            boolean isRec = preferences.getBoolean(IS_RECIPROCAL + i, false);
            if (isRec) {
                iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyMemberBenefitActivity.this, ReciprocalActivity.class);
                        List<Reciprocal> list1 = list.get(pos).getReciprocals();
                        intent.putExtra("reciprocal", (ArrayList<Reciprocal>) list1);
                        startActivity(intent);
                    }
                });
            } else {
                iv.setVisibility(View.GONE);
            }

            RelativeLayout parent = new RelativeLayout(this);
            RelativeLayout.LayoutParams lprams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            parent.setLayoutParams(lprams);


            /*LinearLayout parent = new LinearLayout(this);
            parent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            parent.setOrientation(LinearLayout.HORIZONTAL);*/
            parent.addView(tv_membership_type);
            parent.addView(iv);
            a.addView(parent);


            TextView tv_list_item = new TextView(mContext);
            tv_list_item.setText(Html.fromHtml(list.get(i).getBenefits()));
            tv_list_item.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getInteger
                    (R.integer.benefit_title_text_size));
            tv_list_item.setTextColor(ContextCompat.getColor(mContext, R.color.black));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout
                    .LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, (int) Utility.convertDPtoPixel(mContext, 10), 0, (int) Utility
                    .convertDPtoPixel(mContext, 10));
            tv_list_item.setLayoutParams(params);
            Linkify.addLinks(tv_list_item, Linkify.ALL);
            tv_list_item.setLinkTextColor(ContextCompat.getColor(mContext, R.color.link_color));

            WebView webView = new WebView(mContext);
            webView.setLayoutParams(params);
            webView.loadData(list.get(i).getBenefits(), "text/html; charset=utf-8", "UTF-8");
            a.addView(webView);
            setWebviewPropertes(webView);


            //Other Benefits Title

            if (list.get(i).getOtherBenefits() != null) {
                if (!list.get(i).getOtherBenefits().isEmpty()) {

                    TextView tv_otherBenefitsTitle = new TextView(mContext);
                    String otherBenefitsTitle = "";
                    otherBenefitsTitle = list.get(i).getUpselltext().trim();
//                    tv_otherBenefitsTitle.setTextColor(Color.parseColor(configList.get
//                            (clickedMember).getColorCode()));
                    tv_otherBenefitsTitle.setTextColor(ContextCompat.getColor(mContext, R.color
                            .black));
                   /* if(otherBenefitsTitle.contains("</")){
                        tv_otherBenefitsTitle.setText(Html.fromHtml(otherBenefitsTitle));
                    }else {*/
                    tv_otherBenefitsTitle.setText(Html.fromHtml(otherBenefitsTitle));
                    //}
                    //tv_otherBenefitsTitle.setText(otherBenefitsTitle);
                    tv_otherBenefitsTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources()
                            .getInteger
                                    (R.integer.benefit_title_text_size));

                    View view = new View(mContext);
                    LinearLayout.LayoutParams params_other = new LinearLayout.LayoutParams
                            (LinearLayout
                                    .LayoutParams.WRAP_CONTENT, 4);
                    params_other.setMargins(0, (int) Utility.convertDPtoPixel(mContext, 5), 0, (int)
                            Utility.convertDPtoPixel(mContext, 5));
                    view.setLayoutParams(params_other);
//                    view.setBackgroundColor(Color.parseColor(configList.get(clickedMember)
//                            .getColorCode()));
                    view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));

                    WebView webViewOtherBTitle = new WebView(mContext);
                    webViewOtherBTitle.setLayoutParams(params);
                    webViewOtherBTitle.loadData(otherBenefitsTitle, "text/html; charset=utf-8", "UTF-8");

                    a.addView(webViewOtherBTitle);
                    a.addView(view);
                    setWebviewPropertes(webViewOtherBTitle);

                    TextView tv_otherBenifitsList = new TextView(mContext);
                    tv_otherBenifitsList.setText(Html.fromHtml(list.get(i).getOtherBenefits()));
                    tv_otherBenifitsList.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources()
                            .getInteger
                                    (R.integer.benefit_title_text_size));
                    tv_otherBenifitsList.setTextColor(ContextCompat.getColor(mContext, R.color
                            .black));
                    Linkify.addLinks(tv_otherBenifitsList, Linkify.ALL);
                    tv_otherBenifitsList.setLinkTextColor(ContextCompat.getColor(mContext, R.color.link_color));

                    WebView webViewOtherBList = new WebView(mContext);
                    webViewOtherBList.loadData(list.get(i).getOtherBenefits(), "text/html; charset=utf-8", "UTF-8");
                    a.addView(webViewOtherBList);
                    setWebviewPropertes(webViewOtherBList);

                }
            }

            View view = new View(mContext);
            LinearLayout.LayoutParams params_border = new LinearLayout.LayoutParams(LinearLayout
                    .LayoutParams.WRAP_CONTENT, 4);
            params_border.setMargins(0, (int) Utility.convertDPtoPixel(mContext, 5), 0, (int)
                    Utility.convertDPtoPixel(mContext, 5));
            view.setLayoutParams(params_border);
//            view.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode
// ()));
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
            a.addView(view);

        }
        sv_container.addView(a);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void setWebviewPropertes(WebView webView) {
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= 19) {
// chromium, enable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Intent intent = new Intent(MyMemberBenefitActivity.this,MyWebViewActivity.class);
                intent.putExtra(MyWebViewActivity.LINK_URL,url);
                startActivity(intent);

                return true;
            }
        });
    }
}
