package com.inficare.membershipdemo.pojo;

import org.json.JSONException;
import org.json.JSONObject;

public class DownloadCard {

    private Boolean success;
    private Integer statuscode;
    private String result;

    public DownloadCard(String json) {
        try {
            JSONObject object=new JSONObject(json);
            success=object.getBoolean("success");
            statuscode=object.getInt("statuscode");
            result=object.getString("result");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(Integer statuscode) {
        this.statuscode = statuscode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


}

