package com.inficare.membershipdemo.pojo;

/**
 * Created by akshay.kumar on 19-04-2018.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VersionControl {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("devicetype")
    @Expose
    private Integer devicetype;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("having_version_control")
    @Expose
    private Integer havingVersionControl;
    @SerializedName("is_force_update")
    @Expose
    private Boolean isForceUpdate;
    @SerializedName("is_current")
    @Expose
    private Boolean isCurrent;
    @SerializedName("create_date")
    @Expose
    private String createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(Integer devicetype) {
        this.devicetype = devicetype;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getHavingVersionControl() {
        return havingVersionControl;
    }

    public void setHavingVersionControl(Integer havingVersionControl) {
        this.havingVersionControl = havingVersionControl;
    }

    public Boolean getIsForceUpdate() {
        return isForceUpdate;
    }

    public void setIsForceUpdate(Boolean isForceUpdate) {
        this.isForceUpdate = isForceUpdate;
    }

    public Boolean getIsCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(Boolean isCurrent) {
        this.isCurrent = isCurrent;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

}