package com.inficare.membershipdemo.dialogFragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.LanguageRecyclerAdapter;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LanguageDialogFragment extends DialogFragment {

    @BindView(R.id.rv_language)
    RecyclerView rvLanguage;
    Unbinder unbinder;
    @BindView(R.id.tv_select_language)
    TextView tvSelectLanguage;
    @BindView(R.id.tvStrip)
    View tvStrip;
    private Context mContext;
    public static final String TAG = "LanguageDialogFragment";
    private SharedPreferences.Editor editor;
    private SharedPreferences prefs;

    private List<Configuration> configurationsList;

    private int clickedMember;

    private MuseumDemoPreference preferences;

    private int color;

    private Typeface faceHeader, faceRegular;

    private LanguageRecyclerAdapter adapter;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor langEditor;
    private String language;
    private Map<String, JSONObject> languageMap;
    private TextView toolbar_title;
    private String language_already_selected, no_internet;
    private int languageId;
    private boolean showCheckedLanguage;

    public static LanguageDialogFragment newInstance() {
        LanguageDialogFragment fragment = new LanguageDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_language_dialog, container, false);
        unbinder = ButterKnife.bind(this, view);

        initialize();

        initLanguage();

        setUi();

        setUi(language);

        setAdapter();

        return view;

    }

    private void setAdapter() {

        LinearLayoutManager manager = new LinearLayoutManager(mContext,
                LinearLayoutManager.VERTICAL, false);
        rvLanguage.setLayoutManager(manager);
        adapter = new LanguageRecyclerAdapter(mContext, configurationsList.get(clickedMember).getLanguageList(),
                configurationsList.get(clickedMember).getMid(), this, language_already_selected,
                no_internet, configurationsList.get(clickedMember).getColorCode(), showCheckedLanguage, language);
        rvLanguage.setAdapter(adapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvLanguage.getContext(),
                manager.getOrientation());

        rvLanguage.addItemDecoration(dividerItemDecoration);

    }

    private void setUi() {

        tvSelectLanguage.setTypeface(faceHeader);

        tvSelectLanguage.setTextColor(Color.parseColor(configurationsList.get(clickedMember).getColorCode
                ()));

        tvStrip.setBackgroundColor(Color.parseColor(configurationsList.get(clickedMember).getColorCode
                ()));

    }

    private void initialize() {

        mContext = getActivity();

        prefs = mContext.getSharedPreferences(Constants.APPRATER, Context.MODE_PRIVATE);

        editor = prefs.edit();

        configurationsList = MemberParser.parsConfig(mContext);

        preferences = MuseumDemoPreference.getInstance(mContext);

        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);

        color = Color.parseColor(configurationsList.get(clickedMember).getColorCode
                ());


        faceHeader = Utility.setFaceTypeHeader(mContext);
        faceRegular = Utility.setFaceTypeContent(mContext);

        showCheckedLanguage = getArguments().getBoolean(Constants.LANGUAGE_ID);


    }

    private void initLanguage() {
        langPreferences = mContext.getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, Context.MODE_PRIVATE);
        langEditor = langPreferences.edit();
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);


        language = langPreferences.getString(configurationsList.get(clickedMember).getMid(), "");
        languageId = langPreferences.getInt(configurationsList.get(clickedMember).getMid() + Constants.LANGUAGE_ID, -1);

        Log.d(TAG, configurationsList.get(clickedMember).getMid() + configurationsList.get(clickedMember).getName() + " : " + language);

        if (language.isEmpty() || language == null) {
            langEditor.putString(configurationsList.get(clickedMember).getMid(), "");
            Log.d(TAG, configurationsList.get(clickedMember).getMid() + configurationsList.get(clickedMember).getName() + " :en");
            editor.commit();
        }

        if (languageId == -1) {
            editor.putInt(configurationsList.get(clickedMember).getMid() + Constants.LANGUAGE_ID, 1);
            Log.d(TAG, configurationsList.get(clickedMember).getMid() + configurationsList.get(clickedMember).getName() + " :1");
            editor.commit();

        }


    }

    private void setUi(String language) {

        String title = "title";
        if (language.equalsIgnoreCase(Constants.ENGLISH)) {

        } else {
            title = title + "_" + language;
        }


        try {
            tvSelectLanguage.setText(languageMap.get(LanguageConstants.SELECT_LANGUAGE).getString(title));
            language_already_selected = languageMap.get(LanguageConstants.LANGUAGE_ALREADY_SELECTED).getString(title);
            no_internet = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();

    }


}
