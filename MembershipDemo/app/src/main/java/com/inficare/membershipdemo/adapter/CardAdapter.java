package com.inficare.membershipdemo.adapter;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.util.LruCache;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnWindowFocusChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.google.zxing.BarcodeFormat;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.activities.QRCodeActivity;
import com.inficare.membershipdemo.activities.ReciprocalActivity;
import com.inficare.membershipdemo.interfaces.SetCardPosition;
import com.inficare.membershipdemo.membership.MemberList;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.LetterSpacingTextView;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import me.xiaopan.barcodescanner.EncodeUtils;


public class CardAdapter extends PagerAdapter implements
        OnWindowFocusChangeListener, OnClickListener {
    private Context mContext;

    private LayoutInflater inflater;

    transient private Typeface faceheader;

    transient private Typeface faceRegular, typeface;

    private MemberList listData;

    ImageView bannerImage;

    private Dialog dialogSelection;

    private SetCardPosition positionListner;

    private int pos;
    String cardUrl;

    ArrayList<ViewAnimator> animatorArray = new ArrayList<ViewAnimator>();
    public static String CALLER_CARD = "card";
    ArrayList<ImageView> imageArray = new ArrayList<ImageView>();
    MuseumDemoPreference preferences;
    int clickedMember;
    Configuration config;
    private String language, member_since, valid_through;
    private Map<String, JSONObject> languageMap;

    public CardAdapter(Context context, MemberList list, SetCardPosition setPoition) throws JSONException {
        mContext = context;
        listData = list;
        this.positionListner = setPoition;
        imageArray.clear();
        animatorArray.clear();
        faceheader = Utility.setFaceTypeHeader(mContext);
        faceRegular = Typeface.createFromAsset(mContext.getAssets(),
                "fonts/Raleway-Medium.ttf");
        typeface = Typeface.createFromAsset(mContext.getAssets(),
                "fonts/Raleway-Regular.ttf");

        preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        List<Configuration> configurationList = MemberParser.parsConfig(mContext);
        config = configurationList.get(clickedMember);
        //cardUrl = config.getImagePath() + "/" + config.getCardBannerImage();
        language = mContext.getSharedPreferences(Constants.LANGUAGE_SHARED_PREFERENCE, Context.MODE_PRIVATE)
                .getString(configurationList.get(clickedMember).getMid(), "");
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        String title = "title";
        if (language.equalsIgnoreCase(Constants.ENGLISH)) {

        } else {
            title = title + "_" + language;
        }
        try {
            member_since = languageMap.get(LanguageConstants.MEMBER_SINCE).getString(title);
            valid_through = languageMap.get(LanguageConstants.VALID_THROUGH).getString(title);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((CardView) arg1);

    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        pos = position;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.ticket_popup_anchorage,
                container, false);
        findTicketViews(view, position);

        ((ViewPager) container).addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((CardView) object);
    }

    private void findTicketViews(View dialog, int pos) {

        ImageView barcodeImage = (ImageView) dialog
                .findViewById(R.id.barcode_image);

        LinearLayout llChildCount = (LinearLayout) dialog.findViewById(R.id.llChildCount);
        TextView childCountLabel, childCount;
        childCountLabel = (TextView) dialog.findViewById(R.id.childCountLabel);
        childCount = (TextView) dialog.findViewById(R.id.childCount);
        if (config.isChildCount()) {
            try {
                int n = Integer.parseInt(listData.get(pos).getNoOfChildren());
                if (n > 0) {
                    llChildCount.setVisibility(View.VISIBLE);
                    childCountLabel.setText(config.getChildCountLabel());
                    childCount.setText(listData.get(pos).getNoOfChildren());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            llChildCount.setVisibility(View.GONE);
        }

        ImageView iv_expire_logo = (ImageView) dialog.findViewById(R.id.iv_expire_logo);
        if (listData.size() > 0) {
            if (!Utility.isExpiredCard(listData.get(0).getValidThroughTimeSpan())) {
                iv_expire_logo.setVisibility(View.VISIBLE);
            } else {
                iv_expire_logo.setVisibility(View.GONE);
            }
        }

        final ImageView profilePic = (ImageView) dialog
                .findViewById(R.id.profile_pic);
        ImageView changePic = (ImageView) dialog
                .findViewById(R.id.img_prof_change);
        changePic.setColorFilter(Color.parseColor(config.getColorCode()), PorterDuff.Mode.SRC_ATOP);
        LinearLayout prof_layout = (LinearLayout) dialog.findViewById(R.id.prof_layout);
        if (config.isProfilePhoto()) {
            changePic.setVisibility(View.VISIBLE);
            prof_layout.setVisibility(View.VISIBLE);
        } else {
            changePic.setVisibility(View.GONE);
            prof_layout.setVisibility(View.GONE);
        }
        TextView memberName = (TextView) dialog
                .findViewById(R.id.cardholder_name);
        TextView memberAddress = (TextView) dialog
                .findViewById(R.id.cardholder_address);
        memberAddress.setTextColor(Color.parseColor(config.getColorCode()));
        TextView validSince = (TextView) dialog.findViewById(R.id.member_since);
        TextView validSinceVal = (TextView) dialog
                .findViewById(R.id.member_since_val);
        TextView validthrough = (TextView) dialog
                .findViewById(R.id.valid_through);
        TextView validthroughVal = (TextView) dialog
                .findViewById(R.id.valid_through_val);
        TextView mid = (TextView) dialog.findViewById(R.id.cardholder_mid);
        LinearLayout lldate = (LinearLayout) dialog.findViewById(R.id.llDate);
        final ImageView bannerImage2 = (ImageView) dialog.findViewById(R.id.static_image);
        bannerImage = bannerImage2;
        if(config.getIsImgCentrAlign()==1){
            bannerImage2.setScaleType(ImageView.ScaleType.FIT_CENTER);
        }else {
            bannerImage2.setScaleType(ImageView.ScaleType.FIT_XY);
        }

        final TextView tvOrgName = dialog.findViewById(R.id.tvOrgName);
        ImageView ivLogo = (ImageView) dialog.findViewById(R.id.ivLogo);
        ViewAnimator animator = (ViewAnimator) dialog
                .findViewById(R.id.animator);
        TextView museumName = (TextView) dialog.findViewById(R.id.museum_name);
        museumName.setSelected(true);
        museumName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        museumName.setSingleLine(true);

        LetterSpacingTextView barcodeNumbers = (LetterSpacingTextView) dialog
                .findViewById(R.id.barcode_numbers);

        TextView member_type = (TextView) dialog.findViewById(R.id.text_type);
        ImageView ivCardLogo = (ImageView) dialog.findViewById(R.id.ivCardLogo);
        member_type.setBackgroundColor(Color.parseColor(config.getColorCode()));
        memberName.setTypeface(faceheader, Typeface.BOLD);
        memberAddress.setTypeface(faceRegular);
        validSince.setTypeface(typeface);
        validSinceVal.setTypeface(typeface);
        validthrough.setTypeface(typeface);
        validthroughVal.setTypeface(typeface);
        //museumName.setSpacing(0);
        museumName.setText(config.getName().toUpperCase());
        museumName.setTypeface(faceRegular);
        member_type.setTypeface(faceRegular);
        barcodeNumbers.setSpacing(1);
        validSince.setAllCaps(true);
        validthrough.setAllCaps(true);
        /*   barcodeNumbers.setText("" + listData.get(pos).getConstituentID());*/
        memberName.setText(listData.get(pos).getFirstName() + " "
                + listData.get(pos).getLastName());

        memberName.setSelected(true);
        String city = listData.get(pos).getCity();
        String state = listData.get(pos).getState();

        String addr = "";
        if (state.contains(city)) {
            addr = state;
        } else {
            addr = city + "," + state;
        }
        if(config.getIsAddressLine()){
            memberAddress.setVisibility(View.VISIBLE);
        }else {
            memberAddress.setVisibility(View.GONE);
        }
        memberAddress.setText(addr + " "
                + listData.get(pos).getZip());
        validSinceVal.setText(Utility.convertTimestampToDateForMember(
                Utility.getDateToTimestamp(listData.get(pos)
                        .getMemberSinceTimeSpan())));

        if (listData.get(pos).getProfileImage().equals("")) {
            profilePic.setImageResource(R.drawable.avatar_icon);
        } else {
            Utility.showImageFromUrl(listData.get(pos).getProfileImage(),
                    profilePic, animator);

        }

        validthroughVal.setText(Utility.convertTimestampToDateForMember(
                Utility.getDateToTimestamp(listData.get(pos)
                        .getValidThroughTimeSpan())));

        if (listData.get(0).getMembershipleveltype().equals("40 Below")) {
            member_type.setText(listData.get(0).getMembershiplevel() + "/40 BELOW");
            /* bannerImage.setImageResource(R.drawable.screen_pink);*/
        } else {
            member_type.setText(listData.get(pos).getMembershiplevel()
            );
        }
        Bitmap bitmap;

        String id;

        if (config.getBarcode_member_type().equals("1")) {
            id = listData.get(pos).getMembershipID();

        } else if (config.getBarcode_member_type().equals("2")) {
            id = listData.get(pos).getConstituentID();


        } else if (config.getBarcode_member_type().equals("3")) {
            id = listData.get(pos).getNumber();

        } else {
            id = listData.get(pos).getMembershipID();
        }
        barcodeNumbers.setText("" + id);
        if (Utility.isTablet(mContext)) {

           /* bitmap = Bitmap.createScaledBitmap(getBarcodeBitmap(id), (int) Utility
                    .convertDPtoPixel(mContext, 450), (int)
                    Utility.convertDPtoPixel(mContext, 80), true);*/
            //bitmap=getBarcodeBitmap(id,(int)Utility.convertDPtoPixel(mContext, 80),(int) Utility.convertDPtoPixel(mContext, 550));

        } else {
           /* bitmap = Bitmap.createScaledBitmap(getBarcodeBitmap(id), (int) Utility
                    .convertDPtoPixel(mContext, 450), (int)
                    Utility.convertDPtoPixel(mContext, 50), true);*/
            //bitmap=getBarcodeBitmap(id,(int)Utility.convertDPtoPixel(mContext, 100),(int) Utility.convertDPtoPixel(mContext, 550));
        }

        bitmap = getBarcodeBitmap(id, (int) Utility.convertDPtoPixel(mContext, 80), (int) Utility.convertDPtoPixel(mContext, 550));
        barcodeImage.setImageBitmap(bitmap);
        mid.setTypeface(typeface);
        childCountLabel.setTypeface(typeface);
        childCount.setTypeface(typeface);

        barcodeImage.setClickable(true);
        barcodeImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, QRCodeActivity.class);
                i.putExtra("guestPassId", id);
                i.putExtra("usedStatus", "");
                if (!Utility.isExpiredCard(listData.get(0).getValidThroughTimeSpan())) {
                    i.putExtra("usedStatus", "Expired");
                }
                mContext.startActivity(i);
            }
        });
       /* mid.setText(config.getSearchType().toUpperCase() + " " + listData.get(pos)
                .getMembershipID());*/
        String data = listData.get(pos).getMembershipID();
        String searchType = listData.get(pos).getsearchType();
        if (searchType != null) {
            if (searchType.equals("1")) {
                data = listData.get(pos).getMembershipID();


            } else if (searchType.equals("2")) {
                data = listData.get(pos).getConstituentID();


            } else if (searchType.equals("3")) {
//                data = listData.get(pos).getNumber();
//                mid.setText(listData.get(pos).getSearchLabel() + " " + data);

            }
        }

        mid.setText(listData.get(pos).getSearchLabel().toUpperCase() + " " + data);


        validSince.setText(member_since);
        validthrough.setText(valid_through);
        if (config.isMemberidHide()) {
            mid.setVisibility(View.GONE);
        } else {
            mid.setVisibility(View.VISIBLE);
        }

        profilePic.setOnClickListener(this);

        changePic.setOnClickListener(new DialogClick(pos));
        animatorArray.add(animator);
        imageArray.add(profilePic);

        ImageLoader.getInstance().displayImage(listData.get(pos).getCardBanner(), bannerImage2, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
                loadDefaultImage(bannerImage2,tvOrgName);
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
                loadDefaultImage(bannerImage2,tvOrgName);
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                tvOrgName.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        loadDefaultImage(bannerImage2,tvOrgName);
        LinearLayout ll_since = (LinearLayout) dialog.findViewById(R.id.ll_since);
        if (listData.get(pos).getMembersince().isEmpty()) {
            //rl.setVisibility(View.GONE);
            ll_since.setVisibility(View.GONE);
            //lldate.setVisibility(View.VISIBLE);
        } else {
           /* rl.setVisibility(View.VISIBLE);
            lldate.setVisibility(View.GONE);
            ImageLoader.getInstance().displayImage(listData.get(pos).getRoamLogoPath(),ivLogo);*/
            ll_since.setVisibility(View.VISIBLE);
        }
        if (listData.get(pos).getRoamLogoPath() != null && listData.get(pos).getRoamLogoPath().length() > 5) {
            ivCardLogo.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(listData.get(pos).getRoamLogoPath(), ivCardLogo);
            ivCardLogo.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listData.get(pos).getMembershiplevelID() != null) {
                        Intent intent = new Intent(mContext, ReciprocalActivity.class);
                        intent.putExtra(CALLER_CARD, listData.get(pos).getMembershiplevelID().trim());
                        mContext.startActivity(intent);
                    }

                }
            });
        } else {
            ivCardLogo.setVisibility(View.GONE);
        }

    }


    private static Bitmap getBarcodeBitmap(String content, int height, int width) {
        Bitmap bmp = null;
        try {
            bmp = EncodeUtils.encode(content, BarcodeFormat.CODE_39, null, width,
                    height, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;

       /* Bitmap bmp = null;
        try {
            bmp = EncodeUtils.encode(content, BarcodeFormat.CODE_39, null, 250,
                    100, null, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bmp;
*/
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, Base64.DEFAULT);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        LayoutParams lp = bannerImage.getLayoutParams();
        int width = lp.width;
        int height = (int) (width * .62);
        lp.height = height;
        bannerImage.setLayoutParams(lp);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            // case R.id.img_prof_change:
            // dialogSelection = Utility.initializeDialog(mContext,
            // R.layout.selection_prof_pic);
            // TextView selectGallery = (TextView) dialogSelection
            // .findViewById(R.id.pick_gallery);
            //
            // TextView selectCamera = (TextView) dialogSelection
            // .findViewById(R.id.pick_camera);
            //
            // selectGallery.setOnClickListener(this);
            // selectCamera.setOnClickListener(this);
            //
            // Utility.setscreen(dialogSelection);
            // dialogSelection.show();
            // break;

            // case R.id.pick_gallery:
            // Utility.selectFromGallery(mContext);
            // dialogSelection.dismiss();
            // positionListner.setPosition(pos, animatorArray.get(pos),
            // imageArray.get(pos));
            // break;
            // case R.id.pick_camera:
            // Utility.selectFromCamera(mContext);
            // dialogSelection.dismiss();
            // positionListner.setPosition(pos, animatorArray.get(pos),
            // imageArray.get(pos));
            // break;
            default:
                break;
        }

    }

    class SelectionClick implements OnClickListener {
        private int pos;

        public SelectionClick(final int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.pick_gallery:
                    //code for Marshmallow
//                    if (Build.VERSION.SDK_INT >= 23) {
//                        if (mContext.checkCallingPermission(Manifest.permission
// .WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//                         ActivityCompat.requestPermissions((Activity) mContext,new
// String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
//                        } else {
//                            Utility.selectFromGallery(mContext);
//                            dialogSelection.dismiss();
//                            positionListner.setPosition(pos, animatorArray.get(pos),
//                                    imageArray.get(pos), false);
//                        }
//
//                    } else {
//                        Utility.selectFromGallery(mContext);
                    dialogSelection.dismiss();
                    positionListner.setPosition(pos, animatorArray.get(pos),
                            imageArray.get(pos), false, 1);
                    //}
                    break;
                case R.id.pick_camera:
                    //code for Marshmallow


                    //  Utility.selectFromCamera(mContext);
                    dialogSelection.dismiss();
                    positionListner.setPosition(pos, animatorArray.get(pos),
                            imageArray.get(pos), false, 2);
                    break;

                case R.id.pick_fb:
//code for Marshmallow


                    dialogSelection.dismiss();
                    if (Utility.isNetworkAvailable(mContext)) {
                        positionListner.setPosition(pos, animatorArray.get(pos),
                                imageArray.get(pos), true, 3);
                    } else {
//					TagInfo info = GetTagData.getTag(mContext);
//					Utility.showAlertDialog(mContext, info.getAlertNoInternet())
                        Utility.showAlertDialog(mContext, mContext.getResources().getString(R
                                .string.msg_no_internet));
                    }

                    break;
                default:
                    break;
            }

        }
    }

    class DialogClick implements OnClickListener {
        private int pos;

        public DialogClick(final int pos) {
            this.pos = pos;
        }

        @Override
        public void onClick(View v) {
            dialogSelection = Utility.initializeDialog(mContext,
                    R.layout.selection_prof_pic);
            TextView selectGallery = (TextView) dialogSelection
                    .findViewById(R.id.pick_gallery);

            TextView selectCamera = (TextView) dialogSelection
                    .findViewById(R.id.pick_camera);

            TextView selectFB = (TextView) dialogSelection
                    .findViewById(R.id.pick_fb);

            selectGallery.setOnClickListener(new SelectionClick(pos));
            selectCamera.setOnClickListener(new SelectionClick(pos));
            selectFB.setOnClickListener(new SelectionClick(pos));
            Utility.setscreen(dialogSelection);
            dialogSelection.show();
        }

    }

    public static Bitmap getBitmapFromMemCache(String key) {
        if(CustomGrid.mMemoryCacheShare!=null)
        return CustomGrid.mMemoryCacheShare.get(key);

        return null;
    }

    private void loadDefaultImage(ImageView imageView,TextView tvOrgName){
        try {
            String uri = config.getImagePath() + "/" + config.getLogo();
            Bitmap cacheBitmap = getBitmapFromMemCache(uri);
            if(cacheBitmap!=null) {
                tvOrgName.setVisibility(View.GONE);
                imageView.setImageBitmap(cacheBitmap);
            } else {
                tvOrgName.setVisibility(View.VISIBLE);
                tvOrgName.setText(config.getName());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
