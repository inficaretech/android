package com.inficare.membershipdemo.membership;

import java.io.Serializable;

public class MemberInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7246520842588064093L;
    private int profilePic;
    private String name;
    private String address;
    private int barCodeNo;
    private String since;
    private String through;
    private String type;
    private String Membershipleveltype;
    private String IsExpire;
    private String ExpiryMessageText;
    private String cardUrl;
    private int HaveLogoPath;
    private String RoamLogoPath;
    private String Membersince;
    private String CardBanner;
    private String searchType;
    private String SearchLabel;
    private String NoOfChildren;
    private String Country;
    private String MembershiplevelID;
    public String getRoamLogoPath() {
        return RoamLogoPath;
    }

    public void setRoamLogoPath(String roamLogoPath) {
        RoamLogoPath = roamLogoPath;
    }

    public int getHaveLogoPath() {
        return HaveLogoPath;
    }

    public void setHaveLogoPath(int haveLogoPath) {
        HaveLogoPath = haveLogoPath;
    }

    public String getMembershipID() {
        return MembershipID;
    }

    public String getExpiryMessageText() {
        return ExpiryMessageText;
    }

    public void setExpiryMessageText(String expiryMessageText) {
        ExpiryMessageText = expiryMessageText;
    }

    public void setMembershipID(String membershipID) {
        MembershipID = membershipID;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    // new one according to api
    private String MembershipID;

    private String Membershiplevel;

    private String City;

    private String State;

    private String Zip;

    private String FirstName;

    private String LastName;

    private String MemberSinceTimeSpan;

    private String ValidThroughTimeSpan;

    private String Number;

    private String ProfileImage;

    private MemberSpouseList SpouseList;

    private String ConstituentID;


    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(int profilePic) {
        this.profilePic = profilePic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getBarCodeNo() {
        return barCodeNo;
    }

    public void setBarCodeNo(int barCodeNo) {
        this.barCodeNo = barCodeNo;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public String getThrough() {
        return through;
    }

    public void setThrough(String through) {
        this.through = through;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMemberSinceTimeSpan() {
        return MemberSinceTimeSpan;
    }

    public void setMemberSinceTimeSpan(String memberSinceTimeSpan) {
        MemberSinceTimeSpan = memberSinceTimeSpan;
    }

    public String getValidThroughTimeSpan() {
        return ValidThroughTimeSpan;
    }

    public void setValidThroughTimeSpan(String validThroughTimeSpan) {
        ValidThroughTimeSpan = validThroughTimeSpan;
    }

    public MemberSpouseList getSpouseList() {
        return SpouseList;
    }

    public void setSpouseList(MemberSpouseList spouseList) {
        SpouseList = spouseList;
    }

    public String getMembershipleveltype() {
        return Membershipleveltype;
    }

    public void setMembershipleveltype(String membershipleveltype) {
        Membershipleveltype = membershipleveltype;
    }

    public String getMembershiplevel() {
        return Membershiplevel;
    }

    public void setMembershiplevel(String membershiplevel) {
        Membershiplevel = membershiplevel;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getConstituentID() {
        return ConstituentID;
    }

    public void setConstituentID(String constituentID) {
        ConstituentID = constituentID;
    }

    public String getIsExpire() {
        return IsExpire;
    }

    public void setIsExpire(String isExpire) {
        IsExpire = isExpire;
    }

    public String getCardUrl() {
        return cardUrl;
    }

    public void setCardUrl(String cardUrl) {
        this.cardUrl = cardUrl;
    }

    public String getMembersince() {
        return Membersince;
    }

    public void setMembersince(String membersince) {
        Membersince = membersince;
    }

    public String getCardBanner() {
        return CardBanner;
    }

    public void setCardBanner(String cardBanner) {
        CardBanner = cardBanner;
    }

    public String getsearchType() {
        return searchType;
    }

    public void setsearchType(String data) {
        searchType = data;
    }

    public String getSearchLabel() {
        return SearchLabel;
    }

    public void setSearchLabel(String searchLabel) {
        SearchLabel = searchLabel;
    }

    public String getNoOfChildren() {
        return NoOfChildren;
    }

    public void setNoOfChildren(String noOfChildren) {
        NoOfChildren = noOfChildren;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }


    public String getMembershiplevelID() {
        return MembershiplevelID;
    }

    public void setMembershiplevelID(String membershiplevelID) {
        MembershiplevelID = membershiplevelID;
    }
}
