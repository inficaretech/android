package com.inficare.membershipdemo.networks;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.inficare.membershipdemo.methods.Constants;
import com.inficare.membershipdemo.methods.SSLTest;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Class used for Network operations
 *
 * @author neha.saini
 */
public final class HttpMethod {

    private static HttpMethod instance;

    private static Context context;
    public static String username = "admin";
    /**
     * Class Name
     */
    private static final String CN = "HttpMethod";

    public enum TYPE {
        GET, PUT, POST
    }

    private HttpMethod(final Context context) {

        this.context = context;
    }

    public static HttpMethod getInstance(final Context context) {

        if (instance == null) {
            instance = new HttpMethod(context);
        }

        return instance;

    }

    public String doOperation(final TYPE type, final String url) {
        String responseStr = null;
        switch (type) {
            case GET:
                if (url.contains("http://")) {
                    responseStr = doGetOperation(url);
                } else {
                    responseStr = getJSONString(url);
                }

                break;

            case PUT:

                break;

            case POST:

                break;

            default:

                break;
        }

        return responseStr;

    }

    private static String getJSONString(String url) {
        String jsonString = null;
        HttpsURLConnection linkConnection = null;
        /*
         * HttpsURLConnection urlConnection =
		 * (HttpsURLConnection)url.openConnection();
		 * urlConnection.setSSLSocketFactory(context.getSocketFactory());
		 * InputStream in = urlConnection.getInputStream();
		 * copyInputStreamToOutputStream(in, System.out);
		 */
        Log.i(CN, "Request url:" + url);
        try {
            URL linkurl = new URL(url);
            linkConnection = (HttpsURLConnection) linkurl.openConnection();

            String basicAuth = "Basic " + Base64.encodeToString((username + ":" + username)
                    .getBytes(), Base64.NO_WRAP);
            linkConnection.setRequestProperty("Authorization", basicAuth);

           /* linkConnection.setSSLSocketFactory(SSLTest
                    .getSSLSocketFactory(context));*/
            linkConnection.setConnectTimeout(20000);
            linkConnection.setReadTimeout(20000);

            int responseCode = linkConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream linkinStream = linkConnection.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int j = 0;
                while ((j = linkinStream.read()) != -1) {
                    baos.write(j);

                }
                byte[] data = baos.toByteArray();
                jsonString = new String(data);
            }
        } catch (ConnectTimeoutException cte) {
            jsonString = Constants.timeoutException;


        } catch (IOException e) {
            jsonString = Constants.timeoutException;
            Log.e(CN, "IOException ");
            e.printStackTrace();

        } catch (Exception e) {
            jsonString = Constants.timeoutException;
            Log.e(CN, "Exception ");
            // e.printStackTrace();
        } finally {
            if (linkConnection != null) {
                linkConnection.disconnect();
            }
        }
        Log.i(CN, "Response data:" + jsonString);
        return jsonString;

    }

    private String doGetOperation(final String link) {

        String str = null;
        HttpURLConnection connection = null;
        BufferedReader reader = null;
        StringBuffer buffer = new StringBuffer();
        Log.i(CN, "Request url:" + link);
        try {
            URL url = new URL(link);
            connection = (HttpURLConnection) url.openConnection();
            String basicAuth = "Basic " + Base64.encodeToString((username + ":" + username)
                    .getBytes(), Base64.NO_WRAP);
            connection.setRequestProperty("Authorization", basicAuth);
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Keep-Alive", "header");
            connection.connect();
            int responseCode = connection.getResponseCode();
//            if (responseCode == HttpURLConnection.HTTP_OK) {

            InputStream stream = connection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(stream));
            // buffer=new StringBuffer();
            String line = "";
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            str = buffer.toString();
//            }
            Log.d("str", str);
//			InputStream stream=connection.getInputStream();
//			reader=new BufferedReader(new InputStreamReader(stream));
//			// buffer=new StringBuffer();
//			String line="";
//			while ((line=reader.readLine())!=null){
//				buffer.append(line);
//			}
        } catch (MalformedURLException e) {
            str = Constants.timeoutException;
            e.printStackTrace();
        } catch (IOException e) {
            str = Constants.timeoutException;
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.i(CN, "Response data:" + str);
        return str;
    }


    public String postData(String link, Map<String, Object> parameter) {
        String response = null;
        try {
            Log.d("MY_LOG", "Request url: " + link);
            Log.d("MY_LOG", "Request Json data: " + new JSONObject(parameter));
            URL url = new URL(link);

            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, Object> param : parameter.entrySet()) {
                if (postData.length() != 0) postData.append('&');

                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);
            int responseCode = conn.getResponseCode();
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (int c; (c = in.read()) >= 0; )
                sb.append((char) c);
            response = sb.toString();
            Log.d("MY_LOG", "Response data: " + response);
        }/*catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/ catch (Exception e) {
            e.printStackTrace();

        }

        return response;
    }
}
