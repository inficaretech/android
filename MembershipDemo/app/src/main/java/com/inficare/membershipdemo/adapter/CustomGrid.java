package com.inficare.membershipdemo.adapter;

/**
 * Created by akshay.kumar on 1/13/2017.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.interfaces.MyImageLoader;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.pojo.Configuration;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

public class CustomGrid extends BaseAdapter {
    private Context mContext;
    private List<Configuration> configurations;
    int flag;
    MuseumDemoPreference preferences;
    ImageView imageView, gridImageIcon;
    TextView tvOrgName;
    private boolean isNotified;
    private LruCache<String, Bitmap> mMemoryCache;
    public static LruCache<String, Bitmap> mMemoryCacheShare;

    public CustomGrid(Context c, List<Configuration> configurations, int flag) {
        mContext = c;
        //this.configurations = configurations;
        this.configurations = configurations;

        //Collections.sort(configurations);
        this.flag = flag;
        preferences = MuseumDemoPreference.getInstance(c);
        configCache();
        myNotify();

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return configurations.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            if (flag == 0)
                grid = inflater.inflate(R.layout.grid_single, null);
            else
                grid = inflater.inflate(R.layout.grid_single2, null);
            ///ImageLoader.getInstance().displayImage(list.get(position),imageView);
            //imageView.setImageResource();

        } else {
            grid = (View) convertView;
        }
        imageView = (ImageView) grid.findViewById(R.id.grid_image);
        gridImageIcon = (ImageView) grid.findViewById(R.id.gridImageIcon);
        ImageView ivFav = (ImageView) grid.findViewById(R.id.ivFav);
        RelativeLayout rl = (RelativeLayout) grid.findViewById(R.id.rl);
        tvOrgName = grid.findViewById(R.id.tvOrgName);
        tvOrgName.setText(configurations.get(position).getName());
        boolean isFavorite = preferences.getBoolean(configurations.get(position).getName(), false);

        if (isFavorite)
            ivFav.setVisibility(View.VISIBLE);
        else
            ivFav.setVisibility(View.GONE);


        String uri = configurations.get(position).getImagePath() + "/" + configurations.get(position).getLogo();


        //ImageLoader.getInstance().displayImage(uri, gridImageIcon);

        // Load image, decode it to Bitmap and return Bitmap to callback
        /*ImageLoader.getInstance().loadImage(uri, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                Toast.makeText(mContext,"onLoadingComplete",Toast.LENGTH_LONG).show();
                gridImageIcon.setImageBitmap(loadedImage);
            }
        });*/
        Bitmap bitmap = getBitmapFromMemCache(uri);
        if (bitmap!=null) {
            tvOrgName.setVisibility(View.INVISIBLE);

            if (bitmap != null) {
                gridImageIcon.setImageBitmap(bitmap);
            }else {

            }
        } else {
            tvOrgName.setVisibility(View.VISIBLE);
            gridImageIcon.setImageBitmap(null);

            MyImageLoader myImageLoader = new MyImageLoader() {
                @Override
                public void onImgLoaded(int mPosition, String imageUri, Bitmap loadedImage) {
                    addBitmapToMemoryCache(imageUri, loadedImage);
                    notifyDataSetChanged();
                    isNotified=true;
                    Log.i("notifyDataSetChanged","called");
                }

            };
            myImageLoader.mLoadImage(uri, position);

        }



        try {
            /*GradientDrawable myGrad = (GradientDrawable)rl.getBackground();
            myGrad.setStroke(2, Color.parseColor(configurations.get(position).getColorCode()));*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        return grid;
    }

    public void setData(List<Configuration> configurations) {
        this.configurations = configurations;
        notifyDataSetChanged();
    }

    private void configCache() {
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
      mMemoryCacheShare=mMemoryCache;
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
            Log.i(""+this,"getView key "+key);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    private void myNotify(){
        notifyDataSetChanged();
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
                Log.i("notifyDataSetChanged","called delay");
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                        Log.i("notifyDataSetChanged","called delay again");
                    }
                }, 3000);
            }
        }, 3000);
    }
}
