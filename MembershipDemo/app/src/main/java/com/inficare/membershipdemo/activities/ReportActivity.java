package com.inficare.membershipdemo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.inficare.membershipdemo.R;
import com.inficare.membershipdemo.adapter.ReportAdapter;
import com.inficare.membershipdemo.membership.MemberParser;
import com.inficare.membershipdemo.methods.LanguageConstants;
import com.inficare.membershipdemo.methods.MuseumDemoPreference;
import com.inficare.membershipdemo.methods.MyLanguage;
import com.inficare.membershipdemo.methods.Utility;
import com.inficare.membershipdemo.pojo.Configuration;
import com.inficare.membershipdemo.pojo.Report;
import com.inficare.membershipdemo.pojo.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Abhishek.jha on 10/21/2016.
 */

public class ReportActivity extends AppCompatActivity {

    private Context mContext;

    //Toolbar elements
    private Toolbar toolbar;
    private TextView tb_title;
    int clickedMember;
    List<Configuration> configList;
    //define views
    private RecyclerView reportRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ReportAdapter reportAdapter;
    private EditText report_member_id;
    private Button btn_go;
    View topStrip;
    //UI Elements
    private ProgressDialog dialog;

    private SharedPreferences langPreferences;
    private SharedPreferences.Editor editor;
    private String language;
    private Map<String, JSONObject> languageMap;
    public static final String TAG = ReportActivity.class.getSimpleName();
    private String please_wait, please_enter, membership_number, no_internet_found, alert, invalid, ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        mContext = this;
        MuseumDemoPreference preferences = MuseumDemoPreference.getInstance(mContext);
        clickedMember = preferences.getInt(MuseumDemoPreference.CLICKED_MEMBER, -1);
        if(clickedMember==-1)
        {
            Toast.makeText(this,getResources().getString(R.string.something_went_wrong),Toast.LENGTH_LONG).show();
            return;
        }
        configList = MemberParser.parsConfig(this);
        setToolbarLayout();
        findViews();
        initLanguage();
        setUi(language);
    }

    private void initLanguage() {
        language = Utility.getCurrentLanguage(mContext, configList.get(clickedMember).getMid());
        languageMap = MyLanguage.getInstance().getMyLanguage(mContext);
        Log.d(TAG, configList.get(clickedMember).getMid() + configList.get(clickedMember).getName() + " : " + language);
    }

    private void setUi(String language) {

        String title = Utility.getTitle(language);

        try {
            tb_title.setText(languageMap.get(LanguageConstants.REPORT).getString(title));
            btn_go.setText(languageMap.get(LanguageConstants.SEARCH).getString(title));

            please_wait = languageMap.get(LanguageConstants.PLEASE_WAIT).getString(title) + "...";

            membership_number = languageMap.get(LanguageConstants.MEMBERSHIP_NUMBER).getString(title);
            please_enter = languageMap.get(LanguageConstants.PLEASE_ENTER).getString(title);
            no_internet_found = languageMap.get(LanguageConstants.NO_INTERNET_CONNECTION_FOUND).getString(title);
            alert = languageMap.get(LanguageConstants.ALERT).getString(title);
            invalid = languageMap.get(LanguageConstants.INVALID).getString(title);
            ok = languageMap.get(LanguageConstants.OK).getString(title);


            report_member_id.setHint(membership_number);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private void setToolbarLayout() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        tb_title = (TextView) findViewById(R.id.tb_title);
        tb_title.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_button);
        upArrow.setColorFilter(Color.parseColor(configList.get(clickedMember).getColorCode()),
                PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_button));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void findViews() {
        dialog = Utility.showProgressDialog(mContext, please_wait);
        report_member_id = (EditText) findViewById(R.id.report_member_id);

        if (configList.get(clickedMember).getKeybord().equalsIgnoreCase(Configuration.NUMERIC)) {
            report_member_id.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            report_member_id.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        }
        // report_member_id.setTextColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        topStrip = findViewById(R.id.topStrip);
        topStrip.setBackgroundColor(Color.parseColor(configList.get(clickedMember).getColorCode()));
        btn_go = (Button) findViewById(R.id.btn_go);
        Utility.changeDrawableColor(this, btn_go, configList.get(clickedMember).getColorCode());
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utility.hideKeyboard(ReportActivity.this);
                if (Utility.isNetworkAvailable(mContext)) {
                    String memberId = report_member_id.getText().toString().trim();
                    if (memberId.isEmpty()) {
                        Toast.makeText(ReportActivity.this, please_enter + " " + membership_number, Toast.LENGTH_LONG).show();
                    } else {
                        findMemberReport(memberId);
                    }

                } else {
                    Utility.showToast(mContext, no_internet_found);
                }
            }
        });
        reportRecyclerView = (RecyclerView) findViewById(R.id.report_view);
        mLinearLayoutManager = new LinearLayoutManager(mContext);
        reportRecyclerView.setLayoutManager(mLinearLayoutManager);
        reportRecyclerView.setHasFixedSize(true);

    }

    private void findMemberReport(String memberId) {
        dialog.show();
        Call<Response> memberReportCall = Utility.callRetrofit().getMemberReport(Utility.getHeaderData(), memberId, configList.get(clickedMember).getMid());
        memberReportCall.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                dialog.dismiss();
                try {
                    if (response.body().getSuccess().equals("true")) {

                        setUIData(response.body().getResult().getReport());
                    } else {
                        setUIData(null);
                        myAlert();
                    }
                } catch (Exception e) {
                    setUIData(null);
                    myAlert();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                dialog.dismiss();
                setUIData(null);
                myAlert();
            }
        });

    }


    private void setUIData(Report report) {
        if (report != null) {
            reportAdapter = new ReportAdapter(mContext, report, language, languageMap);
            reportRecyclerView.setAdapter(reportAdapter);
        } else {
            reportRecyclerView.setAdapter(null);
        }
    }

    private void myAlert() {
        new AlertDialog.Builder(this)
                .setTitle(alert)
                .setMessage(invalid + " " + membership_number)
                .setPositiveButton(ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
